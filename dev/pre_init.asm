include "sym/main.sym"

org $7fa4

     di
     ld a, $80                           ; Activamos las interrupciones
     ld i, a
     im 2
     ld sp, STACK_POINTER

     ld hl, $fc00
     ld de, $5f5f
     ld bc, 3
     ldir

     ld b, 6
     call SETRAMBANK

     call INITWYZ

;     ld b, 0
     call SETRAMBANK0

     ei
     ld a, 1
     out ($fe), a
     call WAITFORKEY_NOUPDATE
     xor a
     out ($fe), a
     jp INIT

ISR_LONGHOOK:    
     push af
     push hl
     push de
     push bc
     push ix
     push iy

     ld hl, TICKS
     inc (hl)

     ld a, (LAST_RAM_BANK)
     push af

     ld b, 6
     call SETRAMBANK

     call WYZPLAYER_ISR
     
     pop bc
     call SETRAMBANK

     pop iy
     pop ix
     pop bc
     pop de
     pop hl
     pop af
     ei
     ret
;     ;reti
