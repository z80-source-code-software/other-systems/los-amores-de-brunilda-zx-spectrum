org $c000

include "sym/page1.sym"

;LINES_IN_A_ROW:
;    defb 0
MASK_CHAR:
    defw 0
TEXT_ATTR:
    defb STANDARD_ATTR
X_INIT:   ; equ 7 ;2
     defb 7

X_LIMIT EQU 42
Y_LIMIT EQU 22
Y_INIT EQU $14
TEXT_INIT EQU $1407

TS_WAITFORKEYR EQU 08101H

STANDARD_ATTR equ $30
TOKEN_BUFFER_SIZE equ 32;18

TOKEN_BUFFER:
     defs TOKEN_BUFFER_SIZE, 0

NAME:
     ld a, d
     and a
     jr z, PRINTSTR_FORMAT

     NAME_LOOP:
     inc hl
     ld a, (hl)
     and a
     jr nz, NAME_LOOP

     inc hl
     dec d
     jr NAME

; MESSAGE: MESS + NEWINE

;MESSAGE:
;     call MESS
;     jr NEWLINE

OBJ_COUNTER:

     ld hl, CHARSET + 16 * 8            ; Inicio del 1
     add a, a                           ; *2
     add a, a                           ; *4
     add a, a                           ; *8
     ld e, a
     ld d, 0
     add hl, de                        ; Ahora tenemos en HL el origen del n�mero a imprimir

     call TS_DF_LOCATE                   ; DE = Direcci�n a imprimir

     call SPRINT

     ld a, d
     sub 8
     ld d, a

     dec e
     ld hl, CHARSET + 56 * 8            ; La X
     jp SPRINT


; MESS: Imprime el mensaje n�mero E, en las coordenadas BC

IF COMPILE_VERSION=VERSION_GAL
MESS_2NDTABLE:
    ld hl, MESS_GAL_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_GAL_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_ITA
MESS_2NDTABLE:
    ld hl, MESS_ITA_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_ITA_INDEX
ENDIF


IF COMPILE_VERSION=VERSION_SPA
MESS_2NDTABLE:
    ld hl, MESS_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_ENG
MESS_2NDTABLE:
    ld hl, MESS_ENG_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_ENG_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_CAT
MESS_2NDTABLE:
    ld hl, MESS_CAT_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_CAT_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_CAT2
MESS_2NDTABLE:
    ld hl, MESS_CAT2_INDEX+512
    jr MESS_HOOK
MESS:
     ld hl, MESS_CAT2_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_EUS
MESS_2NDTABLE:
    ld hl, MESS_EUS_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_EUS_INDEX
ENDIF

IF COMPILE_VERSION=VERSION_NELO
MESS_2NDTABLE:
    ld hl, MESS_SEJUAN_INDEX+512
    jr MESS_HOOK

MESS:
     ld hl, MESS_SEJUAN_INDEX
ENDIF

     MESS_HOOK:
     push bc                      ; Nos guardamos las coordenadas     
     ld d, 0
     add hl, de
     add hl, de

     ld e, (hl)
     inc hl
     ld d, (hl)
     ex de, hl

; ============================================================================================================================
; PENTEXT: Descomprime el texto indicado en HL
; Optimizado (mucho) por Metalbrain
; ============================================================================================================================

PENTEXT:                exx                             ;Mantener HL en el set alternativo
                        ld      de, STRING_BUFFER-1     ;Buffer para escribir, empieza con -1 para contrarestar el primer incremento
                        ld      b, 128                  ;Bitstream: solo el bit marcador. De B pasar� a A
PT_loop:                ld      hl, COMPRESS_TABLE      ;HL apunta a la tabla principal
                        inc     de                      ;Apuntar a la siguiente posici�n
PT_next5bits:           ld      a, b                    ;El bitstream en A
                        ld      bc, 8                   ;El bit marcador empieza estando en el bit 3 del registro C. Tras 5 rotaciones,
                                                        ;  saldr� y activar� el flag carry. B lo ponemos a 0 para sumar posteriormente a HL
PT_getnewbit:           add     a, a                    ;Extraer un bit a la izquierda
                        jr      nz, PT_nogetnewbyte     ;Si sigue habiendo bits en el bitstream, no hace falta leer nada de la memoria
                        exx                             ;Restaurar HL para que apunte a los datos comprimidos que estamos leyendo
                        ld      a, (hl)                 ;Leer siguiente byte en el bitstream
                        inc     hl                      ;Apuntar al siguiente
                        exx                             ;HL vuelve al registro alternativo
                        adc     a, a                    ;Extraer un bit a la izquierda, y al mismo tiempo meter el bit marcador en el bit 0
PT_nogetnewbyte:        rl      c                       ;Introducir el bit extraido (y mover el bit marcador) hacia la izquierda. Cuando
                                                        ;  salga el bit marcador, activar� el carry.
                        jr      nc, PT_getnewbit        ;Si no hay carry, sigue leyendo bits

                        add     hl, bc                  ;A�adir a HL el valor de 5 bits leido. Ahora HL apunta al siguiente caracter
                        ld      b, a                    ;Guardar bitstream actual en el registro B
                        ld      a, (hl)                 ;Leer byte en A
                        ld      (de),a                  ;Escribirlo. Si es un n�mero negativo, ya lo sobreescribiremos
                                                        ;   en la siguiente pasada con el valor correcto
                        and     a                       ;Comprobar valor
                        jr      z, PT_exit              ;Si era 0, salir de la rutina
                        jp      p, PT_loop              ;Si es positivo, repetir para el siguiente valor

                        rra                             ;Comprobar bit 0 (rotando a la derecha, el valor sale en carry)
                        ld      l, COMPRESS_TABLE+32   ;En principio la segunda
                        jr      c, PT_next5bits         ;Si sali� el bit 1, es que ten�amos $FF -> ir a leer el valor de la segunda tabla
                        ld      l, (COMPRESS_TABLE+64)%256   ;Si sali� el 0, pues usamos la tercera
                        jr      PT_next5bits            ;Ir a leer el valor de la tercera tabla
PT_exit:

; ============================================================================================================================
     pop bc                        ; Recuperamos nuestras coordenadas
     ld hl, STRING_BUFFER
;     jr PRINTSTR_FORMAT

PRINTSTR_FORMAT:
     exx                        ; Vamos a usar los registros alternativos para no cargarnos los datos de entrada

     ld hl, TOKEN_BUFFER
     ld de, TOKEN_BUFFER+1
     ld bc, TOKEN_BUFFER_SIZE - 1
     ld (hl), 0
     ldir                       ; Limpiamos el buffer de tokens

;     ld a, STANDARD_ATTR
;     ld (TEXT_ATTR), a

     exx                        ; Recuperamos nuestros datos de entrada

     ld de, TOKEN_BUFFER

     ex af, af'
     xor a                      ; A' va a ser el contador de la longitud de la palabra
     ex af, af'

     PSTR_FORMAT_LOOP0:

     ld a, (hl)

     cp 32                      ; El car�cter actual es un espacio?
     jr z, PSTR_FORMAT_HOOK0    ; Si es que s�: se acaba la palabra
     and a                      ; Si es un 0, se acaba la frase, pero la �ltima palabra tambi�n hay que escribirla
     jr z, PSTR_FORMAT_HOOK0

     ld (de), a                 ; Ponlo en el b�ffer
     inc de                     ; Y pasamos al siguiente
     inc hl
     cp 32
     jr c, PSTR_FORMAT_LOOP0    ; Por debajo de 32 son c�digos de control, y no cuentan para el formateo
     ex af, af'
     inc a                      ; Y sumamos uno a la duraci�n de la palabra
     ex af, af'
     jr PSTR_FORMAT_LOOP0

     PSTR_FORMAT_HOOK0:

     push hl

     ex af, af'
     add a, c                   ; Sumamos a A' (Longitud de la palabra) la Columna actual
     cp X_LIMIT                 ; Lo comparamos con el l�mite
     call nc, NEWLINE           ; Si nos pasamos, nueva l�nea

     ld hl, TOKEN_BUFFER
     call PRINTSTR
     pop hl

     ld a, (hl)                 ; Si el primer car�cter a imprimir es un 0
     and a                      ; es que ya has terminado, y te puedes volver
     ret z

     inc hl                     ; Ahora hay que saltarse el espacio: En la frase original...
     inc c                      ; Y en la pantalla

     jr PRINTSTR_FORMAT

SET_ATTR
     inc hl
     ld a, (hl)
     inc hl
     ld (TEXT_ATTR), a
     jr PRINTSTR

; PRINTSTR: Imprime la cadena apuntada por HL, en las coordenadas BC

PRINTSTR:

     ld a, (hl)
     and a
     ret z

     cp 1
     jr z, SET_ATTR

     cp 13
     jr z, CODE_NEWLINE

     push hl
     push bc
     call PRINTCHR
     pop bc
     pop hl

     inc hl
     inc c
     jr PRINTSTR
     
CODE_NEWLINE:
     push hl
     call NEWLINE
     pop hl
     inc hl
     jr PRINTSTR

; PRINTCHR: Imprime el car�cter cuyo c�digo est� en A en las coordenadas BC
; Entrada: A = Caracter a imprimir
; Salida: DE = Direcci�n donde est� el car�cter
; La coordenada C (Columna) va en columnas de 6 p�xels

PRINTCHR:
;      halt

      ld h, 0
      ld l, a
      add hl, hl
      add hl, hl
      add hl, hl
      ld de, CHARSET
      dec d                             ; Hay que restar 256 a la direcci�n de origen, porque empezamos en el espacio (32)
      add hl, de                        ; Y como lo hemos multiplicado por 8, 32 * 8 = 256

;      jr PRINT_6x8


; PRINT_6x8:
      ld a, c             ; a = column

      add a, a
      add a, a
      add a, c
      add a, c            ; a = column * 6

      push af

;      srl a
;      srl a
;      srl a               ; a = (column * 6) / 8
      rrca
      rrca
      rrca
      and 31

      ld c, a             ; Ahora C = Columna, en columnas de 8 p�xeles

      call TS_DF_LOCATE

      exx
      ld b, 8             ; B' contador del bucle: 8 p�xeles de alto

      P68_LOOP:

      ld e, 3
      ld d, 255
      ld (MASK_CHAR), de  ; En MASK_CHAR metemos 00000011 11111111

      exx

      ld c,(hl)           ; En C metemos la l�nea que corresponda del car�cter que vamos a escribir (0-7)
      ld b, 0             ; Y en B un 0

      pop af              ; Nos traemos, y volvemos a subir, AF. En A est� el p�xel donde empezamos a escribir
      push af
      and 7               ; Est� ajustado a caracter?
      call nz, P68_Rot    ; Si no es as�, hay que "rotar" el car�cter hasta el sitio que le corresponda

      push hl             ; Subimos HL, puntero al car�cter
      ld hl, MASK_CHAR
      ld a, (de)          ; Nos traemos a A lo que hay en la memoria de v�deo en la l�nea donde vamos a escribir
      and (hl)            ; Y hacemos un AND con MASK_CHAR: Borramos los 6 p�xeles que vamos a utilizar
      or c                ; OR con C: Escribimos nuestra l�nea que nos hab�amos tra�do
      ld (de), a          ; Y el resultado lo ponemos en la memoria de v�deo

      inc hl              ; Siguiente octeto de p�xeles: Lo mismo nuestro car�ter est� ubicado entre dos caracteres
      inc de              ; Ya hemos incrementado la m�scara, y ahora incrementamos el octeto en la memoria de v�deo
      ld a, (de)          ; Igual que antes...
      and (hl)
      or b
      ld (de), a
      dec de              ; Nos volvemos al car�cter de antes
      pop hl              ; Nos traemos a HL: Puntero al car�cter

      inc d               ; Siguiente l�nea en la memoria de v�deo
      inc hl              ; Siguiente l�nea del car�cter

      exx                 ; En B' llev�bamos el contador del bucle
      djnz P68_LOOP       ; Has terminado?
      exx

;      ld a, (TEXT_ATTR)   ; Si el ATTR para escribir es el "Est�ndard"
;      cp STANDARD_ATTR    ; No lo imprimas: Directamente...
;      jr z, TS_NOATTR     ; ... vuelvete

      dec d               ; Escribe el atributo en el primer car�cter de los dos que puede que hayamos usado
      call TS_WRITEATTR

      pop af              ; Hemos subido AF, nos lo tenemos que volver a bajar
      and 7               ; Si hemos usado un segundo car�cter
      ret z               ; vamos a ponerle el atributo
      inc e

      jp TS_WRITEATTR

 ;     TS_NOATTR:

;      pop af              ; Pero le "deb�amos una a la pila"
;      ret


P68_Rot:

      srl c               ; En BC tenemos la l�nea que toca del el car�cter que vamos a imprimir
      rr b                ; Inicialmente ubicada en C, la rotamos a la derecha (el orden en memoria es CB)

      exx
      srl e               ; En DE tenemos la m�scara que hay que aplicar para mezclar
      set 7, e            ; Lo rotamos igual, a la derecha, llenando de 1's por la izquierda
      rr d
      exx

      dec a               ; Hemos corrido un p�xel
      and 7               ; �Estamos ya ajustados a car�cter?

      jr nz, P68_Rot      ; Si todav�a no, hay que seguir rotando

      exx
      ld (MASK_CHAR), de  ; Guarda la m�scara como ha quedado
      exx

      ret

NEWLINE:
;      ld hl, LINES_IN_A_ROW
      ld a, (X_INIT)
      ld c, a
      ld a, b
      cp Y_LIMIT
      jr z, NL_LIMIT
      inc b
;      inc (hl)
      ret

      NL_LIMIT:

;      ld a, (hl)
;      inc (hl)
;      cp 2
;      jr nz, SCROLL_ONE_LINE

;      ld (hl), 0

      ld de, $50fd
      ld hl, CHARSET+24
      call SPRINT

      ld de, $50fe
      ld hl, CHARSET+24
      call SPRINT

      ld a, $d6 ;PAPER_YELLOW | INK_RED | BRIGHT | FLASH
      ld ($5afd), a
      ld ($5afe), a

      call TS_WAITFORKEYR

      ld a, STANDARD_ATTR ;PAPER_YELLOW | INK_RED | BRIGHT | FLASH
      ld ($5afd), a
      ld ($5afe), a
      
      NL_HOOK:

      xor a
      ld de, $5085
      call CLEAR_TEXT
      ld de, $50a5
      call CLEAR_TEXT
      ld de, $50c5
      call CLEAR_TEXT

      ld bc, $1407        ; Restauramos BC para que empiece a escribir al principio del pergamino

      ret

      CLEAR_TEXT:
      ld l, e
      ld c, 26
      CT_1:
      ld b, 8
      ld h, d
      CT_2:
      ld (hl), a
      inc h
      djnz CT_2
      inc l
      dec c
      jr nz, CT_1
                                           ; Ahora limpiamos los atributos
      ld d, $5a
      ld h, d
      ld l, e
      inc e
      ld (hl), STANDARD_ATTR
      ld bc, 25
      ldir


      ret

;
;       SCROLL_ONE_LINE:
;
;       ld de, $5085            ; Scrollamos la primera linea
;       ld hl, $50a5
;       exx
;       ld b, 8
;       call SCROLL_SR
; 
;       ld de, $50a5            ; Y ahora la segunda...
;       ld hl, $50c5
;       exx
;       ld b, 8
;       call SCROLL_SR
; 
;       ld hl, $50c5            ; Y nos falta limpiar la tercera
;       ld c, 8
;       SCO_LOOP0:
;       ld b, 26
;       SCO_LOOP1:
;       ld (hl), 0
;       inc hl
;       djnz SCO_LOOP1
;       ld l, $c5
;       inc h
;       dec c
;       jr nz, SCO_LOOP0
; 
;       ld hl, $5aa5              ; Ahora vamos a scrollar los atributos
;       ld de, $5a85
;       ld bc, 26
;       ldir
; 
;       ld hl, $5ac5
;       ld de, $5aa5
;       ld bc, 26
;       ldir
; 
;       ld hl, $5ae5
;       ld de, $5ac5
;       ld bc, 26
;       ldir
; 
;       ld a, (X_INIT)            ; Una vez scrollado todo, restauramos BC para imprimir al principio de la
;       ld b, Y_LIMIT             ; tercera l�nea
;       ld c, a
; 
;       ret
; 
;       SCROLL_SR:
; 
;       exx
; 
;       ld bc, 26
;       ldir
; 
;       ld a, -26
;       add a, e
;       ld e, a
;       inc d
; 
;       ld a, -26
;       add a, l
;       ld l, a
;       inc h
; 
;       exx
;       djnz SCROLL_SR
;
;       ret

KEY_TO_ENDTEXT:
      ld de, $50fd
      ld hl, CHARSET
      call SPRINT

      ld de, $50fe
      ld hl, CHARSET
      call SPRINT

      ld a, 8 | 5 | $40
      ld ($5afd), a
      ld ($5afe), a
      
      ret

; SPRINT: Imprime en la direcci�n DE el car�cter apuntado por HL

SPRINT:
      ld b, 8
      SPRINT_LOOP:
      ld a, (hl)
      ld (de), a
      inc hl
      inc d
      djnz SPRINT_LOOP
      ret

TS_WRITEATTR:
     ld l, e                  ;  4 Vamos a preparar HL apuntando a la direcci�n de ATTR que corresponda

     ld a, d
;     srl a                    ;  8
;     srl a                    ;  8
;     srl a                    ;  8 A conten�a D, es decir, el Byte Alto de la direcci�n de DATAFILE de pantalla
     rrca
     rrca
     rrca
     and 31

     or $58                   ;  7 Al hacer todo esto, la estamos convirtiendo a direcci�n ATTR
     ld h, a                  ;  4 Es decir, tenemos en HL la direcci�n ATTR donde tenemos que escribir

     ld a, (TEXT_ATTR)
     ld (hl), a

     ret

; DF_LOCATE: De vuelve en HL la direcci�n del archivo de pantalla
; Entrada: BC Coordenadas: B = L�nea C = Columna
; Salida: DE Direcci�n del archivo de pantalla
; Se conservan HL y BC

TS_DF_LOCATE:
    ld a, b
    and #f8
    add a, #40
    ld d, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld e, a
    ret


; --------------------------------------------------------------------------------------------
include "../gfx/asm/charset_AD_int.asm"
; --------------------------------------------------------------------------------------------
; Caracteres especiales:
; �   = [
; �   = \
; �   = ]
; �   = ^
; �   = _
; �   = {
; �   = |
; �   = }
; �   = ~
; (c) = El Copyright no se puede poner directamente, hay que poner su c�digo (127) como n�mero


; El # equivale a la flechita para abajo de "Continuar..."
; Y el ESPACIO equivale al borde inferior del pergamino, y lo usaremos para restaurarlo tras poner ese "Continuar..."

; Un byte 1 es indicador de un cambio atributos, cogiendo el siguiente byte como el valor de atributos a poner
; a partir de ese momento (Hay que restaurarlos a STANDARD_ATTR, salvo en los nombres de personajes, que ya se hace autom�ticamente)
;
; Un byte 13 es un salto de l�nea manual

CHAR_NAMES:
IF COMPILE_VERSION = VERSION_NELO
     defb 1,       $38     , 'Fray Nelo',0
     defb 1, $40 | $30     , 'Fray Quqo',0
ENDIF
IF COMPILE_VERSION = VERSION_SPA
     defb 1,       $38     , 'Fray Gonzalo',0
     defb 1, $40 | $30     , 'Fray Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_GAL
     defb 1,       $38     , 'Frade Gonzalo',0
     defb 1, $40 | $30     , 'Frade Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_ENG
     defb 1,       $38     , 'Fray Gonzalo',0
     defb 1, $40 | $30     , 'Fray Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_ITA
     defb 1,       $38     , 'Fra Gonzalo',0
     defb 1, $40 | $30     , 'Fra Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_CAT
     defb 1,       $38     , 'Fra Gonzalo',0
     defb 1, $40 | $30     , 'Fra Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_CAT2
     defb 1,       $38     , 'Fra Gonzalo',0
     defb 1, $40 | $30     , 'Fra Ces[reo',0
ENDIF
IF COMPILE_VERSION = VERSION_EUS
     defb 1,       $38     , 'Fray Gonzalo',0
     defb 1, $40 | $30     , 'Fray Ces[reo',0
ENDIF

IF COMPILE_VERSION = VERSION_ENG
     defb 1, $40 |   8 | 7 , 'Innkeeper',0
     defb 1, $40 | $10 | 7 , 'Drunk',0
     defb 1, $40       | 7 , 'Mr.Cirilo',0
     defb 1, $40 | $20     , 'Housekeeper', 0
     defb 1, $40 | $28     , 'Mr.Justiniano', 0     
     defb 1, $40 | $28     , 'Peasant', 0
     defb 1, $40 | $30     , 'Witch', 0
     defb 1, $40 | $20     , 'Locksmith',0
     defb 1, $40       | 2 , 'Brunilda''s soul',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Ant^n''s soul', 0
ENDIF

IF COMPILE_VERSION = VERSION_GAL
     defb 1, $40 |   8 | 7 , 'Pousadeiro',0
     defb 1, $40 | $10 | 7 , 'Borracho',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Ama de chaves', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Campesi~o', 0
     defb 1, $40 | $30     , 'Meiga', 0
     defb 1, $40 | $20     , 'Xerrajeiro',0
     defb 1, $40       | 2 , 'Anima de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Anima de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_SPA
     defb 1, $40 |   8 | 7 , 'Posadero',0
     defb 1, $40 | $10 | 7 , 'Borracho',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Ama de llaves', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Paisano', 0
     defb 1, $40 | $30     , 'Meiga', 0
     defb 1, $40 | $20     , 'Cerrajero',0
     defb 1, $40       | 2 , 'Fantasma de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Fantasma de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_NELO
     defb 1, $40 |   8 | 7 , 'Posadero',0
     defb 1, $40 | $10 | 7 , 'Borracho',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Ama de llaves', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Paisano', 0
     defb 1, $40 | $30     , 'Meiga', 0
     defb 1, $40 | $20     , 'Cerrajero',0
     defb 1, $40       | 2 , 'Fantasma de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Fantasma de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_ITA
     defb 1, $40 |   8 | 7 , 'Locandiere',0
     defb 1, $40 | $10 | 7 , 'Ubriaco',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Governante', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Contadino', 0
     defb 1, $40 | $30     , 'Strega', 0
     defb 1, $40 | $20     , 'Fabbro',0
     defb 1, $40       | 2 , 'Fantasma de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Fantasma de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_CAT
     defb 1, $40 |   8 | 7 , 'Posadero',0
     defb 1, $40 | $10 | 7 , 'Borratxo',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Majordoma', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Pais�', 0
     defb 1, $40 | $30     , 'Meiga', 0
     defb 1, $40 | $20     , 'Serraller',0
     defb 1, $40       | 2 , 'Fantasma de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Fantasma de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_CAT2
     defb 1, $40 |   8 | 7 , 'Posadero',0
     defb 1, $40 | $10 | 7 , 'Borratxo',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Majordoma', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Pais�', 0
     defb 1, $40 | $30     , 'Meiga', 0
     defb 1, $40 | $20     , 'Serraller',0
     defb 1, $40       | 2 , 'Fantasma de Brunilda',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Fantasma de Ant^n', 0
ENDIF

IF COMPILE_VERSION = VERSION_EUS
     defb 1, $40 |   8 | 7 , 'Ostalaria',0
     defb 1, $40 | $10 | 7 , 'Mozkorra',0
     defb 1, $40       | 7 , 'D.Cirilo',0
     defb 1, $40 | $20     , 'Giltzain', 0
     defb 1, $40 | $28     , 'D.Justiniano', 0
     defb 1, $40 | $28     , 'Baserritarra', 0
     defb 1, $40 | $30     , 'Sorgina', 0
     defb 1, $40 | $20     , 'Sarrailagilea',0
     defb 1, $40       | 2 , 'Brunilda-ko mamua',0
     defb 1, $40       | 7 , '???', 0
     defb ' ',0
     defb ' ',0
     defb ' ',0
     defb 1, $40 | $28     , 'Ant^n-ko mamua', 0
ENDIF

BOSS_NAMES:
     defb 1, $40 | $30 | 2 , 'Urco' , 0
     defb 1, $40 | $30 | 2 , 'Mouro', 0
     defb 1, $40 | $30 | 3 , 'Serpe', 0
     defb 1, $40 | $30 | 3 , 'Tardo', 0
IF COMPILE_VERSION=VERSION_ENG
     defb 1, $40 | $30     , 'Witch', 0
ENDIF
IF COMPILE_VERSION=VERSION_SPA
     defb 1, $40 | $30     , 'Meiga', 0
ENDIF
IF COMPILE_VERSION=VERSION_GAL
     defb 1, $40 | $30     , 'Meiga', 0
ENDIF
IF COMPILE_VERSION=VERSION_NELO
     defb 1, $40 | $30     , 'Meiga', 0
ENDIF
IF COMPILE_VERSION=VERSION_CAT
     defb 1, $40 | $30     , 'Meiga', 0
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
     defb 1, $40 | $30     , 'Meiga', 0
ENDIF
IF COMPILE_VERSION=VERSION_ITA
     defb 1, $40 | $30     , 'Strega', 0
ENDIF
IF COMPILE_VERSION=VERSION_EUS
     defb 1, $40 | $30     , 'Sorgina', 0
ENDIF
     
CHAPTER_NAME:

IF COMPILE_VERSION=VERSION_ENG
     defb 1, 6 | $40, ' The arrival ', 0
     defb 1, 6 | $40, '  The Witch', 0
     defb 1, 6 | $40, 'The apparition', 0
     defb 1, 6 | $40, 'Together at last', 0
     defb 1, 6 | $40, 'The revelation', 0
ENDIF

IF COMPILE_VERSION=VERSION_SPA
     defb 1, 6 | $40, '  La llegada', 0
     defb 1, 6 | $40, '   La meiga', 0
     defb 1, 6 | $40, ' La aparici^n', 0
     defb 1, 6 | $40, 'Por fin juntos', 0
     defb 1, 6 | $40, 'La  revelaci^n', 0
ENDIF

IF COMPILE_VERSION=VERSION_NELO
     defb 1, 6 | $40, '  La llegada', 0
     defb 1, 6 | $40, '   La meiga', 0
     defb 1, 6 | $40, ' La aparici^n', 0
     defb 1, 6 | $40, 'Por fin juntos', 0
     defb 1, 6 | $40, 'La  revelaci^n', 0
ENDIF

IF COMPILE_VERSION=VERSION_GAL
     defb 1, 6 | $40, '  A  Chegada', 0
     defb 1, 6 | $40, '   A  meiga', 0
     defb 1, 6 | $40, ' A  aparici^n', 0
     defb 1, 6 | $40, 'Por fin xuntos', 0
     defb 1, 6 | $40, ' A revelaci^n', 0
ENDIF

IF COMPILE_VERSION=VERSION_ITA
     defb 1, 6 | $40, '   L''arrivo', 0
     defb 1, 6 | $40, '  La  strega', 0
     defb 1, 6 | $40, 'L'' aparizione', 0
     defb 1, 6 | $40, 'Assieme alla fine', 0
     defb 1, 6 | $40, 'La rivelazione', 0
ENDIF

IF COMPILE_VERSION=VERSION_CAT
     defb 1, 6 | $40, ' L''arribada', 0
     defb 1, 6 | $40, '   La meiga', 0
     defb 1, 6 | $40, '  L''aparici^', 0
     defb 1, 6 | $40, 'Per  fi  junts', 0
     defb 1, 6 | $40, ' La  revelaci^', 0
ENDIF

IF COMPILE_VERSION=VERSION_CAT2
     defb 1, 6 | $40, ' L''arribada', 0
     defb 1, 6 | $40, '   La meiga', 0
     defb 1, 6 | $40, '  L''aparici^', 0
     defb 1, 6 | $40, 'Per  fi  junts', 0
     defb 1, 6 | $40, ' La  revelaci^', 0
ENDIF

IF COMPILE_VERSION=VERSION_EUS
     defb 1, 6 | $40, '   Helduera', 0
     defb 1, 6 | $40, '   Sorgina', 0
     defb 1, 6 | $40, '   Agerpena', 0
     defb 1, 6 | $40, 'Azkenik batera', 0
     defb 1, 6 | $40, 'Ezagutaraztea', 0
ENDIF


PRESS_FIRE:
IF COMPILE_VERSION=VERSION_ENG
     defb 1, 6 | $40, ' Press  fire',0
ENDIF
IF COMPILE_VERSION=VERSION_SPA
     defb 1, 6 | $40, 'Pulsa  fuego',0
ENDIF
IF COMPILE_VERSION=VERSION_NELO
     defb 1, 6 | $40, 'Pulsa  fuego',0
ENDIF
IF COMPILE_VERSION=VERSION_GAL
     defb 1, 6 | $40, 'Prensa''fogo''', 0
ENDIF
IF COMPILE_VERSION=VERSION_ITA
     defb 1, 6 | $40, 'Pressa fuoco',0
ENDIF
IF COMPILE_VERSION=VERSION_CAT
     defb 1, 6 | $40, ' Pulsa tret ',0
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
     defb 1, 6 | $40, ' Pulsa tret ',0
ENDIF

IF COMPILE_VERSION=VERSION_EUS
     defb 1, 6 | $40, '''Sua''sakatu',0
ENDIF




IF COMPILE_VERSION=VERSION_SPA
include "mess/MESS.asm"
ENDIF

IF COMPILE_VERSION=VERSION_ENG
include "mess/MESS_ENG.asm"
ENDIF

IF COMPILE_VERSION=VERSION_NELO
include "mess/MESS_SEJUAN.asm"
ENDIF

IF COMPILE_VERSION=VERSION_ITA
include "mess/MESS_ITA.asm"
ENDIF

IF COMPILE_VERSION=VERSION_GAL
include "mess/MESS_GAL.asm"
ENDIF

IF COMPILE_VERSION=VERSION_CAT
include "mess/MESS_CAT.asm"
ENDIF

IF COMPILE_VERSION=VERSION_CAT2
include "mess/MESS_CAT2.asm"
ENDIF

IF COMPILE_VERSION=VERSION_EUS
include "mess/MESS_EUS.asm"
ENDIF

STRING_BUFFER:
     defs BUFFER_SIZE, 0
