org $c000
include "version.asm"

COMPRESSED_BMPS:
     defw CHAPTER_BMP, NUM1_BMP, NUM2_BMP, NUM3_BMP, NUM4_BMP, NUM5_BMP

MAP4:
     defb 88
     incbin "../bin/map4.opt"
MAP4_DOORS:
     incbin "../bin/map4_doors.opt"
MAP4_SIGNS:
     incbin "../bin/map4_signs.bin"
TILESET4:
     incbin "../bin/tileset4.opt"

MAP5:
     defb 54
     incbin "../bin/map5.opt"
MAP5_DOORS:
     incbin "../bin/map5_doors.opt"
MAP5_SIGNS:
     incbin "../bin/map5_signs.bin"
TILESET5:
     incbin "../bin/tileset5.opt"


; --------------------------------------------------------------------------------------------

PERGAMINO:
incbin "../gfx/asm/pergamino.bin"

FIGHT_SCR:
incbin "../bin/fight_scr.bin.opt"

FIGHT_GRAPHICS:
incbin "../bin/fight_graphics.opt"

TITLE_SCR:
IF COMPILE_VERSION=VERSION_ENG
incbin "../bin/title_eng.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_GAL
incbin "../bin/title_gal.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_ITA
incbin "../bin/title_ita.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_SPA
incbin "../bin/title.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_NELO
incbin "../bin/title.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT
incbin "../bin/title_cat.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
incbin "../bin/title_cat.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_EUS
incbin "../bin/title_eus.bin.opt"
ENDIF


CREDITS_SCR:
IF COMPILE_VERSION=VERSION_ENG
incbin "../bin/credits.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_GAL
incbin "../bin/creditos_gal.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_ITA
incbin "../bin/credits_ita.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_SPA
incbin "../bin/creditos.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_NELO
incbin "../bin/creditos.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT
incbin "../bin/creditos_cat.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
incbin "../bin/creditos_cat2.bin.opt"
ENDIF
IF COMPILE_VERSION=VERSION_EUS
incbin "../bin/creditos_eus.bin.opt"
ENDIF

CHAPTER_BMP:
IF COMPILE_VERSION=VERSION_ENG
incbin "../bin/chapter.opt"
ENDIF
IF COMPILE_VERSION=VERSION_GAL
incbin "../bin/capitulo.opt"
ENDIF
IF COMPILE_VERSION=VERSION_ITA
incbin "../bin/capitolo.opt"
ENDIF
IF COMPILE_VERSION=VERSION_SPA
incbin "../bin/capitulo.opt"
ENDIF
IF COMPILE_VERSION=VERSION_NELO
incbin "../bin/capitulo.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT
incbin "../bin/capitol.opt"
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
incbin "../bin/capitol.opt"
ENDIF
IF COMPILE_VERSION=VERSION_EUS
incbin "../bin/kapitulu.opt"
ENDIF

NUM1_BMP:
incbin "../bin/num_1.opt"
NUM2_BMP:
incbin "../bin/num_2.opt"
NUM3_BMP:
incbin "../bin/num_3.opt"
NUM4_BMP:
incbin "../bin/num_4.opt"
NUM5_BMP:
incbin "../bin/num_5.opt"
