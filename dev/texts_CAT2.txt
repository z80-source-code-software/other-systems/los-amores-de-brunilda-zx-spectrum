# \01 = Set attribute
# \0D = New line
#
#

San Arteixo do Montalvo
Serralleria de Xosé
Pousada de Xan
Déu protegeixi aquesta casa i als que hi viuen
Deus lari Berobreo aram posuit pro salute
# 05
Ermita de San Arteixo
\01\45Galicia, 30 de octubre de 1830.\01\07
Dos monjos franciscans es dirigeixen en peregrinació pel camí de Santiago\0D\0D\01\44Fra Gonzalo\01\07 y \01\44Fra Cesáreo\01\07 porten tot el dia caminant i decideixen descansar al primer poble que troben. 
En perdre tota la seva fe, Fra Gonzalo va embogir i va quedar vagant pels boscos.\0D\0DN'hi ha qui explica que el va veure guiant la Santa Companya.\0D\0DFra Cesáreo va morir a casa de l'apotecari, i fou enterrat a l'aldea.
Amb una mica de sort, \01\46Sant Arteixo do Montalvo\01\07 us acullirà com tants altres han fet al llarg del seu viatge.\01\30
# 10
Fra Gonzalo, aquella no és la posada. Tenim que buscar-la el més aviat possible.
Fra Gonzalo, hem de buscar on dormir. No voldreu pas que ho fem al bosc?
Hem de trobar on allotjar-nos abans no es faci fosc.
Sí, germà. A mi aquests paratges em fan pavor de nit. Mai se sap què hi pot haver.
Ja esteu amb les vostres pors i supersticions?
# 15
No em puc reprimir, és la foscor. Mai m'hi podré acostumar.
Bé, busquem. Que ens cau la nit a sobre.
La pau de Déu sigui amb vos, germà. Cerquem allotjament per aquesta nit.
Ho sento, bons homes, estic fent reformes i no tinc cap habitació disponible.
Però necessitem un lloc on dormir, aquests boscos fan molta por per la nit.
# 20
Tranquil, Fra Cesáreo, alguna solució trobarem.
És clar, bons homes. Ben segur que Don Justiniano, l'apotecari, estarà encantat d'oferir-vos casa seva. És molt amic del capellà i eseent vostès pelegrins, tindrà la caritat d'acullir-vos.
Compte! Ic! Us envia a la gola del llop Ic! Aquella casa està maleïda, Ic!
Calla borratxo, no espantis a aquests homes de Déu. 
La donzella es va suïcidar  Ic! i la seva ànima vaga pels passadissos Ic!
# 25
Calla, borratxo!
No es preocupi, nosaltres no som supersticiosos. On puc trobar l'apotecari?
És millor que parleu amb Don Cirilo, el capellà, a l'església.
Aquest poble està maleït Ic! l'única solució Ic! és oblidar bevent
Dispensin a aquesta pobra ànima perduda per l'alcohol, no el creguin.
# 30
Moltes gràcies, Fra Gonzalo, ara descansem en pau. 
Si us plau, parlem amb el capellà el més aviat possible. 
Benvinguts, germans. Us estava esperant. Aquesta és una aldea petita, i les notícies volen.
Bona nit, germà. Som Fra Gonzalo i Fra Cesáreo, pelegrins de l'Apòstol, i busquem on passar la nit...
Sí, sí, és clar, volen hospedatge i no hi ha lloc a la posada.\0DTenien que haver vingut a veure'm abans de res.
#35
Ens han dit que l'apotecari podria allotjar-nos si vos media
Per descomptat. Fins i tot jo mateix, aquí a la sagristia, pero és petita, i no tinc tant lloc com D. Justiniano.\0DÉs un home ric, i a casa seva té moltes habitacions buides.
Els franciscans no som amics de luxes: només aspirem a un sostre i un matalàs.
Per suposat, però segur que agreiran l'amabilitat d'un bon cristià que els ofereix un llit tou i un bon sopar.\0DNo sempre es presenta aquella oportunitat, i això faria el Camí una mica menys dur.
Queda molt lluny la casa de l'apotecari?
#40
Una petita passejada pel bosc. És millor que sortim ja, abans que sigui nit tancada.
Perdoneu a Fra Cesáreo, l'obscuritat el torna supersticiós i poruc.
Fa bé. La nit és refugi de coses dolentes i la prudència és una virtut. Sortim.
Seguiu-me, germans. El camí és curt però és fàcil que et perdis.\0DSi s'allunyen massa els esperaré, però donem-nos pressa per arribar abans que caigui la nit.
Germà, seguim a D. Cirilo. La nit està al caure.
#45
Ah de la casa!
Don Cirilo! Quina sorpresa a aquesta hora de la nit!
Sí, veritat? No són hores per anar per aquests verals, jeje\0DVinga, deixa'ns entrar, que hem de parlar amb D. Justiniano.
Sí, però és gairebé l'hora de sopar!
Doncs més al meu favor: On sopa un, sopen quatre! jeje
#50
Amic Cirilo, no t'esperava tant tard. Què et porta per aquí?
Saps que no surto de nit, si no és necessari.\0DHan arribat a l'aldea aquests dos germans peregrinant a Santiago, i busquen on passar la nit. Llavors he pensat que podries fer una bona acció.
Per descomptat, Cirilo. Germans, sigueu benvinguts a aquesta, la meva humil llar.\0DMaruxa, prepara les habitacions de convidats, que els germans es queden a dormir... i a sopar, perquè tindran gana, veritat?
Sí, moltes gràcies, Don Justiniano. Que Déu us ho pagui.
Jo tambè tinc gana, que fa molt bona olor, i aquestes viandes cal provar-les, jeje
#55
Tu sempre igual. Per descomptat que et quedes a sopar, i dormir, que ja és nit tancada.
Si us plau, seguiu-me.
El sopar estarà llest de seguida. Baixeu al menjador quan estigueu instal·lats.
D'acord. Moltes gràcies.
Passin, germans. Us estàvem esperant.
#60
Ara anem a prendre unes copetes d'orujo que ajudaran a la digestió.
El orujo de D. Justiniano és realment bo, si el venguès es faria d'or.
Tonteries, no ho penso vendre: si el venc no en podríem gaudir... haha
Cert, millor no el venguis... hehe
Us mostraré el meu laboratori però segur que a vostès no els interessa la medecina i la investigació científica.
#65
A mi sí, m'encantaria fer-hi un cop d'ull: Vaig estudiar medecina abans de prendre els hàbits.
Un col·lega, vaja, vaja... crec que tindrem una estupenda vetllada.
Jo ho sento, però els meus ossos em demanen descansar. Seguiu la vostra conversa, fins demà.
Jo tambè marxaré a dormir. El bon pelegrí se'n va a dormir aviat i matina molt.
Quina nit, què malsons. No havia d'haver begut tant orujo.
#70
És estrany. Sembla que Fra Cesáreo no hagués dormit aquí aquesta nit.
Aquesta porta sembla fermament tancada.
No puc marxar d'aquí sense Fra Cesáreo.
Fra Gonzalo! No es pot imaginar el que ha passat.
Fra Cesáreo! Apotecari, què li passa?
#75
No ho sabem, va apareixer en l'ala oest de la casa, aquella part sempre està tancada.
Sempre està tancada des que...
Llavors, com és que hi era Fra Cesáreo?
No ho sabem. Ahir a la nit em vaig quedar adormit al laboratori mentre Fra Cesáreo seguia mirant els meus experiments. \0DEn despertar vaig veure papers tirats que deixaven un rastre fins l'ala oest i allí el vaig trobar, a terra.
Però... està mort?
#80
No, encara viu, però està inconscient i res el fa despertar.
Creiem que és presa d'un encanteri.
Don Cirilo, vostè és capellà! No ha de creure en supersticions.
Fill, fa molt que vaig aprendre que en aquestes terres tot és possible.
Hauríem d'avisar a la meiga. Segur que podria ajudar-nos.
#85
És ridícul, crec que el tindríem que portar a la capital per que el tractin.
Feu-nos cas, germà. Aquest no és un mal del cos, si no de l'ànima.
Nomès la meiga pot ajudar-nos. Heu d'anar a buscar-la. Viu en una cabana, al bosc que hi ha entre aquesta casa i el poble.
Nosaltres ens quedarem cuidant el malalt per si despertés.
Què us sembla aquest D. Justiniano, germà?
Un bon cristià, generós i simpàtic, la veritat.
#90
A mi tambè, pero... he sentit un calfred en entrar en aquesta casa. Espero que no sigui res.
No busqui més explicació que un corrent d'aire, germà. Serà això, segur. Baixem a sopar.
Baixem a sopar, germà. Ens estan esperant.
El meu treball és obrir panys, no n'hi ha cap que s'em resisteixi.
#95
Aquest armari està obert, però no veig el motiu de ficar-me la roba de la majordoma... Almenys no en aquest moment.
Més tard...
Al matí següent...
Això és un crani humà! No ho agafaré sense una bona raó.
Quina desgràcia, aquesta casa no és la mateixa des de fa uns quants anys.
#100
He d'anar a buscar la meiga a veure si pot ajudar-nos.
No l'ajudarà si no li paga pels seus serveis. Tingui aquesta gallina, serà suficient.
Ha passat alguna cosa terrible! Pugeu, el senyor l'espera dalt, amb el seu pobre amic.
Se m'ha acabat l'orujo i fins al mes que ve no m'arriba la comanda. Aquest poble cada dia beu més.\0DDonaria diners per una ampolla, encara que nomès fos una.
Dispensi, necessito els serveis d'una meiga... Podria indicar-me on trobar-la?
#105
Meiga? Al bosc hi viu una que diuen que ho és: 
Aneu amb compte amb ella, no és de fiar i, per descomptat, mai treballa gratis.
Al nord del poble trobaran la posada de Xan. De ben segur que allà hi podran ajudar-los.
Però una cosa li diré, germà: Per poder trobar-la, ha de creure en ella. Aquesta fe en la seva església no ho permetrà. No puc anar-me'n del poble i deixar abandonat el pobre Fra Cesáreo.
Un monjo foraster entrant a la casa d'una meiga. Això sí que és una sorpresa.
#110
Don Cirilo m'ha enviat a buscar-vos. El meu company ha caigut, segons ell, presa d'un encanteri que el manté inconscient.
L'aniré a veure, però els meus serveis no són gratuïts.
Acceptaria aquesta gallina a canvi?
Amb aquesta gallina faré un brou que li anirà fenomenal als meus pobres vells ossos. Anem a veure aquest embruixat.
Està bé. Buscaré alguna cosa que la pugui complaure.
#115
Aquesta vella meiga no és com vostès els capellans. No em comprarà amb or.
Si vol que l'acompanyi, haurà de pagar-me amb alguna cosa.
Gràcies a Déu que heu vingut, mira, és aquest pobre home. 
És clar. En l'ala oest, la nit de difunts i en aquest estat no és més que la maledicció de Brunilda. 
Oh, Déu meu, Brunilda de nou.
#120
Brunilda? Qui és Brunilda? Santa Brunilda? Tot això és ridícul!
Res és ridícul. Brunilda es va suïcidar en aquesta casa i la seva ànima vaga per la casa\0DNecessita una altra ànima per sortir del purgatori, i ha agafat la d'aquest pobre home.
No crec en aquestes coses, segur que és un col·lapse.
No cregui si no vol, però si no fa alguna cosa per la seva ànima, el seu amic continuarà així fins que mori, i, en això segur que pensem igual, és una cosa que succeirà molt aviat si segueix en aquest estat. 
Feu cas, Fra Gonzalo, ella sap el que diu.
#125
Proposo esperar a veure si desperta i demà prendrem una decisió Què li sembla Fra Gonzalo?
Molt millor així.
Si canvieu d'opinió, ja sabeu on visc, adéu.
Fra Cesáreo segueix inconscient. Hem de portar-lo demà a la ciutat. Ara intentaré descansar una mica.
Les dotze de la nit i no he pogut aclucar l'ull. Quina nit.
#130
SANT PARE!
No s'espanti, pare. Escolti la meva història...
No puc creure el que veig!
Soc Brunilda, o millor dit, el seu esperit.\0DJo treballava en aquesta casa, era part del servei. Fa anys vaig tenir un romanç secret amb Antón, el fill de l'apotecari. El seu pare no sabia res i va arribar l'hora d'explicar-ho.
El meu amat va sortir al bosc amb el seu pare per sincerar-se, pero mai arribà aquell moment, uns bandits sortiren al pas, i el van matar.\0DAixí que em vaig assabentar, no ho vaig resistir: La bogeria em va invaïr i vaig suicidar-me.
#135
Tenia l'esperança de reunir-me amb el meu amat en l'altra vida, però per alguna raó, vago per aquesta casa des de llavors.
I la teva història qué té que veure amb el meu germà Fra Cesáreo?
Molt. La desgràcia regna en aquesta casa des de llavors, la seva ànima està com la meva, atrapada. 
Sóc un home de fe, no crec que això estigui passant...
Sé com solucionar aquesta situació. Les nostres desgràcies estan encadenades: Si m'ajuda a mi, s'ajuda a sí mateix.
#140
Explica'm i faré el que pugui.
Vagi al cementiri, darrera de l'església, aquesta mateixa nit. Allà ens veurem.
No us preocupeu. Jo li obro.
Aquesta és la meva tomba. No se per què, pero sento que quelcom va malament a dins.
Que alguna cosa va malament?
#145
Percebo clarament que alguna cosa va malament a la tomba. Cava i obre-la, si us plau.
Però com?
Aconseguiu una pala. Mireu en algun jardí, els jardiners solen usar-les.
Us ho vaig dir, alguna cosa anava malament a la meva tomba!
Pare Sant, falta el crani!
#150
Si us plau, trobeu el meu crani, i fiqueu-lo junt al cos.
Entenc que la vostra ànima no trobi descans.
No trobaré descans fins que estigui junt al meu amor. Quan tingui el crani, enterreu-nos junts.
Tot el que està passant em porta tristíssims records.Brunilda era tan bona!
Ara veuràs!
#155
Qui parla?
Em porto el teu cos a una altra banda!
Noooooo! Ara la meva ànima no podrà descansar!
No puc creure el que he vist!
Estàs segur de voler abandonar la partida? (S/N)
# 160
La teva fe t'impedirà ajudar a Brunilda, hahaha
Fra Gonzalo, desperteu! Què feu aquí? Com ha entrat?
Don Cirilo... no s'ho creurà... Brunilda... el seu cos... la seva tomba...
Brunilda? Sabia que al final tenia que veure amb tot això...\0DNomès una persona pot ajudar-nos en aquest moment: La meiga. Aneu a buscar-la. Jo em quedaré a l'esglèsia, pregant per l'ànima del seu amic.
Aquesta història de l'ànima de la majordoma és el súmmum.\0DProu desgràcies ha hagut ja en aquesta casa.\0DHe decidit repassar la literatura científica a la recerca de casos com el del seu amic.
# 165
Si troba quelcom que cregui ens pot ajudar, porteu-me'l.
Aquest bolet és extraordinari! No sabia que creixessin per aquí. Estic segur que ens pot ajudar en el cas del seu company, però he de comprovar-ho en el laboratori.
Si us plau, germà. Deixeu allò on està. Aquest crani ha pertangut a la meva família des de que el meu avi va anar a Salamanca a estudiar medecina.
Aquest bolet extrany que m'ha portat ens ajudarà molt, estic segur, però necessito més temps.
Bé, suposo que ha vingut a demanar-me consell i consell us donaré. \0DSé qui té el crani i on podràs trobar les quatre parts de l'esquelet de Brunilda.
# 170
L'esquelet està en quatre cavernes dels antics celtes. Per arribar a elles, primer recolliu en aquesta gerra l'aigua de els 7 gorgs del Monte das Pozas i regueu amb ella els monòlits que hi ha junt a les seves entrades.
El crani el té l'apotecari al seu despatx. Sospito que te bastant a veure en aquesta història: no crec que li agradès que el seu fill s'enamorés de la criada.\0DQuan ho tingueu tot o necessiteu ajuda vingeu a veure'm.
Li donaré un fanal perquè s'ilumini a les cavernes a canvi de 7 bolets.
Cerqueu aquests set bolets, i torneu a veure'm.
Aquí teniu el fanal. Ara aconseguiu l'esquelet.
# 175
Encara no tens l'esquelet complert. Com vols que t'ajudi?
Monte das Siete Pozas.
Compte amb els despreniments.
Això està escrit en uns caracters que els meu ulls cristians no saben desxifrar, i encara més, no haurien d'haver vist.
Aquest cova està en total obscuritat! Seria temerari entrar sense res amb que iluminar-me!
# 180
Els set gorgs sagrats dels Celtes es troben davant teu. La seva aigua conté la saviesa dels vells Druides, i t'ajudarà a trobar el teu camí.
En aquell cartell fica que hi ha despreniments. No tindria que passar sense una bona raó.
No necessito més aigua d'aquest gorg, ja la vaig recollir abans.
La meiga va dir que necessito l'aigua de cadascun dels set gorgs perquè tingui efecte.
Vegem què passa...
# 185
No he de malgastar l'aigua.
La superfície del monòlit no reflexa ni un sol raig de sol.
Està bé, té els ossos. Heu guanyat una batalla, però no la guerra.
Això t'ensenyarà a no ficar-te amb qui no toca.
Et vencerè amb la meva dansa macabra.
# 190
Agenolla't i seré caritatiu amb tú.
Ni parlar-ne, això mai!
Repeteix el meu ball, però fes-ho en el moment just o et venceré!
Has begut oli, fraret!
Vade retro, monstre!
# 195
No lluitaré de nou contra aquest monstre si puc evitar-ho.
Molt bé. Ara, mentre jo invoco l'ànima de Brunilda, aneu al cementiri per ficar els ossos al seu lloc.
Aneu al cementiri a col·locar els ossos. Invocar un ànima del purgatori és un treball ardu i perillós que no s'ha d'interrompre.
Ho has portat tot! Per fi podré descansar en pau!
No passa res. Realment totes aquestes parts són meves?
# 200
Sí, totes. També el crani, que el tenia l'apotecari en el seu despatx.
No! Aquest crani no és meu. Ja el tenia abans de que jo morís, va pertànyer al seu avi.
Jaja! Heu picat els dos, el teu crani el tinc jo.
Tu! Sempre ens vas mirar amb mals ulls
Si jo no puc estar amb Antón, que no ho estigui ningú, i tu me'l vas prendre.
# 205
Torna'ns el crani, no guanyes res amb això, Antón està mort.
Sí, però el gust de la venjança és molt dolç!
Moltes gràcies, mai vaig pensar que la meiga estigués gelosa de nosaltres.
Ara ja no cal preocupar-se per ella.
Brunilda, amor, ja estem junts.
# 210
Sí, només falta que el teu pare accepti la nostra relació i que s'oficiï una missa per les nostres ànimes.
Fra Gonzalo, ens ajudareu?
El meu diari us donarà la pista d'on trobar les cartes que ens enviàvem. 
Mostreu-li al meu pare: Això serà prou perquè s'assabenti de la nostra relació i accepti.
Busca en la meva habitació, en l'ala oest de la casa.
# 215
Veig que no has tingut prou. Està bé, et mostraré el meu veritable poder...
Aquest orujo que em vas portar, pare, és excepcional. M'ha salvat la pell.
Lloat sigui Déu, una ampolla d'orujo. Li compro! Tingueu aquestes monedes, crec que serà suficient.
He estat meditant que potser aquest orujo que destila l'apotecari podria despertar el meu germà, no creieu?
Lloat sia Déu! Gran idea. Aquest orujo ressuscita els morts, porteu-vos aquesta ampolla, és la millor, segur que us ajuda... però no li digueu a D. Justiniano que li he donat, o em farà fora.
# 220
Voleu anar a l'ala oest? No podeu, el senyor mai us deixaria, sóc l'única a qui deixa les claus, i, disculpeu-me, pare, però en això no us ajudaré: S'estan tornant tots bojos!
No puc anar vestit així per allí i que algú em vegi. Necessito tornar a vestir els meus hàbits.
La clau? Vostè veurà, però no vagi de nit, ja sabeu el que passa i no vull lamentar més desgràcies.
No em molestis més, estic molt ocupat.
El diari diu que les cartes estan amagades darrera un passadís en la biblioteca.\0DPer accedir, cal empenyer la llibreria del racó nord.
# 225
L'apotecari té uns exemplars excel·lents en aquesta biblioteca.
Aquesta ha de ser la llibreria que diu Brunilda. Si l'empenyo una mica potser cedeixi...
Aquest és el cofre que descrivia Brunilda al seu diari, però està tancat amb clau! Necessitaré obrir-ho per agafar les cartes. 
Bon pany, clar que el puc obrir, però aquest pobre serraller necessita diners per poder pagar el seu menjar...
A canvi d'aquestes monedes, us obrirè encantat aquest cofre.\0D\0DAquí teniu, germà.
# 230
Aquestes cartes són del meu fill per... Brunilda? Estaven enamorats i no van gosar a dir-m'ho! Ella es va suïcidar per amor!\0DAra ho entenc. S'ha de dir una missa pel seu descans: Parlaré amb el capellà. Us espero allí.
Aquest crucifix m'ha permés resar quan estava a punt de perdre la fe. M'ha salvat de caure en la bogeria!
Germà, estic a punt de tornar-me boig amb tot això. Seguiré resant amb l'esperança que Déu ho resolgui tot en la seva infinita bondat.
Encara que vulgui evitar-ho, les supersticions dels vilatans fan que la meva fe flaquegi.
Sento que estar entre aquests murs sagrats reforça la meva fe per moments.
# 235
Moltes gràcies, Fra Gonzalo, m'ha donat molta pau saber tota l'història.
Ja descansen en tombes contigües. Ara només falta que els hi diguem una misa, les seves ànimes descansaran i Fra Cesáreo tornarà en si.
Això serà si jo ho permiteixo. La meva ànima està condenada però la d'ells mai descansarà!
Esperit maligne, torna a l'abisme del que mai vas haber de sortir.
Oh, no! Em derriteixo, em derriteixo!!!
# 240
Finalment el mal va vencer.\0D\0DFra Gonzalo, el capellà i l'apotecari van morir en el cementiri i les seves ànimes, junt a la de Fra Cesáreo van quedar vagant per la zona.\0D\0DLa maledicció va invaïr l'aldea i va quedar abandonada pels segles.
Per fi hem aconseguit que les seves ànimes descansin, i ara la meiga ha desaparegut per sempre.
I Fra Cesáreo? Hi haurà despertat?
Anem corrent a casa a veure'l!
Fra Cesáreo, lloat sia Déu! Heu despertat!
# 245
Estic una mica marejat però estic bé, quant he dormit?
Molt. I us heu perdut una de bona. Us ho explicarem durant el sopar.
Quina gran història, Fra Gonzalo. Ja sabia jo que aquestos boscos amagaven grans perils.
El que compta és que al final tot ha sortit bé.\0DAnem al llit, que el sopar ha estat opípar i l'orujo de l'apotecari és realment fort.
Fra Gonzalo, anem, que ja és molt tard, tenim que emprendre el camí.
# 250
Eh? Vaja, m'he adormit, l'orujo d'ahir nit... he somniat amb Brunilda, la seva ànima m'ha tornat a visitar en somnis...
I desprès d'aquestes paraules, els dos frares van continuar la seva marxa cap a Santiago, i qui sap quines noves aventures trobaríen en el camí...\0D\0D          El final?
Fra Cesáreo, Brunilda, l'història de Brunilda, vos inconscient, Antón, les seves ànimes, la meiga...
Però, de què parleu? Ahir nit vam arribar aquí i vam sopar amb l'apotecari, us vau prendre uns quants orujos que ja es veu us van produïr malsons... Jaja
Llavors...
# 255
Llavors res. Vinga germà, reprenguem la marxa que encara ens queda molt per arribar a Santiago.
#--------------------------------------------------------------------------------------------------------------------------------
# TABLA 2
#--------------------------------------------------------------------------------------------------------------------------------
L'habitació de la majordoma, al contrari que la resta de la casa, està molt desordenada: Ni tan sols ha tancat el seu armari, per exemple.
Mecessito la clau de l'ala oest.
No, pare. De cap manera. No puc deixar-li la clau de l'ala oest, i menys desprès de tot el que ha passat.
En aquest prestatge predominen els llibres de medecina.
La química és la matèria que ocupa aquesta prestatgeria.
#  5
Pel que es veu, l'apotecari està molt interessat en les matemàtiques.
La botànica també forma part de les matèries estudiades per Don Justiniano.
En aquest prestatge troben el seu espai narracions històriques.
Llibres religiosos ocupen les baldes d'aquesta prestatgeria.
Aquesta sembla l'habitació de Brunilda.
#  10
Antigues novel·les troben el seu lloc aquí. Es veu que fa molt que ningú les llegeix.
Les cartes estan aquí dins. Ara trobi qui l'ajudi a obrir el cofre.