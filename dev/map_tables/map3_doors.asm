MAP3_DOORS:
    ;     Origen X, Origen Y, Off X, Off Y, Mapa dest, Y destino, X Destino
    ;                                                        $ff   Escena

     defb       2,        43,     0,     2,         3,       $ff,        27       ; Salida 1
     defb       2,        43,     0,     2,         3,         7,        12       ; Salida 1
     defb      67,        43,     0,     2,         3,       $ff,        27       ; Salida 2
     defb      67,        43,     0,     2,         3,         6,        25       ; Salida 2
     defb       3,        89,     0,     2,         3,       $ff,        27       ; Salida 3
     defb       3,        89,     0,     2,         3,        18,        33       ; Salida 3
     defb      48,        89,     0,     2,         3,       $ff,        27       ; Salida 4
     defb      48,        89,     0,     2,         3,        29,        11       ; Salida 4

     defb      52,        23,     1,     2,         2,        19,        87       ; Mazmorra 2
     defb      87,        20,     1,     2,         2,        24,        52       ; Mazmorra 2

     defb      47,        74,     1,     2,         2,        70,        47       ; Mazmorra 4
     defb      47,        71,     1,     2,         2,        75,        47       ; Mazmorra 4

     defb      63,        74,     1,     2,         2,        70,        63       ; Mazmorra 4
     defb      63,        71,     1,     2,         2,        75,        63       ; Mazmorra 4

     defb      86,        74,     1,     2,         2,        70,        86       ; Mazmorra 4
     defb      86,        71,     1,     2,         2,        75,        86       ; Mazmorra 4

     defb      86,        59,     1,     2,         2,        55,        86       ; Mazmorra 4
     defb      86,        56,     1,     2,         2,        60,        86       ; Mazmorra 4

     defb      70,        59,     1,     2,         2,        55,        70       ; Mazmorra 4
     defb      70,        56,     1,     2,         2,        60,        70       ; Mazmorra 4

     defb      63,        59,     1,     2,         2,        55,        63       ; Mazmorra 4
     defb      63,        56,     1,     2,         2,        60,        63       ; Mazmorra 4

     defb      65,        69,     2,     1,         2,        69,        69       ; Mazmorra 4
     defb      69,        69,     2,     1,         2,        69,        65       ; Mazmorra 4

     defb      36,         0,     1,     2,         2,       $ff,        40      ; Malo 1
     defb      61,        16,     2,     1,         2,       $ff,        41      ; Malo 2
     defb       7,        56,     1,     2,         2,       $ff,        42      ; Malo 3
     defb      61,        46,     2,     1,         2,       $ff,        43      ; Malo 4

     defb $ff
