MAP4_DOORS:
    ;     Origen X, Origen Y, Off X, Off Y, Mapa dest, Y destino, X Destino
    ;                                                        $ff   Escena
     defb        0,       14,     2,     2,         0,       $ff,        61       ; Mapa 1 (0): Retorno al pueblo
     defb        0,       15,     2,     2,         0,       $ff,        61       ; Mapa 1 (0): Retorno al pueblo
     defb        0,       16,     2,     2,         0,       $ff,        61       ; Mapa 1 (0): Retorno al pueblo


     ;defb        0,       14,     2,     2,         0,        26,        61       ; Mapa 1 (0): Retorno al pueblo
     ;defb        0,       15,     2,     2,         0,        27,        61       ; Mapa 1 (0): Retorno al pueblo
     ;defb        0,       16,     2,     2,         0,        28,        61       ; Mapa 1 (0): Retorno al pueblo

     defb       77,       66,     1,     2,         3,        66,        57       ; Salida casa Meiga
     defb       57,       65,     1,     2,         3,       $ff,        19       ; Entrada a casa de la Meiga

     defb       54,      $ff,     2,     2,         3,        42,        84       ; Entrada (1) a pre-subida
     defb       55,      $ff,     2,     2,         3,        42,        85       ; Entrada (2) a pre-subida

     defb       84,       43,     2,     2,         3,         0,        54       ; Salida (1) de pre-subida
     defb       85,       43,     2,     2,         3,         0,        55       ; Salida (1) de pre-subida


     defb       42,       14,     1,  16+2,         3,       $ff,        62       ; Entrada ermita
;     defb       42,       14,     1,     2,         3,        51,        77       ; Entrada ermita
     defb       77,       52,     1,     2,         3,       $ff,        26       ; Salida ermita
     defb       77,       52,     1,  28+2,         3,        15,        42       ; Salida ermita

     defb       78,      $ff,     2,     2,         4,       $ff,        32       ; Subida al monte 1
     defb       78,      $ff,     2,     2,         4,        88,        24       ; Subida al monte 1
     defb       79,      $ff,     2,     2,         4,       $ff,        32       ; Subida al monte 2
     defb       79,      $ff,     2,     2,         4,        88,        25       ; Subida al monte 2

     defb       12,        6,     0,     2,         2,       $ff,        31       ; Cueva 1
     defb       12,        6,     0,     2,         2,        41,         2       ; Cueva 1
     defb       25,        5,     0,     2,         2,       $ff,        31       ; Cueva 2
     defb       25,        5,     0,     2,         2,        41,        67       ; Cueva 2
     defb       33,       17,     0,     2,         2,       $ff,        31       ; Cueva 3
     defb       33,       17,     0,     2,         2,        87,         3       ; Cueva 3
     defb       11,       28,     0,     2,         2,       $ff,        31       ; Cueva 4
     defb       11,       28,     0,     2,         2,        87,        48       ; Cueva 4


     defb $ff
