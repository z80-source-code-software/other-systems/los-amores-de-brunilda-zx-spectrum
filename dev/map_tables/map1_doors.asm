MAP1_DOORS:
    ;     Origen X, Origen Y, Off X, Off Y, Mapa dest, Y destino, X Destino, Y Camara, X Camara
    ; En el offset Y solo coge los dos bits de menos peso. Los dem�s se usan para indicar una canci�n a sonar al pasar por esa "puerta"
    ; Hay que desplazar dos bits a la izquierda el n�mero de la canci�n

     defb        1,       15,     1,     2,         0,       $ff,        11       ; Puerta cerrada siempre
     defb        2,       27,     1,     2,         0,       $ff,        11       ; Puerta cerrada siempre
     defb        8,        5,     1,     2,         0,       $ff,        11       ; Puerta cerrada siempre
     defb       17,       23,     1,     2,         0,       $ff,        11       ; Puerta cerrada siempre

     defb       11,       25,     1,     2,         0,       $ff,        24       ; Casa 1 entrada
     defb       11,       25,     1,     2,         0,        84,        44       ; Casa 1 entrada
     defb       44,       85,     1,     2,         0,        26,        11       ; Casa 1 salida

     defb       49,       82,     1,     2,         0,        75,        44       ; Casa 1 habitacion
     defb       44,       76,     1,     2,         0,        83,        49       ; Casa 1 salida habitacion

     defb       31,        5,     1,     2,         0,       $ff,        23       ; Posada entrada
     defb       31,        5,     1,  20+2,         0,        86,        26       ; Posada entrada + Cancion 5
     defb       26,       87,     1,  44+2,         0,         6,        31       ; Posada salida

     defb        3,        5,     1,     2,         0,       $ff,        24       ; Cerrajería entrada
     defb        3,        5,     1,     2,         0,        75,        28       ; Cerrajería entrada
     defb       28,       76,     1,     2,         0,         6,         3       ; Cerrajería salida

     defb       18,        1,     2,     2,         0,       $ff,        25       ; Iglesia entrada 1
     defb       18,        1,     2,  16+2,         0,        86,         8       ; Iglesia entrada 1
     defb        8,       87,     2,     2,         0,       $ff,        26       ; Iglesia salida 1
     defb        8,       87,     2,  44+2,         0,         2,        18       ; Iglesia salida 1

     defb       19,        1,     0,     2,         0,       $ff,        25       ; Iglesia entrada 2
     defb       19,        1,     0,  16+2,         0,        86,         9       ; Iglesia entrada 2
     defb        9,       87,     0,     2,         0,       $ff,        26       ; Iglesia salida 1
     defb        9,       87,     0,  44+2,         0,         2,        19       ; Iglesia salida 2

     defb       15,       66,     2,     2,         0,       $ff,        24       ; Sacristía entrada 1
     defb       15,       66,     2,     2,         0,        64,        43       ; Sacristía entrada 1
     defb       43,       65,     2,     2,         0,        67,        15       ; Sacristía salida 2

     defb       16,       66,     0,     2,         0,       $ff,        24       ; Sacristía entrada 2
     defb       16,       66,     0,     2,         0,        64,        44       ; Sacristía entrada 2
     defb       44,       65,     0,     2,         0,        67,        16       ; Sacristía salida 2

     defb        1,       66,     2,     2,         0,       $ff,        46       ; Cementerio entrada 1
     defb        1,       66,     2,  40+2,         0,        56,        18       ; Cementerio entrada 1
     defb       18,       57,     2,     2,         0,       $ff,        27       ; Cementerio salida 1
     defb       18,       57,     2,  16+2,         0,        67,         1       ; Cementerio salida 1

     defb        2,       66,     0,     2,         0,       $ff,        46       ; Cementerio entrada 2
     defb        2,       66,     0,  40+2,         0,        56,        19       ; Cementerio entrada 2
     defb       19,       57,     0,     2,         0,       $ff,        27       ; Cementerio salida 2
     defb       19,       57,     0,  16+2,         0,        67,         2       ; Cementerio salida 2

     defb       16,       46,     2,     2,         0,       $ff,        27       ; Pante�n entrada 1
     defb       16,       46,     2,     2,         0,        64,        30       ; Panteón entrada 1
     defb       30,       65,     2,     2,         0,       $ff,        26       ; Pante�n salida 1
     defb       30,       65,     2,     2,         0,        47,        16       ; Panteón salida 1

     defb       17,       46,     0,     2,         0,       $ff,        27       ; Pante�n entrada 1
     defb       17,       46,     0,     2,         0,        64,        31       ; Panteón entrada 2
     defb       31,       65,     0,     2,         0,       $ff,        26       ; Pante�n salida 2
     defb       31,       65,     0,     2,         0,        47,        17       ; Panteón salida 2
     
     defb       16,       54,     2,     2,         0,       $ff,        28       ; Entrada cementerio
     defb       17,       54,     2,     2,         0,       $ff,        28       ; Entrada cementerio

     defb       61,        6,     2,     2,         1,         7,         0       ; Mapa 2 (1): Casa del boticario
     defb       61,        7,     2,     2,         1,         8,         0       ; Mapa 2 (1): Casa del boticario
     defb       61,        8,     2,     2,         1,         9,         0       ; Mapa 2 (1): Casa del boticario

     defb       61,       26,     2,     2,         3,        14,         0       ; Mapa 4 (3): Bosque de la Meiga
     defb       61,       27,     2,     2,         3,        15,         0       ; Mapa 4 (3): Bosque de la Meiga
     defb       61,       28,     2,     2,         3,        16,         0       ; Mapa 4 (3): Bosque de la Meiga

     defb $ff
