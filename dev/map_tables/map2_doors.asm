MAP2_DOORS:
    ;     Origen X, Origen Y, Off X, Off Y, Mapa dest, Y destino, X Destino

     defb        0,        7,     2,     2,         0,         6,        60     ; Retorno al mapa 1 (0)
     defb        0,        8,     2,     2,         0,         7,        60     ; Retorno al mapa 1 (0)
     defb        0,        9,     2,     2,         0,         8,        60     ; Retorno al mapa 1 (0)

     defb        7,        2,     2,     2,         1,       $ff,        27      ; Entrada a la casa del Boticario
     defb        7,        2,     2,     2,         1,        16,        44      ; Entrada a la casa del Boticario
     defb       44,       17,     2,     2,         1,       $ff,        12      ; Salida casa boticario
     defb       44,       17,     2,     2,         1,         3,         7      ; Salida casa boticario
     defb        8,        2,     0,     2,         1,       $ff,        27      ; Entrada a la casa del Boticario
     defb        8,        2,     0,     2,         1,        16,        45      ; Entrada a la casa del Boticario
     defb       45,       17,     0,     2,         1,       $ff,        12      ; ; Salida casa boticario
     defb       45,       17,     0,     2,         1,         3,         8      ; ; Salida casa boticario

     defb       38,       14,     2,     1,         1,        17,        13      ; Recibidor -> Despacho
     defb       13,       17,     2,     1,         1,        14,        38      ; Despacho -> Recibidor

     defb       51,       14,     2,     1,         1,       $ff,        47      ; Recibidor -> Cocina
     defb       51,       14,     2,     1,         1,        34,        24      ; Recibidor -> Cocina
     defb       24,       34,     2,     1,         1,        14,        51      ; Cocina -> Recibidor

     defb       44,        1,     2,     2,         1,        72,        51      ; Recibidor -> Comedor
     defb       51,       73,     2,     2,         1,         2,        44      ; Comedor -> Recibidor
     defb       45,        1,     0,     2,         1,        72,        52      ; Recibidor -> Comedor
     defb       52,       73,     0,     2,         1,         2,        45      ; Comedor -> Recibidor

     defb        2,       20,     2,     1,         1,        27,        18      ; Despacho -> Laboratorio
     defb       18,       27,     2,     1,         1,        20,         2      ; Laboratorio -> Despacho

     defb        2,       15,     2,     1,         1,        $ff,       72      ;66,        19      ; Despacho -> Biblioteca
     defb       19,       66,     2,     1,         1,        15,         2      ; Biblioteca -> Despacho

     defb       15,        1,     2,     1,         1,        78,        42      ; Jardin -> Jardin lateral
     defb       42,       78,     2,     1,         1,         1,        15      ; Jardin lateral -> Jardin
     defb       15,        2,     2,     2,         1,        79,        42      ; Jardin -> Jardin lateral
     defb       42,       79,     2,     2,         1,         2,        15      ; Jardin lateral -> Jardin
     defb       15,        3,     2,     0,         1,        80,        42      ; Jardin -> Jardin lateral
     defb       42,       80,     2,     0,         1,         3,        15      ; Jardin lateral -> Jardin

     defb       32,        3,     2,     1,         1,       $ff,        13      ; Trigger encuentro cama
;     defb       32,        3,     2,     1,         1,         6,        57,         0,      55   ; Distribuidor -> Hab 4
     defb       57,        6,     2,     1,         1,         3,        32      ; Hab 4 -> Distribuidor

     defb       30,        1,     1,     2,         1,       $ff,        24      ; Distribuidor -> Hab 3 (Ama de llaves)
     defb       30,        1,     1,     2,         1,       $ff,        65; 18,        64      ; Distribuidor -> Hab 3
     defb       64,       19,     1,     2,         1,         2,        30      ; Hab 3 -> Distribuidor

     defb       25,        1,     1,     2,         1,        29,        56      ; Distribuidor -> Hab 2
     defb       56,       30,     1,     2,         1,       $ff,        10      ; Trigger de escena de la cena
;     defb       56,       30,     1,     2,         1,         2,        25,         0,      17   ; Hab 2 -> Distribuidor

     defb       20,        1,     1,     2,         1,       $ff,        24      ; Distribuidor -> Hab 1
     defb       20,        1,     1,     2,         1,        40,        64      ; Distribuidor -> Hab 1
     defb       64,       41,     1,     2,         1,         2,        20      ; Hab 1 -> Distribuidor

     defb       19,        3,     2,     1,         1,       $ff,        48      ; Puerta pasillo cerrada
     defb       49,       25,     2,     1,         1,         3,        20      ; Salida pasillo ala oeste

     defb       41,       24,     1,     2,         1,        $ff,       71; 52,        11      ; Habitación Brunilda
     defb       11,       53,     1,     2,         1,        25,        41      ; Salida ^

     defb       34,       26,     1,     2,         1,         2,        82      ; Habitación servicio 1
     defb       82,        1,     1,     2,         1,        25,        34      ; Salida ^

     defb       24,       25,     2,     1,         1,        68,        76      ; Habitación servicio 2
     defb       76,       68,     2,     1,         1,        25,        24

     defb       17,       58,     0,   4+2,         1,       $ff,        52      ; ENtrada pasadizo
     defb        4,       88,     0,     2,         1,       $ff,        53      ; Salida pasadizo
     


     defb $ff
