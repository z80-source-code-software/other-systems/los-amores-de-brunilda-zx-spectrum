MAP1_SIGNS:
;            X,   Y, Mess, Seq

     defb   20,  28,    0, $ff
     defb    5,   6,    1, $ff
     defb   33,   6,    2, $ff
     defb   13,  26,    3, $ff
     defb    4,  16,    3, $ff
     defb   10,   6,    3, $ff
     defb   38,  26,    4, $ff
     defb   38,  14,    4, $ff
     defb   36,  10,    0, $ff
     defb   26,  74,  $ff,  17
     defb   $ff
