MAP5_DOORS:
    ;     Origen X, Origen Y, Off X, Off Y, Mapa dest, Y destino, X Destino
    ;                                                        $ff   Escena
     defb       24,        89,    2,     2,         3,         0,        78       ; Bajada 1
     defb       25,        89,    2,     2,         3,         0,        79       ; Bajada 2
     
     defb       31,        64,    1,     2,         4,        $ff,       31       ; Cueva 1 entrada
     defb       31,        64,    1,     2,         4,         86,       45       ; Cueva 1 entrada
     defb       45,        87,    1,     2,         4,        $ff,       27       ; Cueva 1 salida
     defb       45,        87,    1,     2,         4,         65,       31       ; Cueva 1 salida
     
     defb        3,        45,    1,     2,         4,        $ff,       31       ; Cueva 2 entrada
     defb        3,        45,    1,     2,         4,         71,       49       ; Cueva 2 entrada
     defb       49,        72,    1,     2,         4,        $ff,       27       ; Cueva 2 salida
     defb       49,        72,    1,     2,         4,         46,        3       ; Cueva 2 salida
     
     defb       33,        33,    1,     2,         4,        $ff,       31       ; Cueva 3 entrada
     defb       33,        33,    1,     2,         4,         53,       40       ; Cueva 3 entrada
     defb       40,        54,    1,     2,         4,        $ff,       27       ; Cueva 3 salida
     defb       40,        54,    1,     2,         4,         34,       33       ; Cueva 3 salida
     
     defb        8,        31,    1,     2,         4,        $ff,       31       ; Cueva 4 entrada
     defb        8,        31,    1,     2,         4,         36,       40       ; Cueva 4 entrada
     defb       40,        37,    1,     2,         4,        $ff,       27       ; Cueva 4 salida
     defb       40,        37,    1,     2,         4,         32,        8       ; Cueva 4 salida
     
     defb       30,         1,    1,     2,         4,        $ff,       31       ; Cueva 5 entrada
     defb       30,         1,    1,     2,         4,         17,       45       ; Cueva 5 entrada
     defb       45,        18,    1,     2,         4,        $ff,       27       ; Cueva 5 salida
     defb       45,        18,    1,     2,         4,          2,       30       ; Cueva 5 salida

     defb $ff
