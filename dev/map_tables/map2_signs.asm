MAP2_SIGNS:
;            X,   Y, Mess, Seq

     defb   58,  17,  $ff,  16
     defb   58,  18,  $ff,  16
     
     defb   15,  59,  $ff,  49
     defb   16,  59,  $ff,  49
     defb   17,  59,  $ff,  49
     defb   18,  59,  $ff,  49
     
     defb    3,  61,  $ff,  50
     defb    4,  61,  $ff,  50
     defb    5,  61,  $ff,  50
     defb    6,  61,  $ff,  50

     defb    9,  61,  $ff,  66
     defb   10,  61,  $ff,  66
     defb   11,  61,  $ff,  66
     defb   12,  61,  $ff,  66

     defb    3,  64,  $ff,  67
     defb    4,  64,  $ff,  67
     defb    5,  64,  $ff,  67
     defb    6,  64,  $ff,  67

     defb    9,  64,  $ff,  68
     defb   10,  64,  $ff,  68
     defb   11,  64,  $ff,  68
     defb   12,  64,  $ff,  68

     defb    3,  67,  $ff,  69
     defb    4,  67,  $ff,  69
     defb    5,  67,  $ff,  69
     defb    6,  67,  $ff,  69

     defb    9,  67,  $ff,  70
     defb   10,  67,  $ff,  70
     defb   11,  67,  $ff,  70
;     defb   12,  67,  $ff,  50

     defb    3,  71,  $ff,  66
     defb    4,  71,  $ff,  66
     defb    5,  71,  $ff,  66
     defb    6,  71,  $ff,  66

     defb    9,  71,  $ff,  50
     defb   10,  71,  $ff,  50
     defb   11,  71,  $ff,  50
     defb   12,  71,  $ff,  50


     defb   $ff
