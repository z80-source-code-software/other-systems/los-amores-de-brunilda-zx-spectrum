org $c000

MAP1:
     defb 62            		; Anchura del mapa
     incbin "../bin/map1.opt"		; Datos del mapa
MAP1_DOORS:
     incbin "../bin/map1_doors.opt"
MAP1_SIGNS:
     incbin "../bin/map1_signs.bin"
TILESET1:
     incbin "../bin/tileset1.opt"

MAP2:
     defb 90
     incbin "../bin/map2.opt"
MAP2_DOORS:
     incbin "../bin/map2_doors.opt"
MAP2_SIGNS:
     incbin "../bin/map2_signs.bin"
TILESET2:
     incbin "../bin/tileset2.opt"
     
MAP3:
     defb 90
     incbin "../bin/map3.opt"
MAP3_DOORS:
     incbin "../bin/map3_doors.opt"
MAP3_SIGNS:
     incbin "../bin/map3_signs.bin"
TILESET3:
     incbin "../bin/tileset3.opt"
MAP3END:
