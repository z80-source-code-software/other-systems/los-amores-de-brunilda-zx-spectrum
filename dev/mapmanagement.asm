; Tablas de mapas:
; Mapa 1 = Pueblo
; Mapa 2 = Casa boticario
; Mapa 3 = Cavernas
; Mapa 4 = Bosque y casa meiga
; Mapa 5 = Monte

TILESET_TABLE:
     defw TILESET1, TILESET2, TILESET3, TILESET4, TILESET5

DOORS_TABLE:
     defw MAP1_DOORS, MAP2_DOORS, MAP3_DOORS, MAP4_DOORS, MAP5_DOORS

CSIGNS_TABLE:
     defw MAP1_SIGNS, MAP2_SIGNS, MAP3_SIGNS, MAP4_SIGNS, MAP5_SIGNS

MAP_TABLE:
     defw MAP1, MAP2, MAP3, MAP4, MAP5
     
MEMPAGE_TABLE
     defb 3, 3, 3, 1, 1     

; Carga el mapa n� A (de la tabla MAP_TABLE) en el buffer de p�gina 0 con el TILESET que le corresponda
LOAD_MAP:

     ld (FLAGS+4), a

     ld hl, MEMPAGE_TABLE
     call GET_BYTE_TABLE
     ld b, a
     push bc

     ld a, (FLAGS+4)
     ld hl, TILESET_TABLE
     call GET_WORD_TABLE
     ld hl, MAP
     ex de, hl

     pop bc
     push bc
     call SETRAMBANK

     call DEEXO

;     ld b, 0
     call SETRAMBANK0

     ld hl, MAP
     ld de, SUPERTILE_0
     ld bc, 4096
     ldir

     ld hl, MAP
     ld de, MAP+1
     ld bc, 8099
     ld (hl), 0
     ldir
     
     pop bc
     call SETRAMBANK

     ld a, (FLAGS+4)
     ld hl, DOORS_TABLE
     call GET_WORD_TABLE
     ld hl, DOOR_TABLE
     ex de, hl
     call DEEXO


     ld a, (FLAGS+4)
     ld hl, CSIGNS_TABLE
     call GET_WORD_TABLE
     ld hl, SIGNS_TABLE
     ex de, hl
     ld bc, SIGNS_TABLE_SIZE
     ldir

     ld hl, MAP_TABLE
     call GET_WORD_TABLE
     ld hl, MAP
     ex de, hl
     
     ld a, (hl)
     ld (MAP_WIDTH), a
     inc hl
     call DEEXO

;     ld b, 0
     call SETRAMBANK0
     
     call AUTOCAM            ; �Necesario?

     call DEL_SPRITES
     ld b, 22
     jp RUNSCRIPT


LOCATE_SUPERTILE:

     ld hl, MAP
     ld a, l
     add a, c
     ld l, a
     ld a, 0
     adc a, h    ; Sumada la X sobre la base del buffer del mapa completo
     ld h, a

     ld a, b
     and a
     ret z

     ld a, (MAP_WIDTH)
     ld c, a

     LST_LOOP1:

     ld a, l
     add a, c
     ld l, a
     ld a, 0
     adc a, h
     ld h, a

     djnz LST_LOOP1
     
     ret

; Devuelve en A el supertile que hay en el mapa en las coordenadas BC (B = Y ; C = X)

WHATSUPERTILEAT:

     ld a, MAP_HEIGHT-1
     cp b
     ld a, 0
     ret c

     ld a, (MAP_WIDTH)
     cp c
     ld a, 0
     ret c
     ret z

     push bc
     push hl

     call LOCATE_SUPERTILE

     WHATSUPERTILEAT_HOOK0:
     ld a, (hl)
     pop hl
     pop bc
     ret

ANIMATE_TILES:
    ld a, (FLAGS+4)
    cp 1
    jp z, AT_BOTICARIO

    cp 3
    ret nz

    ld a, (CYCLES)
    bit 3, a
    ld a, 177
    jr z, AT_MEIGA_WRITE
    ld a, 151

    AT_MEIGA_WRITE:

     ; MAP + 57*88 + 77
    ld ($73e5), a
    ret

    AT_BOTICARIO:

    ld a, (CYCLES)
    bit 3, a
    ld a, 164
    jr z, AT_BOT_WRITE
    ld a, 122

    AT_BOT_WRITE:

    ld ($60e7), a
    ld ($6892), a
    ld ($6acf), a
    ld ($6ce6), a
    ld ($7652), a
    ret
