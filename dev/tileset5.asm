TILESET_5

org $e000

TILESET4_1:
     include "../gfx/asm/tileset05_01.asm"

org $e000 + 32*8
TILESET4_2:
     include "../gfx/asm/tileset05_02.asm"

org $e000 + 64*8
TILESET4_3:
     include "../gfx/asm/tileset05_03.asm"

org $e800
TILESET4_TILESET:
     include "../gfx/asm/tileset05_tileset.asm"

END TILESET_5
