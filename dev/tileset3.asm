TILESET_3

org $e000

TILESET3_1:
     include "../gfx/asm/tileset03_01.asm"

org $e000 + 32*8
TILESET3_2:
     include "../gfx/asm/tileset03_02.asm"

org $e000 + 64*8
TILESET3_3:
     include "../gfx/asm/tileset03_03.asm"

org $e800
TILESET3_TILESET:
     include "../gfx/asm/tileset03_tileset.asm"

END TILESET_3
