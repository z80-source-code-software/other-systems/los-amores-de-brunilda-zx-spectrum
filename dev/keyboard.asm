KEY_B           EQU       0
KEY_H           EQU       1
KEY_Y           EQU       2
KEY_6           EQU       3
KEY_5           EQU       4
KEY_T           EQU       5
KEY_G           EQU       6
KEY_V           EQU       7
KEY_N           EQU       8
KEY_J           EQU       9
KEY_U           EQU     #0A
KEY_7           EQU     #0B
KEY_4           EQU     #0C
KEY_R           EQU     #0D
KEY_F           EQU     #0E
KEY_C           EQU     #0F
KEY_M           EQU     #10
KEY_K           EQU     #11
KEY_I           EQU     #12
KEY_8           EQU     #13
KEY_3           EQU     #14
KEY_E           EQU     #15
KEY_D           EQU     #16
KEY_X           EQU     #17
KEY_SS          EQU     #18
KEY_L           EQU     #19
KEY_O           EQU     #1A
KEY_9           EQU     #1B
KEY_2           EQU     #1C
KEY_W           EQU     #1D
KEY_S           EQU     #1E
KEY_Z           EQU     #1F
KEY_SP          EQU     #20
KEY_EN          EQU     #21
KEY_P           EQU     #22
KEY_0           EQU     #23
KEY_1           EQU     #24
KEY_Q           EQU     #25
KEY_A           EQU     #26
KEY_CS          EQU     #27

STR_KEYREAD:

DEFB 'BHY65TGVNJU74RFCMKI83EDX','Z'+5,'LO92WSZ','Z'+6,'Z'+7,'P01QA', 'Z'+4

; WAITFORNOKEY: Espera a que no se est� pulsando ninguna tecla
WAITFORNOKEY:
    ld hl, WFNK_HOOK
    push hl
    ld hl, (CONTROL_PTR)
    jp (hl)
    
    WFNK_HOOK:
    ld a, (JOYSTICK)
    and a
    jr nz, WAITFORNOKEY


    xor a
    in a, (#FE)
    cpl
    and #1f
    jr nz, WAITFORNOKEY
    ret

; WAITFORKEY: Espera a que se pulse una tecla
WAITFORKEYR:
    ld a, $ff
WFKR_HOOK:
    ld (NEED_UPDATE), a
    call WAITFORNOKEY

WAITFORKEY:    
    ld a, (NEED_UPDATE)
    and a
    call nz, UPDATE_ALL

    ld hl, WFK_HOOK
    push hl
    ld hl, (CONTROL_PTR)
    jp (hl)
    
    WFK_HOOK:
    ld a, (JOYSTICK)
    bit 4, a
    ret nz
    
    xor a
    in a, (#FE)
    cpl
    and #1f
    jr z, WAITFORKEY
    ret

; WAITFORKEY: Espera a que se pulse una tecla
WAITFORKEY_NOUPDATE:
   xor a
   jr WFKR_HOOK

KFIND:
     call KFIND1
     ld a, d
     cp #ff
     ret z

     ld e, d
     ld d, 0
     ld hl, STR_KEYREAD
     add hl, de
     ret

KFIND1:
     ld de, #ff2f
     ld bc, #fefe

NXHALF:
     in a, (c)
     cpl
     and #1f
     jr z, NPRESS

     inc d
     ret nz

     ld h,a
     ld a,e
KLOOP:
     sub 8
     srl h
     jr nc, KLOOP

     ret nz

     ld d,a

NPRESS:
     dec e
     rlc b
     jr c, NXHALF

     cp a
     ret z


; KTEST1
; ENTRADA: A = Valor del scancode de la tecla
; SALIDA: NOCARRY si est� pulsada, CARRY si no lo est� (y BC = 0 si no lo est�)
; Se conservan HL y DE

KTEST1:
     ld c, a
     and 7
     inc a
     ld b,a
     srl c
     srl c
     srl c
     ld a,5
     sub c
     ld c,a
     ld a, #fe
HIFIND:
     rrca
     djnz HIFIND

     in a, (#fe)
NXKEY:
     rra
     dec c
     jr nz, NXKEY
     ret




CONTROLS_KEYB:
     ld hl, JOYSTICK
     ld (hl), 0

     ld iy, (KEYS_PTR)
     ld a, (iy+1)
     call KTEST1
     jr nc, JOY_RIGHT
     

     ld a, (iy)
     call KTEST1
     call nc, JOY_LEFT
     
     CK_UPANDDOWN:

     ld a, (iy+2)
     call KTEST1
     jr nc, JOY_UP

     ld a, (iy+3)
     call KTEST1
     call nc, JOY_DOWN
     
     CK_FIRE:

     ld a, (iy+4)
     call KTEST1
     call nc, JOY_FIRE

     ret

CONTROLS_KEMP:
     ld c, #1f
     in b, (c)

     ld hl, JOYSTICK
     ld (hl), b
     ret

JOY_UP:
;     ld hl, JOYSTICK
     set 3, (hl)
     jr CK_FIRE
JOY_DOWN:
;     ld hl, JOYSTICK
     set 2, (hl)
     ret
JOY_RIGHT:
;     ld hl, JOYSTICK
     set 0, (hl)
     jr CK_UPANDDOWN
JOY_LEFT:
;     ld hl, JOYSTICK
     set 1, (hl)
     ret
JOY_FIRE:
;     ld hl, JOYSTICK
     set 4, (hl)
     ret
