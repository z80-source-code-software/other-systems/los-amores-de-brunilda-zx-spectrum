; HEX_TO_TEXT: Convierte el valor de A en hexadecimal en texto en la direcci�n indicada en DE (big endian)
HEX_TO_TEXT:
     call HT_CONV

HT_CONV:
     ld hl, HEX_NUMBERS
     ld bc, 0
     rla
     rl c     
     rla
     rl c     
     rla
     rl c     
     rla
     rl c
     
	add hl, bc
	ex af, af'
	ld a, (hl)
	ld (de), a
	ex af, af'
	inc de
	ret

; TEXT_TO_HEX: Lee el valor en texto en HL y lo pone en A (big endian) 
TEXT_TO_HEX:
     ld e, 0
     call TH_CONV

TH_CONV:
	ex af, af'
	ld a, (hl)
	exx
	ld hl, HEX_NUMBERS+16
	ld bc, 17
	cpdr

	ld a, c
	exx

	rla
	rla
	rla
	rla

	rla
	rl e
	rla
	rl e
	rla
	rl e
	rla
	rl e

	ld a, e
	inc hl
	ret

	
	
	
	

; GEN_PWD: Genera la contrase�a
; Estructura de la contrase�a:
; 5 bytes
; +0 = Inventario
;	Bit 0      : Pala
; 	Bit 1 	   : Seta rara
;	Bit 2      : Seta normal
;	Bit 3      : Farol
;	Bit 4      : Crucifijo
; 	Bits 5 - 7 : Bitmap crucifijos
; +1 = Flag 0
; +2 = Flag 10
; +3 = Flag 3 + Flag 8
;	Bits 0 - 5 : Flag 3 (fe)
;	Bits 6 - 7 : Flag 8 (N� Crucifijos)
; +4 = Checksum

GEN_PWD:
	ld a, (FLAGS+13)
	and 14			; 0x0000 1110
        sra a
	ld c, a  

	ld a, O_CRUCIFIJO
	call GPWD_OBJ_CARR
	rl c

	ld a, O_FAROL
	call GPWD_OBJ_CARR
	rl c
  
	ld a, O_SETA_N
	call GPWD_OBJ_CARR
	rl c

	ld a, O_SETA_R
	call GPWD_OBJ_CARR
	rl c

	ld a, O_PALA
	call GPWD_OBJ_CARR
	rl c

	ld a, c
	exx
	ld hl, PWD_CHECKSUM	
	ld (hl), a
	exx
	ld de, PWD_PWD
	call HEX_TO_TEXT

	exx
	ld a, (FLAGS)
	add a, (hl)
	ld (hl), a
	ld a, (FLAGS+10)
	add a, (hl)
	ld (hl), a
	exx
	
	ld a, (FLAGS)
	call HEX_TO_TEXT
	ld a, (FLAGS+10)
	call HEX_TO_TEXT
	ld a, (FLAGS+3)
	ld b, a
	ld a, (FLAGS+8)
	rrca
	rrca
	or b
	push af
	call HEX_TO_TEXT
	pop af
	exx
	add a, (hl)
	ld (hl), a
	exx
	jp HEX_TO_TEXT
	
	; GPWD_OBJ_CARR: Devuelve CARRY si el objeto pasado en A est� en el inventario
	GPWD_OBJ_CARR:

	ld b, MAX_INVENTORY
	ld hl, INVENTORY
	GPWD_OC_LOOP:
	
	cp (hl)
	scf
	ret  z
	inc hl
	djnz GPWD_OC_LOOP
	and a
	ret


     


; Extrae en A el byte n� A de la tabla de bytes se�alada por HL
; Utiliza DE
GET_BYTE_TABLE:
      ld e, a
      ld d, 0
      add hl, de
      ld a, (hl)
      ret

; Extrae en DE el word n� A de la tabla de words se�alada por HL
; Utiliza DE
GET_WORD_TABLE:
      ld e, a
      ld d, 0
      add hl, de
      add hl, de
      ld e, (hl)
      inc hl
      ld d, (hl)
      ret

; SETRAMBANK0: Activa la p�gina de RAM 0
; Utiliza: AF y BC

SETRAMBANK0:
      ld b, 0

; SETRAMBANK: Activa la p�gina de RAM especificada en el registro B
; Entrada: B -> P�gina a activar
; Utiliza: AF y BC

SETRAMBANK:
;     di
     ld a, b
     ld (LAST_RAM_BANK), a
     ld  a, ($5B5C)
     and $F8
     or  b
     ld  bc, $7FFD
     ld  ($5B5C), a
     out (c), a
     ret


exo_mapbasebits EQU $5f64 ;:defs	156	;tables for bits, baseL, baseH
DEEXO:
include "d.asm"


RAND8:
	ld hl, RAND_BYTE
	ld a, r
	add a, (hl)
	ld h, 1
	add a, $fd
	ld l, a
	ld a, (hl)
	ld (RAND_BYTE), A
	ret
