; ADD_OBJECT
; A�ade un objeto tipo H a la lista en las coordenadas BC del mapa L
ADD_OBJECT:
     ld ix, OBJ_DATA-BYTES_OBJ
     ld de, BYTES_OBJ

     ADD_OBJECT_LOOP:
          add ix, de
          ld a, (ix)
          cp $ff
          jr nz, ADD_OBJECT_LOOP

     ld (ix), l
     ld (ix+1), b
     ld (ix+2), c
     ld (ix+3), h
     ret

; DEL_OBJECT: Borra el objeto pasado en IX, y desplaza la lista hacia atr�s a partir de ah� sustituyendo su hueco
; DEL_OBJECT:
;      ;ld e, ixl
;      ;ld d, ixh
;      push ix
;      pop de             ; M�s lento, pero nos ahorramos un byte frente a los LD's
;                          ; DE (Destino) = Objeto actual
;
;      ld hl, OBJ_DATA + BYTES_OBJ * OBJ_NUM
;      and a              ; Quita el carry
;      sbc hl, de
;      ld b, h
;      ld c, l             ; BC (Byte Counter) = �ltimo byte de la lista din�mica - Objeto actual (nos pasaremos en un objeto, copiando los siguientes bytes en los datos del �ltimo objeto)
;      
;      ld a, BYTES_OBJ
;      add a, e
;      ld l, a
;      ld a, 0
;      adc a, d
;      ld h, a                  ; HL = DE + BYTES_OBJ ; Origen = Siguiente Objeto
;      
;      ldir                ; Ya tenemos la lista desplazada: Ahora hay que llenar el �ltimo objeto como "vac�o" con $ff
;      
;      dec de
;      push de
;      pop hl
;      dec de
;      ld bc, BYTES_OBJ - 1
; 
;      lddr
;      
;      ret
     

;SHOW_OBJECTS: Muestra los objetos en el mapa
SHOW_OBJECTS:
     ld ix, OBJ_DATA

     exx
     ld b, OBJ_NUM

     SHOW_OBJECTS_LOOP:
     exx

     ld a, (ix+3)
     cp O_AGUA
     jr nc, SHOW_OBJECTS_SKIP

     ld a, (FLAGS+4)
     cp (ix)
     call z, SHOW_THIS_OBJECT

     SHOW_OBJECTS_SKIP:
     ld de, BYTES_OBJ
     add ix, de
     exx
     djnz SHOW_OBJECTS_LOOP

     ret

SHOW_THIS_OBJECT:
     ; Coordenadas en pantalla = 2 * (Cordenadas - C�mara) - Ofsfset C�mara

     ld hl, (CAMERA_Y)        ; 16

     ld a, (ix+1)             ; 19     A la coordenada ST Y
     sub l                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos (ST de 2 x 2 tiles)
     ld b, a                  ;  4
     ld a, (CAMERA_OFFSET_Y)
     neg
     add a, b                      ;   Le restas el Offset de C�mara
     ld b, a                       ;   Y lo pones en B

     ld a, (ix+2)             ; 19     A la coordenada ST X
     sub h                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos (ST de 2 x 2 tiles)
     ld c, a                  ;  4
     ld a, (CAMERA_OFFSET_X)
     neg
     add a, c                      ;   Le restas el Offset de C�mara
     ld c, a                       ;   Y lo pones en C
     
     ld a, (ix+3)
     
     call OBJ_GET_GFX_ADRESS
     
     call SHOWSPR_PRINT
     inc c
     call SHOWSPR_PRINT
     inc b
     dec c
     call SHOWSPR_PRINT
     inc c
     jp SHOWSPR_PRINT
     
;     ret
     
OBJ_GET_GFX_ADRESS:

     ld l, a
     ld h, 0
     add hl, hl
     add hl, hl
     add hl, hl
     add hl, hl
     add hl, hl
     ld de, OBJECTS
     add hl, de
     ex de, hl                     ; DE Apunta al inicio del gr�fico del objeto

     exx
     ld l, a
     ld h, 0
     add hl, hl
     add hl, hl
     ld de, OBJECTS+32*MAX_OBJ ; OBJECTS_ATTR
     add hl, de                    ; HL' apunta al inicio de los atributos del objeto
     exx
     ret

PRECHECK_IGO:
     ld b, (ix+1)
     ld c, (ix+2)
     ld a, (ix+3)
     add a, b
     ld h, a     ; En H llevamos la coordenada Y m�s su offset
     ld a, (ix+4)
     add a, c
     ld l, a     ; En L llevamos la coordenada X m�s su offset
     ret


CHECK_IF_GETTING_OBJECTS:

;     ld ix, PRINCIPAL_SPR
     ld a, (ix+0)
     and a
     ret nz      ; Solo el jugador

     ld ix, OBJ_DATA-BYTES_OBJ

     exx
     ld b, OBJ_NUM

     CIGO_LOOP:
     exx
     ld de, BYTES_OBJ
     add ix, de

     ld a, (FLAGS+4)
     cp (ix+0)
     jr nz, CIGO_NEXT

     ld a, (ix+1)                ; Primero comprobamos la vertical
     cp b
     jr z, CIGO_CHECKHOR

     cp h
     jr nz, CIGO_NEXT

     CIGO_CHECKHOR:

     ld a, (ix+3)
     ld (FLAGS+5), a          ; Tipo de objeto -> Flag 5
     ld de, SC18-1

     ld a, (ix+2)
     cp c
     ex de, hl
     jr z, CIGO_GET
;     jr z, GET_THIS_OBJECT

     cp e
     jr z, CIGO_GET
;     jr z, GET_THIS_OBJECT

     CIGO_NEXT:
     exx
     djnz CIGO_LOOP
     ld a, $ff
     ld (CAN_GET_ANYOBJECT), a
     ret
     
     CIGO_GET:
     ld a, (CAN_GET_ANYOBJECT)
     inc a
     cp O_AGUA-1 ; Para que no nos avise de que no podemos coger dos veces el agua de una misma poza la primera vez que pasamos
     ret nc

     ld a, (ix+3)
     ld (CAN_GET_ANYOBJECT), a
     
     ld (OBJ_REF_IX), ix
     ld a, (ix+3)
     cp O_AGUA
     jp nz, RUNSCRIPT_RUN
     call RUNSCRIPT_RUN
     ld ix, (OBJ_REF_IX)
     ld (ix+3), O_AGUA_F
     ld (ix), 4
     ret

MUSHROOMS_YCOORDS:
     defb $0b, $09, $21, $2a, $3c, $16, $01

GET_MUSHROOM:
     ld a, (ix+1)
     ld hl, MUSHROOMS_YCOORDS
     ld b, 1

     GMR_LOOP:
     cp (hl)
     jr z, GMR_HOOK

     sla b
     inc hl
     jr GMR_LOOP

     GMR_HOOK:
     ld a, (FLAGS+10)
     xor b
     ld (FLAGS+10), a
     jp ADD_INVENTORY

     ;ret

CRUX_YCOORDS:
     defb 58, 74, 64

GET_CRUX:
     ld a, (ix+1)
     ld hl, CRUX_YCOORDS
     ld b, 2

     GCR_LOOP:
     cp (hl)
     jr z, GCR_HOOK

     sla b
     inc hl
     jr GCR_LOOP

     GCR_HOOK:
     ld a, (FLAGS+13)
     xor b
     ld (FLAGS+13), a
     ret

     
; GET_THIS_OBJECT: Coge el objeto indicado en flag 5: Lo borra (si existe) de la lista de objetos "existentes", y lo a�ade al inventario
GET_THIS_OBJECT:
     ld ix, (OBJ_REF_IX)

     ld (ix), $ff
     
     ld a, (ix+3)
     cp O_SETA_N
     jr z, GET_MUSHROOM

     cp O_CRUCIFIJO
     call z, GET_CRUX

     jp ADD_INVENTORY ;call
     ; call DEL_OBJECT

;     jp DRAW_INVENTORY

; DRAW_INVENTORY: Dibuja todo el inventario
DRAW_INVENTORY:

     ld h, $50
     ld d, $50
     ld a, 7
     DI_CLEARCOUNTERS_LOOP:
     ld l, $cd
     ld e, $ce
     ld bc, 17
     ld (hl), 0
     ldir
     inc h
     inc d
     dec a
     jr nz, DI_CLEARCOUNTERS_LOOP

; Empezamos en 29, 20
; M�ximo 8 objetos
     ld hl, FLAGS+1
     bit 7, (hl)
     res 7, (hl)
     ret nz

     ld bc, $141d
     
     ld hl, INVENTORY
     ld d, MAX_INVENTORY

     INVENTORY_LOOP:    
     ld a, (hl)
     cp $ff
     ret z
     push hl
     call DRAW_OBJECT_INV
     pop hl
     inc hl     
     dec d
     jr nz, INVENTORY_LOOP
     ret


;DRAW_OBJECT_INV: Dibuja el objeto pasado en A en el hueco que corresponda en el marcador     
DRAW_OBJECT_INV:
     push bc
     push af

     call OBJ_GET_GFX_ADRESS
     call DOI_DRAW_CHAR
         
     inc c
     call DOI_DRAW_CHAR
     inc b
     dec c
     call DOI_DRAW_CHAR
     inc c
     call DOI_DRAW_CHAR
     
     pop af
     cp O_SETA_N
     jr c, DOI_EXIT

     cp O_JARRA
     jr nc, DOI_EXIT
     
     sub 10
     ld e, a

     ld d, c     
     ld b, 4
     call SETRAMBANK     

     ld b, 21
     ld c, d
     dec c
     
     ld hl, FLAGS
     ld d, 0
     add hl, de
     ld a, (hl)
     
     cp 2
     jr c, DOI_EXIT

     inc b
     inc c
     call OBJ_COUNTER
     
    
     DOI_EXIT:
;     ld b, 0
     call SETRAMBANK0
     pop bc
     dec c
     dec c
     ret
    
DOI_DRAW_CHAR:     
     call PRINT
     call ATT_LOCATE
     exx
     ld a, (hl)
     inc hl
     exx
     and 64+7   ; Desprecia el PAPER
     ld (hl), a
     ret


;ADD_INVENTORY: A�ade un objeto de tipo que haya en FLAG 5 a la lista de inventario (si no existe ya ning�n objeto tipo A, ya que hay objetos "m�ltiples" iguales)
ADD_INVENTORY:
     ld hl, INVENTORY
;     ld a, (ix+3)
     ld a, (FLAGS+5)
     ld bc, MAX_INVENTORY
     cpir
     ret z ; --> YA TEN�AMOS UN OBJETO COMO ESTE, luego no hay que a�adirlo (habr� que saltar a comprobar qu� objeto, y a�adir 1 al flag correspondiente)

     ex af, af'               ; Cambia A por A' para guardar ah� el tipo de objeto
     ld hl, INVENTORY
     ld a, $ff
     ld bc, MAX_INVENTORY
     
     cpir                     ; Busca ese $ff "to guapo" que indica que ah� hay un hueco disponible
                              
                              ; Habr�a que comprobar que no hayamos llegado al "tope" del inventario... pq si no la cagar�amos... : nz --> Hemos llegado al tope (no ha encontrado ning�n $FF en los BC bytes que ha buscado)
                              ; pero no lo pongo para ahorrar esa memoria y ese tiempo. �VALORAR LUEGO SI REALMENTE NO ES POSIBLE PASARSE DE ESE TOPE
     dec hl
     ex af, af'               ; Recuperamos tipo de objeto
     ld (hl), a
     ret

; DESTROY_OBJECT: Destruye un objeto "cogido", cuyo "tipo" se pasa en A
DESTROY_OBJECT:
     ld hl, INVENTORY
     ld bc, MAX_INVENTORY
     cpir
     ; ret nz  --> Vu�lvete si no has encontrado el objeto... pero se supone que si venimos aqu� ya sabemos que S� ten�amos el objeto

     ld d, h
     ld e, l
     dec de          ; Ahora HL = Siguiente objeto y DE = Objeto a borrar.
                     ; BC sale del CPIR con el n�mero de bytes de la lista que nos "quedan" desde el objeto a borrar
     ldir
     ld a, $ff
     ld (de), a     ; Ponemos un $ff en el �ltimo lugar, que ese hueco est� vac�o ahora ya s� o s�

     ret
