PREPARE_FIGHT:
     call FADE_OUT
     call WP_STOP_SOUND


     ld b, 5
     ld hl, FIGHT_MOVEMENT_SORT
     xor a
     PF_FMS:
     ld (hl), a
     inc a
     inc hl
     djnz PF_FMS

     ld a, (FLAGS+11)
     cp 5
     call nc, FIGHT_REORDER

     ld b, 1
     call SETRAMBANK

     ld hl, FIGHT_GRAPHICS
     ld de, MAP
     call DEEXO

     ld hl, FIGHT_SCR
     ld de, $4000
     call DEEXO

;     ld b, 0
     call SETRAMBANK0

     ld hl, FIGHT_SPEED
     ld a, (FLAGS+11)
     bit 2, a
     jr z, FL_SLOW
     inc a
     ld (hl), 16
     jr FL_SLOW2
     FL_SLOW:
     ld (hl), 32
     FL_SLOW2:
     and 3
     add a, 2
     ld (FIGHT_MAXSORTSOFMOVEMENT), a


     ld a, MAX_FIGHT_ENERGY
     ld (FIGHT_ENERGY_MONSTER), a

     ld hl, FLAGS+11
     ld (FIGHT_ENERGY_ME), a
     
     ld b, 0
     ld ix, FIGHT_SCANTABLE
     ld c, 11

     CREATE_SCANTABLE_LOOP:

     call DFL_CALLER
     ld (ix+0), l
     ld (ix+1), h
     inc ix
     inc ix
     inc b
     ld a, b
     cp 193
     jr nz, CREATE_SCANTABLE_LOOP

     PREPARE_FIGHT_AFTERMAGIC:

     ld hl, FIGHT_SKULLSLEVEL
     xor a
     ld (hl), a
     dec hl
     ld (hl), a
     
     call SHOW_PERGAMINO

     call DRAW_BOSSx2
;     ld hl, FIGHT_OFFSET
;     jp FIGHT_NEWMOVE


FIGHT_NEWMOVE:
     ld hl, FIGHT_OFFSET
     ld a, (FIGHT_SPEED)
     ld (hl), a    ; OFFSET

     ld ix, FIGHT_MOVEMENTS - FIGHT_MOVEMENT_BYTES
     ld de, FIGHT_MOVEMENT_BYTES
     ld a, $ff
     ld b, FIGHT_MAXMOVEMENTS

     FNM_SEARCH:
     add ix, de
     cp (ix)
     jr z, FNM_FOUND
     djnz FNM_SEARCH
     ret
     
     FNM_FOUND:

     call RAND8
     and 7
     ld (ix), a
     cp 5
     ret nc

     ld (ix+1), -16  ; Coordenada Y

     ld e, a
     ld d, 0
     ld hl, FIGHT_MOVEMENT_SORT
     add hl, de
     ld a, (hl)
     add a, a
     ld (ix+2), a   ; Coordenada X
     ret

FIGHT_DRAWMOVE:
     ld iy, FIGHT_SCANTABLE
     ld e, (ix+1)                  ; Coordenada Y
     ld d, 0
     add iy, de
     add iy, de                    ; Son 2 bytes por coordenada
     
;     ld h, 0
     ld h, d
     ld l, (ix)
     add hl, hl ; x2
     add hl, hl ; x4
     add hl, hl ; x8
     add hl, hl ; x16
     add hl, hl ; x32
     ld bc, ARROWS
     add hl, bc


     ld c, 15
     ld b, e
     FDM_DRAW_LOOP:

     ld a, b
     cp 152
     jr nc, FDM_SKIP_DRAW

     ld e, (iy)
     ld d, (iy+1)
     ld a, (ix+2)                  ; Coordenada X
     add a, e
     ld e, a                       ; Ahora DE contiene la direcci�n donde empezar a dibujar

     ldi
     ldi

     inc iy
     inc iy

     inc b

     inc c
     FDM_DL_CHECK:
     jr nz, FDM_DRAW_LOOP

     ret


     FDM_SKIP_DRAW:
     inc b
     
     ld iy, FIGHT_SCANTABLE
     ld e, b                      ; Coordenada Y
     ld d, 0
     add iy, de
     add iy, de                    ; Son 2 bytes por coordenada

     inc hl
     inc hl

     dec c
     jr FDM_DL_CHECK


; FIGHT_UPDATE_POSITIONS
; Actualiza las coordenadas de cada "ficha", y las dibuja en las coordenadas nuevas
; Si "toca", inserta ficha nueva
FIGHT_UPDATE_POSITIONS:
     ld ix, FIGHT_MOVEMENTS - FIGHT_MOVEMENT_BYTES
     ld b, FIGHT_MAXMOVEMENTS
     
     FUP_LOOP:
     exx
     
     ld de, FIGHT_MOVEMENT_BYTES
     add ix, de

     inc (ix + 1) ; Coordenada Y

     ld a, (ix)
     cp 5
     jr nc, FUP_EMPTYSPACE

     ld a, (ix + 1)
     cp 255-16
     jr nc, FUP_DRAW
     
     cp 152
     jr c, FUP_DRAW
     
     ld hl, FIGHT_ETU_ME
     ld (hl), 16
     call DRAW_LITTLE_STUFF
     jr FUP_EMPTYSPACE
     
     
     FUP_DRAW:    
     call FIGHT_DRAWMOVE
     jr FUP_NEXT
     

     FUP_EMPTYSPACE:
     ld (ix), $ff

     FUP_NEXT:
     exx
     djnz FUP_LOOP

     ld hl, FIGHT_OFFSET
     dec (hl)
     jp z, FIGHT_NEWMOVE         ; Valorar insertar el nuevo movimiento en el hueco de la ficha que sale por abajo directamente

     ret
     





INITIAL_CONVERSATION:

     call BOSS_SAYS
     ld e, 189
     call INIT_CONV_SAYS

     xor a
     call SOMEONE_SAYS
     ld e, 190
     call INIT_CONV_SAYS

     call BOSS_SAYS
     ld e, 191
     call INIT_CONV_SAYS

     INITCONV_HOOK:

     call BOSS_SAYS
     ld e, 192
     ;jp INIT_CONV_SAYS

     INIT_CONV_SAYS:
     ld bc, $1507
     call MESS_CALLER
     ld b, 75
     ICS_LOOP:
     halt
     djnz ICS_LOOP
     ret


CS_STARSANDSKULLS:
     ld ix, FIGHT_SCANTABLE; - 2
;     dec ix

     ld a, 31

     CSCD_L2:
     ex af, af'
     ld a, 18
     add a, (ix+0)
     ld e, a
     ld d, (ix+1)

     ld a, 18
     add a, (ix+2)
     ld l, a
     ld h, (ix+3)

     REPT 3
     ldd
     ENDM

     ld a, -22;-7
     add a, e;(ix+0)
     ld e, a
;     ld d, (ix+1)

     ld a, -22;-7
     add a, l;(ix+2)
     ld l, a
;     ld h, (ix+3)

     REPT 3
     ldd
     ENDM

     inc ix
     inc ix
     ex af, af'
     dec a
     jp nz, CSCD_L2
     ret

FIGHT_END:
     call WP_STOP_SOUND
     call CLEAR_TRACK
     
     
     call BOSS_SAYS
     
     exx
     ld hl, FIGHT_WAYOFSCROLL
     ld (hl), -1
     exx

     ld e, 188
     ld hl, FLAGS+12
     res 7, (hl)

     ld a, (FIGHT_ENERGY_MONSTER)
     and a
     jr nz, FEE
     
     exx
     ld (hl), 0
     exx

     dec e
     set 7, (hl)
     ld a, (hl)
     dec hl
     inc (hl)
     ld a, (hl)

     cp 7
     jr nz, FEE

     ld e, 239

     FEE:
     ld d, 6
     push de
     call FM_PAINTINRED
     ld e, 1
     call WP_PLAY_SFX
     pop de
     FEE_SHAKE:
     push de
     xor a
     call SHAKE_BIGSPRITE
     ld a, 7
     call SHAKE_BIGSPRITE
     pop de
     dec d
     jr nz, FEE_SHAKE
     
     call INIT_CONV_SAYS
     call WAITFORKEY_NOUPDATE
     call FADE_OUT

     ld sp, (PATH_TO_FOLLOW)

     ret


FIGHT_SHOWENERGY:
     ld de, $f920

     ld a, (FIGHT_ENERGY_MONSTER)
     cp MAX_FIGHT_ENERGY
     jr z, FSE2

;     srl a
;     srl a
     rra
     rra
     and 63

     ld b, a
;     srl a ; /2
;     srl a ; /4
;     srl a ; /8
     rra
     rra
     rra
     and 31

     ld hl, $4682
     add a, l
     ld l, a

     ld a, b
     and 7
     jr z, FSE1_DRAW

     ld b, a

     FSE1P_LOOP
     scf
     rra
     djnz FSE1P_LOOP
     scf
     rra

     FSE1_DRAW:

     call FSE_DRAW


     FSE2:

     ld a, (FIGHT_ENERGY_ME)
     and a
     jp z, FIGHT_END
     cp MAX_FIGHT_ENERGY
     ret z

;     srl a
;     srl a
     rra
     rra
     and 63

     ld b, a
;     srl a
;     srl a
;     srl a
     rra
     rra
     rra
     and 31


     ld hl, $469d
     neg
     add a, l
     ld l, a

     ld a, b
     and 7
     jr z, FSE_DRAW

     ld b, a
     xor a

     FSE2P_LOOP
     scf
     rla
     djnz FSE2P_LOOP
     scf
     rla

     FSE_DRAW:

     ld (hl), a
     inc h
     ld (hl), a
     add hl, de
     ld (hl), a
     inc h
     ld (hl), a

     ret

LETS_FIGHT:
     ld (PATH_TO_FOLLOW), sp

     call PREPARE_FIGHT
     call INITIAL_CONVERSATION

     LF_HOOK:

        xor a

     ld hl, FIGHT_ETU_ME
;     ld (hl), 0
     ld (hl), a
     ld hl, FIGHT_ETU_MONSTER
;     ld (hl), 0
     ld (hl), a

     ld hl, FIGHT_MOVEMENTS
     ld de, FIGHT_MOVEMENTS+1
     ld bc, FIGHT_MAXMOVEMENTS * FIGHT_MOVEMENT_BYTES -1
;     ld bc, FIGHT_MAXMOVEMENTS * FIGHT_MOVEMENT_BYTES
;     dec bc

     ld (hl), $ff
     ldir

     ld a, (FLAGS+11)
     cp 4
     ld a, 3
     jr c, LF_PLAYSONG
     dec a;ld e , 3
     LF_PLAYSONG:

     call WP_PLAY_SONG
     REPT 23
     halt
     ENDM
     
;
     ;ld a, (FLAGS+11)
;
     ;REPT 24
     ;halt
     ;ENDM
;
     ;cp 4
     ;jr c, FIGHT_CYCLE
;
     ;REPT 10
     ;halt
     ;ENDM

     FIGHT_CYCLE:
     halt
     call FIGHT_UPDATE_POSITIONS

     call CS_STARSANDSKULLS

     call FIGHT_UPDATEENERGY
     call FIGHT_SHOWENERGY


     ld hl, FL_HOOK
     push hl
     ld hl, (CONTROL_PTR)
     jp (hl)

     FL_HOOK:

     ld a, (PLAYER_CANMOVE)
     and a
     jp nz, FC_AFTERMOVE


     ld a, (hl) ;(JOYSTICK)
     ld (PLAYER_CANMOVE), a
     ld c, 4
     
;     bit 0, a
;     jp nz, FIGHT_MOVE
     rrca
     jp c, FIGHT_MOVE

     dec c; ld b, 3
;     bit 1, a
;     jp nz, FIGHT_MOVE
     rrca
     jp c, FIGHT_MOVE

     dec c;ld b, 2
;     bit 4, a
     bit 2, a
     jp nz, FIGHT_MOVE

     dec c; ld b, 1
;     bit 3, a
     bit 1, a
     jp nz, FIGHT_MOVE

     dec c ; ld b, 0
;     bit 2, a
     rrca
     jp c, FIGHT_MOVE
;     jp nz, FIGHT_MOVE

     FC_AFTERMOVE:
     ld a, (hl) ;(JOYSTICK)
     ld (PLAYER_CANMOVE), a

     FIGHT_ENDCYCLE:

     ld a, PAPER_CYAN | BRIGHT
     call COLOR_HEAD

     jp FIGHT_CYCLE

FIGHT_UPDATEENERGY:
     xor a
     ld hl, FIGHT_ETU_MONSTER
     or (hl)
     jp z, FUE_ME
     dec (hl)
     ld hl, FIGHT_ENERGY_MONSTER
     dec (hl)
     jp z, FIGHT_END
     ld a, (hl)
     cp MAX_FIGHT_ENERGY / 2
     call z, LETS_REORDER

     FUE_ME:     
     xor a
     ld hl, FIGHT_ETU_ME
     or (hl)
     ret z


     dec (hl)
     ld hl, FIGHT_ENERGY_ME
     dec (hl)
     jp z, FIGHT_END
     ret


;-----------------------------------------------
     FMOV_FOUND:
     ld a, (ix + 1)
     sub 152-8-15;-16
     jr nc, FM_FOUND_P
     
     ld a, 152-8-15
     sub (ix+1)
     ;neg

     FM_FOUND_P:    ; Ahora tengo en A la desviaci�n en p�xeles del "p�xel �ptimo"
     
     cp 16
     ret nc
     

     ld hl, FIGHT_ETU_ME
     ld c, a
     add a, a
     add a, (hl)
     ld (hl), a
     dec hl
     ld a, 16
     sub c
     srl a
     ;add a, (hl)
     ld (hl), a


     ld iy, FIGHT_SCANTABLE
     ld e, (ix+1)                  ; Coordenada Y
     ld d, 0
     add iy, de
     add iy, de                    ; Son 2 bytes por coordenada

     ld c, 15
     ld b, e
     FM_ERASE_LOOP:

     ld a, b
     cp 152
     jr nc, FM_ERASE_HOOK

     ld e, (iy)
     ld d, (iy+1)
     ld a, (ix+2)                  ; Coordenada X
     add a, e
     ld e, a                       ; Ahora DE contiene la direcci�n donde empezar a dibujar

     xor a
     ld (de), a
     inc de
     ld (de), a

     inc iy
     inc iy

     inc b

     dec c
     jr nz, FM_ERASE_LOOP
     
     FM_ERASE_HOOK:
     ld (ix), 5

     jr FA_RET
     
     ;ret
FIGHT_MOVE:

     ld ix, FIGHT_MOVEMENTS - FIGHT_MOVEMENT_BYTES
     ld b, FIGHT_MAXMOVEMENTS
     ld a, c

     FMOV_LOOP:
     push af
     push bc
     ld de, FIGHT_MOVEMENT_BYTES
     add ix, de
     cp (ix)
     call z, FMOV_FOUND
     pop bc
     pop af
     djnz FMOV_LOOP

     ld hl, FIGHT_ETU_ME
     ld a, (hl)
     add a, 16
     ld (hl), a

     FA_RET:

     ld a, INK_RED | PAPER_YELLOW | BRIGHT
     call COLOR_HEAD

;     call DRAW_LITTLE_STUFF
;     jp FIGHT_CYCLE

     ld hl, FIGHT_CYCLE
     push hl


DRAW_LITTLE_STUFF:
     ld a, (FIGHT_ETU_MONSTER)
;     srl a
     srl a
     call z, WORST_MOVEMENT
     cp 4
     call z, PERFECT_MOVEMENT

     ;cp 4
     ;call z, ERASE_BIGSKULLS
     cp 2
     call c, ERASE_BIGSTARS
     

     push af
     ld hl, STARS - 24
     ld e, $7b
     call DRAWLITTLE_SEARCH
     pop af

     ld hl, STARS + 72
     ld e, $62

DRAWLITTLE_SEARCH:
     ld bc, 24
     add hl, bc
     dec a
     jp nz, DRAWLITTLE_SEARCH

     DRAWLITTLE_DRAW:
     ld a, e
     ld d, $40

     REPT 7
     ldi
     ldi
     ldi
     inc d
     ld e, a
     ENDM
     ret

WORST_MOVEMENT:
     call WMHOOK

     WMHOOK:

     ld hl, FIGHT_SKULLSLEVEL
     inc (hl)
     ld a, (hl)
     ld c, a
     cp 7
     ret z
     cp 8
     jp z, FIGHT_MONSTERMAGIC

     ld hl, $58c1
     ld d, 0
     ld b, d
     ld e, a
     add hl, de

     ex de, hl

     ld hl, BIGSTARS_ATTR-2
;     ld b, 0
     add hl, bc
     add hl, bc

     ldi
     ex de, hl
     ld bc, $1f
     add hl, bc
     ex de, hl
     ldi

     ld a, 1
     ret

PERFECT_MOVEMENT:

     ex af, af'

     call ERASE_BIGSKULLS

     ld hl, FIGHT_STARSLEVEL
     inc (hl)
     ld a, (hl)
     ld c, a
     cp 7
     jp z, FIGHT_DOMAGIC

     ld hl, $58d7
     ld d, 0
     ld b, d
     ld e, a
     add hl, de

     ex de, hl

     ld hl, BIGSTARS_ATTR-2
;     ld b, 0
     add hl, bc
     add hl, bc

     ldi
     ex de, hl
     ld bc, $1f
     add hl, bc
     ex de, hl
     ldi   

     ex af, af'
     ret


FIGHT_MONSTERMAGIC:
     call BOSS_SAYS
     ld e, 193
     call INIT_CONV_SAYS

     ld a, -1

     ld hl, MAGICSKULL_ATTR - 1
     ld de, $5949   ; 10, 23
     jr FIGHT_MAGIC

FIGHT_DOMAGIC:     
     xor a
     call SOMEONE_SAYS
     ld e, 194
     call INIT_CONV_SAYS

     ld a, 1

     ld hl, MAGICSTAR_ATTR - 1
     ld de, $5957   ; 10, 23
;     jr FIGHT_MAGIC


FIGHT_MAGIC:
     ld (FIGHT_WAYOFSCROLL), a
     ld (FIGHT_ETU_ME), a
     neg
     ld (FIGHT_ETU_MONSTER), a

     ld (FIGHT_ATTRPTR), hl
     ld (FIGHT_COORDPTR), de


     call WP_STOP_SOUND

     call CLEAR_TRACK

;     call DM_COLUMN

     ld e, 2
     call WP_PLAY_SFX

     ld a, 1

     FM_LOOP:
     halt

     push af
     ld de, (FIGHT_COORDPTR)
     call DM_SCROLL

     pop af
;     push af

     cp 11
     jr nc, FM_LOOP_SKIPNEWCOL

     push af

     ld hl, (FIGHT_ATTRPTR)
     add a, l
     ld l, a
     ld de, (FIGHT_COORDPTR)
     call DM_COLUMN
     pop af

     FM_LOOP_SKIPNEWCOL:

;     pop af
     inc a
     cp 25
     jp nz, FM_LOOP

     call FM_PAINTINRED

     ld e, 0
     call WP_PLAY_SFX

     ld b, 16
     FM_ADJUSTENERGY_LOOP:
     push bc

     ld hl, FIGHT_ENERGY_ME
     ld a, (hl)
     cp MAX_FIGHT_ENERGY
     jr z, FM_ADJ_MONSTER
     ld a, (FIGHT_ETU_ME)
     add a, (hl)
     ld (hl), a
     jp z, FIGHT_END

     FM_ADJ_MONSTER:
     ld hl, FIGHT_ENERGY_MONSTER
     ld a, (hl)
     cp MAX_FIGHT_ENERGY
     jr z, FM_ADJ_AFTERADJ
     ld a, (FIGHT_ETU_MONSTER)
     add a, (hl)
     ld (hl), a
     jp z, FIGHT_END


     FM_ADJ_AFTERADJ:

     call FIGHT_SHOWENERGY

     ;ld a, 0
     xor a
     call SHAKE_BIGSPRITE
     ld a, 7
     call SHAKE_BIGSPRITE
     pop bc
     djnz FM_ADJUSTENERGY_LOOP


     ld b, 50
     FM_PAUSE_LOOP:
     halt
     djnz FM_PAUSE_LOOP

     ld hl, FIGHT_ATTR
     ld de, $5800
     ld bc, 576
     ldir

     call PREPARE_FIGHT_AFTERMAGIC
     call INITCONV_HOOK
     jp LF_HOOK

FM_PAINTINRED:

     ld h, $59
     ld d, h

     ld l, $20

     ld a, (FIGHT_WAYOFSCROLL)
     bit 1, a
     ld a, 8
     jr z, FM_PAINTINRED_HOOK

     ld l, $37

     FM_PAINTINRED_HOOK:

     ld (FIGHT_OFFSET), a
     ld e, l
     inc e

     FM_PAINTINRED_LOOP:

     ex af, af'
     ld a, (FIGHT_OFFSET)
     ld c, a
     ld b, 0
     ex af, af'

     push hl
     push de

     ld (hl), 64 | 2
     ldir

     pop de
     pop hl

     ld bc, $20
     add hl, bc
     ex de, hl
     add hl, bc
     ex de, hl

     dec a
     jr nz, FM_PAINTINRED_LOOP
     ret

SHAKE_BIGSPRITE:
     ld ix, FIGHT_SCANTABLE + 72 + 72

     ex af, af'
     ld a, (FIGHT_WAYOFSCROLL)
     ld c, a
     ex af, af'
     bit 7, c
     jr z, SHBS_HOOK0

     xor 7

     SHBS_HOOK0:

     exx
     ld b, 64

     SHBS_LOOP:
     exx

     ld l, (ix)
     ld h, (ix+1)
     ld e, l
     ld d, h
     inc hl

     ; a tiene que ser 7 para ir alante, y 0 para ir atr�s
     and a
     jr z, SHBS_HOOK1

     ld b, 0
     ld c, a
     add hl, bc
     ex de, hl
     add hl, bc

     SHBS_HOOK1:
     ld bc, -11

     ex af, af'
     ;ld a, (FIGHT_WAYOFSCROLL)
     bit 7, a
     jr z, SHBS_HOOK2
     ld bc, 11

     SHBS_HOOK2:
     ex af, af'

     add hl, bc
     ex de, hl
     add hl, bc
     ex de, hl

     ld bc, 8
     exx
     ld hl, LDDIR+2
     ld (hl), $b0 ; LDIR
     and a
     jr z, LDDIR
     ld (hl), $b8 ; LDDR

     LDDIR:
     exx
     lddr ; o ldir ... depende
     ex de, hl
     ld (hl), 0

     inc ix
     inc ix

     exx
     djnz SHBS_LOOP

     halt
     ret


DM_COLUMN:
     exx
     ld b, 7

     DM_COLUMN_LOOP
     exx

     ldi
     ld bc, 9
     add hl, bc
     ex de, hl
;     ld bc, $1f
     ld c, $1f
     add hl, bc
     ex de, hl

     exx
     djnz DM_COLUMN_LOOP

     ret


  DM_SCROLL:
     ld hl, FIGHT_WAYOFSCROLL
     ld a, 14
     ld c, a
     ld b, (hl)
     bit 7, b
     jp z, DM_SCROLL_BEGIN

     neg

     DM_SCROLL_BEGIN:

     ld h, a
     sub e
     neg
     ld e, a
     ld a, h
     ld h, d
     ld a, e
     add a, b
     ld l, a
     ld a, c
     
;             ld c, $1f

     DM_SCROLL_LOOP1:

     push hl
     push de

;                ld b, 0
     ex af, af'

     exx
     ld b, 7
     DM_SCROLL_LOOP2:
     exx

     ldi
     ld bc, $1f
     add hl, bc
     ex de, hl
     add hl, bc
     ex de, hl
     exx
     djnz DM_SCROLL_LOOP2

     ld hl, FIGHT_WAYOFSCROLL
     ld b, (hl)

     pop de
     pop hl

     ex af, af'

     inc e
     inc l

     bit 7, b
     jr z, DM_SCROLL_HOOK

     dec e
     dec l
     dec e
     dec l

     DM_SCROLL_HOOK:

     dec a

     jr nz, DM_SCROLL_LOOP1

     ret

ERASE_BIGSTARS
     ld hl, FIGHT_STARSLEVEL
     ld e, $d8
     jp RESET_MAGICINDICATOR

ERASE_BIGSKULLS:
     ld hl, FIGHT_SKULLSLEVEL
     ld e, $c2
     ;jr RESET_MAGICINDICATOR

RESET_MAGICINDICATOR:
     ld (hl), 0

     ld h, $58
     ld l, e
     ld d, h
     inc e
     ld (hl), 1
;     ld bc, 5
;     ldir
     REPT 5
     ldi
     ENDM

     ld bc, 27
     add hl, bc
     ex de, hl
     add hl, bc
     ex de, hl
     ld (hl), 1
;     ld bc, 5
;     ldir
     REPT 5
     ldi
     ENDM
     ret

COLOR_HEAD
     ld hl, $5a0b
     REPT 10
     ld (hl), a
     inc hl
     ENDM
     ld hl, $5a2b
     REPT 10
     ld (hl), a
     inc hl
     ENDM
     ret

CLEAR_TRACK:
     ld hl, $580a
     ld a, 32
     CT_ERASETRACK:
     halt
     push af
     push hl
     ld ix, FIGHT_SCANTABLE+1 ; Para CS_STARSANDSKULLS
     call CS_STARSANDSKULLS
     pop hl
     pop af
     dec a
     cp 19
     jr nc, CT_ERASETRACK
     ld b, 12
     FM_EW:
     ld (hl), 0
     inc hl
     djnz FM_EW
     ld c, 20
     add hl, bc
     and a
     jr nz, CT_ERASETRACK

     ERASE_TRACK:

     ld ix, FIGHT_SCANTABLE
     ld a, 152
     CT_LOOP:
     ld l, (ix)
;     ld e, (ix)
     ld e, l
     ld h, (ix+1)
;     ld d, (ix+1)
     ld d, h
     inc e
     ld bc, 09
     ld (hl), 0
     ldir
     inc ix
     inc ix
     dec a
     jr nz, CT_LOOP
;     call FIGHT_NEWMOVE
;     ret
     jp FIGHT_NEWMOVE

FIGHT_REORDER:

     call RAND8
     and 63
     ld b, a
     FRO_LOOP:
     push bc
     call RAND8
     pop bc
     and 7
     cp 4
     jr nc, FRO_LOOP
     ld hl, FIGHT_MOVEMENT_SORT
     ld d, 0
     ld e, a
     add hl, de
     ld a, (hl)
     inc hl
     ld c, (hl)
     ld (hl), a
     dec hl
     ld (hl), c
     djnz FRO_LOOP
     ret
     
LETS_REORDER
     ld a, (FLAGS+11)
     cp 5
     ret c

     dec hl

     call WP_STOP_SOUND

     ld e, 4
     call WP_PLAY_SFX

     call BOSS_SAYS
     ld e, 154
     call INIT_CONV_SAYS

     call ERASE_TRACK


     ld hl, $580b
     ld a, 32
     REDTRACK_LOOP:
     halt
     push af
     push hl
     call CS_STARSANDSKULLS
     pop hl
     pop af
     dec a
     cp 16
     jr nc, REDTRACK_LOOP
     ld b, 10
     RT_EW:
     ex af, af'
     ld a, (hl)
     and 64+7
     or 16
     ld (hl), a
     inc hl
     ex af, af'
     djnz RT_EW
     ld c, 22
     add hl, bc
     and a
     jr nz, REDTRACK_LOOP

     sbc hl, bc
     dec hl

     ld a, 16
     BLUETRACK_LOOP:
     halt
     halt
     ld b, 10
     BT_EW:
     ex af, af'
     ld a, (hl)
     and 64+7
     or 8
     ld (hl), a
     dec hl
     ex af, af'
     djnz BT_EW
     ld c, 22
     and a
     sbc hl, bc
     dec a
     jr nz, BLUETRACK_LOOP

     call FIGHT_REORDER 
     call INITCONV_HOOK
     call FIGHT_NEWMOVE
     
     jp LF_HOOK
