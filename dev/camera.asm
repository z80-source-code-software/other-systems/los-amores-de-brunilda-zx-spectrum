CAMERA_RIGHT:
     call I_ERASE_EYES

     ld hl, CAMERA_X

     ld de, CAMERA_OFFSET_X     ; Ponemos lo contrario en el offset de c�mara
     ld a, (de)
     inc a
     and 1
     ld (de), a
;     and a                      ; Si ahora es 0, no hay que cambiar la direcci�n de origen del b�ffer, solo no aplicar offset
     ret nz                     ; As� que te vuelves

     inc (hl)

     ld hl, (MAP_OFFSET_ADDR)
     inc hl
     ld (MAP_OFFSET_ADDR), hl   ; Aumentamos en un supertile la direcci�n de origen del mapa
     ret

CAMERA_LEFT:
     call I_ERASE_EYES

     ld hl, CAMERA_X

     ld de, CAMERA_OFFSET_X     ; Cambiamos el offset de c�mara
     ld a, (de)
     dec a
     and 1
     ld (de), a
;     and a                      ; Si ahora es 1, no hay que cambiar la direcci�n de origen del b�ffer, solo no aplicar offset
     ret z                      ; As� que te vuelves

     dec (hl)

     ld hl, (MAP_OFFSET_ADDR)
     dec hl
     ld (MAP_OFFSET_ADDR), hl
     ret



CAMERA_UP:
     call I_ERASE_EYES

     ld hl, CAMERA_Y

     ld de, CAMERA_OFFSET_Y     ; Ponemos lo contrario en el offset de c�mara
     ld a, (de)
     inc a
     and 1
     ld (de), a
;     and a                      ; Si ahora es 1, no hay que cambiar la direcci�n de origen del b�ffer, solo no aplicar offset
     ret z                      ; As� que te vuelves

     dec (hl)

     ld hl, (MAP_OFFSET_ADDR)
     ld a, (MAP_WIDTH)
     ld e, a
     ld d, 0
     sbc hl, de
     ld (MAP_OFFSET_ADDR), hl   ; Aumentamos en un supertile la direcci�n de origen del mapa
     ret

CAMERA_DOWN:
     call I_ERASE_EYES

     ld hl, CAMERA_Y

     ld de, CAMERA_OFFSET_Y     ; Ponemos lo contrario en el offset de c�mara
     ld a, (de)
     inc a
     and 1
     ld (de), a
;     and a                      ; Si ahora es 0, no hay que cambiar la direcci�n de origen del b�ffer, solo no aplicar offset
     ret nz                     ; As� que te vuelves

     inc (hl)

     ld hl, (MAP_OFFSET_ADDR)
     ld a, (MAP_WIDTH)
     ld e, a
     ld d, 0
     add hl, de
     ld (MAP_OFFSET_ADDR), hl   ; Aumentamos en un supertile la direcci�n de origen del mapa
     ret

ERASE_BORDER:
     ld a, (FLAGS+6)
;     cp 1
     dec a
     ret nz

     ld ix, PRINCIPAL_SPR
     call SHOWSPR_CALC_PRE

     ld a, (PATH_TO_FOLLOW+3)

;     cp 0
     and a
     jr z, EB_GOLEFT
;     cp 1
     dec a
     jr z, EB_GORIGHT
;    cp 2
     dec a
     jr z, EB_GODOWN
;    jr EB_GOUP
EB_GOUP:
     ld a, 7
     add a, b
     ld b, a

     ld a, -5
     add a, c
     ld c, a
     ld de, $0001
;    jr EBD0

     EBD0:

     ld a, $0e

     EBD:
     ex af, af'

     ld a, c
     cp 31
     jr nc, NEXT_EBD
     ld a, b
     cp 19
     jr nc, NEXT_EBD
     
     call ATT_LOCATE
     ld (hl), 0

     NEXT_EBD:
     ld a, d
     add a, b
     ld b, a

     ld a, e
     add a, c
     ld c, a
     
     ex af, af'
     dec a
     jr nz, EBD
     ret

EB_GOLEFT:
     ld a, -4
     add a, b
     ld b, a

     ld a, 7
     add a, c
     ld c, a
     ld de, $0100
     jr EBD0
     
EB_GORIGHT:
     ld a, -4
     add a, b
     ld b, a

     ld a, -4
     add a, c
     ld c, a
     ld de, $0100
     jr EBD0

EB_GODOWN:
     ld a, -4
     add a, b
     ld b, a

     ld a, -5
     add a, c
     ld c, a
     ld de, $0001
     jr EBD0

; AUTOCAM
; 1 Buscar el siguiente 0 hacia abajo un m�ximo de 4 casillas
; 2 Desplazar la cam hacia arriba 9 - casillas "seguras" encontradas antes o hasta que vaya a entrar un 0

AUTOCAM:
      xor a
      ld (CAMERA_OFFSET_Y), a
      ld (CAMERA_OFFSET_X), a


      ld hl, PRINCIPAL_SPR+1
      ld b, (hl)
      inc hl
      ld c, (hl)

      push bc
      ld d, 4 ; ST_ALTO/2 -1
      ld e, ST_ALTO ; 10

      AUTOCAM_LOOP0:

      dec e
      inc b
      call WHATSUPERTILEAT
      and a
      jr z, AUTOCAM_HOOK0
      dec d
      jr nz, AUTOCAM_LOOP0

      ld e, 5

      AUTOCAM_HOOK0:      ; Ahora tenemos que desplazar la camara E hacia arriba... siempre que no entre un "0"

      pop bc


      AUTOCAM_LOOP1:
      dec b
      call WHATSUPERTILEAT
      inc b
      and a
      jr z, AUTOCAM_HOOK1
      dec b

      dec e
      jr nz, AUTOCAM_LOOP1

      AUTOCAM_HOOK1:


      push bc
      ld d, 7 ; ST_ANCHO/2 -1
      ld e, ST_ANCHO ; 10

      AUTOCAM_LOOP2:

      dec e
      inc c
      call WHATSUPERTILEAT
      and a
      jr z, AUTOCAM_HOOK2
      dec d
      jr nz, AUTOCAM_LOOP2

      ld e, 7

      AUTOCAM_HOOK2:      ; Ahora tenemos que desplazar la camara E hacia arriba... siempre que no entre un "0"

      pop bc


      AUTOCAM_LOOP3:
      dec c
      call WHATSUPERTILEAT
      inc c
      and a
      jr z, AUTOCAM_HOOK3
      dec c

      dec e
      jr nz, AUTOCAM_LOOP3

      AUTOCAM_HOOK3:

      ld a, c
      ld (CAMERA_X), a
      ld a, b
      ld (CAMERA_Y), a

;       xor a
;       ld (CAMERA_OFFSET_Y), a
;       ld (CAMERA_OFFSET_X), a

      call CALC_MAP_ADDR

      jp RENEW_BUFFER

