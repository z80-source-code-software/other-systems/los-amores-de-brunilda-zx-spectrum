ORG $6000

FIGHT_GRAPHIC_DATA:

include "../gfx/asm/ARROWS.ASM"

include "../gfx/asm/BOSSES.ASM"

include "../gfx/asm/BOSSES_ATTR1.ASM"
include "../gfx/asm/BOSSES_ATTR2.ASM"
include "../gfx/asm/BOSSES_ATTR3.ASM"
include "../gfx/asm/STARS.ASM"
include "../gfx/asm/BIGSTARS_ATTR.ASM"
include "../gfx/asm/MAGICSTAR_ATTR.ASM"
include "../gfx/asm/MAGICSKULL_ATTR.ASM"
include "../gfx/asm/FIGHT_ATTR.ASM"
