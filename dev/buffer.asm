MAP2BUFFER:
     ld bc, (MAP_OFFSET_ADDR)  ; BC => mapa con el offset que corresponda aplicado
     ld de, BUFFER_COLOR       ; DE => Buffer de color

     exx
     ld a, (BUFFER_ADDR+1)
     xor 15
     ld (BUFFER_ADDR+1), a
     ld de, (BUFFER_ADDR)      ; DE' => Buffer de tiles
                               ; Hacemos un XOR con 15 para poner aqu� el buffer nuevo
                               ; Uno est� en F000 y otro en F300.
     ld b, $0a                 ; Contador de bucle vertical: 10 ST's
     ex af, af'
     MAP2BUFFER_BVer:
          push bc                ; Guardamos contador del bucle vertical, para dejar B libre
          ld a, $10               ; Contador bucle horizontal: 16 ST's
          ld c, $ff               ;  7 Para que los LDI no afecten al registro B
          exx                     ;  4 Pasamos a registros "normales"
     MAP2BUFFER_BHor:
          ex af, af'              ;  4

          ld a, (bc)              ;  7 Cogemos en A el n�mero del supertile que corresponda

          push bc                 ; 11 Nos guardamos BC (Puntero al ST actual en el Mapa)

          exx                     ;  4 Cambiamos HL, BC y DE por los registros alternativos

;          ld c, $ff               ;  7 Para que los LDI no afecten al registro B

          ld h, $1c;(STILES/8)        ;  7  ; (STILES/8) = 28, al multiplicarlo por 8 = E0
          ld l, a                 ;  4  ; Ponemos HL' apuntando al supertile n�mero A
          add hl, hl              ; 11
          add hl, hl              ; 11
          add hl, hl              ; 11
;          ld a, STILES            ;  7 Los Supertiles empiezan en $e000, luego no hace falta sumar
;          add a, h                ;  4 el registro menos significativo, pq es 00, y as� nos
;          ld h, a                 ;  4 ahorramos 6 T-States por ciclo
;          ld bc, SUPERTILE_0      ; 10
;          add hl, bc              ; 11 (HL = BC + A*8)
                                  ;     Ahora HL' => Contenido del supertile correspondiente

          ldi                     ; 16 Copiamos primer tile en buffer
          ldi                     ; 16 Copiamos segundo tile en buffer

;          push de                 ; 11 Guardamos DE, que ahora apunta al primer tile del siguiente ST en el buffer

          ld b, e               ; 4 B = E

          ld a, e                 ;  4 Vamos a apuntar con DE' al "Segundo rengl�n" de la fila de tiles que estamos poniendo
          add a, 30               ;  7
          ld e, a                 ;  4
          
          ld a, d               ; 4 AB = DE original (m�s r�pido que la pila)

          ldi                     ; 16 Copiamos primer tile segundo renglon
          ldi                     ; 16 Copiamos �ltimo tile

;          pop de                  ; 10 Restauramos DE, apuntando al siguiente ST para la pr�xima vuelta del bucle

          ld d,a                ; Restauramos DE de AB, apuntando al siguiente ST para la pr�xima vuelta del bucle
          ld e,b                ;

          push hl                 ; 11 Nos guardamos HL' que apunta (ahora) al primer c�digo de color del ST

          exx                     ;  4 Volvemos a los registros normales. Ahora DE apunta al Buffer de color

          pop hl                  ; 10 Recuperamos HL' en HL => Primer c�digo de color del ST

          ldi                     ; 16 Copiamos primer c�digo de color
          ldi                     ; 16 Copiamos segundo c�digo de color

          push de                 ; 11 Guardamos DE, que ahora apunta al primer tile de color del siguiente ST en el buffer

          ld a, e                 ;  4 Vamos a apuntar con DE' al "Segundo rengl�n" de la fila de tiles que estamos poniendo
          add a, 30               ;  7
          ld e, a                 ;  4
          
          ldi                     ; 16 Copiamos primer tile segundo renglon
          ldi                     ; 16 Copiamos �ltimo tile

          pop de                  ; 10 Restauramos DE, apuntando al siguiente ST para la pr�xima vuelta del bucle

          pop bc                  ; 10 Rescatamos BC, que nos la cargamos con los LDI
          inc bc                  ;  6 Siguiente ST del mapa

          ex af, af'              ;  4
          dec a                   ;  4
          jp nz, MAP2BUFFER_BHor  ; 10 Cerramos bucle "horizontal"

          ; Hay que ajustar los punteros antes de saltar al siguiente ciclo del bucle "vertical"

          ld a, (MAP_WIDTH)       ; 13 Vamos a desplazar el puntero del mapa a la siguiente fila
          sub ST_ANCHO+1          ;  7
          add a, c                ;  4
          ld c, a                 ;  4

          ld hl, 32               ; 10 

          ld a, h                 ;  4 Aprovechamos que H vale 0
          adc a, b                ;  4
          ld b, a                 ;  4          

          add hl, de              ; 11 Desplazamos el puntero en el buffer de color
          ex de, hl               ;  4

          exx                     ;  4 Pasamos a registros alternativos, donde tenemos el buffer de tiles

          ld hl, 32               ; 10 Desplazamos el puntero en el buffer tiles
          add hl, de              ; 11
          ex de, hl               ;  4

;          ld a, $10               ;  4 Restauramos contador del bucle horizontal <- eso se hace al comienzo del bucle
          pop bc                  ; 10 recuperamos contador bucle vertical
          djnz MAP2BUFFER_BVer    ; 13,8 Cerramos bucle "vertical"
          ret

HARD_ATTR_AT
     inc b                        ; El marco alrededor del �rea de juego
     inc c                        ; Mide 1 x 1 caracteres
     call ATT_LOCATE
     ld (hl), d
     dec b
     dec c
     ret


PRINT_NIGHTTILEAT:
     ld a, c

     cp 30
     ret nc

     ex af, af'

     ld a, b
     cp 18
     ret nc

     push bc
     push de
     ld b, a
     ex af, af'
     ld c, a


     call GET_TILE_AT
     ex af, af'
     call GET_ATTR_AT
     ex af, af'

     inc a
;     ld h, 0                 ;  7           ; Ponemos HL' apuntando al tile n�mero A
     ld d,TILES	             ;  7
     ld e, a                 ;  4
;     add hl, hl              ; 11
;     add hl, hl              ; 11
;     add hl, hl              ; 11
;     ld de, TILE_0           ; 10
;     add hl, de              ; 11 (62 T-States en total este HL = Tile_0 + A*8)
;     ex de, hl

     inc b
     inc c
     
     ld hl, SKIP_TILE_AT_NIGHT     ; Si el tile est� validado, vamos a saltarnos a SKIP_TILE_AT_NIGHT, pero pasando primero por ATT_LOCATE
     push hl                       ; As� que subimos SKIP_TILE_AT_NIGHT a la pila, y vamos a ATT_LOCATE con un JP
;     cp $ff                        
     and a		           ; Cuando encuentre el RET, volver� al contenido de la pila, es decir, SKIP_TILE_AT_NIGHT
     jp z, ATT_LOCATE
     
     pop af
     
      call DF_LOCATE
      REPT 7
      ld a, (de)
      ld (hl), a
      inc d
      inc h
      ENDM
      ld a, (de)
      ld (hl), a
     ld a, h
;     dec a
;     srl a                   ;  8
;     srl a                   ;  8
;     srl a                   ;  8
     rrca		      ;  4
     rrca		      ;  4
     rrca		      ;  4
     and 31                   ;  7 A conten�a el Byte Alto de la direcci�n de DATAFILE de pantalla
     
     or $58                   ;  7 Al hacer todo esto, la estamos convirtiendo a direcci�n ATTR
     ld h, a                  ;  4 Es decir, tenemos en HL la direcci�n ATTR donde tenemos que escribir

     SKIP_TILE_AT_NIGHT:
     ex af, af'
     pop de
     and d
     or e

     ld (hl), a
     pop bc

     ret


GET_TILE_AT:
     ld hl, (BUFFER_ADDR)
     jp GETTILEAT_ATTRHOOK

GET_ATTR_AT:
     ld hl, BUFFER_COLOR

     GETTILEAT_ATTRHOOK:
     ld a, (CAMERA_OFFSET_X)
     add a, l
     add a, c
     ld l, a
     ld a, 0
     adc a, h                  ; Sumada la X sobre la base del buffer del mapa completo
     ld h, a

     ld a, (CAMERA_OFFSET_Y)
     add a, b
     and a
     jp z, GETATTRAT_HOOK0

     ld d, 32                  ; 32 tiles de ancho por fila del buffer
     ld e, a

     GETATTRAT_LOOP1:

     ld a, l
     add a, d
     ld l, a
     ld a, 0
     adc a, h
     ld h, a

     dec e
     jp nz, GETATTRAT_LOOP1

     GETATTRAT_HOOK0:
     ld a, (hl)
     ret

DUMP_BUFFER_NIGHT:
     ; Las coordenadas en pantalla del sprite ser�an:
     ; Dimensi�n ST*(Coordenadas en ST - C�mara (ST)) + Offset Sprite - Offset C�mara

     

     ld hl, (CAMERA_Y)        ; 16
     ld ix, PRINCIPAL_SPR

     ld a, (ix+1)             ; 19     A la coordenada ST Y
     sub l                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos (ST de 2 x 2 tiles)
     add a, (ix+3)            ; 19     Le sumas el Offset del Sprite
     sub 4
     ld b, a                  ;  4
     ld a, (CAMERA_OFFSET_Y)
     neg
     add a, b                      ;   Le restas el Offset de C�mara
     ld b, a                       ;   Y lo pones en B

     ld a, (ix+2)             ; 19     A la coordenada ST X
     sub h                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos
     add a, (ix+4)            ; 19     Le sumas el Offset del Sprite
     sub 4
     ld c, a                  ;  4
     ld a, (CAMERA_OFFSET_X)
     neg
     add a, c                      ;   Y le restas el Offset de C�mara
     ld c, a                       ;   Para ponerlo en C

     ; Ahora tenemos en BC las coordenadas "de pantalla" (sin marco) del sprite de IX


    exx
    ld hl, LIGHT_TABLE               ; HL' apunta a la tabla con el circulito
    ld b, $0a                     ; BC' para el contador del bucle

    DBN_LOOP:
    ld c, $0a
    DBN_LOOP_IN:
    ld a, (hl)                       ; En A ponemos el valor que sea de la tabla del halo
    exx

    ld de,0
    and a
    jp z, DBN_DRAWTILE

;    cp $ff
;    ld de, $ff00                     ; D = AND E = OR
    dec d               ; DE=$ff00
    cp d
    jp z, DBN_DRAWTILE               ; Dibuja el tile

    DBN_NFF:
    ld d, a                         ; D = AND (0) ???
;    ld e, 1                          ; E = OR (A)
    inc e

    DBN_DRAWTILE:
    call PRINT_NIGHTTILEAT           ; Dibuja el tile

    DBN_HOOK:
    inc c                            ; Siguiente columna
    exx
    inc hl                           ; Siguiente valor de la tabla
    dec c                            ; Siguiente vuelta del bucle
    jp nz, DBN_LOOP_IN                  ; Cierre bucle horizontal
;    ld c, $0a                        ; Restauramos C' para el bucle
    exx
    ld a, -10                        ; Restauramos C (columna) a la de la izquierda
    add a, c
    ld c, a
    inc b                            ; Incrementamos fila
    exx
    djnz DBN_LOOP                    ; Cierre bucle vertical

    ret


DUMP_BUFFER:
     ld hl, (LAST_BUFFER_POINTER)   ; en HL va el puntero al b�fer "antiguo" (ya con el offset que tuvo aplicado)

     ld de, (BUFFER_ADDR)           ; en DE va a ir el puntero al b�fer "actual", pero hay que aplicar el offset que corresponda
     ld bc, BUFFER_COLOR            ; Vamos a poner BC apuntando al buffer de color + Offset correspondiente

     ld a, (CAMERA_OFFSET_Y)        ; Primero el vertical
     ;cp 1                           ; Si hay offset de tile vertical
     dec a
     jp nz, DB_OFFSETX_TILE
     ld e, (ST_ANCHO+1)*ANCHO_ST    ; Desplazate un tile hacia abajo

     DB_OFFSETX_TILE:

     ld a, (CAMERA_OFFSET_X)        ; Ahora el offset horizontal
     and 1                          ; Si hay offset horizonatal
     add a, e                       ; Hay que incrementar los dos punteros en un tile
     ld e, a                        ;
     ld c, a                        ; En el buffer de color tambi�n

     ld (LAST_BUFFER_POINTER), de   ; Y guardamos el puntero "actual" como puntero "antiguo" para el pr�ximo ciclo

     ld a, (FLAGS+6)
     dec a
     jp z, DUMP_BUFFER_NIGHT
     inc a
     exx
     ld de, $4000 + 32 * OFFSETYONSCREEN + OFFSETXONSCREEN     ; DE' = 16384 (1er tercio)

     ld c, 8 - OFFSETYONSCREEN                       ; C' = Contador del bucle

     jp z, DUMPBUFFERDARK_CYCLE1

DUMPBUFFER_CYCLE1:
        ld b, (ST_ANCHO*ANCHO_ST)/6
DUMPBUFFER_CYCLE1_INT:
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_1 	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_2 	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1_6	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1_6	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1_6:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
        dec	b
        jp 	nz,DUMPBUFFER_CYCLE1_INT
	exx                    ;  4

        REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
		inc hl                     ; 6
		inc de                     ; 6
		inc bc                     ; 6
        ENDM

        exx                    ;  4 Registros ALTERNATIVOS

        REPT 32 - ST_ANCHO*ANCHO_ST  ;     Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)...
        	inc e                   ;  4  ... incrementamos el puntero a la pantalla
	ENDM

        dec c				;  4 ; Fin de bucle
	jp nz,DUMPBUFFER_CYCLE1

	ld de, $4800 + OFFSETXONSCREEN ; DE' = 2� tercio
	ld c, 8                        ; C' = Contador del bucle

DUMPBUFFER_CYCLE2:

	ld b, (ST_ANCHO*ANCHO_ST)/6		; Tantas veces como Tiles de ancho tengamos que poner

	DUMPBUFFER_CYCLE2_INT:
     
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_1	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_2	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2_6	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2_6	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2_6:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

	dec b
	jp nz, DUMPBUFFER_CYCLE2_INT

	exx                    ;  4

        REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
        	inc hl                     ; 6
		inc de                     ; 6
		inc bc                     ; 6
	ENDM

	exx                    ;  4 Registros ALTERNATIVOS

        REPT 32 - ST_ANCHO*ANCHO_ST  ; Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)
        	inc e                  ;  4
	ENDM

	dec c
        jp nz, DUMPBUFFER_CYCLE2

	ld de, $5000 + OFFSETXONSCREEN ; DE' = 3er tercio
	ld c, 2 + OFFSETYONSCREEN          ; B'  = Contador del bucle

DUMPBUFFER_CYCLE3:
          ld b, (ST_ANCHO*ANCHO_ST)/6	; Tantas veces como Tiles de ancho tengamos que poner
DUMPBUFFER_CYCLE3_INT:
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_1	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
		
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_2	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3_6	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3_6	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			exx                     ;  4 Registros normales
     			ld a, (bc)              ;  7 Ponemos en A el color que hay que pintar en la pantalla (BC apunta al buffer de color)
     			exx
			ld (hl), a              ; 7
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3_6:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

	  dec b
          jp nz, DUMPBUFFER_CYCLE3_INT

          exx                    ;  4 

          REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
               inc hl                     ; 6
               inc de                     ; 6
               inc bc                     ; 6
          ENDM

          exx                    ;  4 Registros ALTERNATIVOS

          REPT 32 - ST_ANCHO*ANCHO_ST  ; Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)
               inc e                  ;  4
          ENDM

          dec c
          jp nz, DUMPBUFFER_CYCLE3
     ret


DUMPBUFFERDARK_CYCLE1:
        ld b, (ST_ANCHO*ANCHO_ST)/6
DUMPBUFFERDARK_CYCLE1_INT:
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_1	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla
		
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_2	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC1D_6	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC1D_6	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$58		; Valor fijo en el primer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC1D_6:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

        dec b
        jp nz,DUMPBUFFERDARK_CYCLE1_INT
	exx                    ;  4

        REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
		inc hl                     ; 6
		inc de                     ; 6
		inc bc                     ; 6
        ENDM

        exx                    ;  4 Registros ALTERNATIVOS

        REPT 32 - ST_ANCHO*ANCHO_ST  ;     Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)...
        	inc e                   ;  4  ... incrementamos el puntero a la pantalla
	ENDM

        dec c				;  4 ; Fin de bucle
	jp nz,DUMPBUFFERDARK_CYCLE1

	ld de, $4800 + OFFSETXONSCREEN ; DE' = 2� tercio
	ld c, 8                        ; C' = Contador del bucle

DUMPBUFFERDARK_CYCLE2:

	ld b, (ST_ANCHO*ANCHO_ST)/6		; Tantas veces como Tiles de ancho tengamos que poner

	DUMPBUFFERDARK_CYCLE2_INT:
     
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_1	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_2	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC2D_6	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC2D_6	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$59		; Valor fijo en el segundo tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC2D_6:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


		dec b
	jp nz, DUMPBUFFERDARK_CYCLE2_INT

	exx                    ;  4

        REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
        	inc hl                     ; 6
		inc de                     ; 6
		inc bc                     ; 6
	ENDM

	exx                    ;  4 Registros ALTERNATIVOS

        REPT 32 - ST_ANCHO*ANCHO_ST  ; Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)
        	inc e                  ;  4
	ENDM

	dec c
        jp nz, DUMPBUFFERDARK_CYCLE2

	ld de, $5000 + OFFSETXONSCREEN ; DE' = 3er tercio
	ld c, 2 + OFFSETYONSCREEN          ; B'  = Contador del bucle

DUMPBUFFERDARK_CYCLE3:
          ld b, (ST_ANCHO*ANCHO_ST)/5		; Tantas veces como Tiles de ancho tengamos que poner
DUMPBUFFERDARK_CYCLE3_INT:
               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3D_1	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3D_1	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3D_1:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3D_2	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3D_2	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3D_2:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3D_3	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3D_3	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3D_3:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3D_4	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3D_4	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3D_4:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla


               exx                    		;  4  Registros NORMALES
               ld a, (de)             		;  7  Comparamos los dos buffers entre s�
               cp (hl)                		;  7  En DE tenemos el nuevo y en HL tenemos el antiguo
               jr z,DONTDRAWTHISTILE_CYC3D_5	; 12  Si es el mismo, no pintamos el tile
                                        	; -5
               inc a                    	;  4
               jr z,DONTDRAWTHISTILE_CYC3D_5	; 12  Si el tile est� "validado" no hay que pintar nada
                                        	; -5

			exx                     ;  4  Lo vamos a hacer con los registros alternativos
			ld h,TILES		;  7
     			ld l, a                 ;  4

     			REPT 7                  ; 7 filas de un caracter
				ld a, (hl)      ;  7    Imprimimos ese caracter en la pantalla (donde apunta DE')
     				ld (de), a      ;  7
     				inc d           ;  4
     				inc h           ;  4
			ENDM
     			ld a, (hl)              ;  7    La �ltima fila (fuera de la macro porque ya no hace falta incrementar los punteros)
     			ld (de), a              ;  7

     			ld a, d                 ;  4 Restauramos DE' a la primera fila del car�cter
     			and $f8                 ;  7
     			ld d, a                 ;  4

     			ld l, e                  ;  4 Vamos a preparar HL' apuntando a la direcci�n de ATTR que corresponda

			ld h,$5a		; Valor fijo en el tercer tercio

			ld (hl), 1              ; 10
     			exx                     ; 4
DONTDRAWTHISTILE_CYC3D_5:
		inc l                 ;  4   ; Incrementamos los punteros a los dos buffers
		inc e                 ;  4
		inc c                 ;  4   ; Tambi�n el b�ffer de color

		exx                    ;  4
		inc e                  ;  4   ; Incrementamos el puntero de pantalla

          dec b
          jp nz, DUMPBUFFERDARK_CYCLE3_INT

          exx                    ;  4 

          REPT ANCHO_ST              ; El Buffer tiene un SuperTile de m�s, y al terminar la fila, hay que "salt�rselo
               inc hl                     ; 6
               inc de                     ; 6
               inc bc                     ; 6
          ENDM

          exx                    ;  4 Registros ALTERNATIVOS

          REPT 32 - ST_ANCHO*ANCHO_ST  ; Tantas veces como columnas se quedan sin rellenar (para pasarnos a la fila siguiente en pantalla)
               inc e                  ;  4
          ENDM

          dec c
          jp nz, DUMPBUFFERDARK_CYCLE3
     ret


INVALIDATE_TILE:
     ld hl, (LAST_BUFFER_POINTER)
     jp WRITE_TILE

VALIDATE_TILE:
     ld hl, (BUFFER_ADDR)           ; en DE va a ir el puntero al b�fer "actual", pero hay que aplicar el offset que corresponda

     ld a, (CAMERA_OFFSET_Y)        ; 13     Primero el vertical
     and a                       ;        Si la c�mara est� en coordenada impar
     jp z, VT_OFFSETX_TILE
     ld l, (ST_ANCHO+1)*ANCHO_ST   ; Desplazate un tile hacia abajo

     VT_OFFSETX_TILE:

     ld a, (CAMERA_OFFSET_X) ; 13         ; Ahora el offset horizontal
;     and a                        ; Si la camara est� en coordenada impar
     add a, l                ;  4  ; A��deselo al puntero
     ld l, a                 ;  4

WRITE_TILE
     ld a, l
     add a, c
     ld l, a

     ld a, b
     and a
     jp z, WRITETILE_HOOK

     push de
     ld de, 32

     WRITETILE_LOOP:
     add hl, de
     dec a
     jp nz, WRITETILE_LOOP
     pop de

     WRITETILE_HOOK:
     
     ;ld a, $ff
     dec a      ;A val�a 0
     cp (hl)                    ; Si ya estaba invalidado de antes
     scf                        ; Se vuelve marcando el carry
     ret z                      ; Para que no parpadee pintando un sprite encima de otro

     ccf                        ; Tiene el carry seteado, as� que, ya que no estaba invalidado de antes, hay que complementarlo

     ld (hl), a

     ret

FADE_IN:
     ld a, (FLAGS+6)
;     cp $ff
     inc a
     jp nz, UPDATE_ALL

     call MAP2BUFFER              ; Primero imprimimos todo, pero sin atributos
     ld hl, BUFFER_COLOR
     ld de, BUFFER_COLOR + 1
     ld bc, 639; ((ST_ANCHO + 1) * ANCHO_ST) * ((ST_ALTO + 1) * ALTO_ST) - 1
     ld (hl), 0
     ldir
     call DUMP_BUFFER

     call MAP2BUFFER               ; Para restaurar el buffer de color
     call SHOW_SPRITES
     call SHOW_OBJECTS

     ld de, BUFFER_COLOR

     ld a, (CAMERA_OFFSET_Y)        ; Primero el vertical
     bit 0, a                       ; Si hay offset de tile vertical
     jr z, FI_OFFSETX_TILE
     ld e, (ST_ANCHO+1)*ANCHO_ST    ; Desplazate un tile hacia abajo

     FI_OFFSETX_TILE:

     ld a, (CAMERA_OFFSET_X)        ; Ahora el offset horizontal
     and 1                          ; Si hay offset horizonatal
     add a, e                       ; Hay que incrementar el puntero en un tile
     ld e, a                        ;

     ; Ahora DE apunta al buffer de atributos con el offset correspondiente
     push de

     exx
     ld d, 7                        ; 7 vueltas de ciclo en total


     FI_LOOP1:
     exx
     pop de
     push de
     ld hl, $5800 + 32 + 1          ; Atributos donde vamos a empezar a pintar
     exx

     halt

     ld c, ST_ALTO * ALTO_ST
     FI_LOOP2:
     ld b, ST_ANCHO * ANCHO_ST
     FI_LOOP3:
     exx

     ld a, (de)                    ; Cogemos lo que hay en el b�ffer
     and a
     jr z, FI_CHECKBRIGHT
     and 7                         ; Nos quedamos s�lo la tinta
     ld b, a

     ld a, (hl)                    ; Ahora lo que hay en los atributos
     and 7                         ; E igual, nos quedamos solo la tinta
     cp b                          ; Comparamos
     jr z, FI_CHECKPAPER           ; Si es igual ya est�...

     inc (hl)                      ; Si no, subimos la tinta de los atributos

     FI_CHECKPAPER:

     ld a, (de)                    ; Volvemos a coger lo que hay en el b�ffer
     and $38                       ; Ahora nos quedamos s�lo con el papel
     ld b, a

     ld a, (hl)                    ; Y ahora los atributos
     and $38
     cp b
     jr z, FI_CHECKBRIGHT

     ld a, (hl)                    ; Aumentamos en uno los atributos...
     add a, 8                      ; Hay que montar m�s l�o que con la tinta
     ld (hl), a

     FI_CHECKBRIGHT:

     ld a, (de)
     and $c0
     ld b, a

     ld a, (hl)
     or b
     ld (hl), a
     
     inc hl
     inc de
     exx
     djnz FI_LOOP3
     exx
     inc de
     inc de                             ; Tenemos un ST (2 tiles) de m�s en el b�ffer
     inc hl
     inc hl                             ; Hay que aumentar tambi�n el puntero a la pantalla porque hay dos caracteres de margen (uno a cada lado)
     exx
     dec c
     jr nz, FI_LOOP2
     dec d
     jr nz, FI_LOOP1

     pop de

     ld a, (BUFFER_ADDR+1)
     xor 15
     ld (BUFFER_ADDR+1), a

;     ret


RENEW_BUFFER:
         ld hl, BUFFER_ADDR
         ld e, (hl)
         inc hl
         ld d, (hl)
         ld h, d
         ld l, e
         inc e
         ld (hl), $ff
         ld bc, 639
         ldir
         ret
