# \01 = Set attribute
# \0D = New line
#
#

San Arteixo do Montalvo
Serralleria de Xosé
Pousada de Xan
Deu protegeixi aquesta casa i als seus habitants
Deus lari Berobreo aram posuit pro salute
# 05
Ermita de San Arteixo
\01\45Galicia, 30 d' octubre de 1830.\01\07
Dos monjos franciscans es dirigeixen en peregrinació pel camí de Santiago.\0D\0D\01\44Fray Gonzalo\01\07 i \01\44Fray Cesáreo\01\07 porten tot el día caminant i decideixen descansar al primer poblat que troben.
Al perdre tota la seva fe, Fray Gonzalo es va tornar boig i es va quedar vagant pels boscos.\0D\0D Hi ha qui explica que el va veure guiant a la Santa Compaña.\0D\0DFray Cesáreo va morir a casa del boticari, i va ser enterrat al poblet.
Amb una mica de sort, \01\46San Arteixo do Montalvo\01\07 els acollirà com tantes altres ho han fet al llarg del seu viatge.\01\30
# 10
Fray Gonzalo, aquesta no és la posada. Hem de buscar-la el més aviat posible.
Fray Gonzalo, hem de buscar on dormir. No pretendrà fer-ho al bosc?
Hem de buscar a on allotjarnos abans que es faci de nit.
Sí, germà. A mi aquests paratges em fan paure de nit. Mai se sap que hi pot haver.
Ja esteu amb les vostres pors i supersticions?
# 15
No puc reprimir-me, és la foscor. Mai em podré acostumar.
Bé, busquem. Se'ns cau la nit a sobre.
La pau de Déu sigui amb vostè, germà. Busquem allotjament per aquesta nit.
Ho sento bons homes, estic fent reformes i no tinc cap habitació disponible.
Però necessitem un lloc on dormir, aquests boscs fan molta por de nit..
# 20
Tranquil Fray Cesáreo, alguna solució trobarem.
És clar, bons homes. Segur que Don Justiniano, el boticari, estarà encantat d'oferir-vos la seva casa. És molt amic del cura i al ser vostès peregrins, tindrà la caritat d'hostatjar-los.
Compte! ic! Us envía a la boca del llop ic! Aquesta casa està maleida ic!
Calla borratxo, no espantis a aquests homes de Déu.
La donzella es va suicidar ic! i la seva ànima vaga pels passadissos ic!
# 25
Calla borratxo!
No es preocupi, nosaltres no som supersticiosos. On puc trobar al boticari?
És millor que parlin amb Don Cirilo, el cura, a l'església.
Aquest poble està maleit ic! l'única solució ic! és oblidar bevent ic!
Disculpin a aquesta pobre ànima perduda per l'alcohol, no li creguin.
# 30
Moltes gràcies Fray Gonzalo, ara descansem en pau.
Si us plau, parlem amb el cura el més aviat possible.
Benvinguts, germans. Els estava esperant. Aquesta és una aldea petita, i les notícies volen.
Bona nit, germà. Som Fray Gonzalo i Fray Cesáreo, peregrins de l'Apóstol, i busquem on pasar la nit...
Sí, sí, és clar, volen hospedatge i no hi ha lloc a la posada.\0DHauríen d'haber vingut a veurem abans de res.
#35
Ens han dit que el boticari podría allotjarnos s vostè intervingués.
Per descomptat. Fisn i tot jo mateix, aquí a la sacristía, però és petita i no tinc tant de lloc com D. Justiniano.\0DÉs un home ric, i la seva casa té moltes habitacions buides.
Els franciscans no som amics de luxes: nomès aspirem a un sostre i una màrfega.
Per descomptat, però segur que agrairan l'amabilitat d'un bon cristià que els ofereixi un llit tou i un bon sopar.\0DNo sempre és presenta aquesta oportunitat, i això faria el Camí una mica menys dur.
Està molt lluny la casa del boticari?
#40
Una passejadeta pel bosc. És millor que sortim ja, abans que sigui negra nit.
Disculpi a Fray Cesáreo, la foscor li torna supersticiós i temorós.
Fa bé. La nit és refugi de coses dolentes i la prudència és una virtud. Sortim.
Seguiu-me, germans. El camí és curt però és fàcil perdres.\0DSi s'allunyen massa els esperaré, però donem-nos pressa per arribar abans que caigui la nit.
Germà, seguim a D. Cirilo. La nit està al caure.
#45
Toc, toc! Ah de la casa!
Don Cirilo! Quina sopresa a aquestes horres de la nit!
Sí, veritat? No són hores per anar per aquests verals, jeje\0DApa, deixa'ns entrar, que hem de parlar amb D. Justiniano.
Sí, però és gairebé l'hora de sopar...
Doncs més al meu favor: On sopa un, sopan quatre... jeje
#50
Amic Cirilo, no t'esperava tan tard. Què et porta per aquí?
Saps que no surto de nit si no és necessari.\0DHan arribat al poblat aquests dos germans pelegrinant a Santiago, i busquen on passar la nit. Llavors vaig pensar que podries fer una bona acció.
Per descomptat, Ciril. Germans, siguin benvinguts a aquesta, la meva humil morada.\0DMaruxa, prepara les habitacions de convidats, que els germans es queden a dormir... i a sopar, perquè tindran gana, oi?
Sí, moltes gràcies, don Justiniano. Que Déu els ho pagui.
Jo també tinc gana, que fa molt bona olor, i aquestes viandes cal provar-les, jeje
#55
Tu sempre igual. Per descomptat que et quedes a sopar, i a dormir, que ja és negra nit.
Si us plau, segueixin-me.
El sopar estarà a punt de seguida. Baixin al menjador quan s'hagin instal.lat.
D'acord. Moltes gràcies.
Passin, germans. Els estàvem esperant.
#60
Ara anem a prendre unes copetes d'orujo que ajudaran a la digestió.
L'orujo de D. Justiniano és realment bo, si el vengués es faria d'or.
Ximpleries, no penso vendre'l: si el venc no podríem gaudir-lo... jajaja
És cert, millor no el venguis... jejeje
Els hi ensenyaria el meu laboratori però segur que a vostès no els interessa la medecina i la investigació científica.
#65
A mi sí, m'encantaria fer una ullada: Vaig estudiar medecina abans de prendre els hàbits.
Un col.lega, vaja, vaja... crec que tindrem una estupenda vetllada.
Jo ho sento, però els meus ossos em demanen descansar. Segueixin amb la seva conversa, fins demà.
Jo també m'en vaig a dormir. El bon pelegrí se'n va a dormir aviat i matina molt.
Quina nit, quins malsons. No vaig haver d'haver begut tant orujo.
#70
Que estrany. Sembla que Fray Cesáreo no hagués dormit aquí aquesta nit...
Aquesta porta sembla fermament tancada.
No me n'he d'anar d'aquí sense Fray Cesáreo.
Fray Gonzalo! No es pot imaginar el que ha passat.
Fray Cesáreo! Boticari, què li passa?
#75
No ho sabem, va aparèixer al'ala oest de la casa, aquesta part sempre està tancada...
Sempre està tancada des que ...
Llavors, com és que hi era Fray Cesáreo?
No ho sabem. Ahir a la nit em vaig quedar adormit al laboratori mentre Fray Cesáreo seguia mirant els meus experiments. \0DEn despertar vaig veure papers tirats que deixaven un rastre fins l'ala oest i allí el vaig trobar, al terra.
Però, està mort?
#80
No, encara viu, però està inconscient i res el fa despertar.
Creiem que és pres d'un encanteri.
Don Cirilo és vostè capellà! no ha de creure en supersticions.
Fill, fa molt que vaig aprendre que en aquestes terres tot és possible.
Hauríem d'avisar la meiga. Segur que podria ajudar-nos.
#85
És ridícul, crec que hauríem de portar-lo a la capital a que el tractin.
Feu-nos cas, germà. Aquest no és un mal del cos, sinó de l'ánima.
Només la meiga ens pot ajudar. Ha d'anar a buscar-la. Viu en una cabana, al bosc que hi ha entre aquesta casa i el poble. Nosaltres ens quedarem cuidant al malalt.
Què li sembla aquest D. Justiniano, germà?
Un bon cristià, generós i simpàtic, la veritat.
#90
A mi també, però... he sentit un calfred al entrar en aquesta casa.
No busqui més explicació que un corrent d'aire, germà.
Serà això, segur. Baixem a sopar...
Baixem a sopar, germà. Ens estan esperant.
La meva feina és obrir panys, no n'hi ha cap que se'm resisteixi.
#95
Aquest armari està obert, però no veig el motiu de posar-me les robes de la majordoma... almenys en aquest moment.
Més tard...
Al matí seguent...
Això és un crani humà! No ho agafaré sense una bona raó.
Quina desgràcia, aquesta casa no és la mateixa des de fa uns quants anys.
#100
He d'anar a buscar a la meiga a veure si ens pot ajudar.
No li ajudarà si no li paga pels seus serveis. Tingui aquesta gallina, serà suficient.
Ha passat una cosa terrible! Pugi, el senyor l'espera a dalt, al costat del seu pobre amic.
Se m'ha acabat l'orujo i fins al mes que ve no m'arriba la comanda. Aquest poble cada dia beu més.\0DDonaria diners per una ampolla, encara que només fos una.
Disculpi, necessito els serveis d'una meiga... Podria indicar-me on trobar-la?
#105
¿Meiga? Al bosc viu una que diuen que ho és: Aneu amb compte amb ella, no és de fiar i, per descomptat, mai treballa gratis.
Al nord del poblet trobaran la posada de Xan. Segur que allà podran ajudar-los.
Però una cosa li diré, germà: Per poder trobar-la, ha de creure en ella. Aquesta fe en la seva església no ho permetrà.
No puc anar-me'n del poble i deixar abandonat al pobre Fray Cesáreo.
Un monjo foraster entrant a la casa d'una meiga. Això sí que és una sorpresa.
#110
Don Cirilo m'ha enviat en la seva búsqueda. El meu company ha caigut, segons ell, presa d'un encanteri que el manté inconscient.
Aniré a veure'l, però els meus serveis no són gratuits.
Acceptaria aquesta gallina a canvi?
Amb aquesta gallina faré un brou que li anirà fenomenal als meus pobres vells ossos. Anem a veure a aquest embruixat.
Està bé. Buscaré alguna cosa que pugui complaure-la.
#115
Aquesta vella meiga no és com vostès els capellans. No em comprarà amb or.
Si vol que l'acompanyi, haurà de pagar-me amb alguna cosa.
Gràcies a Déu que has vingut, mira, és aquest pobre home.
Està clar. A l'ala oest, la nit de difunts i en aquest estat no és més que la maledicció.
Oh, Déu meu, Brunilda de nou.
#120
¿Brunilda? Qui és Brunilda? Tot això és ridícul!
Res és ridícul. Brunilda es va suicidar en aquesta casa i la seva ànima vaga per la casa.\0DNecessita una altra ànima per sortir del purgatori, i ha agafat la d'aquest pobre home.
No crec en aquestes coses, segur que és un col.lapse.
No cregui si no vol, però si no fa alguna cosa per la seva ànima el seu amic continuarà així fins que mori i, en això segur que opinem igual, és una cosa que succeirà molt aviat si segueix en aquest estat.
Feu-li cas, Fray Gonzalo, ella sap el que diu.
#125
Proposo esperar a veure si desperta i demà prendrem una decisió. Què li sembla Fray Gonzalo?
Molt millor així.
Si canvieu d'opinió, ja sabeu on visc, adéu.
Fray Cesáreo segueix inconscient. Hem de portar-lo demà a la ciutat. Ara intentaré descansar una mica.
Les dotze de la nit i no he pogut dormir, quina nit.
#130
PARE SANT!
No s'espanti, pare. Escolteu la meva història...
No puc creure el que veig!
Sóc Brunilda, o millor dit, el seu esperit.\0DJo servia en aquesta casa, era part del servei. Fa anys vaig viure un festeig secret amb Antón, el fill del boticari. El seu pare no sabia res i va arribar el moment de explicar-ho.
El meu estimat va sortir al bosc amb el seu pare per sincerar-se, però mai va arribar aquest moment: uns bandits van sortir al pas, i el van matar.\0DQuan ho vaig saber, no vaig poder resistir-ho: La bogeria em va envair i em vaig suicidar.
#135
Tenia l'esperanca de reunir-me amb el meu estimat en l'altra vida, però per algun motiu, vago per aquesta casa des de llavors.
I la teva història què té a veure amb el meu germà Fra Cesáreo?
Molt. La desgràcia reina aquesta casa des de llavors, la seva ànima està com la meva, atrapada.
Sóc un home de fe, no crec que això estigui passant...
Sé com solucionar aquesta situació. Les nostres desgràcies estan encadenades: Si m'ajuda a mi, l'ajuda a ell.
#140
Explica i faré el que pugui.
Aneu al cementiri, darrere de l'església, aquesta mateixa nit. Allà ens veurem.
No es preocupi. Jo li obro.
Aquesta és la meva tomba. No sé per què, però sento que alguna cosa va malament a dins.
Que alguna cosa va malament?
#145
Percebo clarament que alguna cosa va malament a la tomba. Cavi i obriu-la, si us plau.
Però com?
Aconsegueixi una pala. Miri en algun jardí, els jardiners solen usar-les.
L'hi vaig dir, alguna cosa anava malament en la meva tomba!
Pare Sant, falta el crani!
#150
Si us plau, trobi el meva crani, i poseu-lo costat del cos.
Entenc que la teva ànima no trobi descans.
No trobaré descans fins que estigui al costat del meu amor. Quan tingui el meu crani, enterrins junts.
Tot el que està passant em porta tristíssims records. Brunilda era tan bona!
Ara veuràs!
#155
Qui parla?
Em porto el teu cos a una altra banda!
Noooooo! Ara la meva ànima no podrà descansar!
No puc creure el que he vist!
Estàs segur de voler abandonar la partida? (S / N)
# 160
La teva fe t'impedirà ajudar a Brunilda, jajaja
Fray Gonzalo, desperti! Què fa aquí? Com ha entrat?
Er... Don Cirilo... no s'ho creurà... Brunilda... el seu cos... la seva tomba...
Brunilda? Sabia que al final estava relacionada amb tot això...\0DNomés una persona pot ajudar-nos en aquest moment: La meiga. Aneu a buscar-la. Jo em quedaré a l'Església, pregant per l'ànima del seu amic.
Aquesta història de l'ànima de la criada és el súmmum.\0DProu desgràcies hi han hagut ja en aquesta casa.\0DHe decidit repassar la literatura científica a la recerca de casos com el del seu amic.
# 165
Si troba alguna cosa que cregui que pugui ajudar, portim-ho.
Aquest bolet és extraordinari! No sabia que creixessin per aquí. Estic segur que ens pot ajudar en el cas del seu company, però he de comprovar-ho al laboratori.
Si us plau, germà. Deixi això on està. Aquest crani ha format part de la meva família des que el meu avi era estudiant.
Aquest bolet estrany que m'ha portat ens ajudarà molt, estic segur, però necessito més temps.
Bé, suposo que ha vingut a demanar-me consell i consell li donaré.\0DSé qui té el crani i on podrà trobar les quatre parts de l'esquelet de Brunilda.
# 170
L'esquelet es troba en quatre cavernes dels antics celtes. Per arribar-hi, primer reculli en aquesta gerra l'aigua de les 7 basses del Monte das Pozas i regui amb ella els monòlits que hi ha al costat de les seves entrades.
El crani el té el boticari al seu despatx. Sospito que té bastant a veure en aquesta història: no crec que li agradés que el seu fill s'enamorés de la criada.\0DQuan tingui tot o necessiti ajuda vingui a veure'm.
Li donaré un fanal perquè s'il.lumini en les cavernes a canvi de 7 bolets.
Busqui aquests set bolets, i torni a veure'm.
Aquí té aquest fanal. Ara aconsegueixi l'esquelet.
# 175
Encara no tens l'esquelet complet. Com vols que t'ajudi?
Monte das siete pozas.
Compte amb els despreniments.
Això està escrit en uns caràcters que els meus cristians ulls no saben desentranyar... i encara és més, no haurien d'haver vist.
Aquesta cova està en total foscor! Seria temerari entrar sense alguna cosa amb què alumbrarme!
# 180
Les set basses sagrades dels Celtas es troben davant teu. La seva aigua conté la saviesa dels vells Druides, i t'ajudarà a trobar el teu camí.
En aquest cartell posa que hi ha despreniments. No hauria de passar sense una bona raó.
No necessito més aigua d'aquesta bassa, ja la vaig agafar abans.
La meiga va dir que necessito l'aigua de cadascuna de les set basses perquè tingui efecte.
Vegem què passa...
# 185
No he de malbaratar l'aigua.
La superfície del monòlit no reflecteix ni un sol raig de sol.
Està bé, tingues els ossos. Has guanyat una batalla, però no la guerra.
Això t'ensenyarà a no ficar-te amb qui no deus.
Et venceré amb la meva dansa macabra.
# 190
Doblégat i seré caritatiu amb tu.
Ni parlar, això mai!
Repeteix el meu ball, però fes-ho en el moment just o et venceré...
Ensumat aquesta, fraret!
Vade retro, monstre!
# 195
No lluitaré novament contra aquest monstre si puc evitar-ho.
Molt bé. Ara, mentre jo invoco l'ánima de Brunilda, aneu al cementiri per posar els ossos al seu lloc.
Aneu al cementiri a col.locar els ossos. Invocar un ánima del purgatori és un treball ardu i perillós que no s'ha d'interrompre.
Ha portat tot! Per fi podré descansar en pau!
No passa res ... Realment totes aquestes parts són meves?
# 200
Sí, totes, també el crani, que el tenia el boticari al seu despatx.
No! Aquest crani no és meu. Ja el tenia abans que jo morís, va pertànyer al seu avi.
JaJa! Heu picat els dos, el teu crani el tinc jo.
Tu! Sempre ens vas mirar amb mals ulls.
Si jo no puc estar amb Antón, que no ho estigui ningú i tu m'ho me'l vas treure.
# 205
Retorna'ns el crani, no guanyes res amb això, Antón està mort.
Sí, però el gust de la venjanca és molt dolc!
Moltes gràcies, mai vaig pensar que la meiga tingués gelosia de nosaltres.
Ara ja no cal preocupar-se per ella.
Brunilda, amor, ja estem junts.
# 210
Sí, només falta que el teu pare accepti la nostra relació i que s'oficii una missa per les nostres ànimes.
Fray Gonzalo, ens ajudarà?
El meu diari li donarà la pista d'on trobar les cartes que ens enviàvem.
Ensenyise-las al meu pare: Això serà suficient per que s'assabenti de les nostres coses i accepti.
Cerca a la meva habitació, a l'ala oest de la casa.
# 215
Veig que no n'has tingut prou. Està bé, t'ensenyaré el meu veritable poder...
Aquest orujo que em va portar, pare, és excepcional. M'ha salvat la pell.
Lloat sigui Déu, una ampolla d'orujo. Se la compro! Agafi aquestes monedes, crec que serà suficient.
He estat meditant que potser aquest orujo que destil.la el boticari pugui despertar al meu germà, ¿no creu?
Lloat sigui Déu! Gran idea. Aquest orujo ressuscita els morts, porteu aquesta ampolla, segur que l'ajuda, però no li digui al Sr. Justiniano que li he donat, o m'acomiadarà.
# 220
Vol anar a l'ala oest? No pot, el senyor mai li deixaria, sóc l'única a la qual presta les claus, i, disculpim, pare, però en això no li ajudaré: S'estan tornant tots bojos!
No puc anar així vestit per aquí i que algú em vegi. Necessito tornar a posar-me els meus hàbits.
La clau? Vostè veurà, però no vagi de nit, ja sap el que passa i no vull lamentar més desgràcies.
No em molestis més, estic molt ocupat.
El diari diu que les cartes estan amagades darrera d'un passadís a la biblioteca.\0DPer accedir-hi, cal empènyer la llibreria del racó nord.
# 225
El boticari té uns exemplars excel.lents en aquesta biblioteca.
Aquesta ha de ser la llibreria que diu Brunilda. Si la empenyo una mica potser cedeixi...
Aquest és el cofre que descrivia Brunilda al seu diari, però està tancat amb clau! Necessitaré obrir-lo per agafar les cartes.
Bon pany, clar que la puc obrir, però aquest pobre manyà necessita diners per poder pagar el seu menjar...
A canvi d'aquestes monedes, li obriré encantat aquest cofre.\0D\0DAquí té, germà.
# 230
Aquestes cartes són del meu fill per... Brunilda? Estaven enamorats i no es van atrevir a dir-m'ho! Ella es va suicidar per amor!\0DAra ho entenc. Cal dir una missa en el seu descans: Parlaré amb el capellà. L'espero allà.
Aquest crucifix m'ha permès resar quan estava a punt de perdre la fe. M'ha salvat de caure en la bogeria!
Germà, estic a punt de tornar-me boig amb tot això. Seguiré pregant amb l'esperanca que Déu ho resolgui tot en la seva infinita bondat.
Encara que vulgui evitar-ho, les supersticions dels vilatans fan que la meva fe flaquegi.
Sento que estar entre aquests murs sagrats reforca la meva fe per moments.
# 235
Moltes gràcies, Fray Gonzalo, m'ha donat molta pau saber tota la història.
Ja descansen en tombes contigues. Ara només queda que els diguem una missa, les seves ànimes descansaran i Fray Cesáreo tornarà en si.
Això serà si jo ho permeto. La meva ànima està condemnada però la d'ells dos mai descansarà!
Esperit maligne, torna a l'abisme del que mai vas haver sortit.
Oh, no! Em fonc, em fonc!!!
# 240
Finalment el mal va vèncer.\0D\0DFra Gonzalo, el capellà i el boticari van morir al cementiri i les seves ànimes, al costat de la de Fra Cesáreo van quedar vagant per la zona.\0D\0DLa maledicció va envair el poblet i va quedar abandonat pels segles.
Per fi hem aconseguit que les seves ànimes descansin, i ara la meiga ha desaparegut per sempre.
I Fray Cesáreo? Haurà despertat?
Anem corrent a casa a veure!
Fray Cesáreo, Lloat sigui Déu! Ha despertat!
# 245
Estic una mica marejat però estic bé. Quant he dormit?
Molt. I s'ha perdut una bona. Se la explicarem durant el sopar.
Quina gran història, Fray Gonzalo. Ja sabia jo que aquests boscos amagaven grans perills.
El que compta és que al final tot ha sortit bé.\0DAnem al llit, que el sopar ha estat opípar i l'orujo del boticario és realment fort.
Fray Gonzalo, anem, ja és molt tard, hem d'emprendre el camí.
# 250
Eh? Vaja, m'he adormit, l'orujo d'ahir a la nit... he somiat amb Brunilda... La seva ànima m'ha tornat a visitar en somnis...
I després d'aquestes paraules, els dos frares van continuar la seva marxa cap a Santiago, i qui sap quines noves aventures trobarien en el Camí...\0D\0DEl final?
Fray Cesáreo, Brunilda, la història de Brunilda, vostè inconscient, Antón, les seves ànimes, la meiga...
Però, de què parla? Ahir a la nit vam arribar aquí i vam sopar amb el boticari, vostè es va prendre uns quants orujos que ja veig que li van produir malsons. No ha d'abusar de l'alcohol, germà... JaJa
Llavors...
# 255
Llavors res. Vinga germà, reprenguem la marxa que encara queda molt per arribar a Santiago.
#--------------------------------------------------------------------------------------------------------------------------------
# TABLA 2
#--------------------------------------------------------------------------------------------------------------------------------
L'habitació de la majordoma, al contrari que la resta de la casa, està molt desordenada: Ni tan sols ha tancat el seu armari, per exemple.
Necessito la clau de l'ala oest.
No, pare. De cap manera. No puc deixar-li la clau de l'ala oest, i menys després de tot el que ha passat.
En aquest prestatge predominen els llibres de medecina.
La química és la matèria que ocupa aquesta prestatgeria.
#  5
Pel que es veu, el boticari està molt interessat en les matemàtiques.
La botànica també forma part de les matèries estudiades per Don Justiniano.
En aquest prestatge troben el seu lloc narracions històriques.
Llibres religiosos ocupen les baldes d'aquesta prestatgeria.
Aquesta sembla l'habitació de Brunilda.
#  10
Antigues novel.les troben el seu lloc aquí. Es veu que fa molt que ningú les llegeix.
Les cartes hi són a dins. Ara trobi qui l'ajudi a obrir el cofre.