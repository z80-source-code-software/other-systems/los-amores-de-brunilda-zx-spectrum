TILESET_1

org $e000

TILESET1_1:
     include "../gfx/asm/tileset01_01.asm"

org $e000 + 32*8
TILESET1_2:
     include "../gfx/asm/tileset01_02.asm"

org $e000 + 64*8
TILESET1_3:
     include "../gfx/asm/tileset01_03.asm"

org $e800
TILESET1_TILESET:
     include "../gfx/asm/tileset01_tileset.asm"
     

END TILESET_1