; E40C0008F8

;
; Os amores de Brunilda e Ant�n
;
; Code by Luis I. Garc�a Ventura a.k.a. 'Benway'
;
; Copyleft 2012 RetroWorks
;
; --------------------------------------------------------------------------------------------
;include "version.asm"
include "sym/light_table.sym"
;include "sym/page1.sym"
include "sym/page3.sym"
include "sym/page4.sym"
; --------------------------------------------------------------------------------------------

OABEA

org $8101
; --------------------------------------------------------------------------------------------
WAITFORKEY_PAGE4:
;     ld b, 0
     call SETRAMBANK0

     call WAITFORKEYR

     ld b, 4
     call SETRAMBANK
     ret

include "keyboard.asm"
include "data.asm"
include "memmanagement.asm"
include "mapmanagement.asm"
include "scriptrunner.asm"

WP_PLAY_SONG:
     push af
     ld b, 6
     call SETRAMBANK

     call MUTE_PLAYER

     pop af
     call UNPACK_SONG
     xor a
     call LOAD_SONG
;     ld b, 0
     jp SETRAMBANK0

WP_PLAY_SFX:
     push af
     ld b, 6
     call SETRAMBANK

     pop af
     call LOAD_SFX
;     ld b, 0
     jp SETRAMBANK0

WP_STOP_SOUND:
     ld b, 6
     call SETRAMBANK

     call MUTE_PLAYER

;     ld b, 0
     jp SETRAMBANK0

DRAW_BITMAP:
     call SETRAMBANK
     ld de, $4000
     call DEEXO
;     ld b, 0
     jp SETRAMBANK0

DRAW_CBITMAP
     push bc
     push de
     ld d, a
     ld b, 1
     call SETRAMBANK
     ld a, d
     ld hl, COMPRESSED_BMPS
     call GET_WORD_TABLE
     ld hl, MAP
     ex de, hl
     call DEEXO
;     ld b, 0
     call SETRAMBANK0

     pop de
     pop bc
     ld hl, MAP
     call DRAWGFX

     ret

SHOW_MESS2:
      push bc
	 ld b, 4
	 call SETRAMBANK
      pop bc
	 ld d, 0
	 call NAME 
;	 ld b, 0
	 jp SETRAMBANK0

SIGNS_TABLE_SIZE equ 150 ; (149)
SIGNS_TABLE:
    defs SIGNS_TABLE_SIZE, $ff

DOOR_TABLE:
    defs 379;450;512

; -----------------------------------------------------------------------------------------------------------------------------
INIT:
     ld sp, STACK_POINTER
     xor a
     call ESPIRAL

     ld hl, FLAGS
     ld de, FLAGS+1
     ld bc, MAX_FLAG - 1
     ld (hl), 0
     ldir

     ld hl, OBJ_DATA
     ld de, OBJ_DATA+1
     ld bc, BYTES_OBJ*OBJ_NUM-1
     ld (hl), $ff
     ldir

     ld hl, INVENTORY
     ld de, INVENTORY+1
     ld bc, 7
     ld (hl), $ff
     ldir

     ld hl, FLAGS+1
     set 7, (hl)
     ld b, 60
     call RUNSCRIPT

;ld hl, FLAGS+11
;ld (hl),5
;inc hl
;ld (hl),4
;call LETS_FIGHT
;

;kkk0
     ;ld hl, FLAGS+11
     ;ld (hl),1
     ;inc hl
     ;ld (hl), $ff
     ;ld b, 6
;kkk1
     ;ld hl, FLAGS+12
     ;ld a, (hl)
     ;inc a
     ;and 7
     ;cp 4
     ;jp c, kkk2
     ;ld a, 4
;kkk2
     ;ld (hl), a
     ;push bc
     ;call LETS_FIGHT
     ;pop bc
     ;djnz kkk1
     ;jp kkk0


     call RUN_MENU
     ld hl, BUFFER_1
     ld (BUFFER_ADDR), hl
     ld (LAST_BUFFER_POINTER), hl

     call c, ENTER_PWD
     call ESPIRAL
     call CLS

     ld b, 0
     call RUNSCRIPT
     
ENDLESS_LOOP:
     ld hl, EL_HOOK
     push hl
     ld hl, (CONTROL_PTR)
     jp (hl)
     
     EL_HOOK:

     ld a, (JOYSTICK)
     bit 0, a
     call nz, MOVE_RIGHT

     ld a, (JOYSTICK)
     bit 1, a
     call nz, MOVE_LEFT

     ld a, (JOYSTICK)
     bit 2, a
     call nz, MOVE_DOWN

     ld a, (JOYSTICK)
     bit 3, a
     call nz, MOVE_UP

     ld a, (JOYSTICK)
     bit 4, a
     call z, FIRE_NOTPRESSED
     call nz, FIRE_PRESSED

     ld a, KEY_G
     call KTEST1
     ld b, 64
     call nc, RUNSCRIPT
;
         ;ld a, KEY_N
         ;call KTEST1
         ;ld a, 0
         ;call nc, SET_LIGHT
;
         ;ld a, KEY_D
         ;call KTEST1
         ;ld a, 255
         ;call nc, SET_LIGHT
;
          ;ld a, KEY_H
          ;call KTEST1
          ;ld a, 1
          ;call nc, SET_LIGHT
;
          ;ld a, KEY_T
          ;call KTEST1
          ;ld a, -1
          ;call nc, CHANGE_ENERGY
;
          ;ld a, KEY_Y
          ;call KTEST1
          ;ld a, 1
          ;call nc, CHANGE_ENERGY
;
     ld hl, PLAYER_CANMOVE
     ld (hl), $1

     call CHECK_ENERGY_VARIATION              ; Hay zonas de mapa que var�an la "fe" per se

     call MOVE_SPRITES
     call UPDATE_ALL

     ld a, (FLAGS+4)
     cp 4
     jr nz, ENDLESS_LOOP

     ld ix, PRINCIPAL_SPR
     ld de, FGON

     ld a, (ix+1)
     add a, (ix+3)
     ld b, a
     ld c, (ix+2)
     call WHATSUPERTILEAT
     cp 1
     jr nz, LOOP_EXIT

     ld de, ESCALO

     LOOP_EXIT:

     ld (ix+8), e
     ld (ix+9), d

     jp ENDLESS_LOOP

; --------------------------------------------------------------------------------------------
; --------------------------------------------------------------------------------------------
UPDATE_ALL:
     halt
     ld a, (TICKS)
     bit 2, a
     jr z, UPDATE_ALL

     ld hl, CYCLES
     inc (hl)

     call ANIMATE_TILES
     call MAP2BUFFER
     xor a
     ld (TICKS), a

     call SHOW_SPRITES
     call SHOW_OBJECTS
     jp DUMP_BUFFER

;Sumamos A al contador de energ�a, y lo mostramos
CHANGE_ENERGY:
     ld b, 55
     ld hl, FLAGS+3
     add a, (hl)
     jp z, RUNSCRIPT
;     jp c, RUNSCRIPT
     cp 33
     ret nc
     ld (hl), a

     dec hl
     dec hl
     set 7, (hl)
     
     ld b, 30
     call RUNSCRIPT


SHOW_ENERGY:
     ld hl, FLAGS+3
     ld a, (hl)

;     ld b, 55
;     and a
;     jp z, RUNSCRIPT
;     bit 7, a
;     jp nz, RUNSCRIPT

SHOW_ENERGY_:

     ld h, $52
     call SE_CLEAR_LDIR
     inc h
     call SE_CLEAR_LDIR
     inc h
     call SE_CLEAR_LDIR


     ld hl, ONE_OR_TWO_PIXELS+1
     ld (hl), 128+64
     dec a
     ld b, a
     push bc
     call SE_PIXEL

     pop bc
     ld a, b
     cp 31
     jr nz, SE_HOOK
     ld a, 30
     ld b, a
     ld hl, ONE_OR_TWO_PIXELS+1
     ld (hl), 128+64
     jr SE_PIXEL
     
     SE_HOOK:
     
     and 7
     cp 7
;     cp 31
     ret nz


     ld a, b
     ld hl, ONE_OR_TWO_PIXELS+1
     ld (hl), 128

     inc b
     inc a
     
     SE_PIXEL:

     ld h, $52
;     srl a
;     srl a
;     srl a
     rrca
     rrca
     rrca
     and 31
     add a, $a4
     ld l, a

     ld a, b
     and 7
     ld b, a
     xor a

     or b

     ONE_OR_TWO_PIXELS:
     ld a, 0;128+64
     jr z, SE_DRAWPIXEL


     SE_PIXEL_LOOP:
     and a
     rra
     djnz SE_PIXEL_LOOP

     SE_DRAWPIXEL:

     ld (hl), a
     inc h
     ld (hl), a
     inc h
     ld (hl), a

     ret

SE_CLEAR_LDIR:
     ld d, h
     ld l, $a4
     ld e, $a5
     ld bc, 3
     ld (hl), 0
     ldir
     ret


SHOW_MARCADOR:
      ld hl, FLAGS+1
      bit 7, (hl)
      res 7, (hl)
      ret nz
     
;      ld b, 1
;      call SETRAMBANK

      ld hl, MARCADOR
      ld de, $2005
      ld bc, $1300
      call DRAWGFX
;      ld b, 0
;      call SETRAMBANK0
      call SHOW_ENERGY
      jp DRAW_INVENTORY

SHOW_PERGAMINO:
      ld b, 1
      call SETRAMBANK
      ld hl, PERGAMINO
      ld de, $2005
      ld bc, $1300
      call DRAWGFX

      ld b, 4
      call SETRAMBANK

      ld hl, TEXT_ATTR
      ld (hl), STANDARD_ATTR
;      xor a
;      ld (LINES_IN_A_ROW), a

;      ld b, 0
      call SETRAMBANK0

      ld hl, $5a60
      ld bc, $0405

      SP_LOOP0:

      ld (hl), 0
      inc hl
      djnz SP_LOOP0

      ld b, 28
      SP_LOOP1:
      ld (hl), PAPER_YELLOW
      inc hl
      djnz SP_LOOP1

      ld b, 4
      dec c
      jr nz, SP_LOOP0

      ret

FIRE_NOTPRESSED:
      xor a
      ld (ISFIREPRESSED), a
      ret

FIRE_PRESSED:
      ld hl, ISFIREPRESSED
      ld a, (hl)
      and a
      ret nz
      inc (hl)
      
      ld iy, SIGNS_TABLE-4

      FIRE_PRESSED_HOOK:

      ld ix, PRINCIPAL_SPR
      ld c, (ix+2)				; Coordenada X (en ST)
      ld b, (ix+1)				; Coordenada Y (en ST)

      ld a, (ix+5)				; Facing + Fotograma

      ld h, a
      ld l, 0

      ; Facing: 0 (00) = izquierda; 1 (01) = derecha ; 2 (10) = Abajo; 3 (11) = Arriba
      ; Es decir, el bit 1 solo nos dice si el facing es horizontal (0) o vertical (1)

      and 1
      add a, a
      dec a						; Ahora A = a*2-1 : Lo que hay que a�adir a la coordenada de ST horizontal

      bit 1, h
      jr nz, FP_FACVERT_HOOK

      add a, c
      ld c, a
      jr FP_HOOK

      FP_FACVERT_HOOK:
      neg                                       ; Este NEG es porque si miramos hacia arriba a vale +1 y abajo -1, y deber�a ser al rev�s
      jp p, IM_LOOKING_DOWN                     ; Si miramos hacia "abajo", nos da igual el offset
      add a, (ix+3)				; Si est�s a medias en un tile, tienes que mirar ese mismo tile (Cuando miramos hacia arriba, si es hacia abajo, es el tile siguiente)
      IM_LOOKING_DOWN:
      add a, b
      ld b, a


      FP_HOOK:
      push iy
      push bc
      call CHECK_SPRITES_INTERACT  ; Llamamos aqu� a CHECK_SPRITES_INTERACT para que tengan prioridad los sprites sobre los objetos si est�n en el mismo ST
      pop bc
      pop iy
      
      FP_HOOK1:


      ld de, 4
      add iy, de

      ld a, (iy+0)
      cp $ff                          ; Has terminado la tabla??? 
      ret z                           ; Pues vu�lvete ya

      cp c
      jr z, FP_CHECK_VERT

      bit 1, (ix+5)             ; El facing es vertical??? (Hacia arriba o hacia abajo)
      jr z, FP_HOOK1             ; Si no sigue mirando en la tabla
      bit 0, (ix+4)             ; Hay offset X? (estamos entre dos supertiles?)
      jr z, FP_HOOK1             ; Si no lo hay, sigue mirando en la tabla


      dec a                     ; Hay que comprobar la casilla de m�s a la derecha
      cp c                      ; Aqu� hay alg�n "punto marcado" como mensaje / Secuencia???
      jr nz, FP_HOOK1            ; Si ya no lo hay aqu�, hay que seguir mirando la tabla

      FP_CHECK_VERT:

      ld a, (iy+1)
      cp b
      jr nz, FP_HOOK1

      ld c, (iy+0)
      call WHATSUPERTILEAT
      ex af, af'

      ld a, (iy+2)
      cp $ff
      ld b, (iy+3)
      jp z, RUNSCRIPT

      FP_MESSAGE:

      call SHOW_PERGAMINO
      ld bc, $1400
      ex af, af'
      call DRAW_STILEx2

	  ld de, MESS
	  ld (MC_FUNC_HOOK+1), de

      ld e, (iy+2)
      ld bc, TEXT_INIT
      call MESS_CALLER

      call KEY_AFTERTEXT

      jp SHOW_MARCADOR


MOVE_RIGHT:
     ld ix, PRINCIPAL_SPR
     call SPRITE_RIGHT
     ret c                       ; Si no te has movido (porque hay obst�culo), te vuelves ya

     ld a, 1
     call FOLLOW_ME

     ld hl, PLAYER_CANMOVE
     ld (hl), 0

     ld a, (CAMERA_OFFSET_X)
     and a
     jp nz, CAMERA_RIGHT           ; Si hay offset de c�mara, podemos moverla sin comprobar el siguiente supertile: ya lo tenemos "a medias"

     ld a, (CAMERA_X)
     ld b, a
     ld a, (PRINCIPAL_SPR+2)
     sub b
     cp 9                         ; Si no est�s en el centro
     jp c, ERASE_BORDER          ; te vuelves

     ld a, (CAMERA_X)
     add a, ST_ANCHO
     ld c, a                       ; c = primer caracter del tile siguiente al del que hay m�s a la derecha de la pantalla
     ld a, (CAMERA_Y)
     ld b, a

     call WHATSUPERTILEAT          ; Mira a ver que hay ah�

     and a                         ; Y si hay un 0
     jp z, ERASE_BORDER            ; Te vuelves ya (No haces scroll)

     jp CAMERA_RIGHT             ; Scroll!


MOVE_LEFT:
     ld ix, PRINCIPAL_SPR
     call SPRITE_LEFT
     ret c

     xor a
     call FOLLOW_ME

     ld hl, PLAYER_CANMOVE
     ld (hl), 0

     ld a, (CAMERA_OFFSET_X)
     and a
     jp nz, CAMERA_LEFT           ; Si hay offset de c�mara, podemos moverla sin comprobar el siguiente supertile: ya lo tenemos "a medias"

     ld a, (CAMERA_X)
     ld b, a
     ld a, (PRINCIPAL_SPR+2)
     sub b

     cp 7                         ; Si no est�s en el centro
     jp nc, ERASE_BORDER          ; te vuelves

     ld a, (CAMERA_X)
     dec a
     ld c, a                       ; c = primer caracter del tile siguiente al del que hay m�s a la izquierda de la pantalla
     ld a, (CAMERA_Y)
     ld b, a

     call WHATSUPERTILEAT          ; Mira a ver que hay ah�

     and a                         ; Y si hay un 0
     jp z, ERASE_BORDER            ; Te vuelves ya (No haces scroll)

     jp CAMERA_LEFT

MOVE_DOWN:
     ld ix, PRINCIPAL_SPR
     call SPRITE_DOWN
     ret c                         ; Si no te has movido (porque hay obst�culo), te vuelves ya

     ld a, 2
     call FOLLOW_ME

     ld a, (CAMERA_OFFSET_Y)
     and a
     jp nz, CAMERA_DOWN           ; Si hay offset de c�mara, podemos moverla sin comprobar el siguiente supertile: ya lo tenemos "a medias"

     ld a, (CAMERA_Y)
     ld b, a
     ld a, (PRINCIPAL_SPR+1)
     sub b

     cp 4                         ; Si no est�s en el centro
     jp c, ERASE_BORDER          ; te vuelves

     ld a, (CAMERA_X)
     ld c, a
     ld a, (CAMERA_Y)
     add a, ST_ALTO
     ld b, a                       ; b = primer caracter del tile siguiente al del que hay m�s abajo de la pantalla

     call WHATSUPERTILEAT          ; Mira a ver que hay ah�

     and a                         ; Y si hay un 0
     jp z, ERASE_BORDER            ; Te vuelves ya (No haces scroll)

     jp CAMERA_DOWN               ; Scroll!


MOVE_UP:
     ld ix, PRINCIPAL_SPR
     call SPRITE_UP
     ret c                         ; Si no te has movido (porque hay obst�culo), te vuelves ya

     ld a, 3
     call FOLLOW_ME

     ld a, (CAMERA_OFFSET_Y)
     and a
     jp nz, CAMERA_UP              ; Si hay offset de c�mara, podemos moverla sin comprobar el siguiente supertile: ya lo tenemos "a medias"

     ld a, (CAMERA_Y)
     ld b, a
     ld a, (PRINCIPAL_SPR+1)
     sub b

     cp 4                         ; Si no est�s en el centro
     jp nc, ERASE_BORDER          ; te vuelves

     ld a, (CAMERA_X)
     ld c, a
     ld a, (CAMERA_Y)
     dec a
     ld b, a                       ; b = primer caracter del tile siguiente al del que hay m�s arriba de la pantalla

     call WHATSUPERTILEAT          ; Mira a ver que hay ah�

     and a                         ; Y si hay un 0
     jp z, ERASE_BORDER            ; Te vuelves ya (No haces scroll)

     jp CAMERA_UP               ; Scroll!

MAP_EXIT
      ld a, (ix)              ; Solo sale del mapa el personaje jugador
      and a
      ret nz

      ld a, (FLAGS)
      ld b, 1
      cp 3
      jp c, RUNSCRIPT
      ret z

      ld a, (FLAGS+4)
      and a
      jp nz, KNOCKING_ON_A_DOOR

      ld a, (ix+2)                 ; Estamos en el mapa 1, vamos a ver la x
      cp 61
      jp z, KNOCKING_ON_A_DOOR

      ld b, 5
      jp RUNSCRIPT                 ; No podemos irnos y dejar abandonado a FCes


CHECK_COLISION:
      pop hl              ; Nos bajamos la direcci�n de retorno a HL: Esto har� que un RET vuelva al CALL anterior

      call WHATSUPERTILEAT

      and a                             ; Si es 0, nos salimos del mapa!!!
      jp z, MAP_EXIT
      
      cp 64                             ; > 64 = Colisi�n directa
      ccf
      ret c

      cp 32                                  ; Si es menor que 32
      jr nc, KNOCKING_ON_A_DOOR               ; Nos volvemos, pero no a SPRITE_DONDESEA, sino a MOVE_DONDESEA

      ccf
      jp (hl)                                ; Solo volvemos (a SPRITE_XXX) si es un ST < 32
                                             ; Si es > 32 hay o colisi�n, o salto en el mapa


KNOCKING_ON_A_DOOR:
      ld iy, DOOR_TABLE

      KNOCKING_LOOP:

      ld a, (iy)
      cp $ff
      scf
      ret z

      cp (ix+2)
      jp z, KNOCKING_CHECK2

      KNOCKING_LOOP_HOOK1:
      ld de, 7
      add iy, de
      jr KNOCKING_LOOP

      KNOCKING_CHECK2:

      ld a, (iy+1)
      cp (ix+1)
      jr nz, KNOCKING_LOOP_HOOK1

      ld a, (iy + 2)
      cp 2
      jr z, KNOCKING_LOOP_HOOK2                 ; Si hay un 2 en el "offset" horizontal es porque da igual
      cp (ix + 4)
      jr nz, KNOCKING_LOOP_HOOK1

      KNOCKING_LOOP_HOOK2:

      ld a, (iy + 3)
      and 3
      cp 2
      jr z, KNOCKING_LOOP_HOOK3                 ; Si hay un 2 en el "offset" vertical es porque da igual
      cp (ix + 3)
      jr nz, KNOCKING_LOOP_HOOK1

      KNOCKING_LOOP_HOOK3:

      ld a, (ix)
      and a
      jr nz, K_NOSONG
      ld a, (iy + 3)
      and 252
      jr z, K_NOSONG
;      srl a
;      srl a
        rrca
        rrca
      ld e, a
      push iy
      push ix
      call WP_PLAY_SONG
      pop ix
      pop iy

      ; Ya s� que es colisi�n con la puerta que corresponde en la tabla, ahora hay que ir al punto de destino

      K_NOSONG:

      ld a, (iy + 5)
      cp $ff                       ; Si la coordenada Y es $ff, salta a una escena, no a una puerta
      jr nz, NOT_SCENE

      ld a, (ix)                    ; Si no es el jugador, tiene que seguir mirando en la tabla
      and a                         ; Solo el jugador hace saltar los scripts
      jr nz, KNOCKING_LOOP_HOOK1    ; As� que hay que duplicar las entradas en la tabla de donde pueda pasar FCes

      ld b, (iy+6)            ; La escena n�mero (lo que est� en la c�mara X)
      push bc
      push iy
      call UPDATE_ALL
      pop iy
      pop bc
      call RUNSCRIPT
      jr KNOCKING_LOOP_HOOK1

      NOT_SCENE:
      ld (ix+1), a
      ld a, (iy + 6)
      ld (ix+2), a

      ld a, (ix+0)
      and a
      ret nz

      call FADE_OUT

      ld a, (iy + 4)
      push af
      call SHOW_MARCADOR
      pop af
      ld hl, FLAGS+4
      cp (hl)
      jp z, AUTOCAM

      call LOAD_MAP
      jp FADE_IN

 CALC_MAP_ADDR:
      ld hl, MAP

      ld a, (MAP_WIDTH)
      ld e, a
      ld d, 0

      ld a, (CAMERA_Y)
      and a
      jr z, CMA_HOOK

      CMA_LOOP:
      add hl, de
      dec a
      jr nz, CMA_LOOP

      CMA_HOOK:
      ld a, (CAMERA_X)
      ld e, a
      ld d, 0
      add hl, de

      ld (MAP_OFFSET_ADDR), hl
      ret

;SET_LIGHT:
;      ld (FLAGS+6), a
;      xor a
;      call CLS
;      call RENEW_BUFFER
;      jp SHOW_MARCADOR

CHECK_ENERGY_VARIATION:
;      ld ix, PRINCIPAL_SPR

      ld a, (FLAGS+4)
      cp 3
      jr z, ERMITA
      and a
      ret nz

      ld a, (PRINCIPAL_SPR+1)
      cp 60
      ret c
      ld a, (PRINCIPAL_SPR+2)
      cp 18
      ret nc
      jr CEV_DOIT

      ERMITA:
      ld a, (PRINCIPAL_SPR+2)
      cp 68
      ret c
      ld a, (PRINCIPAL_SPR+1)
      cp 44
      ret c
      cp 54
      ret nc

      CEV_DOIT:

      ld a, (CYCLES)
      and 31
      ret nz
      
      ;ld hl, FLAGS+3
      ;ld a, (hl)
      ;cp 32
      ;ret z
      ;inc (hl)
      ;jp SHOW_ENERGY

      ld a, 1
      jp CHANGE_ENERGY


; --------------------------------------------------------------------------------------------
include "screen.asm"
include "objects.asm"

include "spritesmanager.asm"
include "sprite.asm"
include "camera.asm"
include "fight.asm"

IF COMPILE_VERSION=VERSION_NELO
include "../gfx/asm/FGon_NQ.asm"
include "../gfx/asm/FCes_NQ.asm"
ELSE
include "../gfx/asm/FGon.asm"
include "../gfx/asm/FCes.asm"
ENDIF

include "../gfx/asm/ESCALO.asm"
include "../gfx/asm/XAN.ASM"
include "../gfx/asm/BORRACHO.ASM"
include "../gfx/asm/CURA.asm"
include "../gfx/asm/AMA.asm"
include "../gfx/asm/BOT.asm"
include "../gfx/asm/MEIGA.ASM"
include "../gfx/asm/PAISANO.ASM"
include "../gfx/asm/BRUNILDA.ASM"
include "../gfx/asm/piedra.asm"
include "../gfx/asm/EXPLODE.asm"
include "../gfx/asm/ENEMIES.asm"

include "../gfx/asm/OBJECTS.asm"

include "buffer.asm"
include "menu.asm"

MARCADOR:
include "../gfx/asm/marcador.asm"


; --------------------------------------------------------------------------------------------

END OABEA
