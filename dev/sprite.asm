; IX = Inicio Sprite
SHOW_SPRITE:
     call SHOWSPR_CALC

     SHOW_SPRITE_HOOK:
     call SHOWSPR_PRINT
     inc c
     jp SHOWSPR_PRINT
          
SHOW_SPRITE2:
     call SHOWSPR_CALC
     
     ld hl, 16
     add hl, de
     ex de, hl                     ; En DE tenemos el puntero al gr�fico. Como dibujamos los dos �ltimos caracteres, hay que sumar 16 bytes
     
     exx
     inc hl                        ; Igual con HL': Hay que sumar dos bytes para los dos atributos que NO imprimimos ahora
     inc hl
     exx
         
     inc b                         ; Una fila m�s abajo
     jr SHOW_SPRITE_HOOK

SHOWSPR_CALC_PRE:
     ; Las coordenadas en pantalla del sprite ser�an:
     ; Dimensi�n ST*(Coordenadas en ST - C�mara (ST)) + Offset Sprite - Offset C�mara

     ld hl, (CAMERA_Y)        ; 16

     ld a, (ix+1)             ; 19     A la coordenada ST Y
     sub l                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos (ST de 2 x 2 tiles)
     add a, (ix+3)            ; 19     Le sumas el Offset del Sprite
     ld b, a                  ;  4
     ld a, (CAMERA_OFFSET_Y)
     neg
     add a, b                      ;   Le restas el Offset de C�mara
     ld b, a                       ;   Y lo pones en B

     ld a, (ix+2)             ; 19     A la coordenada ST X
     sub h                    ;  4     Le restas la c�mara en ST
     add a, a                 ;  4     Lo multiplicas por dos
     add a, (ix+4)            ; 19     Le sumas el Offset del Sprite
     ld c, a                  ;  4
     ld a, (CAMERA_OFFSET_X)
     neg
     add a, c                      ;   Y le restas el Offset de C�mara
     ld c, a                       ;   Para ponerlo en C

     ; Ahora tenemos en BC las coordenadas "de pantalla" (sin marco) del sprite de IX
     ret

SHOWSPR_CALC:
     call SHOWSPR_CALC_PRE

     ; Como vamos a escribir directamente en la pantalla, necesitamos que el b�ffer no toque all� donde vamos a escribir
     ; por eso "validamos" los tiles implicados.
     ; Esto tiene la ventaja de que no usamos "tiles" para los sprites, aumentando el n�mero de "tiles" que nos quedan disponibles
     ; ya que el m�ximo que tenemos con este sistema es 256

     ld e, (ix+6)             ; 19
     ld d, (ix+7)             ; 19      ; En DE cogemos el PUNTERO AL GR�FICO A DIBUJAR

     exx
     ld a, (ix + 0)
     add a, a
     add a, a
     ld d, 0
     ld e, a
     ld hl, COLOR_SPRITES
     add hl, de                         ; En HL' tenemos el PUNTERO A LOS ATRIBUTOS A PONER
     exx
     
     ret
     
; SHOWSPR_PRINT: Imprime en las coordenadas BC el car�cter se�alado por HL y le pone los atributos se�alados por DE'
; Si las coordenadas BC est�n fuera de la pantalla no imprime
; Tanto imprima como no imprima, a la salida HL vale +8 y DE' +1

SHOWSPR_PRINT:
     ld a, c
     cp ST_ANCHO * ANCHO_ST
     jp nc, SHOWSPR_NOPRINT

     ld a, b
     cp ST_ALTO * ALTO_ST
     jp nc, SHOWSPR_NOPRINT

     call VALIDATE_TILE

     jr c, SHOWSPR_NOPRINT              ; Si volvemos con carry al validar el tile, es que ya estaba validado de antes, luego no hay que pintar nada

     inc b
     inc c

     call CHECK_EYES
    
     call PRINT
     call ATT_LOCATE
     
     dec b
     dec c

     push hl               ; en HL tenemos la direcci�n de atributos EN LA PANTALLA
     push de
     call GET_ATTR_AT      ; Al llamar aqu�, nos traemos a HL la direcci�n de atributos EN EL BUFFER DE ATRIBUTOS
     pop de
     exx
     ld a, (hl)            ; Cogemos en A el atributo a poner
     inc hl                ; Y aumentamos el puntero
     exx

     ld (hl), a            ; Ponemos en el BUFFER DE ATTRIBUTOS el valor que sea
     pop hl                ; Recuperamos el valor de ATRIBUTOS DE PANTALLA

     ex af, af'
     ld a, (FLAGS+6)
;     cp 1
     dec a
     ret z                 ; Si es "de noche", nos volvemos ya
;     and a
     inc a
     jr z, SPR_NIGHT_COLORS
     ex af, af'

     bit 7, a
     jp nz, TRANSPARENT_PAPER  ; Si est� activo el bit de flash, hay que dejar el color de papel que hubiera en ese ST

     ld (hl), a            ; Si es "de d�a" imprimimos directamente en la pantalla tambi�n
     ret                   ; porque si no, al escrollar no lo hace lo suficientemente r�pido, y queda un poco feo

SPR_NIGHT_COLORS:
     ld (hl), 5
     ret

TRANSPARENT_PAPER:
     ex af, af'
     ld a, (hl)
     and $ff-7-64
     ld (hl), a
     ex af, af'
     and 7;+64
     or (hl)
     ld (hl), a
     ret

CHECK_EYES:
     ld a, (FLAGS+6)
;     cp 1
     dec a
     ret nz
     call ATT_LOCATE
     ld a, (hl)
     cp 2
     ret nz
     dec b
     dec c
     pop af         ; Para volver una m�s atr�s
SHOWSPR_NOPRINT:
     exx
     inc hl          ; Aumenta el puntero de los atributos a poner al siguiente car�cter
     exx

     ld a, 8         ; 7 ; Aumenta el puntero del gr�fico del sprite al siguiente car�cter
     add a, e        ; 4
     ld e, a         ; 4
     ld a, 0         ; 7
     adc a, d        ; 4
     ld d, a         ; 4

     ret



; IX -> Base Sprite
SPRITE_RIGHT:
      ld b, 1
SPRITE_RIGHT_1:
      call SPRITE_FACING

      ; PARA OBJETOS ===================================================
      call PRECHECK_IGO
      ld a, (ix + 4)
      xor 1
      add a, l
      ld l, a
      push ix
      call CHECK_IF_GETTING_OBJECTS
      pop ix
      ; ================================================================

      SPR_AF:
      ld b, 1
      call SPRITES_COLLIDE

      ld a, (ix+4)            ; Offset de tiles horizontal del sprite
      and a
      jp nz, SR_HOOK1          ; Si no es 0, ya estamos metidos en un nuevo ST (que no generaba colisi�n), as� que no hay que comprobar nada

      ld b, (ix+1)              ; Coordenada Y del sprite: Comprobamos colisi�n sobre la misma altura donde estamos (metidos con offset de tile)
      ld c, (ix+2)            ; Coordenada X del Sprite

      ld a, (MAP_WIDTH)       ; Vamos a comprobar que no hallamos llegado al l�mite del mapa
      dec a
      cp c
      jp z, MAP_EXIT           ; Y si hemos llegado, nos volvemos

      inc c                   ; El siguiente ST a la derecha

      ld a, (ix+3)            ; Offset tiles vertical
      add a, b                ; Le sumamos el offset: Si estamos "a medias" en vertical, tiene que comprobar el tile de abajo, no el de arriba
      ld b, a

      call CHECK_COLISION

      jp SR_HOOK2

      SR_HOOK0:                ; Repito esto aqu� para tener un punto de entrada alternativo sin revisar colisiones (Para usarlo en los scripts)

      ld a, (ix+4)            ; Offset de tiles horizontal del sprite
      and a
      jp z, SR_HOOK2          ; Si es 0, no hay que aumentar el ST, sino el offset

      SR_HOOK1:

      inc (ix+2)                 ; Nos metemos en el siguiente supertile

      SR_HOOK2:

      ld a, (ix+4)               ; Ponemos lo contrario que hubiera en el offset horizontal del sprite
      xor 1
      ld (ix+4), a

      jp SPRITE_FRAME           ; Actualiza el gr�fico y vu�lvete


; IX -> Base Sprite
SPRITE_LEFT
      ld b, 0
SPRITE_LEFT_1:
      call SPRITE_FACING

      ; PARA OBJETOS ===================================================
      call PRECHECK_IGO
      dec l
      push ix
      call CHECK_IF_GETTING_OBJECTS
      pop ix
      ; ================================================================

      SPL_AF:
      ld b, 0
      call SPRITES_COLLIDE

      ld a, (ix+4)            ; Offset de tiles horizontal del sprite
      and a
      jp nz, SL_HOOK1          ; Si no es 0, ya estamos metidos en un nuevo ST (que no generaba colisi�n), as� que no hay que comprobar nada

      ld b, (ix+1)              ; Coordenada Y del sprite: Comprobamos colisi�n sobre la misma altura donde estamos (metidos con offset de tile)
      ld c, (ix+2)            ; Coordenada X del Sprite

      ld a, c                 ; Vamos a comprobar que no hayamos llegado al l�mite del mapa
      and a
      jp z, MAP_EXIT           ; Y si hemos llegado, nos volvemos

      dec c                   ; El siguiente ST a la izquierda

      ld a, (ix+3)            ; Offset tiles vertical
      add a, b                ; Le sumamos el offset: Si estamos "a medias" en vertical, tiene que comprobar el tile de abajo, no el de arriba
      ld b, a

      call CHECK_COLISION

      SL_HOOK0:                    ; Esto se repite aqu� para tener un punto de entrada alternativo donde no mire la colisi�n (para usarlo en los scripts)
      ld a, (ix+4)            ; Offset de tiles horizontal del sprite
      and a
      jp nz, SL_HOOK1          ; Si es 1, no hay que decrementar el ST, sino el offset

      dec (ix+2)                 ; Nos metemos en el siguiente supertile

      SL_HOOK1:

      ld a, (ix+4)               ; Ponemos lo contrario que hubiera en el offset horizontal del sprite
      xor 1
      ld (ix+4), a

      jp SPRITE_FRAME           ; Actualiza el gr�fico y vu�lvete


; IX -> Base Sprite
SPRITE_DOWN:
      ld b, 2
SPRITE_DOWN_1:
      call SPRITE_FACING

      ; PARA OBJETOS ===================================================
      call PRECHECK_IGO
      ld a, (ix + 3)
      xor 1
      add a, h
      ld h, a
      push ix
      call CHECK_IF_GETTING_OBJECTS
      pop ix
      ; ================================================================

      SPD_AF:
      ld b, 2
      call SPRITES_COLLIDE

      ld a, (ix+3)            ; Offset de tiles vertical del sprite
      and a
      jp nz, SD_HOOK0          ; Si no es 0, ya estamos metidos en un nuevo ST (que no generaba colisi�n), as� que no hay que comprobar nada

      ld b, (ix+1)              ; Coordenada Y del sprite: Comprobamos colisi�n sobre la misma altura donde estamos (metidos con offset de tile)
      ld c, (ix+2)            ; Coordenada X del Sprite

      inc b                   ; El siguiente ST por debajo

      call CHECK_COLISION

      ld a, (ix+4)            ; Offset horizontal del sprite
      add a, c                ; Si es 0 comprobaremos 2 veces las mismas coordenadas
      ld c, a                 ; Pero si es 1, comprobamos los dos ST consecutivos a la derecha

      call CHECK_COLISION


      jp SD_HOOK1


      SD_HOOK0:

      inc (ix+1)                 ; Nos metemos en el siguiente supertile

      SD_HOOK1:

      ld a, (ix+3)               ; Ponemos lo contrario que hubiera en el offset vertical del sprite
      xor 1
      ld (ix+3), a


      ld a, (FLAGS+4)         ; ESCALERA
;      cp 1
      dec a
      jp nz, SPVERT_EXIT

      ld a, (ix+1)               ; Coordenada Y
      cp 12
      jp nz, SPVERT_EXIT

      ld a, (ix+2)               ; Coordenada X
      cp 24
      jp c, SPVERT_EXIT
      cp 28
      jp nc, SPVERT_EXIT

      ld a, (ix+2)
      add a, 19
      ld (ix+2),a

      ld a, (CAMERA_X)
      add a, 19
      ld (CAMERA_X), a

      jp STAIRS_EXIT



; IX -> Base Sprite
SPRITE_UP:
      ld b, 3
SPRITE_UP_1:
      call SPRITE_FACING

      ; PARA OBJETOS ===================================================
      call PRECHECK_IGO
      dec h
      push ix
      call CHECK_IF_GETTING_OBJECTS
      pop ix
      ; ================================================================

      SPU_AF:
      ld b, 3
      call SPRITES_COLLIDE

      ld a, (ix+3)            ; Offset de tiles vertical del sprite
      and a
      jp z, SU_HOOK0          ; Aqu�, sin embargo, comprobamos la colisi�n con el tile "a medias" (con offset de tile) (efecto colisi�n solo con los "pies")
                              ; Si est� "encajado", no comprueba la colisi�n

      ld b, (ix+1)              ; Coordenada Y del sprite: Comprobamos colisi�n sobre la misma altura donde estamos (metidos con offset de tile)
      ld c, (ix+2)            ; Coordenada X del Sprite

      call CHECK_COLISION

      ld a, (ix+4)            ; Offset horizontal del sprite
      add a, c                ; Si es 0 comprobaremos 2 veces las mismas coordenadas
      ld c, a                 ; Pero si es 1, comprobamos los dos ST consecutivos a la derecha

      call CHECK_COLISION

      jp SU_HOOK1

      SU_HOOK0:

      dec (ix+1)                 ; Nos metemos en el siguiente supertile

      SU_HOOK1:

      ld a, (ix+3)               ; Ponemos lo contrario que hubiera en el offset vertical del sprite
      xor 1
      ld (ix+3), a

      ld a, (FLAGS+4)         ; ESCALERA
;      cp 1
      dec a
      jp nz, SPVERT_EXIT

      ld a, (ix+1)               ; Coordenada Y
      cp 11
      jp nz, SPVERT_EXIT

      ld a, (ix+2)               ; Coordenada X
      cp 42
      jp c, SPVERT_EXIT
      cp 47
      jp nc, SPVERT_EXIT

      ld a, (ix+2)
      sub 19
      ld (ix+2),a

      ld a, (CAMERA_X)
      sub 19
      ld (CAMERA_X), a

      STAIRS_EXIT:

      call SPRITE_FRAME
      call CALC_MAP_ADDR
      jp RENEW_BUFFER

      SPVERT_EXIT:

      ld a, (PLAYER_CANMOVE)
      and a
      ret z

      jp SPRITE_FRAME           ; Actualiza el gr�fico y vu�lvete


; b = Facing que corresponde
; IX = Base del sprite
SPRITE_FACING:
      ld a, (PLAYER_CANMOVE)
      and a
      ret z
      
      SPRITE_FACING_0:

      ld a, (ix+5)               ; Facing
      and $0f                    ; Nibble bajo
      cp b                       ; Coincide?
      ret z                      ; Si coincide, vu�lvete, que no tienes que hacer nada
      ld a, (ix+5)
      and $f0
      add a, b
      ld (ix+5), a               ; Si no, pon el que sea
      jp SP_FACING_HOOK1

SPRITE_FRAME:

      ld a, (ix+5)
      add a, $10
      ld (ix+5), a
      and $f0                    ; Nibble alto
      cp $40                     ; Si alcanzas el fotograma "4", es como empezar de nuevo
      jp z, SP_FACING_HOOK0
      cp $20
      jp z, SP_FACING_HOOK1

      ld e, (ix+6)
      ld d, (ix+7)

      ld hl, 64
      cp $30
      jp z, SP_FACING_HOOK_F3
      ld hl, 32

      SP_FACING_HOOK_F3:
      add hl, de

      ld (ix+7), h
      ld (ix+6), l
      ret

      SP_FACING_HOOK0:
      ld a, $0f
      and (ix+5)
      ld (ix+5), a
      SP_FACING_HOOK1:
      ld l, (ix+8)
      ld h, (ix+9)

      ld de, 96

      ld a, (ix+5)
      and $0f
      jp z, SP_FACING_EXIT

      SP_FACING_LOOP:
      add hl, de
      dec a
      jp nz, SP_FACING_LOOP

      SP_FACING_EXIT:
      ld (ix+7), h
      ld (ix+6), l            ; Puntero actual = DE + 96

      ret

ADJUST_FOLLOW_LIST
     ret nc ; Si no ha habido colisi�n, te vuelves ya

     ld hl, PATH_TO_FOLLOW
     ld de, PATH_TO_FOLLOW+1
     ld bc, 3
     ld (hl), $ff
     ldir

     ld ix, PRINCIPAL_SPR
     ld iy, RESTOSPRITES

     ld h, 0
     ld l, (ix+1)
     add hl, hl
     ld b, 0
     ld c, (ix+3)
     add hl, bc

     ex de, hl
     ld h, 0
     ld l, (iy+1)
     add hl, hl
     ld c, (iy+3)
     add hl, bc

     ; Ahora tengo en DE la coordenada Y real en tiles del Jugador y en HL la del PNJ

     and a

     sbc hl, de    ; Le restamos a la del PNJ la del PJ: Si es positivo, hay que ir a la arriba; negativo abajo

     ld a, 3
     jp p, ADD_FOLLOW
     ld a, 2
;     jp ADD_FOLLOW

ADD_FOLLOW:
     ld (PATH_TO_FOLLOW+3), a
     ret

FOLLOW_ME:
     ex af, af'

     ld hl, PATH_TO_FOLLOW+1
     ld de, PATH_TO_FOLLOW
     ld a, (de)
     ldi
     ldi
     ldi
     ex af, af'
     call ADD_FOLLOW

     ld ix, RESTOSPRITES
     ld a, (ix)
     cp 1
     ret nz

     ld hl, ADJUST_FOLLOW_LIST
     push hl

     ex af, af'

     ld b,a
;     cp 0
     and a
     jp z, SPRITE_LEFT_1
;     cp 1
     dec a
     jp z, SPRITE_RIGHT_1
;     cp 2
     dec a
     jp z, SPRITE_DOWN_1
;     cp 3
     dec a
     jp z, SPRITE_UP_1
NULL_MOV:
     ret

UPDATE_FRAME:
     ld a, (CYCLES)
     and 3
     jp z, SPRITE_FRAME
     ret

DRUNK_DRINKS:
     ld e, (ix+8)
     ld d, (ix+9)
     ld l, (ix+6)
     ld h, (ix+7)
     ld bc, 32

     ld a, (ix+5)
     and $0f
     jp z, NEW_DRINK

     ex af, af'
     ld a, (ix+10)
     inc a;(ix+10)
     ld (ix+10), a
     ex af, af'

     cp 1
     jp z, DRINK_NOW

     ex af, af' ; End Drink
     and 31
     ret nz
     dec (ix+2)
     inc (ix+4)
     xor a
     ld (ix+5), a
     ld (ix+6), e
     ld (ix+7), d
     ld hl, COLOR_SPRITES+14
     ld (hl), INK_RED | BRIGHT
     ret

     NEW_DRINK:
     ld b, 0
     ld a, (CYCLES)
     and 31
     ret nz
     add hl, bc
     ld (ix+6), l
     ld (ix+7), h
     inc (ix+5)
     xor a
     ld (ix+10), a
     ret

     DRINK_NOW:
     ex af, af'
     and 15
     ret nz

     inc (ix+2)
     dec (ix+4)
     add hl, bc
     ld (ix+6), l
     ld (ix+7), h
     inc (ix+5)
     ld hl, COLOR_SPRITES+14
     ld (hl), INK_WHITE | BRIGHT
     ret

SPRITE_MOVES:
     ld l, (ix+10)
     ld h, (ix+11)

     ld a, (hl)
;     cp $ff         ; $ff = Fin de secuencia de movimiento
     inc a
     ret z

     ld a, (ix+12)
     and a
     call z, SMOVES_NEWMOVEMENT

     dec (ix+12)

     ld a, (hl)

     ld de, SPRITEMOVES_RET
     push de

     ld b,a
;     cp 0
     and a
     jp z, SPRITE_LEFT_1
;     cp 1
     dec a
     jp z, SPRITE_RIGHT_1
;     cp 2
     dec a
     jp z, SPRITE_DOWN_1
;     cp 3
     dec a
     jp z, SPRITE_UP_1

     pop de         ; Ya no necesitamos tocar el contador, luego podemos volver directamente a desde donde se llam�

;     cp $fd         ; Salto relativo
     cp $fa         ; Salto relativo
     jr z, SPRITEMOVES_JR

;     cp $f0                 ; Si estamos "descansando"...
     cp $ed                 ; Si estamos "descansando"...
     jp z, SP_FACING_HOOK0  ; Posici�n de reposo

;     cp $fe         ; Disparador de secuencia
     cp $fb         ; Disparador de secuencia
     ret nz

;     ld (ix+10), 0 ; Para la siguiente, no repitas la secuencia!!!!

     ld a, (ix+12)
     inc a
     ld b, a
     push ix
     call RUNSCRIPT
     pop ix
     ret

     SPRITEMOVES_RET:

     ret nc
     inc (ix+12)                     ; Si ha colisionado, no se cuenta este turno
     ret

     SPRITEMOVES_JR:
     inc hl
     ld e, (hl)
     ld d, 0
     and a
     sbc hl, de
     jr SMOVES_NEWMOVEMENT+2
     
SMOVES_NEWMOVEMENT:
    inc hl
    inc hl
    ld (ix+10), l
    ld (ix+11), h
    inc hl
    ld a, (hl)
    
;    add a, a
    
    ld (ix+12), a
    dec hl
    ret

PRIEST_MOVES:
     ld a, (FLAGS)
     cp 12
     jr nc, SPRITE_MOVES
     cp 2
     jr c, SPRITE_MOVES
     ret nz
     
     ld iy, PRINCIPAL_SPR

     ld a, (ix+1)
     sub (iy+1)
     jp p, PMP1
     neg
     PMP1:
     ld b, a
     ld a, (ix+2)
     sub (iy+2)
     jp p, PMP2
     neg
     PMP2:
     add a, b
     cp 6
     ret nc

     jp SPRITE_MOVES

ROCKS_FALL:
     ld (ix+12), S_PIEDRA
     ld (ix+11), $f0

     ld a, (PRINCIPAL_SPR+2)               ; La X del jugador
     cp 36                                 ; �Est� en una cueva?
     jp nc, EXPLOSION_RESTARTCYCLE         ; Entonces no tienen que caer rocas

     ld a, (ix+1)
     cp $f0
     jr z, RF_BEGIN

     call SPD_AF
     jr c, RF_CHANGE

     ld (ix+10), $ff                       ; $ff si est� cayendo

     ld a, (ix+1)
     ld hl, CAMERA_Y
     sub (hl)
     cp ST_ALTO
     jp nc, EXPLOSION_RESTARTCYCLE

     ld b, (ix+1)
     ld c, (ix+2)
     call WHATSUPERTILEAT
     cp 1
     ret z
     cp 10
     ret z
     cp 11
     ret z

     ;jr RF_HIDEROCK

     RF_EXPLODEROCK:
     ld (ix+0), S_EXPLOSION
;     ld (ix+10), $ff     
     ld de, EXPLODE
     jp RF_RESTART_GFXPTR
     ;ret

     RF_BEGIN:
     ld a, (PRINCIPAL_SPR+1)
     cp 3
     ret c
     cp 78
     ret nc
     ld (ix), S_PIEDRA
     ld de, PIEDRA
     call RF_RESTART_GFXPTR   ; ix + 10 = $ff si est� cayendo
     
     
     ld a, r
     and 15
     ld hl, CAMERA_X
     add a, (hl)
     ld c, a
     ld (ix+2), a
     ld a, (CAMERA_Y)
     dec a
     ld (ix+1), a
     ld b, a
     call WHATSUPERTILEAT
     cp 1
     ret z
     ld (ix+1), $f0
     ret

     RF_CHANGE:
     ld a, (ix+10)
     cp $ff
     call z, RF_NEWCHANGE
     ld hl, SPR_AF
     and a
     jr z, RF_HOR_MOVE
     ld hl, SPL_AF
     jr RF_HOR_MOVE

     RF_HOR_MOVE:
     ld de, RF_HOR_MOVE_RET
     push de
     jp (hl)
     RF_HOR_MOVE_RET:
     ret nc
     jr RF_EXPLODEROCK

     RF_NEWCHANGE:
     ld a, r
     and 1
     ld (ix+10), a
     ret

     RF_RESTART_GFXPTR:
     ld (ix+5), 0
     ld (ix+6), e
     ld (ix+7), d
     ld (ix+8), e
     ld (ix+9), d
     ld (ix+10), $ff
     ret


UF_EXPLOSION:
     call SPRITE_FRAME
     ld a, (ix+5)
     and a
     ret nz
     inc (ix+10)
     ret nz
     EXPLOSION_RESTARTCYCLE:
     ld a, (ix+12)
     ld (ix), a

     ld a, (ix+11)
     ld (ix+1), a
     ret

ENEMY_MOVES:
     ld (ix+12), S_ENEMIGO    ; +12 para restaurar tras la explosi�n

     ld a, (ix+1)              ; Si la coordenada Y es >= $f0 -> Empieza un nuevo enemigo
     cp $f0
     jr nc, EM_BEGIN

     call ERASE_EYES
     
     ld a, (ix+11)
     cp $f0
     jp z, RF_EXPLODEROCK
     jr nc, EM_MOVES

     
     EM_ASIGNENEMY:
     ld a, (ix+11)
     ld (ix+1), a
     ld (ix+11), $f6

     ld de, ENEMIES
     call RF_RESTART_GFXPTR

     ld a, r
     and 7
     ld (ix+10), a
     and 3
     ld b, a
     jp SPRITE_FACING_0 ; Ajuste del puntero gr�fico


     EM_BEGIN:

     call RAND8
     and 7
     ld hl, CAMERA_Y
     add a, (hl)
     ld b, a

     call RAND8
     and 15
     ld hl, CAMERA_X
     add a, (hl)
     ld c, a

     call WHATSUPERTILEAT
     dec a
     cp 31
     ret nc

     ld a, (PRINCIPAL_SPR+1)
     sub b
     jr nc, EMB_H0
     neg
     EMB_H0:
     ld d, a
     ld a, (PRINCIPAL_SPR+2)
     sub c
     jr nc, EMB_H1
     neg
     EMB_H1:
     add a, d
     cp 4                          ; Cambiar este n�mero para variar la distancia m�nima a la que pueden aparecer los enemigos
     ret c

     ld (ix+1), b
     ld (ix+11), b                 ; Pon en +11 la Y para restaurar tras terminar el "humo de aparici�n"
     ld (ix+2), c
     xor a
     ld (ix+3), a
     ld (ix+4), a

     jp RF_EXPLODEROCK


     EM_MOVES:

     ld hl, SPL_AF
     ld de, SPR_AF

     ld a, (ix+10)
     bit 1, a
     jr nz, EM_MOVES_HOOK1

     ld hl, SPD_AF
     ld de, SPU_AF

     EM_MOVES_HOOK1:

     bit 2, a
     jr nz, EM_MOVES_HOOK2

     ex de, hl

     EM_MOVES_HOOK2:
     ld de, EM_MOVES_RET
     push de
     jp (hl)

     EM_MOVES_RET:
     jr nc, EMR_DE
     dec (ix+11)
     ld a, (ix+10)
     xor 4
     ld (ix+10), a

     EMR_DE:

     call SHOWSPR_CALC_PRE
     inc b
     inc c
     call DRAW_EYE
     inc c
     jp DRAW_EYE

     ret

DRAW_EYE:
     ld a, 30
     cp c
     ret c

     ld a, 18
     cp b
     ret c

     ld a, b
     add a, c
     cp b
     ret z
     cp c
     ret z

     call ATT_LOCATE
     ld a, (hl)
     and a
     ret nz

     push hl
     ld (hl), 2

     call DF_LOCATE
     ld de, EYE_DATA

     REPT 7
     ld a, (de)
     ld (hl), a
     inc h
     inc de
     ENDM
     ld (hl), 0
     pop hl
     ld (hl), 2
     ret
     
ERASE_EYES:

     call SHOWSPR_CALC_PRE     ; Calcula las coordenadas en pantalla del sprite
     inc b
     inc c
     call ERASE_EYE
     inc c
     ;call ERASE_EYE

ERASE_EYE:
     ld a, 30
     cp c
     ret c

     ld a, 18
     cp b
     ret c

     call ATT_LOCATE
     ld a, (hl)
     cp 2
     ret nz

     ld (hl), 0
     ret


EYE_DATA:
    defb 0, 0, 0, 8, $1c, 8, 0, 0
