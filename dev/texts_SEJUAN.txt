# INSTRUCCIONES PARA TRADUCCIÓN
# =============================
#gonzalo es NELO cesareo es QUQO
# Por favor, utiliza un editor de texto plano para modificarlo, en lugar
# de un editor de textos tipo MS-Word, Open-Office, etc...
# El mismo NOTEPAD de Windows es una buena opción.
#
# Las líneas que empiecen con # se ignoran:Puedes usar comentarios con el #
# para recordar por donde vas, por ejemplo,o poner las anotaciones que
# quieras, porque, aunque se te olvidase borrarlo no habría problemas
# al compilar.
#
# Por favor: Deja los comentarios que hay que son un número: Sirven para
# indicar el número de mensaje.
#
# Las líneas en blanco también se ignoran.
#
# Cuando haya una barra \ después viene un número hexadecimal de dos cifras,
# NO SE PUEDE TOCAR. Déjalo tal cual está, incluyendo también los espacios
# circundates: Si hay un espacio antes o después de ese código de escape,
# déjalo ahí, y si no lo hay no lo pongas. Puede ser un poco lioso, porque
# el \0D que verás irá sin espacios ni antes ni después (es el indicador
# de "retorno manual de carro", pero, por favor, déjalo como está.




San Arteixo do Montalvo
Cerrajería de Xosé
Pousada de Xan
Dios proteja esta casa y a sus moradores
Deus lari Berobreo aram posuit pro salute
# 05
Los romanos stuvieron aquí
\01\45Galicia, 30 de octubre de 1830.\01\07
Dos extraños franciscanos se dirigen en peregrinación por el camino de Santiago.\0D\0D\01\44Fray Nelo\01\07 y \01\44Fray Quqo\01\07 llevan todo el día caminando y deciden descansar en la primera aldea que encuentran.
Al perder toda su fe, Fray Nelo enloqueció y quedó vagando por los bosques.\0D\0DHay quien cuenta que le vió guiando la Santa Compaña.\0D\0DFray Quqo murió en casa del boticario, y fue enterrado en la aldea.
Con un poco de suerte \01\46San Arteixo do Montalvo\01\07 les acogerá como tantas otras han hecho a lo largo de su viaje.\01\30
# 10
Fray Nelo,s'aquivocao de posá. Buscala toi reventao!!
Fray Nelo, tengo la tundiga molía. ¿Qui quiere dormir al raso?
Tenemo que buscá una piltra ante qui sisome la luna.
A mí tó lugare me dan mal fario. Nunca si sabe si tará la pareja lo civíle.
¿Ya tá con tú miedo y superstisione?, cohone.
# 15
No puedo aguntá, é el mal fario a lo verde. Nunca me podré de costumbra.
Bueno, busquemo. Se no exa'l relente ensima. Y nó vamo a pon'empapao.
La pá e Dió sea con uté, payo. Bucámo un corxón donde plegás l'oreja.
Lo siento gueno señore, toi siendo refoma y no tengo ninguna bitasión libre.
Pero nesesitamo un sitio ande dormi, unque sea nun rincó de la tabenna.
# 20
Tranqui Fray Quqo, guna solusión contraremo.
Claro, gueno señore. Seuro que Don Justiniano, el boticario, tará encantao d'ofresero su casa. E mu amigo der cura y al ser utede peregrino, tendrá la carida d'hospedarle.
¡Cuidao! ¡ic! Le manda la boca er lobo ¡ic! Sa casa tá mardita ¡ic!
Cállate molo mielda, no suste eto hombre de Dió.
La donsella s'suisidó ¡ic! y su alma vaga po lo pasillo ¡ic!
# 25
¡Cállate borraxo!
No so preocupe, nosotro no semo superstiSioso. ¿Dónde pueo encontra'l boticario?
E mejós q'hablen con Don Cirilo, el cura, n la iglesia.
Ete pueblo etá mardito ¡ic! l'única solusió ¡ic! e mete'le al tintorro ¡ic!
Disculpe a eta pobre alma carriá perdía por el visio,la corruxió y el Iker Jiméne.
# 30
No so preocupe. Muxa grasia.
Hablemo con el curaPayo, cagón tó ya.
Bienvenido, hermano. L'etaba eperando. Ta é una ardea pequeña, y la notisia vuelan.
Na'noxes, curaPayo. Somo Fray Nelo y Fray Quqo, peregrino der Apóstol, y buscamo ande pasá la noxe...
Sí, sí, claro, quieren hopedaje y no hay sitio n'la posada.\0DTenía q' haber devenio a veme ante que ná.
# 35
Tocate lo guevo si lo hubieramo sabio ya tariamo durmiendo.
Po claro. Incluso yo mimo, quí n'la sacristía, pero é pequeña y no tengo tanto sitio como D. Justiniano.\0DE un hombre rico, y su casa tié mucha habitasione vasía.
Ay payo si supiera ande emo dormio.Mientra no nos caiga el relente, ni no pique lo xinxe....
Po supuesto, pero seguro q'agradeserá l'amabilidad dun buen cristiano que lofreca una cama blanda y una buena sena.\0DNo siempre sepresenta sa oportunidá, y esoría el Camino un poco meno duro.
¿Tá mu lejo la casa'l boticario? ¿pedimo un taxi?
# 40
Un paseíto por el bojque. Salgamo ya, ante que sea noxe serrá.
Di-culpe a Fray Quqo, l'ocuridá le vuelve superstisioso y cagueta.
Hase bien. La noxe es refugio de cosa mala y la prudensia é una virtu. Salgamo.
Síganme, hermano. El camino e corto pero e fasil perderse.\0DSi s'alejan demasiao le eperaré, pero démono prisa pa llegá ante de que caiga la noxe.
To de seguí un cura me suena d'argo. Y ese coló paese el pajaro epíno.
# 45
¡Toc, toc! ¡Ah de la casa!
¡Don Cirilo! ¡Qué sorpresa tas hora de la noxe! Qui no tiene casa?
Sí, ¿verdá? No son hora pa ir por eto barrio lleno criminale\0DAnda, déjano d'entrá, que tenemo q'habla con D. Justiniano.
Sí, pero e casi la hora de sená (Siempre dando poculo ete hombre.)
Pué má a mi favó: Donde sena uno, senan cuatro, jeje
# 50
Amigo Cirilo, no t'eperaba tan tarde. ¿Qué te trae por quí?
Sabe que no sargo de noxe, si no e nesesario.\0DHan llegao l'aldea tos do hermano camino a Santiago, y busca ande pasá la noxe. Entonse pensé que podríá aser una buena acsión.
Po supueto, Cirilo. Hermano, sean bienvenido a eta, mi humilde morada.\0DMaruxa, prepara lo cuarto d'invitao, que lo hermano se quedan a dormí... y sená, porque tendrá hambre, ¿verdás?
Sí, muxa grasia, don Justiniano. Que Dió se lo pague que yo no llevo suerto.
Yo tambié tengo hambre, que guele mu bien, y'sa vianda hay que proba la, jeje
# 55
Tú siempre igual. Po supuesto que te queda a de sená, y dormí, que ya é noxe serrá.
Po favó, síganme.
La sena tará lista enseguia. Bajen al comedor en cuanto s'hayan instalao.
Pos fien, pos fale, pos m'alegro.
Pasen, hermano. L' tábamo eperando.
# 60
Ahora vamo a tomá unas copita d'orujo q'ayuda a la digestió, como e copón.
L'orujo de D. Justiniano é realmente gueno, si lo vendiera s'haría d'oro
Tontería, no pienso de vende-lo: si lo vendo no podríamo disfruta-lo... jajaja
Sierto, pequeño sartamonte... jejeje
Lé enseñaría mi laboratorio pero enseguro q'utede no linteresa la medisina y lo adelanto sientifico.
# 65
A mí sí me encantaría exa un ojo: Etudié en Jarvar ante' toma lo hábito.
Un colega, vaya vaya... creo que vamo a tene una tupenda velada
Yo lo siento, pero toi molio.Y me cueta muxo quitame éto refajos.
Yo tambié me voy dormí. Y tú a etudiao en Jarvasete, ¡asiendo jaúla!.
Menuda noxe, qué pasao. Cagón san peo. Seguro q'el orujo era de garrafón.
# 70
Qué traño. Parese q' Fray Quqo no'ubiera dormio aquí ta noxe
Ta puerta tá serrá a cal y canto.
No debo irme d'quí sin Fray Quqo.
¡Fray Nelo! No se pue'imaginaS lo q' ha pasao.
¡Ondia! ¿qué le pasa? A'sio adussio por lo'etraterrestre? Atao jugando al econdite é solo?
# 75     
No lo sabemo, paresió nel ala oete de la casa, sa parte siempre etá serrá...
Siempre etá serrá desde que...
Tonses, ¿cómo conio etaba allí Fray Quqo?
No lo sabemo. Noxe me quedé dormio n'el laboratorio mientras Fray Quqo seguía mirando mi eperimento.\0DAl despertá vi papele tirao po'l suelo que dejabun rastro ata el ala oete y allí taba tirao ne'l suelo.
Pero ¿tá muerto? tie seguro d'terseros?
# 80
No, ún vive, pero tá inconsiente y ná le ase despertá.
Creemo que é preso d'un hechizo.
Don Ciruelo ¡q'tá fumao?! dame argo pa'rcamino. Y aese un pá d'otia y verá como depieta.
Hijo, hase muxo q'aprendí que en eta tierra to é posible.
Deberíamo avisá a la Meiga. Seguro que podría ayudarno.
# 85
¿Que pasa q'ella si tié seguro? Tu tranki ya le doi yoe pá d'otia..
Hágano caso, hermano. Ete no é un mal del cuerpo, sino del ánima.
Sólo la meiga puede ayudano. Debe ir a busca-la. Vive n'una cabaña, n'el bosque q' hay entre eta casa y é pueblo. Nosotro nos quedaremo cuidando del enfermo por si despertase.
¿Qué l'aparesio ete Justiniano, hermano?
Cuando vea la piltra te lo diré.Ondia!! ReiMartí
# 90
Yo... he sentio un'escalofrío a entrá en la casa. Epero q'ete tio sea legá.
No buque má explicasión Tie q'abe una puerta bierta. Copó.
Será eso, seguro. Anda, bajemo a sená...
Bajemo a sená, tengo ló mótago nlo tobillo.
Mi trabajo e brir serradura, no hay ninguna que s'me resista
# 95
Ete armario t'abierto, pero no veo'l motivo de poneme la ropa la vieja'lvisillo... al meno en ete momento.
Aluego depué...
Sin colocá entoavia las faróla...
¡Ondia!,so e na cabesa alguno. No la vi pillá sin una buena rasó.
Qué desgrasia, ta casa no é la mi-ma de-de ase uno cuanto año.
# 100
He d'ir a buscá a la meiga a vé si pué yudano.
No t'ayudará si no le paga por su servisio. Toma ta gallina, será sufisiente.
¡Ha ocurrio algo terrible! Suba, el señor l'epera arriba, junto su pobre amigo.
Se m' acabado l'orujo y ta 'l mé que viene no me llega el pedío. Cago'n mi puta calavera.\0DDaría toa la guita por una botella, unque sólo fuer'una.
Me temo que no pueo ayudale. Yo en cambio nesesito lo servisio d'una meiga... ¿Podría indicame ande encontra-la?
# 105
¿Meiga? N'el bosque vivuna que disen que lo é: Tenga cuidao con ella, no e de fiás y de-de luego nunca trabaja grati.
Al norte l' aldea encontrará la posá de Xan. Seguro que llí podrá ayudále.
Pero una cosa le diré, hermano: Pa podé encontrala, tie que creé n'ella. Esa fe'n su iglesia no se lo permitirá.
Va á irte d'aldea, apagál epétrum i bandonás mamó der Quqo?.
U monje follastero entrando'n la casa d' una meiga. Eso sí que é una sorpresa.
# 110
Don Ciruelo m'a enviao'n su busca. Mi compañero a caío, segú é, presa d'un hechiso que le mantiene inconsiente.
Iré a ve-lo, pero mi servisio no son grati.
¿Asetaría ta gallina avecren?
Con esa gallina haré u cardo que l'irá fenomenas a mi pobre viejo hueso. Vayamo a vé  se'echisao.
Tá bié. Buscaré argo que le gúte... una nimbus2000 quisá?
# 115
Ta vieja meiga no é como utede lo cureta. No me comprará con oro del que cago'l moro.
Si quiere que l'acompañe, tendrá que pagáme con argo.
Grasia Dio q'as venio, mira. Ë ete pobre hombre.
Tá claro. N'el ala oeste, la noxe de difunto y en ete tado no e má que la maldisión de Brunilda.
Oh, Dio mío, Brunilda de nuevo.
# 120
¿Quié é Brunilda? ¡Ta tokandome ya lo guevo!
Brunilda s'suisidó n'eta casa y su ánima vaga por qui.\0DNesesita otra ánima pa salí del purgatorio, y ha cogío la de ete pobre hombre.
Pué podría ave cogió el bus.No creo n'esas cosa.
No crea si no quiere, pero si no ase argo por s'alma su amigo continuará asín ata que muera, y, en eto seguro que opinamo iguá, é argo que susederá mu pronto si sigue en ete estao.
Hágale caso, Fray Nelo, ella sabe lo que dise.
# 125
Propongo esperá a vé si despierta y mañana tomaremo una desisión ¿Qué le parece Fray Nelo?
Yo lo q toi ta lo guevo d'tanta xumina.
Si cambiais d'opinión, ya sabéi ande vivo, adió.
Fray Quqo sigue inconssiente. Hemo de llevále mañana a la siudá. Ahora intentaré descansa un poco
La dose de la noxe y no he pegao ojo, y eso que no e ío putas.
# 130
¡PADRE SANTO!
No s'asuste, padre. Cuxe mi historia...
¡No pueo creé lo que veo!
Soy Brunilda, o mejos dixo, l'espíritu de Brunilda.\0DYo servía en eta casa, era parte del servisio. Ase año viví un noviasgo secreto con Antón, l'hijo'l boticario. Su pare no sabía ná y llegó l'momento de conta-lo.
Mi amado salió de paseo por el bosque con su pare pa sinserarse, pero nunca llegó ese momento: nos bandidos salieron al paso, y lo mataron.\0DCuando supe la notisia, no pue resisti-lo: La locura m'invadió y me suisidé.
# 135
Tenía la esperansa de reunime con mi amado en l'otra vida, pero por argú motivo, vago por eta casa dede entonses.
¿Y tu historia qué tie que vé con mi hermano? Te podía vé ido a tomá polculo.
Muxo. La desgrasia reina ta casa dede entonses, su alma tá como la mía, trapá.
Si fueran lo sivile, te digo fale, pero no me creo  eto me esté pasando...
Sé cómo solucioná ta situasió. Nuetra desgrasia tán encadenas: Si m'ayuda mí, le ayuda a él.
# 140
Toi dando má fuérta qun molino,cohone.
Vaya'l sementerio, detrá de la iglesia, ta misma noxe. Allí nos veremo.
No se preocupe. Yo l'abro.
Ta e mi tumba. No sé por qué, pero siento que argo va mal dentro.
¿Que algo va mal? As tomao actimé?
# 145
Persibo claramente q'argo va mal en la tumba. Cave y ábrala, po favó.
¿Pero cómo?
Consiga una pala. Mire 'n argú jardín, los jardinero suelen usarla.
¡Se lo dije, argo iba mal en mi tumba!
¡Padre Santo, farta la cabesa!
# 150
Po favó, encuentre mi cráneo, y póngalo junto'l cuerpo.
Entiendo que tu alma n'encuentre descanso.¿As pensao comprá un pikolin?
No encontraré descanso ata está al lao de mi amó. Cuando tenga mi cráneo, entiérreno junto.
Todo lo que tá pasando me trae tristísimo recuerdo. ¡Brunilda era tan guena!
¡Nunca descansará!
# 155
¿Quién habla?
¡Me llevo tu cuerpo otra parte!
¡Noooooo! Mi novio e Ricardo Lama!!!Ahora nunca descansaré.
No pueo de creé lo que visto
Q tá ata lo cohone q'qiere ite? S/N
# 160
Tu fe t'impedirá ayudá a Brunilda, jajaja
¡Fray Nelo, depierte! ¿Q'ase quí? ¿Cómo ha entrao?
Er... Don Ciruelo... no so va a creé... Brunilda... su cuerpo... su tumba...
¿Brunilda? Sabía c'al finas tenía que vé con to eto...\0DSólo una persona puede ayudarno n'ete momento: La Meiga. Vaya a busca la, yo me quedaré n l'Iglesia, resando pol alma su amigo.
Esa historia d' alma de la criada é el colmo. Bastante desgrasia habio yá n'eta casa.\0DHe desidio repasá la literatura sientífica en busca de caso como'l de su amigo.¡He comprao Año Cero!
# 165
Si encuentra argo que crea que pueda ayuda, tráigamelo.
¡Ta seta e estrurdinaria! No sabía cresieran por quí. Toy seguro de que no puede ayudas en el caso su compañero, pero he de comproba-lo n'el laboratorio.
Po favó, hermano. Deje eso ande e tá. Se cráneo a pertenesío mi familia dede ase que mi buelo fué a Salamanca a bucá la ranalaplasa.
Ta seta rara que ma traío nos va a ayudá muxo, toy seguro, pero nesesito má tiempo.
Bien, supongo qas venio a pedime consejo y consejo te daré.\0DSé quié tie el cráneo y ande pues encontra la cuatro parte del equeleto de Brunilda.
# 170
L'equeleto tá en cuatro cave-na de lo antiguo selta 'sin boquilla'. Pa llegas a ella, primero tie que recogé l'agua de la 7 pozas der Monte das Pozas y regá con ella caa uno de lo monolito cai junto su entrada. No riegue lo matujo.
El cráneo lo tiene el boticario en su depaxo. Sospecho que tie bastante que vé n'esta historia, no creo que le gustara que su hijo s'enamorara de la criada.\0DCuando tengas to, o nesesite ayuda, ven a ve-me.
Te daré un farol delo xino pa que t' ilumine en la cavena a cambio de 7 seta.
Busca esa 7 seta, y vuelve a ve-me.
Quí tiés el farol. Ahora consigue l'equeleto.
# 175
Entoavia no ties l'equeleto completo. ¿Cómo quies que t'ayude?
Monte das siete pozas
Cuidao con los pedrolos q'caen
Quí será el ioputa grafiqero ca pintarrejao qui? Sú muerto má frejco, tinía qi vé de mirao pa'tro lao!
¡Ta maocuro quel coño una monja! ¡Siende un sigarro o argo pase lumbre, copó!
# 180
La siete posas sagrás de los Seltas Cortos s'allan te tí. Su agua tiene  una miaja anis y un poco hierba. O te pone o encuentra tu camino, fijo.
N'ese carté pone cai desprendimiento. No hay cohóne a pasá.
Otra vé n'eta poza? Tu lo q'quiere e vino jodio. Diqui ya cogio agua, mamó.
La viejaloguevo dijo que nesesito l'agua de ca una de la siete pozas pa que surta efeto.
Cagón tó, con un par, epa!
# 185
No debo malgastá l'agua.
La superfisie del monolito no refleja niun solo rayo de só.
Tá bien, ten lo hueso. Ganáte una batalla, pero no la guerra.
So te enseñará a no metete con quién no debe.
Te venseré con mi dansa macabra.Eeee macarena, aaahhh!
# 190
Doblégate y seré caritativo contigo.
¡Ni habla, so nunca, ante m'hago l'atleti!
Repite mi baile, pero hazlo n'el momento justo o te venseré...
¡Chúpatesa, frailecillo!
¡To poota mare.A tomá pol-culo!
# 195
No lucharé otravé contra ese monstruo e má feo q'una mierda.
Mu fien. Ahora yo invocaré l'alma de Brunilda. Mientra tanto, dirígete al sementerio, pa poné lo hueso n'su sitio.
Vayal sementerio a colocá lo hueso. Invocá n ánima der purgatorio é un trabajo xungo y perigroso que no debe sé interrumpio.
¡Has traío tó! ¡Por fi podrá descansa en paz!
No pasa na... ¿de verdad que toas esa parte son mías?
# 200
Sí, toas, también la cabesa, que lo teníal boticario su despaxo.
¡No! Ese cráneo no e mío. Ya lo teníante de que yo muriera, pertenesió su buelo.
Ja Ja ... Habéi picáo lo dó, tu cráneo lo tengo yo
Tú ... siempre nos mirate con malo ojo.
Si yo no puedo tar con Antón, que no lo eté nadie y tú me lo quitate.
# 205
Devuélveno el kabolo, no gana ná con eto, Antón tá muedto.
Sí, pero'l sabó de la vengansa e mu dulse... Ja Ja
Muxa grasia, nunca pensé que l'bruxalola tuviera selo de nosotro.
Ahora ya no'ay que preocuparse por ella.
Brunilda, amor, ya tamo junto.
# 210
Sí, sólo falta que tu pare septe nuestra relasión y q'agan una misa por nuetra almas.
Fray Nelo, ¿nó ayudará?
Mi diario te dará la pista dande encontrá la carta que nó enviábamo.
Séñeselá mi padre: Eso bastará pa se'ntere de lo nuetro y asepte.
Buca mi bitasión, n'el ala oeste de la casa.
# 215
Veo que no has tenio sufisiente. Tá bien, te enseñaré mi verdadero podés...
S'orujo que me trajo, padre, é excepcionás. M'ha salvao el pellejo.
Alabado sea Dió, una botella d'orujo... ¡Se la compro! Tome esta moneda, creo será sufisiente.
Tao meditando que quisá s'orujo que destila el boticario pueda despertá mi hermano, ¿no cree?
¡Pos tuviera orrao un paseo de pensalo ánte. Llévese ta botella, seguro que l'ayuda... pero no le diga a D. Justiniano que se l'he dado, o me despedirá.
# 220
¿Quierir l'ala oete? No pué, el señó nunca le dejaría, soy l'única a la que presta la llave, y, discúlpeme, pare, peron eso no le voy ayudá: ¡Se tán volviendo tos locos!
No pueo ir asín vestio por ahí y que alguien me vea. Nesesito volvé a poneme mi hábito.
¿La llave? Uté verá, pero no vaya de noxe, ya sabe lo q'ocurre y no quiero lamentá má desgrasias.
No me mole-tes má, toy mu'ocupao.
El diario dise que las carta tán escondia tras un pasadiso n'la biblioteca, al que s'acsede pujando la librería del rincón norte.
# 225
E boticario tie nos ejemplares xelentes: playboy,lib...hustler!!
Ta debe ser la librería que dise Brunilda... si lempujo un poco quisá s'aparte...
Te é el cofre que describía Brunilda su diario, ¡pero tá serrado con llave! Nesesitaré brirlo pa cogé laj carta.
Guena serradura, pero n guantará la patacabra, vé soltando la guita q tengo jambre...
A cambio d'sas monedas, l'abriré encantao ese cofre.
# 230
Tas carta son de mi hijo a Brunilda! Tonses taban namorao, y no setrevieron a desírmelo... Mi hijo murió, y ella s'suisidó por amó...\0DAhora lo'ntiendo tó. Hay que desí una misa pó ete pá de giliposcha. Lespero en el sementerio.
Te crusifijo m'a permitio resá cuando taba punto perdé la fé.¡Qué malo toi de lo mio!
Hermano, toy a punto de volveme loco con tó eto. Seguiré resando con l'esperansa de que Dió lo resuelva to'n su ifinita bondá.
Aunque quiera evitalo, la superstisione de tos lugareños asen que mi fé flaquee.
Siento que ejtá etre eto muro sagrado refuersa mi fé.Nel bar pillaría na cogorsa...
# 235
Muxa grasia, Fray Nelo, m'ha dao muxa paz sabé toa la historia.
Ya descansan en tumba contigua. Ahora sólo quea que les digamo una misa, su almas descansarán y Fray Quqo volverá en sí.
So será si yo lo permito. ¡Mi alma tá condená pero la de ello dó nunca descansará!
Epíritu maligno, ti vi asé picadillo d'amburguesa del burriking!
¡Oh, no! ¡Me derrito, me derrito!
# 240
Finalmente'l mal vensió.\0D\0DMurieron tós nel sementerio y su almas qedaron vagando por la sona.\0D\0DLa maldisión invadió l'aldea y quedó abandoná po lo siglo. Ike Jimene compró la posá i montó un after.
Pofi hemo conseguio que su alma descansen, y hora la meiga desaparesió pa siempre.
¿Habrá despertao Fray Quqo?
¡Vayamo corriendo a casa a vé!
Fray Quqo, ¡labado sea Dió! ¡A despertao!
# 245
Toy molio y resacoso, suguro q'era de garrofón l'orujo, ¿Cuánto e dormio?
Muuunxo. Y sa perdio una guena. Se la contaremo durante la sena.
Qué gran historia, Fray Nelo. Ya sabía yo q'to bosque serraban grande peligro.
Lo que cuenta é qal finas tó salio bien.\0DVayamo la cama, que la sena sido opípara y l'orujo l'boticario e realmente fuerte.
Fray Nelo, vamo, ya é mu tarde, debemo emprendé l camino.
# 250
¿Eh? Vaya, me he dormio, l'orujo de noxe...cagón lo botijodeplatico... he soñao con Brunilda.... Su alma ma vuelto a visitá en sueño... que polculo!
Y tras estas palabras, los dos frailes continuaron su marcha hacia Santiago, y quién sabe qué nuevas aventuras encontarían en el Camino..\0D\0D       ¿El final?
Fray Quqo, Brunilda, la historia de Brunilda, uté inconssiente, Antón, sus alma, la brujalvisillo...
Pero, ¿q'abla? Noxe llegamo quí y senamo con el boticario, uté se tomó uno cuanto orujo i sefumó toa la planta medisinale l'boticario, fumeta qere un fumeta hermano... JaJa
Tonses...
# 255
Tonses ná, venga hermano, vamo a retomá la marxa q ún queda muxo pa llegá Santiago.
#--------------------------------------------------------------------------------------------------------------------------------
# TABLA 2
#--------------------------------------------------------------------------------------------------------------------------------
L'abitasió de la viejalvisillo, al contrario quel resto la casa, tá mu desordená: Ni siquiera serrao su armario, ¡pa'ké!.
Nesesito la llave del ala oete.
No, pare. No pueo dejale la llave del ala oete, y meno despué de tó lo q' ha pasao.
N'este estante predominan lo libro de medisina.
La química é la materia que ocupa ta tantería.
#  5
Po lo que se vé, l'boticario tá mu interesao en la matemáticas. 1 y 1 iguá a 11
La botánica tamién le guta a Don Justiniano. 'Cannabis porreta deluxe ed.'
Ete estante e pa'la historia. 'Hitorioa de la puta mili' y 'Semo pelirrojo'.
Quí pues vé tratados y libros de leyes.
Esta parese l'abitasió de Brunilda.
#  10
Antiguas novela s'encuentran aquí.¡Coño un interviú! Con las hojas pegas,copó
Ahí dentro tán la carta de amó. Busca quién te ayude a abril-lo
