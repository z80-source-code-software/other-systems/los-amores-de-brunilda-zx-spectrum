; A�adir un sprite tipo A en las coordenadas BC del mapa (lo a�ade centrado a supertile)
; HL apunta a la base del sprite (facing 0 fotograma 0)

ADD_SPRITE:
     ex af, af'                  ; Guardamos el acumulador

     ld a, $ff
     ld ix, RESTOSPRITES         ; Inicio de la tabla de sprites
     ld de, BYTES_SPR            ; bytes por sprite

     ADDSPR_LOOP:

     cp (ix)                     ; �Est� libre?
     jp z, ADDSPR_HOOK

     add ix, de                  ; No lo est�
     jp ADDSPR_LOOP

     ADDSPR_HOOK:

     ex af, af'                  ; Recuperamos el Acumulador (Tipo de Sprite)

     ld (ix+0), a                ; Tipo de sprite
     ld (ix+1), b                ; Coordenada Y
     ld (ix+2), c                ; Coordenada X
     inc (ix+3)                  ; Val�a $ff. Se pone a 0. M�s lento que con LD IX,0 , pero ahorramos 1 byte
     inc (ix+4)                  ;
     inc (ix+5)                  ;
     ld (ix+6), l                ; Puntero Actual
     ld (ix+7), h
     ld (ix+8), l                ; Puntero "base" al gr�fico
     ld (ix+9), h
     ld (ix+10), 0               ; Puntero al script de movimiento correspondiente

     ret

; Limpia la tabla de sprites
DEL_SPRITES:

     ld hl, RESTOSPRITES
     ld de, RESTOSPRITES+1
     ld bc, MAXSPRITES*BYTES_SPR-1

     ld (hl), $ff
     ldir

     ret

;

SHOW_SPRITES:
     ld ix, PRINCIPAL_SPR

     ld a, (FLAGS)
     cp 20
     jp nz, SHOW_SPRITES_HOOK

     ld ix, RESTOSPRITES
     
     SHOW_SPRITES_HOOK:

     ld a, (ix+0)
;     cp $ff
     inc a
     jp z, SHOWSPRITES_2

;     cp S_ENEMIGO
     cp S_ENEMIGO+1
     call z, ENEMY_COLORS

     call SHOW_SPRITE
     ld de, BYTES_SPR
     add ix, de

     jp SHOW_SPRITES_HOOK
     
     SHOWSPRITES_2:
     ld ix, PRINCIPAL_SPR

     ld a, (FLAGS)
     cp 20
     jp nz, SHOW_SPRITES2_HOOK

     ld ix, RESTOSPRITES
     
     SHOW_SPRITES2_HOOK:

     ld a, (ix+0)
;     cp $ff
     inc a
     ret z

;     cp S_ENEMIGO
     cp S_ENEMIGO+1
     call z, ENEMY_COLORS

     call SHOW_SPRITE2
     ld de, BYTES_SPR
     add ix, de

     jp SHOW_SPRITES2_HOOK

ENEMY_COLORS:
     ld a, (ix+10)
     and 3
     add a, 3+64    
     ld (CSE), a
     ld (CSE+1), a
     ld (CSE+2), a
     ld (CSE+3), a
     ret
     
I_ERASE_EYES:
    ld a, (FLAGS+6)
    cp 1
    ret nz
     
    ld ix, PRINCIPAL_SPR

    IEE_LOOP:
    ld de, BYTES_SPR
    add ix, de
    ld a, (ix)
;    cp $ff
    inc a
    ret z

    call SHOWSPR_CALC_PRE
    inc b
    inc c
    call ERASE_EYE
    inc c
    call ERASE_EYE

    jr IEE_LOOP


MOVE_SPRITES:
     ld ix, RESTOSPRITES

     MOVESPRITES_LOOP:
     ld a, (ix)
;     cp $ff
     inc a
     ret z

;     ld hl, MOVESPRITES_TABLE - 2            ; El tipo 0 no se define en esa tabla, que empieza en el 1... luego hay que bajar 2 bytes
     ld hl, MOVESPRITES_TABLE - 4            ; El tipo 0 no se define en esa tabla, que empieza en el 1... luego hay que bajar 2 bytes
     call GET_WORD_TABLE
     ex de, hl
     ld de, MVSPR_RET
     push de
     jp (hl)

     MVSPR_RET:
     ld de, BYTES_SPR
     add ix, de
     jp MOVESPRITES_LOOP


MOVESPRITES_TABLE
     defw NULL_MOV, UPDATE_FRAME, DRUNK_DRINKS, PRIEST_MOVES, SPRITE_MOVES, SPRITE_MOVES, SPRITE_MOVES, SPRITE_MOVES, NULL_MOV, NULL_MOV
     defw NULL_MOV, ROCKS_FALL, UF_EXPLOSION, ENEMY_MOVES


SPRITES_COLLIDE:
     ld iy, PRINCIPAL_SPR-BYTES_SPR

     SPCOLL_HOOK:
     ld de, BYTES_SPR
     add iy, de
     ld a, (iy)
     cp $ff
     ret z
     cp (ix+0)
     jr z, SPCOLL_HOOK

     and a                         ; Solo tiene que chequear las colisiones si uno de los dos sprites es el jugador
     jr z, SPCOLL_REALLY_CHECK
     ld a, (ix)
     and a
     ret nz

     SPCOLL_REALLY_CHECK:

     ld a, b
     ld d, (ix+1)
     ld e, (ix+2)

     cp 0       ; Izquierda
     jr nz, SCCP1
     dec e
     jr SPCOLL_HOR

     SCCP1:
     cp 1       ; Derecha
     jr nz, SCCP2
     inc e
     jr SPCOLL_HOR

     SCCP2:
     cp 2       ; Abajo
     jr nz, SCCP3
     ld a, (ix+3)
     neg
     and 1
     add a, d
     ld d, a

     jr SPCOLL_VER

     SCCP3:        ; Arriba
     ld a, (ix+3)
     and a
     jr nz, SPCOLL_VER
     dec d
     jr SPCOLL_VER


     SPCOLL_HOR:
     ld a, (iy+1)
     cp d
     jr nz, SPCOLL_HOOK

     ld a, (iy+3)
     cp (ix+3)
     jr nz, SPCOLL_HOOK

     ld a, (ix+4)
     cp (iy+4)
     jr nz, SPCOLL_HOOK

     ld a, (iy+2)
     cp e
     jr nz, SPCOLL_HOOK


     jr SPCOLL_COLISSION

     SPCOLL_VER:

     ld a, (ix+3)
     cp (iy+3)
     jr z, SPCOLL_HOOK

     ld a, (iy+1)
     cp d
     jr nz, SPCOLL_HOOK

     ld a, (iy+2)
     cp e
     jr z, SPCOLL_COLISSION

     ld a, (iy+4)
     cp (ix+4)
     jr z, SPCOLL_HOOK

     ld a, (iy+2)
     add a, (iy+4)
     sub e
     sub (ix+4)
     jr z, SPCOLL_COLISSION

     jp SPCOLL_HOOK

     SPCOLL_COLISSION:
     pop af
     

     ld a, (ix)
     add a, (iy)
     cp S_PIEDRA
     jr z, ENEMY_COLLIDE
;     jr z, ROCK_COLLIDE
     cp S_ENEMIGO
     jr z, ENEMY_COLLIDE
     scf
     ret nz

ENEMY_COLLIDE:
     push ix
     push iy
     ld e, 1
     call WP_PLAY_SFX
     pop iy
     pop ix

     ld a, (iy)
     and a
     jr z, ENEMY_AT_IX

     push iy
     pop ix

     ENEMY_AT_IX:
     call RF_EXPLODEROCK

     ld a, (ix+12)
     cp S_ENEMIGO
     jr z, EC_EXIT

     ld b, 5
     ROCK_COLLIDE_LOOP:
     push bc
     call MOVE_DOWN
     jr c, EC_EXIT
     call MOVE_SPRITES
     call UPDATE_ALL
     ld hl, CYCLES
     inc (hl)    
     pop bc
     djnz ROCK_COLLIDE_LOOP

     EC_EXIT:
     ld a, -1
     call CHANGE_ENERGY
     ld sp, STACK_POINTER
     jp ENDLESS_LOOP


;CHECK_SPRITES_INTERACT: Comprueba que haya un sprite en las coordenadas pasadas por BC, y, en ese caso, ejecuta la secuencia que corresponda
CHECK_SPRITES_INTERACT:
; Vamos a comprobar la casilla "mirada" por el jugador con las coordenadas de los sprites

      ; Facing: 0 (00) = izquierda; 1 (01) = derecha ; 2 (10) = Abajo; 3 (11) = Arriba
      ; H = sumar a vertical L = Sumar a horizontal (de jugador para alcanzar la "casilla mirada")

     ld a, (ix+5)
     bit 1, a
     jr z, NO_VERTICAL_FACING

     ; Si el facing es vertical, hay que recalcular la "siguiente casilla" (porque si el facing es hacia arriba y hay offset, no mira la siguiente casilla, sino la actual)

     and 1
     add a, a
     dec a
     neg
     add a, (ix+1)
     ld b, a

     NO_VERTICAL_FACING:

     ld iy, RESTOSPRITES + 78; BYTES_SPR + MAXSPRITES * BYTES_SPR

     CSI_LOOP:
     ld de, -BYTES_SPR
     add iy, de

     ld a, (iy)
;     cp $ff
     inc a
     jr z, CSI_LOOP
;     and a
     dec a
     ret z

     call SI_THIS_SPRITE
     jr CSI_LOOP

SI_THIS_SPRITE:

     ld a, (iy+1)
     cp (ix+1)          ; Coordenada "real"
     jr z, CSI_HOR

     cp b               ; Siguiente casilla hacia el facing
     jr z, CSI_HOR

     ; Ahora hay que sumar a las coordenadas del jugador el resultado de: Facing del jugador - Facing del otro sprite
     ; De esa manera, mirar� en la coordenada Y alineada con la parte "inferior" del jugador (si �ste no est� alineado)
     ; y una coordenada m�s arriba del mismo si el jugador est� alineado, pero el otro sprite no lo est�

     ld a, (ix+3)
     sub (iy+3)
     add a, b
     cp (iy+1)
     jr z, CSI_HOR

     ret

     CSI_HOR:

     ld a, (iy+2)
     cp (ix+2)                  ; Coordenada "real"
     jr z, SPRITES_INTERACT

     cp c                      ; Siguiente casilla hacia el facing
     jr z, SPRITES_INTERACT

     ; Ahora hay que sumar a las coordenadas del jugador el resultado de: Facing del otro sprite - Facing del jugador
     ; De esa manera, mirar� en la coordenada X alineada con la parte "derecha" del jugador (si �ste no est� alineado)
     ; y una coordenada m�s a la izquierda del mismo si el jugador est� alineado, pero el otro sprite no lo est�

     ld a, (ix+4)
     sub (iy+4)
     add a, c
     cp (iy+2)
     ret nz

     SPRITES_INTERACT:
     pop af                 ; Para volver una llamada m�s atr�s (si hay un sprite aqu�, ya no puede haber ning�n otro)

     ld a, (iy)
     cp 11
     ret nc

     ld hl, SPR_INT_TABLE-1
     call GET_BYTE_TABLE
     ld b, (hl)
     call RUNSCRIPT

     jp CM26 ; BREAK para que no busque en los ST's si ya ha interactuado con un sprite en esa posici�n

SPR_INT_TABLE:
     defb 1, 3, 4, 15, 14, 8, 6, 20, 17, 29
