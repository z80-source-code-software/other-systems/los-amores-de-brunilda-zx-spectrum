# \01 = Set attribute
# \0D = New line
#
#


San Arteixo do Montalvo
Xose's locksmith's
Xan's Inn
May God protect this house and its guests
Deus lari Berobreo aram posuit pro salute
# 05
St. Arteixo's hermitage
\01\45Galicia, 30th October 1830.\01\07
Two Franciscan monks go on a pilgrimage in the Way of St. James. \01\44Fray Gonzalo\01\07 and \01\44Fray Cesáreo\01\07 have been walking the whole day, so they decide to rest in the first village they find.
Having lost all faith, Fray Gonzalo became mad and was wandering the forest. \0D\0DSomeone saw him leading the Santa Compaña.\0D\0DFray Cesáreo died at pharmacist's home, and was buried in the village.
With a little luck, \01\46San Arteixo do Montalvo\01\07 will host them, as many other villages have done on their way.\01\30
# 10
Fray Gonzalo, that's not the inn. We must find it as soon as possible.
Fray Gonzalo, we must find a place to sleep. You don't really want to stay in the forest, do you?
Fray Cesáreo, we must find a place to sleep before it gets too dark.
Yes, brother. These forests are too scary and dangerous at night. You never know what could show up.
Oh no, not again with your superstitions and fears!
# 15
I can't help it, it's all because of the darkness. I'll never get used to it.
Ok, let's just look for a shelter, the night is falling.
May the peace of God be with you, brother. We are looking for a place to sleep tonight.
I'm very sorry, good people, I'm currently refurbishing the place and I have no rooms available.
But we need a place where to sleep. These forests are so creepy at night.
# 20
Calm down, Fray Cesáreo, we'll find a solution.
Indeed, my friends. I'm sure that Mr. Justiniano, the pharmacist, will be delighted to offer you staying at his place. He's very good friends with the priest, and since you are pilgrims, he'll be willing to help you out.
Look out! Hic! That house is haunted and doomed! Hic!
Shut up, you drunk!, Don't scare these good people.
The lady commited suicide hic! and her soul wanders around the house hic!
# 25
Shut up, drunk!
Don't worry, we don't believe in such things. Where can we find the pharmacist?
Just speak to Mr. Cirilo, the priest; he'll be in the church.
This village is doomed hic!, the only solution is hic! drinking as much as you can to forget hic!
Please, forgive this lost soul there, don't take him very seriously, he has an issue with alcohol.
# 30
Don't worry. Thanks a lot.
Please, let's talk to the priest as soon as possible.
Be welcome, brothers. I was waiting for you. This is a small town, and news travels fast.
Hello, brother. We are Fray Gonzalo and Fray Cesáreo, Apostle's Pilgrims. We are looking for a place to spend the night.
Yes, of course, you are looking for accomodation, but there are no rooms left in the inn.\0DYou should have come see me before.
# 35
We have been told that the pharmacist could host us if you speak with him.
Of course. I could even offer you some accomodation here in the sacristy, but I fear it may be too little compared to Mr.Justiniano's place.\0DHe is a rich man and his house has a lot of empty rooms.
Franciscans don't need luxury, it would be enough with a roof and a matress.
Of course, but for sure you will appreciate the kindness of a good christian who offers you a soft bed and a good dinner.\0DThat's something that you can't find so often, and besides, it would make the Walking much less hard to continue.
Is the pharmacist's house far away from here?
# 40
Just a little walk across the forest. It's better to leave now before night falls.
Forgive Fray Cesáreo, darkness makes him superstitious and scared.
I understand him. Night brings nothing but bad things, and, if possible, we should avoid it. Let's go.
Follow me, brothers. The way is short, but it's easy to get lost.\0DIf you get far accidentally, I'll wait for you, but anyways, let's hurry to arrive before the night falls.
My Brother, let's follow him. Night is approaching.
# 45
Knock, knock! Anybody there?
Mr. Cirilo! What a surprise to see you this late at night!
I know! It's not the best moment to walk around this neighborhood, hehe\0DOk, let us in, we have to talk to Mr. Justiniano.
Ok, but it's almost dinner time...
Even better: There is always room for 3 more people!
# 50
Dear friend Cirilo, I wasn't expecting you this late. What brings you here?
You know I don't go out at night, unless it's necessary.\0DThese two pilgrims have arrived to the village headed to Santiago, and they're looking for a place to stay. So I thought you could do a good deed.
Of course, Cirilo. Brothers, feel very welcome to my humble dwelling.\0DMaruxa, please prepare the guest rooms so they can stay the night... and I guess you are also hungry, right?
Indeed, thanks a lot, Mr Justiniano. God bless you.
I'm also hungry, the food smells so good that I have to stay and try it, hehe.
# 55
You will never change. Of course you will stay here for dinner... and to sleep too. It's already quite dark.
Please, follow me.
Dinner will be ready soon. Please come to the dining room as soon as you have settled down.
Right. Thanks a lot.
Come in, brothers. We were waiting for you.
#60
We are going to have some orujo drinks to help digestion.
Mr. Justiniano's orujo is really good. Should he sell it, he would be rich.
Nah! I'm not planning to sell. If I did, we wouldn't be able to enjoy it... hahaha.
That's right, you better don't sell it! Hehehe.
I would show you my laboratory room, but for sure you're not interested in medicine and scientific investigation.
#65
I am! I would love to have a look: I studied medicine before taking the holy orders.
A colleague! Right, right... We are going to have a great night.
I'm sorry: my body is asking for some sleep. You can keep the conversation, though. I will see you tomorrow.
I'm also going to sleep. A good pilgrim goes to bed early at night and wakes up early in the morning.
What a night! Too many nightmares. I shouldn't have drunk so much orujo.
#70
This is weird. It looks like Fray Cesáreo did not come to sleep here last night!
This door seems to be firmly closed.
I cannot leave without Fray Cesáreo.
Fray Gonzalo! You can't imagine what has happened.
Fray Cesáreo! Pharmacist, what's wrong?
#75
We still don't know. He appeared in the west side of the house, and that area is always locked.
It has always been locked since...
So, how come Fray Cesáreo was there?
We still don't know. I fell asleep last night in the laboratory room while Fray Cesáreo kept looking my experiments.\0DWhen I woke up I saw a bunch of papers all over the floor, which lead to the west side of the house, where I found him.
But, is he dead?
#80
No, He is still alive, but he is unconscious, and nothing seems to wake him.
We think he is under some kind of spell.
Mr. Cirilo, you are a priest! You cannot believe in superstitions.
Son, I long ago learned that everything is possible in these lands.
We should tell the Witch. She will help us out for sure.
#85
This is ridiculous, I think we should take him to the capital to be treated.
Trust us, Brother. This is not an illness of the body, but of the soul.
Only the Witch can help us. Please go find her. She lives in a cabin, in the forest between this house and the village. We'll stay here taking care of him if he wakes up.
What do you think about D. Justiniano, Brother?
A good Christian, generous and kind.
#90
I think so too, but I had a shiver when we entered this house. I hope it doesn't mean anything.
Don't try to fool yourself, that was probably just some air blowing.
It might be so. Ok, let's go have dinner!
Let's go have dinner, Brother. They are waiting for us.
My job is to open locks, no lock can resist me.
#95
This closet is open, but I don't see any reason why I should wear the housekeeper clothes... at least, not right now.
Later on...
Next morning...
That's a human skull! I won't touch it without good reason!
What a tragedy! This house just isn't the same since some years ago.
#100
I have to find the Witch to find out if she can help us.
She will not help us unless we pay for her services. Take this hen, it will be enough.
Something horrible has happened! Go up, they are waiting for you.
I ran out of orujo, and I will not get more until next month. This village needs more alcohol every day.\0DI would pay good money for just one bottle.
Excuse me, I need the services of a Witch. Could you tell me where to find her?
#105
Witch? It's said that there is one in the forest: Beware of her, she is not to be trusted, and of course, she never works for free.
You will find Xan's inn north of the village. You will get some help there.
But let me tell you something, Brother: in order to find her, you must believe in her. Your religious faith will not allow you so.
I cannot leave the village abandoning Fray Cesáreo here.
A travelling monk entering inside a Witch's lair. That's indeed a big surprise.
#110
Mr. Cirilo sent me to find you. My friend has fallen, according to him, under a spell which keeps him unconscious.
I will go see him, but my services aren't free.
Would you take this hen in exchange?
I will cook a soup with that hen which will be helpful for my old bones. Let's go see that bewitched friend.
All right. I'll try to find something for you.
#115
This old Witch is not like you, monks. You cannot buy me with gold.
If you want me to follow you, you will have to pay me with something.
Thank God you came... Look, this is the poor man.
It's obvious. West side of the house, night of the deceased, and his state... This is just Brunilda's curse.
Oh my God, Brunilda again!
#120
Brunilda? Who's Brunilda? Saint Brunilda? All this is absurd!
Not at all. Brunilda commited suicide in this house and her soul wanders around the house.\0DShe needed another soul to get out of the purgatory, and she took this poor man's.
I don't believe in these things. I'm sure he just suffered a collapse.
You don't have to believe in it, but if you don't do anything fast for your friend's soul, he will stay like that forever, and time is running out.
Do what she says, Fray Gonzalo, she knows what's going on.
#125
I suggest waiting until next morning to see if he wakes up. What do you think about it, Fray Gonzalo?
That sounds good.
If you change your mind, you know where to find me. Goodbye.
Fray Cesáreo is still unconscious. We have to take him tomorrow to the hospital in the city. I will try to rest a little now.
It's already midnight and I have not been able to sleep a single minute, what a night!
#130
Holy God!
Don't be afraid, Father. Listen to my story.
I cannot believe what I see!
I'm Brunilda, or better said, her spirit...\0D I used to serve in this house. Many years ago, I had a romance with Anton, the pharmacist's son. We kept it in secret, but the moment to tell about it finally came.
My beloved Anton went to the forest with his father to tell him about our love. But then a bunch of bandits found them and killed him.\0DWhen I heard the news, I couldn't resist it: madness invaded my mind and I committed suicide.
#135
I had the hope to be able to meet my beloved Anton in the afterlife, but for some reason I am trapped in this house.
And your history's related to my Brother Fray Cesáreo?
Very much. This house has been doomed since then and his soul is trapped, like mine.
I'm a man of faith, I can't believe this can be happening...
I know how to solve this situation. Our tragedies are chained: If you help me, you help him.
#140
Tell me, I will do what I can.
Go to the cemetery, behind the church, tonight. I will see you there.
Don't worry. I will open it.
This is my tomb. I don't know why, but I feel like something's wrong inside.
Something's wrong inside?
#145
I clearly sense something's going wrong inside the tomb. Please, dig it and open it.
But how?
Get a shovel. Look for it in some garden. Gardeners use them quite often.
I told you something was wrong in my tomb!
Holy Father! The skull is missing!
#150
Please, find my skull and put it next to its body.
I now understand why your soul is restless.
I will not find any peace until I am next to my loved one. When you get the skull, bury us together!
All that is happening now brings back bad some memories. Brunilda was so nice!
You'll see, monk. Let's do a little twist to the dance...
#155
Who's talking?
I'm taking your body somewhere else!
Noooo!
I cannot believe what I just saw!
Are you sure you want to quit? (Y/N)
# 160
Your faith will not allow you to help Brunilda, hahaha.
Fray Gonzalo, wake up! What are you doing here? How did you get in?
Erm... Mr. Cirilo, you will not believe me... Brunilda... her body... her tomb...
Brunilda? I knew it was something to do with her.\0DOnly one person can help us now: The Witch. Go find her, I will stay in the church, praying for your friend's soul.
That story about the maid's soul was enough. Many tragedies have already happened in this house.\0DI've decided to check my science books searching for a case like your friend's.
# 165
If you find something which could help us, bring it to me.
This mushroom is awesome! I didn't know they grew around here. I'm quite sure that it could be helpful in our case, but I still have to check it out in my laboratory.
Please, Brother. Leave that there. That skull belonged to my family since my grandfather went to Salamanca to study medicine.
This strange mushroom you brought me will be really helpful, I'm sure of that, but I need some time to check it.
Ok, I guess you came seeking advice, and advice I'll give you.\0D I know who has the skull and where you can find the four parts of Brunilda's skeleton.
# 170
The skeleton is hidden inside four old celtic caves. To get there, you first have to pick the water from the 7 pools of the Monte das Pozas, and water with it each of the monoliths which are placed next to their entrances.
The skull is at the pharmacist's  office: I have the feeling that he has a lot to say in this story. I don't think he was pleased with his son falling in love with the housemaid.\0DWhen you manage to gather all, or when you need help, come visit me.
I will give you a lantern to light up inside the caves, in exchange for 7 mushrooms.
Find those 7 mushrooms and come back.
Here you have the lantern. Now get the skeleton.
# 175
You still don't have the whole skeleton. How am I supposed to help you?
Monte das siete pozas
Watch out, landslides.
This is written in characters that my christian eyes don't comprehend... and even more: They shouldn't have seen.
That cave is too dark! It would be too dangerous to get in without any type of light with me!
# 180
The seven sacred pools of the Celts are in front of you. Their water contains the wisdom of the ancient Druids, and it will help you find your way.
That sign warns about landslides. I should not go beyond unless I have a good reason.
I already took the water from this pool.
The Witch told me that I needed water from each one of the seven pools to make it work.
Let's see what happens...
# 185
I should not waste the water.
The surface of the monolith does not reflect a single ray of sun.
All right, here are the bones. You won a battle, but not the war.
That will teach you not to be so arrogant in the future.
I will defeat you with my macabre dance.
# 190
Surrender and I will show mercy.
No way, never!
Repeat my dance, but do it in the right moment or I will beat you!
Take that, you little monk!
Vade retro, monster!
# 195
I will not fight again with that monster, not if I can avoid it.
Very good. I will now invoke Brunilda's soul. In the meantime, go to the cemetery to place the bones in their place.
Go to the cemetery to place the bones. Invoking a soul from the purgatory is a tough and dangerous job and it cannot be interrupted.
You have brought everything! Peace will at last be found!
Nevermind...Are all those parts from me?
# 200
Yes, all of the parts, even the skull, which I found at the pharmacist's office. 
No! This is not my skull! He already had this one before I died, it belonged to his grandfather.
I fooled you all, I own your skull!
You... you never liked us!
If I cannot be together with Antón, nobody will, and you stole him from me!
# 205
Give us the skull back, you gain nothing with this, Anton is dead already.
Yes, but revenge is very sweet, haha!
Thanks a lot, I never thought that the Witch was jealous of us.
There is nothing to worry about her now.
Brunilda, my love, we are finally together.
# 210
Yes, we only need your father's approval, and a mass for our souls.
Fray Gonzalo, will you help us out?
My diary will give you the clues to find the letters we wrote to each other.
Show them to my father: That should be enough for him to find out about us so he can accept it.
Search in my room, in the west side of the house.
# 215
I can see you still did not have enough. All right, I will show you my real power!
That orujo you brought me, Father, is awesome. It saved my life.
God be praised, a bottle of orujo... I want it! Get these coins, I think they will be enough.
I have been thinking about that orujo produced by the pharmacist, it could wake my brother up, don't you think so?
God be praised! Great idea. This orujo can resurrect the dead. Take this bottle, it's the best one, I'm sure that it will be useful... but don't tell D. Justiniano that I gave it to you, or he'll fire me.
# 220
You want to go to the west side? I'm afraid it's not possible. I'm the only one in charge of the keys, and I pray forgiveness, father, but I will not help you on that. Everybody is getting crazy!
I can't be dressed like this, somebody could see me. I need to put on my robes again.
The keys? You will do it under your responsability, but please don't go at night, it's already enough with what has happened so far.
Do not bother me anymore, I'm very busy.
The diary says that the letters are hidden behind a passage in the library, which can be acceded by pushing the bookshelf from the north corner.
# 225
The pharmacist has a really impressive library.
This one must be the one Brunilda told me: if I push it, it may move...
This is the chest that Brunilda described in her diary, but it's locked! I need to open it so I can get the letters.
Good lock!, but I for sure can open it, as long as I get some money to buy myself a meal!
I will open that chest in exchange for those coins.
# 230
These letters are from my son to... Brunilda? So they were in love and never dared to tell me! It was suicide for love! Now I understand. We must have a Mass for their rest: I will talk to the priest. I'll be waiting for you in the cemetery, Father.
This crucifix allowed me to pray when I was about to lose my faith. It saved me from falling into madness!
Brother, I'm going crazy with all this. I will keep praying with the hope that God solves it with his greatness.
Although I do not want to recognize it, the superstitions from these people make my faith falter.
I feel my faith grow when I'm within these holy walls.
# 235
Thanks a lot, Fray Gonzalo, knowing the whole story has comforted me a lot.
They already rest in adjacent tombs. They only miss a Mass in order to rest in peace forever and get Fray Cesáreo back in our world.
I will not allow that. My soul is condemned forever, but theirs will never rest in peace!
Evil spirit!, go back to the abyss that you should not ever have abandoned.
Oh, no! I am melting, I am melting!
# 240
Finally evil prevailed.\0D\0DFray Gonzalo, the priest and the pharmacist died in the cemetery, and their souls, along with the one from Fray Cesáreo, were trapped wandering around.\0D\0DThe village remained doomed and it was abandoned for centuries.
We finally managed to get their soul some peace, and get rid of the Witch forever.
And what about Fray Cesáreo? Will he be awake?
Hurry, let's go home to find out!
Fray Cesáreo, God be praised! He is awake!
# 245
I feel a little bit dizzy, but I'm fine. How long have I been sleeping?
A lot. And you missed a good story. There is a lot to tell in the dinner.
What a great story, Fray Gonzalo. I already knew that these forests kept some big dangers.
What really matters is that everything went well in the end.\0DLet's go to sleep, it was a copious dinner, and the pharmacist's orujo is really strong.
Fray Gonzalo, let's go, it's already late, we should resume our way.
# 250
Eh? Wow, I fell asleep, last night's orujo... I dreamt about Brunilda... Her soul visited me again in dreams...
And, after those words, both monks continued their march to Santiago, and who knows what new adventures they will find on the Way...\0D\0D         The end?
Fray Cesáreo, Brunilda, her story, you falling unconscious, Anton, their souls, the Witch...
But, what are you talking about? We got here last night and had a nice dinner with the pharmacist, you had several glasses of orujo and I see they gave you nightmares. You should not abuse alcohol, Brother, haha!
So...
# 255
So...nothing! Come on Brother, let's retake our way, Santiago is still far away.
#--------------------------------------------------------------------------------------------------------------------------------
# TABLA 2
#--------------------------------------------------------------------------------------------------------------------------------
The housekeeper's room, unlike the rest of the house, is very messy: she hasn't even closed her closet, for instance.
I need the west side key.
No, father. No way. I can't leave you the key, and even less after everything that happened.
This bookshelf is full of medicine books.
Chemistry books find their place on these shelves.
#  5
From what you see, the pharmacist is also very interested in Mathematics.
Botanics is one of the topics studied by Mr. Justiniano.
Historical narratives fill these shelves.
Religious books are placed in these shelves.
This one seems to be Brunilda's room.
#  10
It's been a long time since anybody read any of the novels from these shelves.
Love letters are inside that chest. Try to find someone that can unlock it.
