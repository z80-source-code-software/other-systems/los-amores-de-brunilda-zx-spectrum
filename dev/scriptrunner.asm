;RUNSCRIPT: Corre el script n� B
RUNSCRIPT:
         ;ld hl, PLAYER_CANMOVE
	 ;ld (hl), 1

	 ld hl, SCRIPT_TABLE
	 ld a, b
	 call GET_WORD_TABLE
	 ex de, hl

;RUNSCRIPT_RUN0: Punto alternativo de entrada, con la direcci�n del SCRIPT en HL
RUNSCRIPT_RUN0:
      dec hl
      ld ix, PRINCIPAL_SPR            ; Empezamos con IX apuntando al jugador. Eso nos permite ahorrar 2 bytes en los scripts que hacen de "puertas"
      call RUNSCRIPT_RUN
      jp SHOW_MARCADOR
;      scf
;      ret


RUNSCRIPT_RUN:
         inc hl
	 ld a, (hl)
 	 cp $ff
	 ret z

	 cp END_IF
	 jr z, RUNSCRIPT_RUN

	  cp BREAKPOINT
	  jr nz, RSR_LETSGO
	  BREAKPOINT_HERE:
	  di
	  halt
	  jr RUNSCRIPT_RUN
	  RSR_LETSGO:

         push hl
	 exx
	 ld de, RUNSCRIPT_RET
	 push de

;	 dec a
	 ld hl, COMMAND_TABLE -2
	 call GET_WORD_TABLE
	 push de
	 exx
	 inc hl
	 ld a, (hl)
	 inc hl
	 ret

	 RUNSCRIPT_RET:
	 pop hl
	 ld a, (hl)
	 exx
	 ld hl, PARAMETERS-1
	 call GET_BYTE_TABLE
	 exx
	 ld d, 0
	 ld e, a
	 add hl, de
;	 dec hl
	 jp RUNSCRIPT_RUN+1

	 CM01:				  ; Selecciona personaje
	 ld ix, PRINCIPAL_SPR - BYTES_SPR
	 ld de, BYTES_SPR
	 RS_CP1_LOOP:
	 add ix, de
	 ld b, (ix+0)
	 cp b
	 jr nz, RS_CP1_LOOP
	 ret

	 CM02:			          ; El personaje dice...
	 ld de, MESS

	 CM02_HOOK0:
	 ld (MC_FUNC_HOOK+1), de
	 push hl	  
	 call CM01
	 ld a, (ix+0)
	 call SOMEONE_SAYS
	 pop hl
	 ld e, (hl)
	 CM02_HOOK:
         ld bc, $1507
	 push ix                     ; Hay que preservar IX porque se destruye al descomprimir el texto
	 call MESS_CALLER
	 call KEY_AFTERTEXT
	 pop ix
	 ret

	 CM03:			          ; Personaje izquierda...
	 ld b, 0
	 ld de, SL_HOOK0	; No revises colisiones
	 jr RS_MOVE

	 CM04:			          ; Personaje derecha...
	 ld b, 1
	 ld de, SR_HOOK0	; No revises colisiones
	 jr RS_MOVE

	 CM05:			          ; Personaje arriba...
	 ld b, 3
	 ld de, SPRITE_UP
	 jr RS_MOVE

	 CM06:			          ; Personaje abajo
	 ld b, 2
	 ld de, SPRITE_DOWN
;	 jr RS_MOVE

	 RS_MOVE:			  ; Mover el personaje
	 ld (SCRIPT_POINTER), de
	 push af
	 call SPRITE_FACING_0
	 pop af
	 and a
	 ret z
	 ld b, a
	 RSMOVE_LOOP:
	 push bc
	 ld hl, MY_RET
	 push hl
	 ld h, d
	 ld l, e
	 ld hl, (SCRIPT_POINTER)
	 jp (hl)
	 MY_RET:
	 pop bc
	 djnz RSMOVE_LOOP
	 ret


	 CM07:                            ; Coordenadas al personaje
	 ld (ix+1), a
	 ld a, (hl)
	 ld (ix+2), a
	 ld (ix+3), 0
	 ld (ix+4), 0
	 ret

	 CM08:                            ; Coordenadas c�mara
	 ld (CAMERA_Y), a
	 ld a, (hl)
	 ld (CAMERA_X), a
;	 xor a
;	 ld (CAMERA_OFFSET_X), a
;	 ld (CAMERA_OFFSET_Y), a
         jp CALC_MAP_ADDR

	 CM09:                            ; UpdateAll
	 halt
	 push ix
         call UPDATE_ALL
         pop ix
         ret

	 CM14:                            ; Pause
	 and a
	 jp z, WAITFORKEY_NOUPDATE
	 ld b, a
;	 call WAITFORNOKEY
	 CM14_0:
	 dec b
	 ret z
	 halt

         xor a
         in a, ($fe)
         cpl
         and $1f
         ret nz                    ; Si pulsa una tecla, nos volvemos

	 jr CM14_0

	 CM15:					; Coger objeto
	 push ix
	 call CHECK_OBJECT
	 ld (OBJ_REF_IX), ix
	 ld a, (FLAGS+5)
	 cp O_SETA_N
	 jr c, CM15_HOOK
	 cp O_JARRA
	 jr nc, CM15_HOOK
	 ld hl, FLAGS
	 ld d, 0
	 sub 10
	 ld e, a
	 add hl, de
	 inc (hl)

	 CM15_HOOK:
	 ld e, 6
	 call WP_PLAY_SFX
	 call GET_THIS_OBJECT
	 call DRAW_INVENTORY
	 pop ix
	 ret


	 CHECK_OBJECT: ; Lee el siguiente byte del script, y si no es $ff, lo pone en el flag 5
	 cp $ff
	 ret z
	 ld (FLAGS+5), a

	 ld b, OBJ_NUM
	 ld ix, OBJ_DATA
	 ld de, BYTES_OBJ

	 CO_FINDOBJ:
	 cp (ix+3)
	 ret z
	 
	 add ix, de
	 djnz CO_FINDOBJ
	 ld ix, 0
	 ret

	 CM17:                            ; FadeOut
	 call FADE_OUT
	 xor a
	 call CLS
	 jp RENEW_BUFFER

	 CM18:                            ; Flag valor absoluto
	 call SEARCH_FLAG
	 ld a, (hl)
	 ld (de), a
	 ret

	 CM19:                            ; Flag ADD
	 call SEARCH_FLAG
	 add a, (hl)
	 ld (de), a
	 ret

         SEARCH_FLAG: ; Pone DE apuntando al flag A, y pone en A el valor de ese flag. (HL apunta al siguiente valor despu�s del n� flag)
	 ex de, hl
	 ld b, 0
	 ld c, a
	 ld hl, FLAGS
	 add hl, bc
	 ld a, (hl)
	 ex de, hl
	 ret


	 CM20:
         ld hl, PATH_TO_FOLLOW
         ld de, PATH_TO_FOLLOW+1
         ld bc, 3
         ld (hl), a
         ldir
         ret


	 CM22:                            ; FadeIn
	 push ix
	 call FADE_IN
	 pop ix
	 ret

         CM23:          ; GOTO
	 ld hl, SCRIPT_TABLE
	 call GET_WORD_TABLE
	 ex de, hl
         pop af
         pop af
         jp RUNSCRIPT_RUN + 1

	 CM24:		; MESSAGE
	 ld b, 4
	 ld d, a
	 call SETRAMBANK
	 ld b, d
	 ld c, (hl)
	 inc hl
	 ld e, (hl)
	 ld hl, TEXT_ATTR
	 ld (hl), 7
	 inc hl			; TEXT_INIT
	 ld (hl), c
	 call MESS
;	 ld b, 0
	 jp SETRAMBANK0

	 CM25: ; SET_GFXPTR
	 ld e, a
	 ld d, (hl)
	 ld (ix+6), e
	 ld (ix+7), d
	 ld (ix+8), e
	 ld (ix+9), d
	 ret

         CM26:   ; BREAK
         call SHOW_MARCADOR
         ld sp, STACK_POINTER
	 scf
         jp ENDLESS_LOOP

         CM28:          ; SP_SWITCH_TO
         ld (ix), a
         ret

         CM29:          ; NEW_SPRITE
         ld b, (hl)
         inc hl
         ld c, (hl)
         inc hl
         ld e, (hl)
         inc hl
         ld d, (hl)
         ex de, hl
         jp ADD_SPRITE

         CM30:            ; SET_MOVEMENT
         ld (ix+10), a
	 ld e, a
         ld a, (hl)
         ld (ix+11), a
	 ld d, a
	 ex de, hl
	 inc hl
	 ld a, (hl)
         ld (ix+12), a
         ret

         CM31:            ; ADJUST_OFFSET
         ld (ix+3), a
         ld a, (hl)
         ld (ix+4), a
         ret

	 CM32:                              ; SET_STILE
	 ld b, a
	 ld c, (hl)
	 inc hl
	 ld e, (hl)
	 call LOCATE_SUPERTILE
	 ld (hl), e
	 ret


	 CM33:                  ; RUN_FIGHT
	 ld e, a
	 ld a, (FLAGS+12)
	 and $f8
	 or e
;	 ld hl, (FLAGS+11)
;	 or $f8
;	 or (hl)
;	 ld (hl), a
	 ld (FLAGS+12), a
	 jp LETS_FIGHT

	 CM34:                                ; CREATE_OBJ
         ld d, a
         ld e, (hl)
         inc hl
         ld b, (hl)
         inc hl
         ld c, (hl)
         ex de, hl
         jp ADD_OBJECT
;
	 ;CM35:                                ; PLAY_SONG
	 ;ld e, a
	 ;jp WP_PLAY_SONG
;
	 ;CM36:                                ; PLAY_SFX
	 ;ld e, a
	 ;jp WP_PLAY_SFX

	 CM40:					; FIRE_EFFECT
	 ld hl, FIRE_PALLETE2
	 ld (BUFFER_ADDR), hl
	 xor a
	 ld (SWAP_BACKGROUND_ACTIVE), a

	 call BURN_SCREEN
	 ld hl, BUFFER_1
	 ld (BUFFER_ADDR), hl
	 ret

	 CM41:					; CHAPTER_SIGN
	 push af
	 xor a
	 call CLS 
	 ld bc, $0302
	 ld de, $1806
	 call DRAW_CBITMAP

	 pop af
	 push af
	 ld bc, $031b
	 ld de, $0406
	 call DRAW_CBITMAP


	 ld b, 4
	 call SETRAMBANK
	 pop af
	 dec a
	 ld d, a
	 ld bc, $0a0f
	 ld hl, CHAPTER_NAME
	 call NAME

	 ld a, (FLAGS)
	 and a
	 jr z, SKIP_PWD

	 call GEN_PWD 
	 ld bc, $0e0b
	 ld hl, PASSWORD_MSG
	 ld d, 0
	 call NAME 

	 SKIP_PWD: 

;	 ld b, 0
	 call SETRAMBANK0


	 ld hl, FLAGS+1
	 set 7, (hl)

	 ld a, (FLAGS+4)
	 call LOAD_MAP

	 call WAITFORKEY_NOUPDATE
	 jp CM17 

	 ;ret

	 CM42:
	 ld de, MESS_2NDTABLE
         jp CM02_HOOK0



         CM43: ; BUCLE
         dec hl
	 ld b, a
         LOOP_LOOP:
             push hl
             push bc                      ; Contador del bucle
             call RUNSCRIPT_RUN
             pop bc
             pop de
             ex de, hl                    ; Restauro HL al principio del bucle, y en DE lo tengo apuntando al END_LOOP ($ff) (es el HL que traje del CALL RUNSCRIPT_RUN)
         djnz LOOP_LOOP

         pop af
         pop af
         ex de, hl                        ; Pongo el puntero al script apuntando al END_LOOP (leer� desde justo despu�s)
         jp RUNSCRIPT_RUN

	 CM44:                            ; IF_FLAG_EQ
	 call SEARCH_FLAG
	 cp (hl)
         jr nz, SKIP_THIS_IF
	 ret
         ;CONDITION_TRUE:
	 ;pop af
         ;pop af
         ;call RUNSCRIPT_RUN
         ;jp RUNSCRIPT_RUN

	 CM45:                            ; IF_FLAG_NOTEQ
	 call SEARCH_FLAG
	 cp (hl)
         jr z, SKIP_THIS_IF
         ret;jr CONDITION_TRUE

	 CM46:                            ; IF_OBJ_CARR
	 dec hl
	 exx
	 ld b, MAX_INVENTORY
	 ld hl, INVENTORY
	 IFOBJCAR_LOOP:
	 cp (hl)
	 jr z, IFOBJCAR_HOOK
	 inc hl
	 djnz IFOBJCAR_LOOP
	 exx
	 jr SKIP_THIS_IF
	 IFOBJCAR_HOOK:
         exx
         ret;jr CONDITION_TRUE

         CM47:                            ; IF_FLAG_MAJ
	 call SEARCH_FLAG
	 cp (hl)
         jr c, SKIP_THIS_IF
         ret;jr CONDITION_TRUE

         CM48:                            ; IF_FLAG_NOTMAJ
	 call SEARCH_FLAG
	 cp (hl)
         jr nc, SKIP_THIS_IF
         ret;jr CONDITION_TRUE

	 CM49:				; IF_OBJ_NOTCARR
	 dec hl
	 exx
	 ld b, MAX_INVENTORY
	 ld hl, INVENTORY
	 IFOBJNOTCAR_LOOP:
	 cp (hl)
	 jr z, IFOBJNOTCAR_HOOK
	 inc hl
	 djnz IFOBJNOTCAR_LOOP
	 exx
	 ret;jr CONDITION_TRUE
	 IFOBJNOTCAR_HOOK:
         exx
         jr SKIP_THIS_IF

         CM50:					; IF_FLAG_BIT
         call SEARCH_FLAG
         ld b, (hl)
         ld c, 1
         inc b
         IFBITFLAG_LOOP:
         dec b
         jr z, IFBITFLAG_HOOK
         sla c
         jr IFBITFLAG_LOOP

         IFBITFLAG_HOOK:
         and c
         jr z, SKIP_THIS_IF
         ret;jr CONDITION_TRUE

         SKIP_THIS_IF:
         pop af
         pop af
         ld de, RUNSCRIPT_RUN
         push de
	 SKIP_THIS_IF_:
         inc hl
         ld a, (hl)
         cp $fe
         ret nc

         push hl
         ld hl, PARAMETERS-1
         ld e, a
         ld d, 0
         add hl, de
         ld e, (hl)
         pop hl
	 ld d, 0
	 dec de
	 add hl, de

         cp DO_LOOP
         call nc, SKIP_THIS_IF_

         jr SKIP_THIS_IF_

         CM51:					; CONFIRM
	 ld a, KEYCONFIRM_YES
         call KTEST1
         ret nc

	 ld a, KEYCONFIRM_NO
	 call KTEST1
	 jr nc, SKIP_THIS_IF

	 jr CM51
         


PARAMETERS:

    defb 2, 3, 2, 2, 2, 2, 3, 3, 1, 1
    defb 1, 1, 1, 2, 2, 2, 1, 3, 3, 2
    defb 2, 1, 2, 4, 3, 1, 1, 2, 6, 3
    defb 3, 4, 2, 5, 2, 2, 1, 1, 1, 1
    defb 2, 3

    defb 2, 3, 3, 2
    defb 3, 3, 2, 3, 1


COMMAND_TABLE:
     defw CM01, CM02, CM03, CM04, CM05, CM06, CM07, CM08, CM09, CAMERA_LEFT
     defw CAMERA_RIGHT, CAMERA_UP, CAMERA_DOWN, CM14, CM15, DESTROY_OBJECT, CM17, CM18, CM19, CM20
     defw LOAD_MAP, CM22, CM23, CM24, CM25, CM26, AUTOCAM, CM28, CM29, CM30
     defw CM31, CM32, CM33, CM34, WP_PLAY_SONG, WP_PLAY_SFX, WP_STOP_SOUND, ESPIRAL, INIT, CM40
     defw CM41, CM42

     defw CM43, CM44, CM45, CM46, CM47, CM48, CM49, CM50, CM51

KEY_AFTERTEXT:
     ;di
     ld b, 4
     call SETRAMBANK
     call KEY_TO_ENDTEXT
;     ld b, 0
     call SETRAMBANK0
     call WAITFORKEYR
;     ei
     ret


MESS_CALLER:
      ;di
      exx
      ld b, 4
      call SETRAMBANK
      exx

      MC_FUNC_HOOK:
      call MESS
;      ld b, 0
      call SETRAMBANK0
;      ei
      ret

; SOMEONE_SAYS: Imprime el gr�fico del sprite A, y muestra su nombre en el pergamino limpio

SOMEONE_SAYS:
     ;di
     push af
     call SHOW_PERGAMINO
     pop af
     push af
     ld bc, $1400
     call DRAW_SPRITEx2

     ld b, 4
     call SETRAMBANK

     pop af
     ld d, a
     ld hl, CHAR_NAMES

     SOMEONE_SAYS_HOOK:
     ld bc, TEXT_INIT
     call NAME

     ld hl, TEXT_ATTR
     ld (hl), STANDARD_ATTR
     ld a, 7
     ld (X_INIT), a

;     ld a, 1
;     ld (LINES_IN_A_ROW), a

;     ld b, 0
     jp SETRAMBANK0
;     ei
;     ret

BOSS_SAYS:
     call DRAWBOSS
     ld b, 4
     call SETRAMBANK
     call NL_HOOK
     ld a, (FLAGS+12)
     and 7
;     cp 6
;     jr nz, BS_HOOK
;     dec a
;     BS_HOOK:
     ld d, a
     ld hl, BOSS_NAMES
     jr SOMEONE_SAYS_HOOK

include "scripts.asm"
