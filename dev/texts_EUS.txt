# \01 = Set attribute
# \0D = New line
#
#

San Arteixo do Montalvo
Xose sarraila-denda
Xan ostatua
Jainkoak etxe hau eta bere biztanleak babestu ditzan
Deus lari Berobreo aram posuit pro salute
# 05
San Arteixo baseliza
\01\45Galizia, 1830ko urriaren 30.\01\07
Bi monje frantziskotar Santiagoko bidetik abiatzen dira.\0D\0D\01\44Fray Gonzalo\01\07 eta \01\44Fray Cesareo\01\07 egun osoa daramate ibiltzen eta aurkitzen duten lehen herrixkan pausatzea erabakitzen dute.
Sinesmen guztia galtzean, Fray Gonzalo zoratu zen eta nora ezean geratu zen basoetan.\0D\0DEdonork Santa Campaña gidatzen ikusi zuela kontatzen du.\0D\0DFray Cesareo botikariaren etxean hil zen eta herrixkan hobiratu egin zen.
Zorte pixka batekin,  \01\46San Arteixo do Montalvo\01\07 hartuko die zenbat eta besteak egin duten bere bidaian.\01\30
# 10
Fray Gonzalo, hori ez da ostatua. Lehenbailehen aurkitu behar dugu. 
Fray Gonzalo, lo egiteko leku bat aurkitu behar dugu. Basoan egitea ez izango duzu nahi?
Ilundu baino lehen non ostatatu aurkitu behar dugu.
Bai, anaia. Niri alderdi hauek ikaratzen naute gauean. Inork daki zer dagoen. 
Zuen beldur eta sineskeriekin bazaudete?
# 15
Ezin dut atxikitu, iluntasuna da.
Bila dezagun. Gaua gainean etortzen zaigu.
Jainkoaren pakea zurekin izan dezan, anaia. Gaurko gauarako ostatua aurkitzen dugu.
Sentitzen dut gizon onak, berrikuntza egiten ari naiz eta ez daukat gela librerik.
Baina lo egiteko leku bat behar dugu, baso hauek beldur handia ematen dute gauean.
# 20
Lasai Fray Cesareo, edozein konponbide aurkituko dugu.
Noski, gizon onak. Dudarik gabe Justiniano jauna botikariak poz-pozik egongo dela bere etxea eskaintzeaz. Apaizaren lagun handia da eta zuek erromesak izatean, ostatu ematearen karitatea izango du.
Kontuz! ic! Otsoaren ahora bidaltzen zaituzte, ic! Etxe hau madarikatuta dago, ic!
Isildu zaitez mozkorti, ez ikaratu Jainkoaren gizon hauei.
Mirabeak bere burua hil zen, ic!, eta bere arima korridoretatik alderrai ibiltzen dut, ic!
# 25
Isildu zaitez mozkorti!
Ez kezkatu, guk ez gara superstiziosoak. Non aurkitu dezaket botikariak?
Hobereza Cirilorekin jauna hitz egin dezazuen da, apaiza, elizan.
Herri hau madarikatuta dago, ic!, konponbide bakarra, ic!, edan egiten ahaztea da, ic!
Barkatu alkoholarengatik galduta dagoen arima honi, ez sinestu.
# 30
Eskerrik asko Fray Gonzalo, orain bakean pausatzen dugu.
Mesedez, apaizarekin hitz egin dezagun ahalik eta lasterren.
Ongi etorri, anaiak. Itxaroten nengoen. Hau herrixka txikia da eta berriak hegan egiten dute.
Gabon, anaia. Fray Gonzalo eta Fray Cesareo gara, apostolaren erromesak, eta guau non pasatzea bilatzen ari gara...
Bai, bai, noski, ostatu nahi duzuea eta bentan ez dago lekurik.\0DNiri ikustera etorri behar izan zenuten ezer baino lehen.
#35
Zuk artekaritzen bazenuan botikariak ostatu emango ahal izango duela esan digute.
Bai horixe. Baita nik ere, hemen sakristian, baina txikia da eta ez daukat hainbeste leku Justiniano jauna bezala.\0DDirudun gizon bat da, eta bere etxea gela huts asko dauka.
Frantziskotarrak ez gara luxuzko lagunak: sabai eta lastaira bat bakarrik nahi dugu.
Bai horixe, baina seguru aski ohe bigun bat eta afari on bat eskaintzen duen kristau baten adeitasuna eskertuko dutela.\0DEz beti aurkezten da aukera hau eta hori bidea guztiz gogorrago egingo duela.
Botikariaren etxea oso urrun dago?
#40
Ibilaldi bat basotik. Orain irtetea onena da, gau itxita izan baino lehen.
Barkatu Fray Cesareori, iluntasuna superstuzuisi eta beldurti bihurtzen du. 
Ongi egiten du. Gaua gauza txarren aterpea da eta zuhurtzia dohain bat da. Atera gaitez.
Segui, anaiak. Bidea motxa da para galtzea erreza da.\0DGehiegi urruntzen bazarete itxarongo zaituztet, baina azkar eman dezagun gaua izan baino lehen heltzeko.
Hermano, Cirilo jauna jarrai dezagun. Gaua erortzeko zorian dago.
#45
Toc, toc!
Cirilo jauna! Ze ustekabea gaueko hordu hauetan!
Bai, ezta? Ez da ordua leku bazter hauetatik joateko, jeje\0DAnda, utzi sartzen, Justiniano jaunarekin hitz egin behar dugu.
Bai, baina ia afaltzeko hordua da...
Gehiago nire faborez: batek afaltzen duenean, lau afaltzen dute... jeje
#50
Cirilo laguna, ez nizun hain berandu itxaroten. Zerk ekartzen zaitu hemendik?
Badakizu ez dudala gauetik irteten, beharrezkoa ez bada.\0DHerrizkara bi anai hauek heldu dira erromes ibiltzen Santiagora, eta gaua non pasatzea bilatzen dute. Orduan ekintza on bat egin ahal zenuela pentsatu nuen. 
Bai horixe, Cirilo. Anaiak, ongi etorri nire bizileku apalari.\0DMaruxa, gonbidatuen logelak prestatu, anaiak lo egitera geratzen dira... eta afaltzera, gosea izango dutelako, ezta?
Bai, eskerrik asko, Justiniano jauna. Jainkoak ordain dezazun.
Nik ere gosea daukat, oso ondo usaintzen du eta jaki hauek probatu behar dira, jeje
#55
Zuk beti berdin. Noski afaltzera geratuko zarela, eta lo egitera, gau itxita baita.
Mesedez, segi nazazue.
Afaria laster prest egongo da. Jarri ostean jantokira jaitsi.
Ados. Eskerrik asko.
Pasa, anaiak. Itxaroten zintuztegun.
#60
Orain digestioa egitera lagunduko duten uxual kopatxo batzuk hartuko ditugu.
Justiniano jaunako uxuala ona da benetan, salduz gero urrezkoa egingo litzateke.
Tontakeriak, ez dut salduko: salduz gero ezin izango genuke gozatu... jaja
Egia, ez saldu hobeto... jeje
Nire laborategia erakutsiko lizueke, baina seguru aski zuei ez zaituzte ikerketazientifikoa gustatzen.
#65
Niri bai, begiratu bat ematea zoratuko litzaidake.
Lagun bat, a zer nolakoa! Gau-jai bikain bat edukiko dugula uste dut.
Barkatu, baina nire hezurrak atsedena eskatzen didate. Segi zuen elkarrizketarekin, bihar arte.
Nik ere lo egitera joango naiz. Erromes ona laster oheratzen da eta oso goiz jaikitzen da.
Zelako gaua, zelako amesgaiztoak. Ez nukeen hainbeste uxual edan behar.
#70
Badirudi Fray Cesareo ez duela hemen lo egin gaur gauean.
Ate hau irmoki itxika dirudi.
Ezin nahiz joan hemendik Fray Cesareorik gabe.
Fray Gonzalo! Ezin duzu irudikatu zer gertatu den.
Fray Cesareo! Botikaria, zer gertatzen zaizu?
#75
Ez dakigu, etxeko mendealdeko hegalean agertu zen, beti itxita dagoen alde hori...
Beti itxita dago momentutik...
Orduan, nola da hor zegoela Fray Cesareok?
Ez dakigu. Bart laborategian lo geratu nintzen bitartean Fray Cesareok bere experimentuak begiratzen zegoela. \0DEsnatzean mendealdeko hegalerarte arrasto bat uzten zuten papel utzitak ikusi nituen eta han aurkitu nuen, lurrean.
Hilda dago?
#80
Ez, oraindik bizirik dago, baina konorterik gabe dago eta ezer ez dio esnatzen.
Sorginkeri baten hartuta dagoela uste dugu.
Cirilo jauna zu apaiza zara! Ez duzu superstizioetan sinestu behar.
Semea, orain dela asko ikasu nuela lur hauetan dena ahal dela.
Sorginari ohartarazi beharko genuke. Seguru asko lagundu ahal genuela.
#85
Barregarria da, hiriburura trata dezan eraman beharko genuke.
Jaramon egin dezagun. Hau ez da gorputzako eritasun bat, arimakoa baizik.
Bakarrik sorgina lagundu ahal gautiu. Etxola batean bizi da, etxe hau eta herriaren artean dagoen basoan. Gu gaixoa zaintzen geratuko gara esnatzen bada.
Zer iruditzen zaizu Justiniano jauna, anaia?
Kritau on, noble eta atsegin bat.
#90
Niri ere bai, baina... etxean sartzean hotzikara bat sentitu dut.
Ez bilatu haize-larter bat baino gehiago arrazoirik, anaia.
Hori izango da. Jaitsi gaitezen afaltzera...
Jaitsi gaitezen afaltzera, anaia. Itxaroten gaituzte.
Nire lana sarrailak irekitzea da, ez dago eutsitzen zaidana.
#95
Armairu hau irekita dago, baina ez dut aurkitzen giltzariaren arropak ipintzeko arrazoirik, momentu honetan behintzat.
Beranduago...
Hurrengo goizean...
Hau gizaki garezurra da! Ez dut hartuko arrazoi on bat barik.
Zelako zorigaitza, etxea hau ez da orain dela urte batzuk bezalakoa.
#100
Sorginaren bila joan behar naiz ea lagundu ahal digun.
Ez dio lagunduko bere zerbitzuengatik ordaintzen ez badu. Oilo hay daukat, nahikoa izango da.
Gauza ikaragarri bat gertatu da! Igo, gizona goian itzaroten zaitu, bere lagun txiroarekin.
Uxuala bukatu zait eta hurrengo hilabetera arte ez zait eskaera etortzen.\0DDirua emango nuke botila batengatik, nahiz eta bat izan.
Barkatu, sorgin baten zerbitzuak behar ditut... Non aurki dezaket esan ahal didazu?
#105
Sorgina? Basoan bizitzen da bat esaten dutena dela: kontuz berarekin, ez da fidatzekoa eta dudarik gabe inoiz ez du dohainik lan egiten.
Herrixkaren iparraldean Xan ostatua aurkituko duzue. Seguru aski han lagundu ahal izango zaituztete.
Baina gauza bat ezango dizuet, anaia: bila ahal izateko, berarengan konfiatu behar izango duzue. Fede hori bere elizan ez dio baimena emango.
Ezin naiz herrixkara joan eta Fray Cesareo gaixoari axolagabe utzi..
Kanpotar monje bat sorgina baten etxean sartzen. Hori bai dela sorpresa bat.
#110
Cirilo jauna zure bila bidali nau. Nire kidea erori da, beraren ustez, arduragabe mantentzen dion sorginkeri baten preso.
Ikustera joango naiz, baina nire serbitzuak ez dira hutsean.
Oilo baten truke ontzat hartuko zenuke?
Oilo horrekin nire hezur gaixoei on egingo dien salda bat egingo dut. Joan gaitezen sorgindun hori ikustera.
Ondo dago. Atsegin egiten zaitun zeozer bilatuko dut.
#115
Sorgin zahar hau ez da apaizak pentsatzen duzuen bezala. Ez nau urrearekin erosiko.
Laguntzea nahi baduzu, zeorerrekin ordaindu beharko nauzu.
Eskerrik jainkoari etorri zarela, ikusi, gaizon gaixo honi.
Argi dago. mendebaldeko egalean, hildakoen gauga eta estatu hauetan es da Brunildaren madarikazio baino gehiago izan.
Oh, nire jainkoa, Brunilda berriz.
#120
Brunilda? Nor da Brunilda? Hau barrregarria da!
Ezer ez da barregarria. Brunilda etxe honetan suizidatu egin zen eta bere arima etxetik alderrai ibiltzen da.\0DLibragarri honetatik irtetzeko beste arima behar du, eta gizon gaixoarena hartu du.
Ez dut gauza horietan sinesten, seguru aski kolapso bat dela.
Ez sinestu nahi ez baduzu, baina pere arimarengatik ezer ez baduzu eginten zure laguna horrela jarraituko du hil arte, eta laster gertatuko del gauza bat da egoera honetan jarraituz gero.
Egin ezaiozu kasu, Fray Gonzalo, bera badaki esaten duena.
#125
Ea esnatzen bada itxarotea proposatzen dut eta bihar erabaki bat hartuko dugu. Zer idutzen zaizu Fray Gonzalo?
Askoz hobeto horrela.
Iritziz aldatzen baduzue, badakizue non bizi naizen, agur.
Fray Cesareok arduragabe segi. Bihar hirira eraman beharko genuke. Orain atseden txiki bat hartuko dut. 
Gaukeko hamabiak eta ez dut lorik egin, zelako gaua.
#130
AITA SANTUA!
Ez ikaratu, aita. Entzun nire historia...
Ezin dut sinetsi ikusten ari ddana!
Brunilda naiz, edo hobeto esanda, bere espiritua.\0DNik etxe hau zerbitzatzen nuen, zerbitzuaren parte zen. Orain dela urte asko Antonrekin ezkongai sekretu bat bizi nuen, botikariaren semea. Bere aitak ez zekien ezer eta kontatzeko momentua heldu zen.
Nire maitea Orain dela asko inork irakurtzen dutela ikusten da. basora irten zen bere aitarekin bihotza irekitzeko, baina inoiz heldu sen momentu hori: bidelapur batzuk bidera irten ziren eta hil zuten.\0DHorren berri izan nuenean, ezin izan nuen eutsitu: erotasuna inbaditu ninduen eta suizidatu nuen.
#135
Hurrengo bizitzan nire maitearekin elkartzeko itxaropena nuen, baina arrazoi batengatik, etxe honetatik alderrai ibiltzen dut ordudanik.
Eta zure historioa zer zerikusirik dauka nire anaia Fray Cesareorekin?
Asko. Zorigaitza da nagusi etxe honetan ordudanik, bere arima nirea bezala dago, harrapatuta.
Fedeko gizona naiz, ez dut uste hau pasatzen ari dela...
Badakit nola konpondu situazio hau. Gure zorigaitzak atxilotutak daude. Niri laguntzen banauzu, berari laguntzen dio.
#140
Konta ezazu eta ahal dudana egingo dut.
Hilerrira joan, elizaren atzean, gau bertan. Han ikusiko gara.
Ez kezkatu. Nik irekiko dizut.
Hau nire hilobia da. Barruan zerbait gaizki dabila sentitzen dut.
Zeozer gaizki dabilela?
#145
Argi nabaritzen dut zeozer gaizki dabilela hilobian. Aitzurtu eta ireki, mesedez.
Baina nola?
Pala bat lortu. Lorategi batean begiratu, lorezainak erabiltzen dute.
Esan nizun, zeozer gaizki dabil nire hilobian!
Aita Santua, geruzurra falta da!
#150
Mesedez, aurkitu nire geruzurra, eta ipin ezazu gorputzaren alboan.
Ulertzen dut zure arimak ez aurkitzea atsedena.
Ez dut atsedena aurkituko nire maitearean alboan egon arte. Nire geruzurra edukitzean, elkar lurperatu.
Gertatzen ari den guztia oroipen oso betilun ekartzen zaizkit. Brunilda hain ona zen!
Orain ikusiko duzu!
#155
Nork dago hitz egiten?
Zure gorputza beste alde batera eramaten dut!
Ez! Orain nire arima ezin izango du atseden hartu!
Ezin izan dut sinetsi gertatzen ari den!
Seguru zaude irteera alde batera utzi nahi duzula? (B/E)
# 160
Zure fedea Brunildari laguntza eragotziko zaitu, jaja
Fray Gonzalo, esna zaitez! Zer egiten ari zara hemen? Nola sartu zara?
Cirilo jauna, ez duzu sinetsiko... Brunilda... bere hilobia...
Brunilda? Banekien zeozer ikusi behar zuela honekin...\0DBakarrik pertsona bat lagundu ahal gaitu momentu honetan: sorgina. Joan zaitez bere bila. Ni elizan geratuko naiz, nire lagunaren arimagatik otoitz egiten.
Neskamearen arimaren historio hori mukurua da.\0DHainbeste zorigaitzak egon dira etxe honetan.\0DLiteratura zientifikoa errepasatzea erabaki dut zure lagunaren beste kasu berdinen bila.
# 165
Zeozer lagundu ahal izango duen aurkitzen baduzu, ekar ezazu.
Perretxiko hau apartekoa da! Ez nekien hemen hazten zirela. Seguru nago zure lagunaren kasuan lagundu ahal izango digula, baina laborategian egiaztatu egin behar dut. Perretxiko hau ikaragarria da!
Mesedez, anaia. Utzi hori dagoen lekuan. Geruzur hori nire familiarena izan da nire aitona Medikuntza ikastera joan zenetik Salamancara.
Ekarri didazun perretxiko arraro hau asko lagunduko gaitu, seguru nago, baina denbora gehiago behar dut.
Ongi, suposatzen dut aholku bat esketzera etorri zarela eta aholdu emango dizut.\0DBadakit nork daukan geruzurra eta non aurki ahal izango duzun Brunildaren gorputzeko lau atal faltatzen direnak.
# 170
Eskeletoa antzinako zelten lau haitzuloetan dago. Haiengan heltzeko, lehenik Monte de las Pozasen 7 istilen ura pitxer honetan jaso eta ureztatu harekin haien sarreran dauden errotak.
Geruzurra botikariak dauka bere bulegoan. Susmatzen dut historio honekin zerikusi handia daula: ez dut uste bere semea neskameaz maitemintzea gustatuko litzaion.\0DDena izatean edo laguntza behar dudanean etorri.
7 perretxikoen truke haitzuloak argitzeko argiontzi bat emango dizut.
7 perretxiko horiek bilatu, eta etorri.
Hemen daukazu argiontzia. Orain eskeletoa lortu.
# 175
Oraindik ez daukazu eskeleto guztia. Nola nahi duzu zuk laguntzea?
Monte das siete pozas.
Kontuz lur-jausiekin.
Hau nire begi kristauak argitu ahal ez duten karaktere batuzetan idatzita dago... eta orain gehiago, inoiz ikusi izan ez dutena.
Kobazulo hori erabateko iluntasunean dago! Ausartegia izango zen ezer iluntzeko sartzea!
# 180
Zelten 7 istil sakratuak zure aurrean daude. Bere ura Druiden jakinduria dauka, eta zure bidea aurkitzera lagunduko zaitu.
Horma-irudi horretan lur-jausiak daudela ipintzen du. Ez zenuke pasatu arrazoi baten gaberik.
Ez dut behar istil honetako gehiago ur, lehen hartu dut.
Sorginak esan zuen ondorioa izan detzan 7 istiletako ura behar dudala.
Ikusi dezagun zer gertatzen den...
# 185
Ez dut ura xahutu behar.
Errotaren azalera ez du eguzkiaren izpi bakarra ere islatzen.
Ondo dago, tori hezurrak. Bataila irabazi duzu, baina ez guda.
Hori behar ez duzunarekin ez sartzea erakutsiko zaitu.
Nire dantza makabrorekin menderatuko zaitut.
# 190
Makurtu zaitez eta zurekin eskuzabala izango naiz.
Ezta pentsatu ere! Inoiz!
Errepikatu nire dantza momento hobean egin edo garaituko zaitut!
Zupato hau, lanperna-musu!
Vade retro, monstruo!
# 195
Ez dut borrokatuko berriro monstruo horrekin saihestu ahal badut.
Oso ondo. Orain, Brunildaren arima argudiatzen dudan bitartean, hilerrira joan hezurrak bere lekuan jartzeko.
Joan zaitez hilerrira hezurrak ipintzera. Libragarri baten arima argudiatzea eten behar ez den lan oso neketsu eta arriskutsua da.
Dena ekarri du! Behingoz bakean atseden hartu ahal izango dut!
Ez da ezer gertatzen... Benetan atal gusti horiek nireak direla?
# 200
Bai, guztiak, geruzurra ere, botikariak bere bulegoan zeukan.
Ez! Geruzur hori ez da nirea. Bazeukan ni hil aurretik, bere aitonarena zen.
JaJa! Engainatu zaituztet, geruzurra nik daukat.
Zuk! Beti begi txarrekin ikusi zenigun!
Antonekin egon ahal ez banaiz, inork egon dela eta zuk kendu zenidan.
# 205
Geruzurra itzuli, ez duzu ezer irabazten horrekin, Anton hilda dago.
Bai! Baina mendekuaren zaporea oso goxoa da!
Eskerrik asko, inoiz pentsatu nuen sorgin bat guregandik jelosia izango zuela.
Orain ez dago berarengatik gehiago kezkatu behar.
Brunilda, maitea, bagaude elkarrekin. 
# 210
Bai, bakarrik faltatzen da zure aitak gure lotura onartzea eta gure arimarengatik meza bat eman.
Fray Gonzalo, lagunduko gaituzu? 
Nire egunerokoa bidaltzen genituen gutunak non aurkitu ahal diren arrastroa emango dizu.
Erakutsi nire aitari: hori nahikoa izango da berak gureaz jabetzeko eta onartzeko.
Bila ezazu nire logelan, etxeko mendebalde hegalean.
# 215
Nahikoa izan ez duzula ikusten dut. Ondo dago, nire egiazko ahalmena erakutsiko dizut.
Eman zitzadan uxual hori ohiz kanpokoa da. Larrua salbatu nau.
Uxual botila bat! Erosten dizut! Tori txanpon hauek, uste dut nahikoa izango dela.
Botikaria destilatzen duen uxuala agian nire anaia esna dezala pentsatzen egon naiz, ez duzu uste?
Ideia bikaina. Uxual honek hilak berpizten ditu, eraman ezazu botila hau, onena da seguru laguntzen diola... Baina ez esan Justiniano jaunari eman dizudala edo kaleratuk nau.
# 220
Mendebalde hegalera joan nahi duzu? Ezin duzu, nagusia inoiz utziko zizun, nik giltzak maigatzen didan bakarra naiz, eta, barka nazazu, aita, baina ez dizut horretan lagunduko: erotzen ari zarete!
Ezin naiz hortik horrela jantzita ibili eta edonork ikusteak. Nire abituak berriro ipintzera joan behar dut.
Giltza? Zuk ikusi, baina ez joan gauean, badakizu zer gertatzen den eta ez nukeen zorigaitz gehiago tamaldu nahi.
Ez molestatu gehiago, lanpetuta nago.
Egunerokoa liburutegian pasabide baten atzean gutunak izkutatutak daudela dio.\0DSartzeko, iparraldeko bazterraren liburutegia bultzatu behar da.
# 225
Botikariak liburutegi horretan ale bikainak ditu.
Hau Brunildak dion liburutegia izan behar da. Pizka bat bultzatzen badut agian etsitzen du...
Hau Brunildak bere egunerokoan deskribatzen zuen kutxa da, baina giltzarekin itxita dago! Gutunak hartzeko ireki behar dugu.
Sarrauka ona, noski ireki ahal dudala, baina sarrailagile txiro honek bere janaria ordaintzeko dirua behar du...
Txanpon hauen truke, kutxa hau irekiko dizut poz-pozik.\0D\0DHemen duzu, anaia.
# 230
Gutun hauek nire semearenak dira... Brunildarentzat? Maiteminduta zeudeten eta ez zire ausartu kontatzera! Berak maitasunarengatik suizidatu egin zen!\0DOrain ulertzen dut. Bere atsedenean meza bat egin behar da: apaizarekin hitz egingo dut. Han itxarongo zaitut.
Gurutze hau fede galtzear zorian nengoenenan otoitz egitea ahal izan dut. Erotasunean erortzeaz salbatu nau!
Anaia, burua galtzeko zorian nago. Otoitz egiten jarraituko dut jainkoak dena bere ontasun amaigabean konpondu dezan esperantzarekin.
Nahiz eta saihestu nahi, lekuetako superstizioak nire fedea huts egitea lortzen dute.
Harresi sakratu honetan egotea nire fedea laster indartzen duela sentitzen dut. 
# 235
Eskerrik asko, Fray Gonzalo, pakea handia eman nau historio osoa jakintzea.
Aldameneko hilobietan atseden hartzen dute. Orain bakarrik falta da meza bat esatea, haien arimak atzeden hartuko dute eta Fray Cesareok bere onera etorriko da.
Hori nik onartzen badut. Nire arima kondenatuta dago baina haien biena ez du inoiz atzeden hartuko!
Espitiu maltzurra, inoiz atera izan behar ez zenuen amildegira bueltatu.
O, ez! Desegiten naiz, desegiten naiz!
# 240
Azkenean zorigaitza irabazi zuen.\0D\0DFray Gonzalo, apaiza eta botikaria hilerrian hil ziren eta haien arimak, Fray Cesareoarekin batera lekutik alderrai ibiltzen geratu ziren.\0D\0DMadarikazioa herrixka inbaditu zuen eta axolagabe geratu zen menderen mende.
Azkenik haien arimak atseden hartzea lortu dugu, eta orain sorginak betirako desagertu egin zen.
Eta Fray Cesareok? Esnatu egin da?
Joan gaitezen etxera ikustera!
Jainkoak laudatu dezan! Esnatu egin da!
# 245
Pizka bat zorabiatuta nago baina ondo nago, zenbat lo egin dut?
Asko. Eta gauza itzel bat galdu duzu. Afarian kontatuko dizugu.
Zelako historioa. Banekien nik baso hauek arrisku handiak gordetzen zituela.
Kontatzen duena azkenean dena ondo atera dela da.\0DJoan gaitezen ohera, afaria oparo izan da eta botikariaren uxuala benetan gogorregia da.
Fray Gonzalo, goazen, beranduegi da, bidea hasi egin behar dugu.
# 250
E? Hara! Lo geratu naiz, atzoko uxuala... Brunildarekin amets egin dut... Bere arima ametsetan berriro bisitatu nau...
Eta hitz hauen ostean, bi fraileak Santiagorako ibilaldia jarraitu zuten, eta nork daki zelako abentura berriak aurkituko duten bidaian...\0D\0DBukaera?
Fray Cesareo, Brunilda, Brunildaren historioa, zuk arduragabe, Anton, zuen arimak, sorgina...
Baina, zertaz hitz egiten ari zara? Bart hemen heldu ginen eta botikariarekin afaldu genuen, zuk amesgaizto eragin zintuen uxual batzuk hartu zenituen. Ez zenuke alkoholaz abusatu, anaia... JaJa
Ordan...
# 255
Oruan ezer. Tira anaia, irtetea berrartu dezagun asko geratzen delako Santiagora heltzeko.
#--------------------------------------------------------------------------------------------------------------------------------
# TABLA 2
#--------------------------------------------------------------------------------------------------------------------------------
Giltzariaren logela, beste etxea, ordea, oso eragabetuta dago: bere armairua ere ez du itxi, adibidez.
Mendebalde hegaleko giltza behar dut.
Ez, aita. Inola ere ez. Ezin dizut mendebalde hegaleko giltza utzi, eta gutxiago gertatu denaren ostean.
Apal honetan medikuntzako liburuak nagusitzen dira.
Kimikak apal hau betetzen duen materia da.
#  5
Ikusten denez, botikariak matematiketan oso interesatuta dago.
Botanikak ere Justinianok jauna ikasitako materien parte da.
Apal honetan konaketa historikoak lekua aurkitzen dute.
Erlijio-liburuak apalategi honetako apalak betetzen dute.
Hau Brunildaren logela dirudi.
#  10
Nobela zaharrak aurkitzen dute lekua hemen.
Gutunak han barruan daude. Orain bilatu kutxa irekitzen nork laguntzen dizun.