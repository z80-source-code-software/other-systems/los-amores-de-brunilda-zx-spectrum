TILESET_2

org $e000

TILESET2_1:
     include "../gfx/asm/tileset02_01.asm"

org $e000 + 32*8
TILESET2_2:
     include "../gfx/asm/tileset02_02.asm"

org $e000 + 64*8
TILESET2_3:
     include "../gfx/asm/tileset02_03.asm"

org $e800
TILESET2_TILESET:
     include "../gfx/asm/tileset02_tileset.asm"

END TILESET_2