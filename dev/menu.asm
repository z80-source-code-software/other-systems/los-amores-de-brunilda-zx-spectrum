
FIRE_PALLETE:
     defb INK_BLACK, INK_BLUE, INK_BLUE | PAPER_RED, INK_RED | PAPER_RED, INK_RED | PAPER_YELLOW, INK_YELLOW | PAPER_YELLOW, INK_YELLOW | PAPER_WHITE, INK_WHITE | PAPER_WHITE
REFLEXION_PALLETE:
     defb INK_RED, INK_MAGENTA, INK_YELLOW, INK_YELLOW | BRIGHT, INK_YELLOW | BRIGHT, INK_WHITE, INK_WHITE, INK_WHITE | BRIGHT

FIRE_PALLETE2:
     defb INK_BLACK, INK_BLUE, INK_BLUE | PAPER_GREEN, INK_GREEN | PAPER_GREEN, INK_GREEN | PAPER_CYAN, INK_CYAN | PAPER_CYAN, INK_WHITE | PAPER_CYAN, INK_WHITE | PAPER_WHITE
REFLEXION_PALLETE2:
     defb INK_CYAN, INK_CYAN | BRIGHT, INK_YELLOW, INK_YELLOW, INK_YELLOW | BRIGHT, INK_WHITE, INK_WHITE, INK_WHITE | BRIGHT

FIRE_WIDTH EQU 32
FIRE_HEIGHT EQU 10
FIRE_BUFFER EQU MAP
     

RUN_MENU:
     ld hl, FIRE_PALLETE
     ld (BUFFER_ADDR), hl

     xor a
     ld (FIRE_ACTIVEBACKGROUND), a
     inc a
     ld (SWAP_BACKGROUND_ACTIVE), a

     ld hl, TITLE_SCR
;     ld b, 1
     ld b, a
     call DRAW_BITMAP
     
BURN_SCREEN:
;     ld b, 23
;     ld c, 0
     ld bc,23*256

     BSP_00
     call DF_LOCATE

     ld e, 4

     BSP_0
     ld d, 32
     BSP_1
     ld (hl), 170
     inc hl
     dec d
     jr nz, BSP_1

     dec hl
     inc h

     ld d, 32
     BSP_2
     ld (hl), 85
     dec hl
     dec d
     jr nz, BSP_2

     inc hl
     inc h
     dec e
     jr nz, BSP_0

     dec b
     ld a, b
     cp 23 - FIRE_HEIGHT
     jr nz, BSP_00
     
     xor a
     
     ld hl, FIRE_BUFFER
     ld de, FIRE_BUFFER + 1
     ld bc, FIRE_WIDTH * FIRE_HEIGHT * 2
     ld (hl), a
     ldir
     
     ld hl, CYCLES
     ld (hl), a

     BURN_LOOP:

     ld b, 1+FIRE_WIDTH
     ld de, FIRE_BUFFER + FIRE_WIDTH * FIRE_HEIGHT;-1)

     BL_1:
     ld a, r
;     ld (RAND_BYTE), a
     call RAND8
     and 63
     add a, 12

     ld (de), a
     dec de
     djnz BL_1
     
     BURN_ACT:

     ld a, (PLAYER_CANMOVE)
     cp 8
     jr c, BURN_ACT2

     ld a, r
     cp 30
     jr nc, BURN_ACT2

     ld e, a
     ld d, 0

     ld ix, 2 + FIRE_BUFFER - FIRE_WIDTH -  FIRE_WIDTH + FIRE_WIDTH * FIRE_HEIGHT
     add ix, de

     ld (ix - 1), 24
     ld (ix), 32
     ld (ix + 1), 24
     ld (ix + FIRE_WIDTH - 1), 48
     ld (ix + FIRE_WIDTH), 64
     ld (ix + FIRE_WIDTH + 1), 48

     xor a
     ld (PLAYER_CANMOVE), a
     
     BURN_ACT2:

     ld hl, PLAYER_CANMOVE
     inc (hl)


     ld iy, FIRE_BUFFER

     ld de, FIRE_WIDTH
     add ix, de
     add iy, de

     ld c, FIRE_WIDTH

     BL_2:

     ld b, FIRE_HEIGHT - 1

     BL_3:

     ld a, (iy)
     and a
;     LD L, 0
     ld l, a
     jr z, BL_Z
     dec (iy)

     ld h, 0
;     ld d, 0
     ld d, h
     ld l, (iy-1)
     ;add hl, de
     ld e, (iy+1)
     add hl, de
     ld e, (iy-FIRE_WIDTH)
     add hl, de
     ld e, (iy+FIRE_WIDTH)
     add hl, de

     srl h
     rr l
     srl h
     rr l

     ld e, (iy)
     add hl, de

     srl h
     rr l

     ;ld l, (iy)
     ;xor a
     ;or l
;
     jr z, BL_Z
     dec l
     jr z, BL_Z
     dec l

     BL_Z
     
     ld (iy-FIRE_WIDTH), l
     ;ld (ix-FIRE_WIDTH), l
     ;inc ix
     inc iy
     djnz BL_3

     dec c
     jr nz, BL_2


     ; Dump buffer to screen

     ld ix, $5800 + 14*32
     ld de, 0 ;ld hl, (BUFFER_ADDR)
     call FIRE_DUMP

     ; Reflejo

     ld ix, $5800 + 4 * 32
     ld de, 8;ld hl, REFLEXION_PALLETE
     call FIRE_DUMP

     ld a, KEY_R
     call KTEST1
     ccf
     ret c

     call MENU_CHECKCONTROL
     ret nc

     halt
     
     ld hl, CYCLES
     inc (hl)
     jr z, MENU_SWAPBACKGROUND

     call RAND8
     bit 7, a
     jp z, BURN_ACT
     
     jp BURN_LOOP

MENU_CHECKCONTROL:

     ld hl, CONTROLS_KEYB
     ld (CONTROL_PTR), hl

     ld hl, KEYS_PTR
     ld (hl), KEYS

     ld b, 4
     MCC_LOOP:
     exx
     
     ld iy, (KEYS_PTR)
     ld a, (iy + 4)
     call KTEST1
     ret nc

     ld hl, (KEYS_PTR)    
     ld de, 5
     add hl, de
     ld (KEYS_PTR), hl

     exx
     djnz MCC_LOOP

     ld hl, CONTROLS_KEMP
     ld (CONTROL_PTR), hl

     ld c, #1f
     in b, (c)
     ld a, b
     cp 16
     ret z

     scf
     ret

MENU_SWAPBACKGROUND:
     ld a, (SWAP_BACKGROUND_ACTIVE)
     and a
     ret z ;jp z, BURN_LOOP

     xor a
     call CLS

     ld hl, TITLE_SCR
     ld a, (FIRE_ACTIVEBACKGROUND)
     and a
     jr nz, MSB_DOIT
     
     ld hl, CREDITS_SCR
     
     MSB_DOIT:
     xor 1
     ld (FIRE_ACTIVEBACKGROUND), a
     ld b, 1
     call DRAW_BITMAP
     jp BURN_SCREEN


FIRE_DUMP:
     ld hl, (BUFFER_ADDR)
     add hl, de
     ld bc, FIRE_WIDTH*(FIRE_HEIGHT)
     ld iy, FIRE_BUFFER
     ld (FIRE_PALLETEPOINTER), hl

     BL_4:
     ld a, (iy)

;     srl a
;     srl a
        rrca
        rrca
        and 63

     cp 8
     jr c, BL_H0

     ld a, 7 
     
     BL_H0
     ld hl, (FIRE_PALLETEPOINTER)
     call GET_BYTE_TABLE
     ld (ix), a
     inc iy
     inc ix
     dec bc
     ld a, b
     or c
     jr nz, BL_4
     ret

ENTER_PWD:

      call ESPIRAL
      xor a
      call CLS

      ld hl, PWD_PWD
      ld (hl),'-'
      ld de, PWD_PWD + 1
      ld bc, 9
      ldir

      ld hl, PWD_LEN
      ld (hl), 0

      EPWD_WFNK:
     
      ld hl, PASSWORD_MSG
      ld bc, $070b
      call SHOW_MESS2

      call KFIND
;      cp #ff
      inc a
      jp nz, EPWD_WFNK

      EPWD_LOOPH:

      call KFIND
;      cp #ff
      inc a
      jp z, EPWD_LOOPH

;      cp 40
      cp 41
      jp z, EPWD_DEL

;      cp $21 ; 13
      cp $22
      jr z, EPWD_ENTER

      ld a, (hl)

      cp '0'
      jr c, EPWD_WFNK
      cp 'F'+1
      jr nc, EPWD_WFNK

      ex af, af'
      ld hl, PWD_LEN
      ld a, (hl)
      cp 10
      jr nc, EPWD_WFNK
      ex af, af'

      ld c, (hl)
      ld b, 0
      inc (hl)
      ld hl, PWD_PWD
      add hl, bc
      ld (hl), a

;      ld e, 0
      ld e, b
      call WP_PLAY_SFX

      jr EPWD_WFNK

EPWD_DEL:

     ld e, 1
     call WP_PLAY_SFX

     ld hl, PWD_LEN
     ld a, (hl)
     and a
     jr z, EPWD_WFNK

     dec (hl)
     ld b, 0
     ld c, (hl)
     ld hl, PWD_PWD
     add hl, bc
     ld (hl), '-'

     jp EPWD_WFNK
      
EPWD_ENTER:
     ld a, (PWD_LEN)
     cp 10
     jr nz, EPWD_WFNK

     ; ----- Ahora el check de la PWD

     xor a
     ld (PWD_CHECKSUM), a

     ld b, 4
     ld hl, PWD_PWD
     CHECKPWD_LOOP0:
     call TEXT_TO_HEX
     exx
     ld hl, PWD_CHECKSUM
     add a, (hl)
     ld (hl),a
     exx
     djnz CHECKPWD_LOOP0

     call TEXT_TO_HEX
     ld hl, PWD_CHECKSUM
     cp (hl)
     jp nz, INIT

     ld hl, PWD_PWD+2
     call TEXT_TO_HEX
     call CHECK_FLAG0

     ld (FLAGS), a

     call TEXT_TO_HEX
     ld (FLAGS+10), a
     
     call TEXT_TO_HEX
     ld b, a
     and 31
     ld (FLAGS+3), a
     ld a, b
     rlca
     rlca
     and 3
     ld (FLAGS+8), a

     ld hl, PWD_PWD
     call TEXT_TO_HEX

     ld b, a
     push bc
     
     ld a, O_PALA
     bit 0, b
     call nz, EPWD_GETOBJ

     pop bc
     push bc
     ld a, O_SETA_R
     bit 1, b
     call nz, EPWD_GETOBJ

     pop bc
     push bc
     ld a, O_SETA_N
     bit 2, b
     call nz, EPWD_GETOBJ

     pop bc 
     push bc
     ld a, O_FAROL
     bit 3, b
     call nz, EPWD_GETOBJ
     
     pop bc 
     push bc
     ld a, O_CRUCIFIJO
     bit 4, b
     call nz, EPWD_GETOBJ

     pop bc
     ld a, 0
     rl b
     rla
     rl b
     rla
     rl b
     rla
     rla
     set 0, a
     ld (FLAGS+13), a


     call WP_STOP_SOUND
     ld e, 7
     call WP_PLAY_SFX

     pop af ; Ya no tenemos que volver después del call
     ld hl, ENDLESS_LOOP ; Sino saltar al loop directamente
     push hl
     
     ld hl, PRESS_FIRE
     ld bc, $0a10
     call SHOW_MESS2


     EPWD_WAITFORCONTROL:
     call MENU_CHECKCONTROL     
     jr c, EPWD_WAITFORCONTROL

     ld hl, FLAGS+1
     set 7, (hl)                   ; Sin pintar el marcador al salir...
     ld b, 59                      ; Crea las setas y crucifijos
     call RUNSCRIPT


     ld a, (FLAGS)
     ld b, 13
     cp 4                               ; Capítulo 2
     jp z, RUNSCRIPT

     ld b, 56
     cp 7                               ; Capítulo 3
     jp z, RUNSCRIPT

     inc b
     cp 12                              ; Capítulo 4
     jp z, RUNSCRIPT

     ld hl, OBJ_DATA + 3
     ld de, BYTES_OBJ
     ld b, OBJ_NUM

     EPWD_DESTROYWATERDROPS_LOOP:
     ld a, (hl)
     cp O_AGUA
     jr nz, EPWD_DWL_HOOK
     ld (hl), O_AGUA_F 
     EPWD_DWL_HOOK:
     add hl, de
     djnz EPWD_DESTROYWATERDROPS_LOOP
          
     ld b, 58
     jp RUNSCRIPT
     

EPWD_GETOBJ:
     call CHECK_OBJECT
     ld (OBJ_REF_IX), ix
;     call GET_THIS_OBJECT
;     ret
     jp GET_THIS_OBJECT

CHECK_FLAG0:
     cp 4
     ret z
     cp 7
     ret z
     cp 12
     ret z
     cp 16
     ret z
     
     jp INIT
