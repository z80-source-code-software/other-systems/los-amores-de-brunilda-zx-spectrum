include "./sym/fight_graphics.sym"

; PRINTx2: Imprime un car�cter al doble de su tama�o
; Entrada: BC Coordenadas: B = L�nea C = Columna ; DE = Direcci�n del car�cter a imprimir


PRINTx2:

      call DF_LOCATE         ; Ponemos la direcci�n de memoria de video de destino en HL
      push BC

;      ld c, 2                ; Dos caracteres de alto
;      ld b, 4                ; Cada caracter va a llevar 4 p�xeles
      ld bc,4*256+2

      PRINTx2_LOOP1:

      ld a, (de)
      push de                 ; Nos guardamos DE (Direcci�n de donde estamos imprimiendo)
      ld de, 0

      REPT 4
;      srl a                        ; Metes un 0 en la izquierda de D y sacas lo que haya a la derecha al Carry
      rra               ;No importa lo que se le meta a A por la izquierda
      rr e                         ; Mete el carry anterior a la izquierda de E
      sra e                        ; Corre todo E a la derecha, dejando el bit 7 como estaba (repites bit 7 en bit 7 y bit 6)
                                   ; Con todo esto, hemos duplicado en los dos bits de la izquierda de E el de m�s a la derecha de D
      ENDM              ; Ahora tenemos E con los 4 p�xeles de la derecha de A duplicados en horizontal

      REPT 4
;      srl a                       ; Los 4 siguientes p�xeles ir�n en D
      rra               ;Sigue sin importar
      rr d
      sra d
      ENDM              ; Ahora tenemos A duplicado en horizontal en DE

      ld (hl), d             ; Ah� va D (octeto de la izquierda)
;      inc hl                 ; Uno m�s a la derecha...
      inc l
      ld (hl), e             ; ... va E
      inc h                       ; Un p�xel m�s abajo
      ld (hl), e             ; Se repite E
;      dec hl                 ; Y uno m�s a la izquierda
      dec l
      ld (hl), d             ; volvemos a poner D
      inc h                       ; Lo dejamos preparado para la pr�xima l�nea

      pop de                 ; Recuperamos DE (De donde sacamos lo que imprimimos)

      inc de                 ; Siguiente l�nea de p�xeles a pintar
;      inc d

      djnz PRINTx2_LOOP1

      dec c
      ret z

      pop BC
      inc b
      call DF_LOCATE
;      ld c, 1
;
;      ld b, 4
      ld bc,4*256+1
      jp PRINTx2_LOOP1

; Dibuja el gr�fico a tama�o doble del n� de Boss indicado FLAG 10

DRAW_BOSSx2:

;      ld h, 0
      ld a, (FLAGS+12)
      and 7
;      cp 6
;      jr nz, DBx2_HOOK
;      dec a

;      DBx2_HOOK:

;      ld l, a

;      add hl, hl ; x2
;      add hl, hl ; x4
;      add hl, hl ; x8
;      add hl, hl ; x16
;      add hl, hl ; x32
;      add hl, hl ; x64

        ld l,0
        rra
        rr l
        rra
        rr l
        ld h,a
        
      push hl

      ld de, BOSSES_ATTR1 ;- 64
      add hl, de

      ld de, 22785+32
      ld a, 8

      DWx2_B3
      ld bc, 8
      ldir

      ex af, af'
      ld a, 24
      add a, e
      ld e, a
;      ld a, 0
      ld a, b
      adc a, d
      ld d, a
      ex af, af'
      dec a
      jr nz, DWx2_B3


;      add hl, hl ; x2
;      add hl, hl ; x4
;      add hl, hl ; x8
;      add hl, hl ; x16
;      add hl, hl ; x32
;      add hl, hl ; x64
      pop hl
      add hl, hl ; x128

      ld de, BOSSES ;- 128
      add hl, de
      ex de, hl

      ld h, 4
      ld b, 9

      DWx2_B1:

      ld l, 4
      ld c, 1

      DWx2_B2:

      push hl
      push bc
      call PRINTx2
      pop bc
      pop hl

      inc c
      inc c

      dec l
      jr nz, DWx2_B2
      inc b
      inc b
      dec h
      jr nz, DWx2_B1

      ret


; DRAWBOSS: Dibuja el jefe se�alado en A
DRAWBOSS:
;      ld h, 0
      ld a, (FLAGS+12)
      and 7
;      cp 6
;      jr nz, DB_HOOK
;      dec a

;      DB_HOOK:

;      ld l, a

;      add hl, hl ; x2
;      add hl, hl ; x4
;      add hl, hl ; x8
;      add hl, hl ; x16
;      add hl, hl ; x32
;      add hl, hl ; x64
;      add hl, hl ; x128

        ld l,0
        rra
        rr l
        ld h,a

      ld de, BOSSES ;- 128
      add hl, de
      ex de, hl

      ld h, 4
      ld b, 20

      DWB_B1:

      ld l, 4
      ld c, 0

      DWB_B2:

      push hl
      call PRINT
      pop hl

      inc c

      dec l
      jr nz, DWB_B2
      inc b
      dec h
      jr nz, DWB_B1
      
      ld hl, $5a80
      ld de, $5a81

      ld a, 4

      DWB_BATTR:

      ld bc, 3
      ld (hl), INK_WHITE | BRIGHT
      ldir
;      ld bc, 29
      ld c,29
      add hl, bc
      ex de, hl
      add hl, bc
      ex de, hl
      dec a
      jr nz, DWB_BATTR


      ret


; Dibuja el gr�fico de 16 x 16 del sprite n� A a partir de las coordenadas BC, al doble de su tama�o

DRAW_SPRITEx2:

      ld l, a
      ld h, 0

      add hl, hl
      ld de, TABLA_GFX_SPR
      add hl, de

      ld e, (hl)
      inc hl
      ld d, (hl)
      ex af, af'
      xor a
      or d
      ld a, e
      jp z, DRAW_STILEx2
      ex af, af'
      ex de, hl

      push af
      push bc

      push hl
      push bc

      ld d, h
      ld e, l

      call PRINTx2

      pop bc
      pop hl

      ld de, 8
      add hl, de
      ex de, hl

      push de
      push bc

      inc c
      inc c

      call PRINTx2

      pop bc
      pop hl

      ld de, 8
      add hl, de
      ex de, hl

      inc b
      inc b

      push de
      push bc

      call PRINTx2

      pop bc
      pop hl

      ld de, 8
      add hl, de
      ex de, hl

      inc c
      inc c

      call PRINTx2

      pop bc
      pop af

      cp 3
      jr nz, IS_NOT_THE_DRUNK      ; El borracho no lo pinta con sus atributos
      xor a                        ; Sino con los de FGon (El borracho tiene un car�cter con INK 2)

      IS_NOT_THE_DRUNK:
      ld l, a
      ld h, 0

      add hl, hl
      add hl, hl
      ld de, COLOR_SPRITES
      add hl, de
      ex de, hl

      jp DRAW_ATTRx2


; DRAW_STILEx2: Supertile A en coordenadas BC
DRAW_STILEx2:
;     ld h, 0             ; Ponemos HL apuntando al supertile n�mero A
          ld h, $1c ;(STILES/8)          ;  7  ; (STILES/8) = 28, al multiplicarlo por 8 = E0
     ld l, a             ;
     ex af, af'
     add hl, hl         ;
     add hl, hl         ;
     add hl, hl         ;
;     ld a, STILES
;     add a, h
;     ld h, a
     ex af, af'

     push bc

     call DRAW_TILEx2
     inc c
     inc c
     call DRAW_TILEx2
     dec c
     dec c
     inc b
     inc b
     call DRAW_TILEx2
     inc c
     inc c
     call DRAW_TILEx2

     pop bc

     ex de, hl
;     jp DRAW_ATTRx2


; DRAW_ATTRx2
; Entrada: DE Direcci�n de atributos

DRAW_ATTRx2:
      call ATT_LOCATE
      
;      ld bc, 0     
;      call DRAW_ATTRx2_SUBR
      call DRAW_ATTRx2_SUBR_1ST
      
      ld bc, -31
      call DRAW_ATTRx2_SUBR
      
;      ld bc,29
      ld c,29
      call DRAW_ATTRx2_SUBR
      
      ld bc, -31
;      jp DRAW_ATTRx2_SUBR
      

      DRAW_ATTRx2_SUBR:
      add hl, bc
      DRAW_ATTRx2_SUBR_1ST:
      ld a, (de)
;      and 127                   ; Quita el "FLASH"
      ld (hl), a
;      inc hl
      inc l
      ld (hl), a
      ld bc, 31
      add hl, bc
      ld (hl), a
;      inc hl
      inc l
      ld (hl), a
      inc de
      ret
      



     DRAW_TILEx2:
       push bc
       ld a, (hl)
       inc a
       exx
;       ld h, 0        ;
;       ld h,TILES
;       ld l, a        ;
       ld d,TILES
       ld e, a        ;

;       add hl, hl
;       add hl, hl
;               dec h
;       add hl, hl
;       ld a, TILES
;       add a, h
;       ld h, a
;       ex de, hl
       pop bc
       push bc
       
       call PRINTx2C
       
       
       exx
       inc hl
       pop bc
       ret

PRINTx2C:

      call DF_LOCATE         ; Ponemos la direcci�n de memoria de video de destino en HL
      push BC

;      ld c, 2                ; Dos caracteres de alto
;      ld b, 4                ; Cada caracter va a llevar 4 p�xeles
      ld bc,4*256+2

      PRINTx2C_LOOP1:

      ld a, (de)
      push de                 ; Nos guardamos DE (Direcci�n de donde estamos imprimiendo)
      ld de, 0

      REPT 4
;      srl a                        ; Metes un 0 en la izquierda de D y sacas lo que haya a la derecha al Carry
      rra               ;No importa lo que se le meta a A por la izquierda
      rr e                         ; Mete el carry anterior a la izquierda de E
      sra e                        ; Corre todo E a la derecha, dejando el bit 7 como estaba (repites bit 7 en bit 7 y bit 6)
                                   ; Con todo esto, hemos duplicado en los dos bits de la izquierda de E el de m�s a la derecha de D
      ENDM              ; Ahora tenemos E con los 4 p�xeles de la derecha de A duplicados en horizontal

      REPT 4
;      srl a                       ; Los 4 siguientes p�xeles ir�n en D
      rra               ;Sigue sin importar
      rr d
      sra d
      ENDM              ; Ahora tenemos A duplicado en horizontal en DE

      ld (hl), d             ; Ah� va D (octeto de la izquierda)
;      inc hl                 ; Uno m�s a la derecha...
      inc l
      ld (hl), e             ; ... va E
      inc h                       ; Un p�xel m�s abajo
      ld (hl), e             ; Se repite E
;      dec hl                 ; Y uno m�s a la izquierda
      dec l
      ld (hl), d             ; volvemos a poner D
      inc h                       ; Lo dejamos preparado para la pr�xima l�nea

      pop de                 ; Recuperamos DE (De donde sacamos lo que imprimimos)

      inc d                     ; Siguiente l�nea de p�xeles a pintar

      djnz PRINTx2C_LOOP1

      dec c
      ret z

      pop BC
      inc b
      call DF_LOCATE
;      ld c, 1
;
;      ld b, 4
      ld bc,4*256+1
      jp PRINTx2C_LOOP1

; FADE_OUT: Hace un fundido a negro de atributos
; Destruye el contenido de todos los registros

FADE_OUT:
     ld b, 7
     
     FADEOUT_LOOP:
     
     push bc
     
     ld e, 3
     ld hl, $5800
     halt
     
     FADE_LOOP:
     ld a, (hl)
     ld d, a
     and 7
     jr z, PAPER_HOOK
     dec a
     
     PAPER_HOOK:     
     ld b, a
     ld a, d
     and $38
     jr z, FADE_HOOK      
     sub 8
     
     FADE_HOOK:
     or b
     ld c, a
     ld a, d
     and $c0
     or c
     ld (hl), a
     inc l
     jr nz, FADE_LOOP
     inc h
     dec e
     jr nz, FADE_LOOP
     
     pop bc
     djnz FADEOUT_LOOP
     ret
     

; CLS: Borra la pantalla
; Utiliza los registros hl, bc, y de
CLS:
    ld hl, $5b00
    ld de, $5aff
    ld (hl), l
    ld bc, $1aff
    lddr

    and #38
    rrca        ;Salen ceros, entran ceros
    rrca
    rrca
;    srl a
;    srl a
;   srl a

    out (#fe),a

    ret

; DF_LOCATE: De vuelve en HL la direcci�n del archivo de pantalla
; Entrada: BC Coordenadas: B = L�nea C = Columna
; Salida: HL Direcci�n del archivo de pantalla
; Se conservan DE y BC

DF_LOCATE:
    ld a, b
    and #f8
    add a, #40
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret


; DFL_CALLER: Devuelve en HL la direcci�n del scan solicitado
; Entrada:
;           B = Scan
;           C = Columna

DFL_CALLER:

     ld a, b
     and $c0
     rra
     scf
     rra
     rrca
     xor b
     and $f8
     xor b
     ld h, a

     ld a, b
     and $38
     rlca
     rlca
     add a, c
     ld l, a

     ret

; ATT_LOCATE: De vuelve en HL la direcci�n de atributos
; Entrada: BC Coordenadas: B = L�nea C = Columna
; Salida: HL Direcci�n de atributos
; Se conservan DE y BC

ATT_LOCATE:
    ld a, b
    rrca
    rrca
    rrca
    and 31
;    sra a
;    sra a
;    sra a
    add a, #58
    ld h, a
    ld a, b
    and 7
    rrca
    rrca
    rrca
    add a, c
    ld l, a
    ret


; PRINT: Imprime el car�cter apuntado por DE en las coordenadas BC
; BC: B = Linea, C = Columna
; DE = Direcci�n car�cter
; Como llama a DF_LOCATE, destruye el contenido de HL
; Salida: DE = Apuntando al primer byte DESPU�S del car�cter actual

PRINT:
      call DF_LOCATE
      PRINT_:
;      ld a, 8
;      PRINT_Bucle:
      REPT 8
      ld a, (de)
      ld (hl), a
      inc de
      inc h
      ENDM
;      dec a
;      jr nz, PRINT_Bucle

      ret
      
; DRAWGFX: Dibuja un gr�fico en pantalla (Tiene que estar por l�neas y en zigzag)
; ENTRADA: HL puntero al gr�fico, BC Coordenadas en caracteres, DE: D = Caract Ancho E = Caract. Alto

DRAWGFX:
     ld (DRGVAR1), bc
     ld (DRGVAR2), bc
     ld (DRGVAR3), de

     push de
     
     push hl

     DRAWGFX_B0:

     call DF_LOCATE
     ex de, hl
     pop hl

     ld a, (DRGVAR3 + 1)
     ld b, a
     ld c, 4

     DRAWGFX_B1:

     ld a, (hl)
     ld (de), a
     inc hl
     inc de
     djnz DRAWGFX_B1

     ld a, (DRGVAR3 + 1)
     ld b, a
     dec de
     inc d

     DRAWGFX_B2:

     ld a, (hl)
     ld (de), a
     inc hl
     dec de
     djnz DRAWGFX_B2

     ld a, (DRGVAR3 + 1)
     ld b, a
     inc de
     inc d

     dec c
     jp nz, DRAWGFX_B1


     ld bc, (DRGVAR1)
     inc b
     ld (DRGVAR1), bc


     pop bc           ; Y en BC las dimensiones
     dec c
     push bc
     push hl
     ld bc, (DRGVAR1)
     jp nz, DRAWGFX_B0

     ld bc, (DRGVAR2)


     pop hl

     pop af             ; Le debemos una a la pila :P


     DRAWGFX_B3:

     push bc

     push hl
     call ATT_LOCATE
     ex de, hl
     pop hl             ; Igual que antes: HL = Origen, DE = Destino


     ld bc, (DRGVAR3)           ; Ahora tenemos en BC las dimensiones


     DRAWGFX_B4:

     ld a, (hl)
     ld (de), a
     inc hl
     inc de

     djnz DRAWGFX_B4

     dec c
     jp z, DRAWGFX_Exit

     ld a, c
     ld (DRGVAR3), a

     pop bc
     inc b
     ld a, (DRGVAR2)
     ld c, a
     jp DRAWGFX_B3

DRAWGFX_Exit:
     pop af
     ret

; Efecto de fadeout cerrando la pantalla hacia el centro
ESPIRAL:
    ld ix, DRGVAR1
    ld (ix+0), 23
    ld (ix+1), 32

    ld hl, 22527
    ld de, 32

    ESPIRAL_HOOK0:

    ld b, (ix+1)

    ld a, (ix)
    and a
    ret z
    ld a, (ix+1)
    and a
    ret z

    halt

    ESPIRAL_HOOK1:

    ld (hl), 0
    inc hl
    djnz ESPIRAL_HOOK1

    ld a, (ix+1)
    dec a
    ld (ix+1), a


    ld b, (ix+0)

    ESPIRAL_HOOK2:

    ld (hl), 0
    add hl, de
    djnz ESPIRAL_HOOK2

    ld a, (ix+0)
    dec a
    ld (ix+0), a


    ld b, (ix+1)

    ESPIRAL_HOOK3:

    ld (hl), 0
    dec hl
    djnz ESPIRAL_HOOK3

    ld a, (ix+1)
    dec a
    ld (ix+1), a

    ld b, (ix+0)

    ESPIRAL_HOOK4:

    ld (hl), 0
    sbc hl, de
    djnz ESPIRAL_HOOK4

    ld a, (ix+0)
    dec a
    ld (ix+0), a

    jp ESPIRAL_HOOK0


DRGVAR1
     defw 0
DRGVAR2
     defw 0
DRGVAR3
     defw 0

BLACK             EQU      0
BLUE              EQU      1
RED               EQU      2
MAGENTA           EQU      3
GREEN             EQU      4
CYAN              EQU      5
YELLOW            EQU      6
WHITE             EQU      7

INK_BLACK         EQU      0
INK_BLUE          EQU      1
INK_RED           EQU      2
INK_MAGENTA       EQU      3
INK_GREEN         EQU      4
INK_CYAN          EQU      5
INK_YELLOW        EQU      6
INK_WHITE         EQU      7

PAPER_BLACK       EQU      0
PAPER_BLUE        EQU      8
PAPER_RED         EQU    #10
PAPER_MAGENTA     EQU    #18
PAPER_GREEN       EQU    #20
PAPER_CYAN        EQU    #28
PAPER_YELLOW      EQU    #30
PAPER_WHITE       EQU    #38

BRIGHT            EQU    #40
FLASH             EQU    #80
