ST_ANCHO        EQU  15
ST_ALTO         EQU   9

ANCHO_ST        EQU   2
ALTO_ST         EQU   2

OFFSETXONSCREEN EQU    1
OFFSETYONSCREEN EQU    1

LAST_RAM_BANK:
     defb 0

HEX_NUMBERS:
     defb '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'

IF COMPILE_VERSION=VERSION_ENG
	KEYCONFIRM_YES equ KEY_Y
  ELSE
	IF COMPILE_VERSION=VERSION_EUS
		KEYCONFIRM_YES equ KEY_B
	 ELSE
		KEYCONFIRM_YES equ KEY_S
	ENDIF
ENDIF

IF COMPILE_VERSION=VERSION_EUS
	KEYCONFIRM_NO equ KEY_E
ELSE
	KEYCONFIRM_NO  equ KEY_N
ENDIF

PASSWORD_MSG:

IF COMPILE_VERSION=VERSION_ENG
     defb 1, 3 | $40, ' Password : '
ENDIF
IF COMPILE_VERSION=VERSION_GAL
     defb 1, 3 | $40, 'Contrasinal:'
ENDIF
IF COMPILE_VERSION=VERSION_CAT
     defb 1, 3 | $40, 'Contrasenya:'
ENDIF
IF COMPILE_VERSION=VERSION_CAT2
     defb 1, 3 | $40, 'Contrasenya:'
ENDIF
IF COMPILE_VERSION=VERSION_ITA
     defb 1, 3 | $40, ' Password : '
ENDIF
IF COMPILE_VERSION=VERSION_SPA
     defb 1, 3 | $40, 'Contrase~a: '
ENDIF
IF COMPILE_VERSION=VERSION_NELO
     defb 1, 3 | $40, 'Contrase~a: '
ENDIF
IF COMPILE_VERSION=VERSION_EUS
     defb 1, 3 | $40, ' Pasahitza: '
ENDIF

PWD_PWD:
     defb 'XXXXXXXXXX', 0

KEYS:
     defb KEY_O, KEY_P, KEY_Q, KEY_A, KEY_SP
     defb KEY_O, KEY_P, KEY_Q, KEY_A, KEY_M
     defb KEY_6, KEY_7, KEY_9, KEY_8, KEY_0
     defb KEY_1, KEY_2, KEY_4, KEY_3, KEY_5
     
CONTROL_PTR:
     defw CONTROLS_KEYB
KEYS_PTR:
     defw KEYS
JOYSTICK:
     defb 0

CYCLES:
    defb 0
TICKS:
    defb 0
OBJ_REF_IX:
    defw 0
CAN_GET_ANYOBJECT:
     defb 0
RAND_BYTE:
     defb 0

MAP_HEIGHT EQU 90
MAP_WIDTH:
    defb 40

PLAYER_CANMOVE:
      defb 0
ISFIREPRESSED:
      defb 0
LASTST:
      defb 0

BUFFER_ADDR:
      DEFW 0; BUFFER_1
LAST_BUFFER_POINTER:
      DEFW 0; BUFFER_1
CAMERA_Y:
      defb 0
CAMERA_X:
      defb 0
CAMERA_OFFSET_Y:
      defb 0
CAMERA_OFFSET_X:
      defb 0
MAP_OFFSET_ADDR:
      DEFW MAP + 46 + 62*39;MAP_WIDTH * CAMERA_Y + CAMERA_X

SCRIPT_POINTER:
     defw 0

; Flags ********************************************************************************
; Flag 0:
; 0 = No hemos hablado con el posadero
; 1 = Hemos hablado con el posadero
; 2 = Hemos hablado con el cura (le estamos siguiendo)
; 3 = Estamos esperando a la cena
; 4 = Nos levantamos por la ma�ana, y buscamos a FCes
; 5 = Estamos buscando a la meiga
; 6 = Ya hemos hablado con la meiga por primera vez (ya no nos saluda ella al entrar a su casa)
; 7 = Ya hemos hablado con la meiga, y solo nos deja hablar con ella llevando la gallina
; 8 = Vamos al cementerio a encontrarnos con Brunilda
; 9 = Brunilda ya ha abierto la iglesia
; 10 = Brunilda ya ha abierto el cementerio
; 11 = Hemos hablado con Brunilda, pero nos falta la pala
; 12 = Ya nos ha despertado el cura en el cementerio
; 13 = Tenemos el esqueleto completo y tenemos que ir al cementerio
; 14 = Ya est� repuesto el esqueleto
; 15 = AL luchar con la meiga (por si perdemos, poder saltar directamente a la lucha)
; 16 = Ant�n y Brunilda esperan: �ltimo cap�tulo
; 17 = Xan ya nos ha pedido el orujo
; 18 = Ya le dimos el orujo a Xan
; 19 = El boticario ya tiene las cartas
; 20 = Fin de juego: No dibujes a FGon, que est� acostado

; Flag 1
; Bit 0      = Si puerta de ala oeste abierta
; Bit 1      = Si hemos hablado con FCes en la habitaci�n antes de la cena
; Bits 2 - 5 = Si puerta cueva abierta
; Bit 6      = Si pasadizo abierto
; Bit 7      = SYSTEM **** Se setea si NO tiene que dibujar el marcador al salir del un script

; Flag 2: Contador "Conversaci�n meiga"
; 0 = Al principio
; 1 = Ya nos ha dicho lo de conseguir el agua de las 7 pozas
; 2 = Ya nos ha ofrecido el farol
; 3 = Ya nos dio el farol
; 4 = Est� "invocando el �nima de Brunilda"

; Flag 10: Contador "Conversaci�n boticario"
; Flag 0 - 6: Si seta est� sin coger
; Flag 7: Si le hemos dado la seta rara al boticario 

; Flag 12: Jefes
; Bits 0 - 2 : N�mero de jefe de la lucha actual
; Bits 3 - 6 : Si ya hemos vencido a ese jefe de caverna
; Bit 7      : Si hemos ganado la lucha actual

; Flag 13: BITS:
; Bit 0      : Si ha dicho ya en la ermita que sube la fe (1 = NO lo ha dicho todav�a)
; Bits 1 - 3 : Bitmap crucifijos
; Bit 7      : SYSTEM **** Se setea si NO tiene que hacer sonar la m�sica al cargar un mapa (textos de cap�tulos)

; Flag 14: BITS:
; Bit 0      : Si ha entrado alguna vez en la habitaci�n del ama de llaves
; Bit 1      : Si ha entrado alguna vez en la habitaci�n de Brunilda

FLAGS
    defb 0     ; 00 Contador "estado aventura"
    defb 0     ; 01 BITS
    defb 0     ; 02 Contador "conversaci�n meiga"
    defb 0     ; 03 Fe / Superstici�n. M�x 32 (Fe total)
    defb 0     ; 04 N�mero de mapa activo
    defb 0     ; 05 Objeto referenciado por jugador
    defb 0     ; 06 Luz (0 = noche, 1 = halo, $ff = d�a)
    defb 0     ; 07 N�mero de setas que tenemos (Obj 17)
    defb 0     ; 08 N�mero de crucifijos (Obj 18)
    defb 0     ; 09 N�mero de gotas de agua que tenemos (Obj 19)
    defb 0     ; 10 Bitmap setas + Contador "conversaci�n boticario"
    defb 0     ; 11 Nivel de la lucha
    defb 0     ; 12 BITS: Jefes
    defb 0     ; 13 BITS:
    defb 0     ; 14 BITS
MAX_FLAG equ 14

; Objetos ******************************************************************************
; ESTRUCTURA
; ===============
; +0 Mapa (255 = No Creado, 254 = Llevado)
; +1 Coordenada Y
; +2 Coordenada X
; +3 Tipo
;
; En siguientes versiones del motor, habr�a que plantearse a�adir un byte (o usar el byte + 0)
; para usar algunos bits como indicadores (p. ej. Si objeto visible, si es "cogible", etc...


; TIPOS DE OBJETO
; ================
; 00 Ropa criada
; 01 Orujo
; 02 Cartas
; 03 Cofre
; 04 Cr�neo falso
; 05 Cr�neo verdadero
; 06 Diario
; 07 Dinero
; 08 Esqueleto 1
; 09 Esqueleto 2
; 10 Esqueleto 3
; 11 Esqueleto 4
; 12 Farol
; 13 Gallina
; 14 Llave
; 15 Pala
; 16 Seta "rara"
; 17 Seta "normal"
; 18 Crucifijo
; 19 Jarra con agua
; 20 Jarra vac�a
; $ff "Agua ya cogida": Objeto "virtual" que sirve como trigger de script
MAX_OBJ EQU 21

BYTES_OBJ EQU 4
OBJ_NUM EQU 22     ; objetos m�ximos que existen el "Mundo" (no cogidos) simult�neamente
OBJ_DATA:
     defs OBJ_NUM*BYTES_OBJ

MAX_INVENTORY EQU 8 ; 8 Objetos m�ximo que podemos "llevar" simult�neamente
INVENTORY:
     defs MAX_INVENTORY, $ff
; Esto deber�a poder ser prescindible: Basta con comprobar que el byte +0 de cada objeto sea $fe

; SPRITES ******************************************************************************
MAXSPRITES EQU 6
BYTES_SPR EQU 13

PRINCIPAL_SPR:         ;  12 BYTES POR SPRITE
     DEFB 0            ;  +0 Tipo de Sprite
     DEFB 48           ;  +1 Coordenada Y (en Supertiles) del Sprite
     DEFB 54           ;  +2 Coordenada X (en Supertiles) del Sprite
     defb 0            ;  +3 Offset Y (en tiles) del sprite
     defb 0            ;  +4 Offset X (en tiles) del sprite
     defb 0            ;  +5 Facing (Nibble bajo) y Fotograma (Nibble alto) del sprite
     DEFW FGON         ;  +6 Puntero al gr�fico a dibujar en el siguiente fotograma
     DEFW FGON         ;  +8 Puntero al inicio del gr�fico del sprite (Facing 0 [Izqda] Fotograma 0)
     DEFW 0            ; +10 Puntero del script de movimiento
     defb 0            ; +12 Contador de movimiento

RESTOSPRITES:
     DEFS  MAXSPRITES*BYTES_SPR, $ff

PATH_TO_FOLLOW:
     defs 4, 0

TABLA_GFX_SPR:                   ; Puntero al gr�fico para "Hablar" del personaje. Si es < 256 representa un SuperTile
    defw FGON + 96, FCES + 96, XAN + 32, BORRACHO + 96, CURA + 96, AMA + 96, BOT + 96, PAISANO + 96, MEIGA + 96, 214
    defw BRUNILDA+96, 215, 0, 0, 0, 216

COLOR_SPRITES:
     defb INK_WHITE, INK_WHITE | BRIGHT, INK_WHITE, INK_WHITE | BRIGHT                                    ; 00 Fray Gonz�lo
     defb INK_YELLOW, INK_YELLOW | BRIGHT, INK_YELLOW, INK_YELLOW | BRIGHT                                ; 01 Fray Ces�reo
     defb INK_WHITE , INK_WHITE , BRIGHT | INK_WHITE | PAPER_RED, BRIGHT | INK_WHITE | PAPER_RED          ; 02 Xan
     defb INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT                  ; 03 Borracho
     defb INK_MAGENTA | BRIGHT, INK_MAGENTA | BRIGHT, INK_MAGENTA, INK_MAGENTA                            ; 04 Cura
     defb INK_GREEN, INK_GREEN | BRIGHT, INK_GREEN, INK_GREEN | BRIGHT                                    ; 05 Ama de llaves
     defb INK_CYAN, INK_CYAN | BRIGHT, INK_CYAN, INK_CYAN | BRIGHT                                        ; 06 Boticario
     defb INK_CYAN, INK_CYAN | BRIGHT, INK_CYAN, INK_CYAN | BRIGHT                                        ; 07 Paisano
     defb INK_YELLOW | BRIGHT, INK_YELLOW, INK_YELLOW | BRIGHT, INK_YELLOW                                ; 08 Meiga
     defb INK_GREEN, INK_GREEN | BRIGHT, INK_GREEN, INK_GREEN | BRIGHT                                    ; 09 Cerrajero

     defb INK_RED | BRIGHT, INK_RED | BRIGHT, INK_RED | BRIGHT, INK_RED | BRIGHT                          ; 10 Brunilda
     defb INK_WHITE, INK_WHITE | BRIGHT, INK_WHITE, INK_WHITE | BRIGHT                                    ; 11 Misterio ??
     defb INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT                  ; 12 Piedras
     defb INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT, INK_WHITE | BRIGHT                  ; 13 Explosi�n
CSE: defb INK_RED | BRIGHT, INK_RED | BRIGHT, INK_RED | BRIGHT, INK_RED | BRIGHT                          ; 14 Enemigos
     defb INK_CYAN, INK_CYAN | BRIGHT, INK_CYAN, INK_CYAN | BRIGHT                                        ; 15 Ant�n


FIGHT_SCANTABLE EQU BUFFER_1

NEED_UPDATE:
     defb 0

FIRE_PALLETEPOINTER:
FIGHT_ENERGY_ME:
     defb 0
FIGHT_ENERGY_MONSTER:
     defb 0

PWD_CHECKSUM:
FIGHT_ETU_MONSTER:
     defb 0
PWD_LEN:
FIRE_ACTIVEBACKGROUND:
FIGHT_ETU_ME:
     defb 0
FIGHT_STARSLEVEL:
     defb 0
FIGHT_SKULLSLEVEL:
     defb 0

SWAP_BACKGROUND_ACTIVE:
FIGHT_OFFSET:
     defb 0
FIGHT_SPEED:
     defb 0
FIGHT_WAYOFSCROLL:
     defb 0
FIGHT_COORDPTR:
     defw 0
FIGHT_ATTRPTR:
     defw 0
FIGHT_MAXSORTSOFMOVEMENT:
     defb 0

MAX_FIGHT_ENERGY equ $ff

; Estructura de "movimiento" en la lucha
; +0 Tipo de movimiento
; +1 Coordenada Y de la ficha
; +2 Coordenada X de la ficha

FIGHT_MAXMOVEMENTS equ 10
FIGHT_MOVEMENT_BYTES equ 3
FIGHT_MOVEMENTS
     defs FIGHT_MAXMOVEMENTS * FIGHT_MOVEMENT_BYTES

; --------------------------------------------------------------------------------------------
SUPERTILE_0   EQU $e000
TILE_0        EQU $e800
STILES        EQU $e0
TILES         EQU $e8
BUFFER_1      EQU $f600 ; ((ST_ANCHO+1)*2)*((ST_ALTO+1)*2)
BUFFER_2      EQU $f900
BUFFER_COLOR  EQU $fc00
MAP           EQU $6000

LOAD_SONG     EQU $c070	; CARGA_CANCION
LOAD_SFX      EQU $c459	; INICIA_EFECTO
MUTE_PLAYER   EQU $c04b	; PLAYER_OFF
WYZPLAYER_ISR EQU $c000	; INICIO
INITWYZ       EQU $e6d8	; PLAYER
UNPACK_SONG   EQU $c4bc	; UNPACK_SONG

FIGHT_MOVEMENT_SORT equ $7fa3-5
STACK_POINTER equ $5f5e;dc0
; --------------------------------------------------------------------------------------------
