; Header file generated by PenText

BUFFER_SIZE equ 235

COMPRESS_TABLE:
incbin "mess/MESS_ITA.5tt"

MESS_ITA_INDEX:
DEFW MESS_ITA000, MESS_ITA001, MESS_ITA002, MESS_ITA003, MESS_ITA004, MESS_ITA005, MESS_ITA006, MESS_ITA007, MESS_ITA008, MESS_ITA009
DEFW MESS_ITA010, MESS_ITA011, MESS_ITA012, MESS_ITA013, MESS_ITA014, MESS_ITA015, MESS_ITA016, MESS_ITA017, MESS_ITA018, MESS_ITA019
DEFW MESS_ITA020, MESS_ITA021, MESS_ITA022, MESS_ITA023, MESS_ITA024, MESS_ITA025, MESS_ITA026, MESS_ITA027, MESS_ITA028, MESS_ITA029
DEFW MESS_ITA030, MESS_ITA031, MESS_ITA032, MESS_ITA033, MESS_ITA034, MESS_ITA035, MESS_ITA036, MESS_ITA037, MESS_ITA038, MESS_ITA039
DEFW MESS_ITA040, MESS_ITA041, MESS_ITA042, MESS_ITA043, MESS_ITA044, MESS_ITA045, MESS_ITA046, MESS_ITA047, MESS_ITA048, MESS_ITA049
DEFW MESS_ITA050, MESS_ITA051, MESS_ITA052, MESS_ITA053, MESS_ITA054, MESS_ITA055, MESS_ITA056, MESS_ITA057, MESS_ITA058, MESS_ITA059
DEFW MESS_ITA060, MESS_ITA061, MESS_ITA062, MESS_ITA063, MESS_ITA064, MESS_ITA065, MESS_ITA066, MESS_ITA067, MESS_ITA068, MESS_ITA069
DEFW MESS_ITA070, MESS_ITA071, MESS_ITA072, MESS_ITA073, MESS_ITA074, MESS_ITA075, MESS_ITA076, MESS_ITA077, MESS_ITA078, MESS_ITA079
DEFW MESS_ITA080, MESS_ITA081, MESS_ITA082, MESS_ITA083, MESS_ITA084, MESS_ITA085, MESS_ITA086, MESS_ITA087, MESS_ITA088, MESS_ITA089
DEFW MESS_ITA090, MESS_ITA091, MESS_ITA092, MESS_ITA093, MESS_ITA094, MESS_ITA095, MESS_ITA096, MESS_ITA097, MESS_ITA098, MESS_ITA099
DEFW MESS_ITA100, MESS_ITA101, MESS_ITA102, MESS_ITA103, MESS_ITA104, MESS_ITA105, MESS_ITA106, MESS_ITA107, MESS_ITA108, MESS_ITA109
DEFW MESS_ITA110, MESS_ITA111, MESS_ITA112, MESS_ITA113, MESS_ITA114, MESS_ITA115, MESS_ITA116, MESS_ITA117, MESS_ITA118, MESS_ITA119
DEFW MESS_ITA120, MESS_ITA121, MESS_ITA122, MESS_ITA123, MESS_ITA124, MESS_ITA125, MESS_ITA126, MESS_ITA127, MESS_ITA128, MESS_ITA129
DEFW MESS_ITA130, MESS_ITA131, MESS_ITA132, MESS_ITA133, MESS_ITA134, MESS_ITA135, MESS_ITA136, MESS_ITA137, MESS_ITA138, MESS_ITA139
DEFW MESS_ITA140, MESS_ITA141, MESS_ITA142, MESS_ITA143, MESS_ITA144, MESS_ITA145, MESS_ITA146, MESS_ITA147, MESS_ITA148, MESS_ITA149
DEFW MESS_ITA150, MESS_ITA151, MESS_ITA152, MESS_ITA153, MESS_ITA154, MESS_ITA155, MESS_ITA156, MESS_ITA157, MESS_ITA158, MESS_ITA159
DEFW MESS_ITA160, MESS_ITA161, MESS_ITA162, MESS_ITA163, MESS_ITA164, MESS_ITA165, MESS_ITA166, MESS_ITA167, MESS_ITA168, MESS_ITA169
DEFW MESS_ITA170, MESS_ITA171, MESS_ITA172, MESS_ITA173, MESS_ITA174, MESS_ITA175, MESS_ITA176, MESS_ITA177, MESS_ITA178, MESS_ITA179
DEFW MESS_ITA180, MESS_ITA181, MESS_ITA182, MESS_ITA183, MESS_ITA184, MESS_ITA185, MESS_ITA186, MESS_ITA187, MESS_ITA188, MESS_ITA189
DEFW MESS_ITA190, MESS_ITA191, MESS_ITA192, MESS_ITA193, MESS_ITA194, MESS_ITA195, MESS_ITA196, MESS_ITA197, MESS_ITA198, MESS_ITA199
DEFW MESS_ITA200, MESS_ITA201, MESS_ITA202, MESS_ITA203, MESS_ITA204, MESS_ITA205, MESS_ITA206, MESS_ITA207, MESS_ITA208, MESS_ITA209
DEFW MESS_ITA210, MESS_ITA211, MESS_ITA212, MESS_ITA213, MESS_ITA214, MESS_ITA215, MESS_ITA216, MESS_ITA217, MESS_ITA218, MESS_ITA219
DEFW MESS_ITA220, MESS_ITA221, MESS_ITA222, MESS_ITA223, MESS_ITA224, MESS_ITA225, MESS_ITA226, MESS_ITA227, MESS_ITA228, MESS_ITA229
DEFW MESS_ITA230, MESS_ITA231, MESS_ITA232, MESS_ITA233, MESS_ITA234, MESS_ITA235, MESS_ITA236, MESS_ITA237, MESS_ITA238, MESS_ITA239
DEFW MESS_ITA240, MESS_ITA241, MESS_ITA242, MESS_ITA243, MESS_ITA244, MESS_ITA245, MESS_ITA246, MESS_ITA247, MESS_ITA248, MESS_ITA249
DEFW MESS_ITA250, MESS_ITA251, MESS_ITA252, MESS_ITA253, MESS_ITA254, MESS_ITA255, MESS_ITA256, MESS_ITA257, MESS_ITA258, MESS_ITA259
DEFW MESS_ITA260, MESS_ITA261, MESS_ITA262, MESS_ITA263, MESS_ITA264, MESS_ITA265, MESS_ITA266, MESS_ITA267

MESS_ITA000:
incbin "mess/MESS_ITA_000.5tx"

MESS_ITA001:
incbin "mess/MESS_ITA_001.5tx"

MESS_ITA002:
incbin "mess/MESS_ITA_002.5tx"

MESS_ITA003:
incbin "mess/MESS_ITA_003.5tx"

MESS_ITA004:
incbin "mess/MESS_ITA_004.5tx"

MESS_ITA005:
incbin "mess/MESS_ITA_005.5tx"

MESS_ITA006:
incbin "mess/MESS_ITA_006.5tx"

MESS_ITA007:
incbin "mess/MESS_ITA_007.5tx"

MESS_ITA008:
incbin "mess/MESS_ITA_008.5tx"

MESS_ITA009:
incbin "mess/MESS_ITA_009.5tx"

MESS_ITA010:
incbin "mess/MESS_ITA_010.5tx"

MESS_ITA011:
incbin "mess/MESS_ITA_011.5tx"

MESS_ITA012:
incbin "mess/MESS_ITA_012.5tx"

MESS_ITA013:
incbin "mess/MESS_ITA_013.5tx"

MESS_ITA014:
incbin "mess/MESS_ITA_014.5tx"

MESS_ITA015:
incbin "mess/MESS_ITA_015.5tx"

MESS_ITA016:
incbin "mess/MESS_ITA_016.5tx"

MESS_ITA017:
incbin "mess/MESS_ITA_017.5tx"

MESS_ITA018:
incbin "mess/MESS_ITA_018.5tx"

MESS_ITA019:
incbin "mess/MESS_ITA_019.5tx"

MESS_ITA020:
incbin "mess/MESS_ITA_020.5tx"

MESS_ITA021:
incbin "mess/MESS_ITA_021.5tx"

MESS_ITA022:
incbin "mess/MESS_ITA_022.5tx"

MESS_ITA023:
incbin "mess/MESS_ITA_023.5tx"

MESS_ITA024:
incbin "mess/MESS_ITA_024.5tx"

MESS_ITA025:
incbin "mess/MESS_ITA_025.5tx"

MESS_ITA026:
incbin "mess/MESS_ITA_026.5tx"

MESS_ITA027:
incbin "mess/MESS_ITA_027.5tx"

MESS_ITA028:
incbin "mess/MESS_ITA_028.5tx"

MESS_ITA029:
incbin "mess/MESS_ITA_029.5tx"

MESS_ITA030:
incbin "mess/MESS_ITA_030.5tx"

MESS_ITA031:
incbin "mess/MESS_ITA_031.5tx"

MESS_ITA032:
incbin "mess/MESS_ITA_032.5tx"

MESS_ITA033:
incbin "mess/MESS_ITA_033.5tx"

MESS_ITA034:
incbin "mess/MESS_ITA_034.5tx"

MESS_ITA035:
incbin "mess/MESS_ITA_035.5tx"

MESS_ITA036:
incbin "mess/MESS_ITA_036.5tx"

MESS_ITA037:
incbin "mess/MESS_ITA_037.5tx"

MESS_ITA038:
incbin "mess/MESS_ITA_038.5tx"

MESS_ITA039:
incbin "mess/MESS_ITA_039.5tx"

MESS_ITA040:
incbin "mess/MESS_ITA_040.5tx"

MESS_ITA041:
incbin "mess/MESS_ITA_041.5tx"

MESS_ITA042:
incbin "mess/MESS_ITA_042.5tx"

MESS_ITA043:
incbin "mess/MESS_ITA_043.5tx"

MESS_ITA044:
incbin "mess/MESS_ITA_044.5tx"

MESS_ITA045:
incbin "mess/MESS_ITA_045.5tx"

MESS_ITA046:
incbin "mess/MESS_ITA_046.5tx"

MESS_ITA047:
incbin "mess/MESS_ITA_047.5tx"

MESS_ITA048:
incbin "mess/MESS_ITA_048.5tx"

MESS_ITA049:
incbin "mess/MESS_ITA_049.5tx"

MESS_ITA050:
incbin "mess/MESS_ITA_050.5tx"

MESS_ITA051:
incbin "mess/MESS_ITA_051.5tx"

MESS_ITA052:
incbin "mess/MESS_ITA_052.5tx"

MESS_ITA053:
incbin "mess/MESS_ITA_053.5tx"

MESS_ITA054:
incbin "mess/MESS_ITA_054.5tx"

MESS_ITA055:
incbin "mess/MESS_ITA_055.5tx"

MESS_ITA056:
incbin "mess/MESS_ITA_056.5tx"

MESS_ITA057:
incbin "mess/MESS_ITA_057.5tx"

MESS_ITA058:
incbin "mess/MESS_ITA_058.5tx"

MESS_ITA059:
incbin "mess/MESS_ITA_059.5tx"

MESS_ITA060:
incbin "mess/MESS_ITA_060.5tx"

MESS_ITA061:
incbin "mess/MESS_ITA_061.5tx"

MESS_ITA062:
incbin "mess/MESS_ITA_062.5tx"

MESS_ITA063:
incbin "mess/MESS_ITA_063.5tx"

MESS_ITA064:
incbin "mess/MESS_ITA_064.5tx"

MESS_ITA065:
incbin "mess/MESS_ITA_065.5tx"

MESS_ITA066:
incbin "mess/MESS_ITA_066.5tx"

MESS_ITA067:
incbin "mess/MESS_ITA_067.5tx"

MESS_ITA068:
incbin "mess/MESS_ITA_068.5tx"

MESS_ITA069:
incbin "mess/MESS_ITA_069.5tx"

MESS_ITA070:
incbin "mess/MESS_ITA_070.5tx"

MESS_ITA071:
incbin "mess/MESS_ITA_071.5tx"

MESS_ITA072:
incbin "mess/MESS_ITA_072.5tx"

MESS_ITA073:
incbin "mess/MESS_ITA_073.5tx"

MESS_ITA074:
incbin "mess/MESS_ITA_074.5tx"

MESS_ITA075:
incbin "mess/MESS_ITA_075.5tx"

MESS_ITA076:
incbin "mess/MESS_ITA_076.5tx"

MESS_ITA077:
incbin "mess/MESS_ITA_077.5tx"

MESS_ITA078:
incbin "mess/MESS_ITA_078.5tx"

MESS_ITA079:
incbin "mess/MESS_ITA_079.5tx"

MESS_ITA080:
incbin "mess/MESS_ITA_080.5tx"

MESS_ITA081:
incbin "mess/MESS_ITA_081.5tx"

MESS_ITA082:
incbin "mess/MESS_ITA_082.5tx"

MESS_ITA083:
incbin "mess/MESS_ITA_083.5tx"

MESS_ITA084:
incbin "mess/MESS_ITA_084.5tx"

MESS_ITA085:
incbin "mess/MESS_ITA_085.5tx"

MESS_ITA086:
incbin "mess/MESS_ITA_086.5tx"

MESS_ITA087:
incbin "mess/MESS_ITA_087.5tx"

MESS_ITA088:
incbin "mess/MESS_ITA_088.5tx"

MESS_ITA089:
incbin "mess/MESS_ITA_089.5tx"

MESS_ITA090:
incbin "mess/MESS_ITA_090.5tx"

MESS_ITA091:
incbin "mess/MESS_ITA_091.5tx"

MESS_ITA092:
incbin "mess/MESS_ITA_092.5tx"

MESS_ITA093:
incbin "mess/MESS_ITA_093.5tx"

MESS_ITA094:
incbin "mess/MESS_ITA_094.5tx"

MESS_ITA095:
incbin "mess/MESS_ITA_095.5tx"

MESS_ITA096:
incbin "mess/MESS_ITA_096.5tx"

MESS_ITA097:
incbin "mess/MESS_ITA_097.5tx"

MESS_ITA098:
incbin "mess/MESS_ITA_098.5tx"

MESS_ITA099:
incbin "mess/MESS_ITA_099.5tx"

MESS_ITA100:
incbin "mess/MESS_ITA_100.5tx"

MESS_ITA101:
incbin "mess/MESS_ITA_101.5tx"

MESS_ITA102:
incbin "mess/MESS_ITA_102.5tx"

MESS_ITA103:
incbin "mess/MESS_ITA_103.5tx"

MESS_ITA104:
incbin "mess/MESS_ITA_104.5tx"

MESS_ITA105:
incbin "mess/MESS_ITA_105.5tx"

MESS_ITA106:
incbin "mess/MESS_ITA_106.5tx"

MESS_ITA107:
incbin "mess/MESS_ITA_107.5tx"

MESS_ITA108:
incbin "mess/MESS_ITA_108.5tx"

MESS_ITA109:
incbin "mess/MESS_ITA_109.5tx"

MESS_ITA110:
incbin "mess/MESS_ITA_110.5tx"

MESS_ITA111:
incbin "mess/MESS_ITA_111.5tx"

MESS_ITA112:
incbin "mess/MESS_ITA_112.5tx"

MESS_ITA113:
incbin "mess/MESS_ITA_113.5tx"

MESS_ITA114:
incbin "mess/MESS_ITA_114.5tx"

MESS_ITA115:
incbin "mess/MESS_ITA_115.5tx"

MESS_ITA116:
incbin "mess/MESS_ITA_116.5tx"

MESS_ITA117:
incbin "mess/MESS_ITA_117.5tx"

MESS_ITA118:
incbin "mess/MESS_ITA_118.5tx"

MESS_ITA119:
incbin "mess/MESS_ITA_119.5tx"

MESS_ITA120:
incbin "mess/MESS_ITA_120.5tx"

MESS_ITA121:
incbin "mess/MESS_ITA_121.5tx"

MESS_ITA122:
incbin "mess/MESS_ITA_122.5tx"

MESS_ITA123:
incbin "mess/MESS_ITA_123.5tx"

MESS_ITA124:
incbin "mess/MESS_ITA_124.5tx"

MESS_ITA125:
incbin "mess/MESS_ITA_125.5tx"

MESS_ITA126:
incbin "mess/MESS_ITA_126.5tx"

MESS_ITA127:
incbin "mess/MESS_ITA_127.5tx"

MESS_ITA128:
incbin "mess/MESS_ITA_128.5tx"

MESS_ITA129:
incbin "mess/MESS_ITA_129.5tx"

MESS_ITA130:
incbin "mess/MESS_ITA_130.5tx"

MESS_ITA131:
incbin "mess/MESS_ITA_131.5tx"

MESS_ITA132:
incbin "mess/MESS_ITA_132.5tx"

MESS_ITA133:
incbin "mess/MESS_ITA_133.5tx"

MESS_ITA134:
incbin "mess/MESS_ITA_134.5tx"

MESS_ITA135:
incbin "mess/MESS_ITA_135.5tx"

MESS_ITA136:
incbin "mess/MESS_ITA_136.5tx"

MESS_ITA137:
incbin "mess/MESS_ITA_137.5tx"

MESS_ITA138:
incbin "mess/MESS_ITA_138.5tx"

MESS_ITA139:
incbin "mess/MESS_ITA_139.5tx"

MESS_ITA140:
incbin "mess/MESS_ITA_140.5tx"

MESS_ITA141:
incbin "mess/MESS_ITA_141.5tx"

MESS_ITA142:
incbin "mess/MESS_ITA_142.5tx"

MESS_ITA143:
incbin "mess/MESS_ITA_143.5tx"

MESS_ITA144:
incbin "mess/MESS_ITA_144.5tx"

MESS_ITA145:
incbin "mess/MESS_ITA_145.5tx"

MESS_ITA146:
incbin "mess/MESS_ITA_146.5tx"

MESS_ITA147:
incbin "mess/MESS_ITA_147.5tx"

MESS_ITA148:
incbin "mess/MESS_ITA_148.5tx"

MESS_ITA149:
incbin "mess/MESS_ITA_149.5tx"

MESS_ITA150:
incbin "mess/MESS_ITA_150.5tx"

MESS_ITA151:
incbin "mess/MESS_ITA_151.5tx"

MESS_ITA152:
incbin "mess/MESS_ITA_152.5tx"

MESS_ITA153:
incbin "mess/MESS_ITA_153.5tx"

MESS_ITA154:
incbin "mess/MESS_ITA_154.5tx"

MESS_ITA155:
incbin "mess/MESS_ITA_155.5tx"

MESS_ITA156:
incbin "mess/MESS_ITA_156.5tx"

MESS_ITA157:
incbin "mess/MESS_ITA_157.5tx"

MESS_ITA158:
incbin "mess/MESS_ITA_158.5tx"

MESS_ITA159:
incbin "mess/MESS_ITA_159.5tx"

MESS_ITA160:
incbin "mess/MESS_ITA_160.5tx"

MESS_ITA161:
incbin "mess/MESS_ITA_161.5tx"

MESS_ITA162:
incbin "mess/MESS_ITA_162.5tx"

MESS_ITA163:
incbin "mess/MESS_ITA_163.5tx"

MESS_ITA164:
incbin "mess/MESS_ITA_164.5tx"

MESS_ITA165:
incbin "mess/MESS_ITA_165.5tx"

MESS_ITA166:
incbin "mess/MESS_ITA_166.5tx"

MESS_ITA167:
incbin "mess/MESS_ITA_167.5tx"

MESS_ITA168:
incbin "mess/MESS_ITA_168.5tx"

MESS_ITA169:
incbin "mess/MESS_ITA_169.5tx"

MESS_ITA170:
incbin "mess/MESS_ITA_170.5tx"

MESS_ITA171:
incbin "mess/MESS_ITA_171.5tx"

MESS_ITA172:
incbin "mess/MESS_ITA_172.5tx"

MESS_ITA173:
incbin "mess/MESS_ITA_173.5tx"

MESS_ITA174:
incbin "mess/MESS_ITA_174.5tx"

MESS_ITA175:
incbin "mess/MESS_ITA_175.5tx"

MESS_ITA176:
incbin "mess/MESS_ITA_176.5tx"

MESS_ITA177:
incbin "mess/MESS_ITA_177.5tx"

MESS_ITA178:
incbin "mess/MESS_ITA_178.5tx"

MESS_ITA179:
incbin "mess/MESS_ITA_179.5tx"

MESS_ITA180:
incbin "mess/MESS_ITA_180.5tx"

MESS_ITA181:
incbin "mess/MESS_ITA_181.5tx"

MESS_ITA182:
incbin "mess/MESS_ITA_182.5tx"

MESS_ITA183:
incbin "mess/MESS_ITA_183.5tx"

MESS_ITA184:
incbin "mess/MESS_ITA_184.5tx"

MESS_ITA185:
incbin "mess/MESS_ITA_185.5tx"

MESS_ITA186:
incbin "mess/MESS_ITA_186.5tx"

MESS_ITA187:
incbin "mess/MESS_ITA_187.5tx"

MESS_ITA188:
incbin "mess/MESS_ITA_188.5tx"

MESS_ITA189:
incbin "mess/MESS_ITA_189.5tx"

MESS_ITA190:
incbin "mess/MESS_ITA_190.5tx"

MESS_ITA191:
incbin "mess/MESS_ITA_191.5tx"

MESS_ITA192:
incbin "mess/MESS_ITA_192.5tx"

MESS_ITA193:
incbin "mess/MESS_ITA_193.5tx"

MESS_ITA194:
incbin "mess/MESS_ITA_194.5tx"

MESS_ITA195:
incbin "mess/MESS_ITA_195.5tx"

MESS_ITA196:
incbin "mess/MESS_ITA_196.5tx"

MESS_ITA197:
incbin "mess/MESS_ITA_197.5tx"

MESS_ITA198:
incbin "mess/MESS_ITA_198.5tx"

MESS_ITA199:
incbin "mess/MESS_ITA_199.5tx"

MESS_ITA200:
incbin "mess/MESS_ITA_200.5tx"

MESS_ITA201:
incbin "mess/MESS_ITA_201.5tx"

MESS_ITA202:
incbin "mess/MESS_ITA_202.5tx"

MESS_ITA203:
incbin "mess/MESS_ITA_203.5tx"

MESS_ITA204:
incbin "mess/MESS_ITA_204.5tx"

MESS_ITA205:
incbin "mess/MESS_ITA_205.5tx"

MESS_ITA206:
incbin "mess/MESS_ITA_206.5tx"

MESS_ITA207:
incbin "mess/MESS_ITA_207.5tx"

MESS_ITA208:
incbin "mess/MESS_ITA_208.5tx"

MESS_ITA209:
incbin "mess/MESS_ITA_209.5tx"

MESS_ITA210:
incbin "mess/MESS_ITA_210.5tx"

MESS_ITA211:
incbin "mess/MESS_ITA_211.5tx"

MESS_ITA212:
incbin "mess/MESS_ITA_212.5tx"

MESS_ITA213:
incbin "mess/MESS_ITA_213.5tx"

MESS_ITA214:
incbin "mess/MESS_ITA_214.5tx"

MESS_ITA215:
incbin "mess/MESS_ITA_215.5tx"

MESS_ITA216:
incbin "mess/MESS_ITA_216.5tx"

MESS_ITA217:
incbin "mess/MESS_ITA_217.5tx"

MESS_ITA218:
incbin "mess/MESS_ITA_218.5tx"

MESS_ITA219:
incbin "mess/MESS_ITA_219.5tx"

MESS_ITA220:
incbin "mess/MESS_ITA_220.5tx"

MESS_ITA221:
incbin "mess/MESS_ITA_221.5tx"

MESS_ITA222:
incbin "mess/MESS_ITA_222.5tx"

MESS_ITA223:
incbin "mess/MESS_ITA_223.5tx"

MESS_ITA224:
incbin "mess/MESS_ITA_224.5tx"

MESS_ITA225:
incbin "mess/MESS_ITA_225.5tx"

MESS_ITA226:
incbin "mess/MESS_ITA_226.5tx"

MESS_ITA227:
incbin "mess/MESS_ITA_227.5tx"

MESS_ITA228:
incbin "mess/MESS_ITA_228.5tx"

MESS_ITA229:
incbin "mess/MESS_ITA_229.5tx"

MESS_ITA230:
incbin "mess/MESS_ITA_230.5tx"

MESS_ITA231:
incbin "mess/MESS_ITA_231.5tx"

MESS_ITA232:
incbin "mess/MESS_ITA_232.5tx"

MESS_ITA233:
incbin "mess/MESS_ITA_233.5tx"

MESS_ITA234:
incbin "mess/MESS_ITA_234.5tx"

MESS_ITA235:
incbin "mess/MESS_ITA_235.5tx"

MESS_ITA236:
incbin "mess/MESS_ITA_236.5tx"

MESS_ITA237:
incbin "mess/MESS_ITA_237.5tx"

MESS_ITA238:
incbin "mess/MESS_ITA_238.5tx"

MESS_ITA239:
incbin "mess/MESS_ITA_239.5tx"

MESS_ITA240:
incbin "mess/MESS_ITA_240.5tx"

MESS_ITA241:
incbin "mess/MESS_ITA_241.5tx"

MESS_ITA242:
incbin "mess/MESS_ITA_242.5tx"

MESS_ITA243:
incbin "mess/MESS_ITA_243.5tx"

MESS_ITA244:
incbin "mess/MESS_ITA_244.5tx"

MESS_ITA245:
incbin "mess/MESS_ITA_245.5tx"

MESS_ITA246:
incbin "mess/MESS_ITA_246.5tx"

MESS_ITA247:
incbin "mess/MESS_ITA_247.5tx"

MESS_ITA248:
incbin "mess/MESS_ITA_248.5tx"

MESS_ITA249:
incbin "mess/MESS_ITA_249.5tx"

MESS_ITA250:
incbin "mess/MESS_ITA_250.5tx"

MESS_ITA251:
incbin "mess/MESS_ITA_251.5tx"

MESS_ITA252:
incbin "mess/MESS_ITA_252.5tx"

MESS_ITA253:
incbin "mess/MESS_ITA_253.5tx"

MESS_ITA254:
incbin "mess/MESS_ITA_254.5tx"

MESS_ITA255:
incbin "mess/MESS_ITA_255.5tx"

MESS_ITA256:
incbin "mess/MESS_ITA_256.5tx"

MESS_ITA257:
incbin "mess/MESS_ITA_257.5tx"

MESS_ITA258:
incbin "mess/MESS_ITA_258.5tx"

MESS_ITA259:
incbin "mess/MESS_ITA_259.5tx"

MESS_ITA260:
incbin "mess/MESS_ITA_260.5tx"

MESS_ITA261:
incbin "mess/MESS_ITA_261.5tx"

MESS_ITA262:
incbin "mess/MESS_ITA_262.5tx"

MESS_ITA263:
incbin "mess/MESS_ITA_263.5tx"

MESS_ITA264:
incbin "mess/MESS_ITA_264.5tx"

MESS_ITA265:
incbin "mess/MESS_ITA_265.5tx"

MESS_ITA266:
incbin "mess/MESS_ITA_266.5tx"

MESS_ITA267:
incbin "mess/MESS_ITA_267.5tx"

