; COMANDOS SCRIPTS
; ----------------
; SELECT_SPR      1 01 -> (1) Selecciona personaje. Sig. Byte = n� personaje
; SAYS            2 02 -> (2) Personaje dice. Sig. Byte = Personaje. Sig Byte = n� mensaje. Se queda seleccionado el pers
; GO_LEFT         3 03 -> (1) Personaje izquierda. Sig Byte = N� de tiles que se desplaza (0 = Cambiar s�lo el facing)
; GO_RIGHT        4 04 -> (1) Personaje derecha. Sig Byte = N� de tiles que se desplaza (0 = Cambiar s�lo el facing)
; GO_UP           5 05 -> (1) Personaje arriba. Sig Byte =  N� de tiles que se desplaza (0 = Cambiar s�lo el facing)
; GO_DOWN         6 06 -> (1) Personaje abajo. Sig Byte =  N� de tiles que se desplaza (0 = Cambiar s�lo el facing)
; SET_COORD       7 07 -> (2) Coordenadas Personaje X. Sig Byte = Coordenada Y (en ST) a poner. Sig Byte = Coordenada X (en ST) a poner
; SET_CAM         8 08 -> (2) Coordenadas C�mara Abs. Sig Byte = Coordenada Y (en ST). Sig Byte = Coordenada X (en ST)
; UPDATE_SCR      9 09 -> (0) UpdateAll. Incluye un halt
; CAM_LEFT       10 0A -> (0) C�mara izquierda
; CAM_RIGHT      11 0B -> (0) C�mara derecha
; CAM_UP         12 0C -> (0) C�mara arriba
; CAM_DOWN       13 0D -> (0) C�mara abajo
; PAUSE          14 0E -> (1) Pausa. Sig Byte = N� ciclos. 0 = Hasta tecla
; GET_OBJ        15 0F -> (1) Coger Objeto. Sig byte = N� objeto. Si $ff, entonces el indicado por el Flag 5
; DESTROY_OBJ    16 10 -> (1) Destruir Objeto. Sig byte = n� Objeto. Si $ff, entonces el indicado por el Flag 5
; DO_FADEOUT     17 11 -> (0) Fadeout
; SET_FLAG       18 12 -> (2) Flag abs: Poner en el flag n� Sig Byte, el valor absoluto del Sig Byte (2�)
; ADD_FLAG       19 13 -> (2) Flag rel: Sumar en el flag n� Sig Byte el valor del Sig Byte (2�)
; FOLLOW_LIST    20 14 -> (1) Llena la lista de seguimiento del valor en el siguiente byte
; SET_MAP        21 15 -> (1) Set map. Sig. byte = n� mapa
; DO_FADEIN      22 16 -> (0) Fadein
; GOTO_SEQ       23 17 -> (1) Salta a la secuencia indicada en el Sig. Byte
; MESSAGE        24 18 -> (3) Mensaje +3 en coordenadas +1, +2
; SET_GFXPTR     25 19 -> (2) Pon el Word +1 como puntero gr�fico base del sprite actual
; BREAK          26 1A -> (0) Interrumpe todos los scripts en marcha
; CAM_AUTO       27 1B -> (0) Situa autom�ticamente la c�mara, centrando al jugador
; SP_SWITCH_TO   28 1C -> (1) Cambia el "tipo" del sprite actual al indicado en el byte siguiente
; NEW_SPRITE     29 1D -> (5) Crea un sprite de tipo +1 byte, en las coordenadas +2, +3 bytes, con el gr�fico en +4+5 (word)
; SET_MOVEMENT   30 1E -> (2) Puntero al script de movimiento para el sprite actual. Word
; ADJUST_OFFSET  31 1F -> (2) Ajusta el offset del sprite actual: +1 = Offset Y, +2 = Offset X
; SET_STILE      32 20 -> (3) Coloca, en las coordenadas de los dos siguientes bytes (y, x), el ST indicado en sig. byte (+3)
; RUN_FIGHT      33 21 -> (1) Ejecuta una "lucha" contra el jefe de caverna n� sig byte
; CREATE_OBJ     34 22 -> (4) Crea un objeto +1 en el mapa +2 en las coordenadas Y = +3, X = +4
; PLAY_SONG      35 23 -> (1) Ejecuta la canci�n n�mero + 1
; PLAY_SFX       36 24 -> (1) Haz sonar el SFX n�mero +1
; MUTE_SOUND     37 25 -> (0) Silencia el player de SFX y M�sica
; DO_FADECLOSE   38 26 -> (0) Efecto visual de cierre de pantalla desde el borde hacia el centro
; RESTART_GAME   39 27 -> (0) Recomienza todas las variables y vuelve al men�
; FIRE_EFFECT    40 28 -> (0) Efecto de fuego hasta pulsar tecla
; CHAPTER_SIGN   41 29 -> (1) Texto "Cap�tulo" + el n�mero especificado en + 1 (+ Pause 0 + DO_FADEOUT + SET_MAP (FLAG 4) )
; SAYS_2         42 2A -> (1) Igual que SAYS, pero con la segunda tabla de mensajes


; DO_LOOP        43 2B -> (1) Bucle: Se repite tantas veces como siguiente byte, desde 2� sig byte hasta $ff
; IF_FLAG_EQ     44 2C -> (2) Ejecuta las �rdenes hasta el siguiente ENDIF solo si el flag indicado en el sig. Byte equivale al valor del 2� Byte
; IF_FLAG_NOTEQ  45 2D -> (2) Ejecuta las �rdenes hasta el siguiente ENDIF solo si el flag indicado en el sig. Byte NO equivale al valor del 2� Byte
; IF_OBJ_CARR    46 2E -> (1) Ejecuta las �rdenes hasta el siguiente ENDIF si el objeto indicado en el siguiente byte lo tiene el jugador en su inventario
; IF_FLAG_NOTMIN 47 2F -> (2) Ejecuta las �rdenes hasta el siguiente ENDIF solo si el flag indicado en el sig. Byte NO ES MENOR (>=) que el valor del 2� Byte
; IF_FLAG_MIN    48 30 -> (2) Ejecuta las �rdenes hasta el siguiente ENDIF solo si el flag indicado en el sig. Byte es menor que el valor del 2� Byte
; IF_OBJ_NOTCARR 49 31 -> (1) Ejecuta las �rdenes hasta el siguiente ENDIF si el objeto indicado en el siguiente byte NO est� en el invetario del jugador
; IF_FLAG_BIT    50 32 -> (2) Ejecuta las �rdenes hasta el siguiente ENDIF solo si el flag indicado en el sig. Byte tiene activado el bit indicado en el 2� byte
; IF_CONFIRM     51 33 -> (0) Ejecuta las �rdenes hasta el siguiente ENDIF si el jugador pulsa la tecla especificada como KEYCONFIRM_YES y considera el IF
;                             como NO cumplido si el jugador pulsa la tecla KEYCONFIRM_NO.
; $ff -> FIN DEL SCRIPT

SELECT_SPR      EQU 01
SAYS            EQU 02
GO_LEFT         EQU 03
GO_RIGHT        EQU 04
GO_UP           EQU 05
GO_DOWN         EQU 06
SET_COORD       EQU 07
SET_CAM         EQU 08
UPDATE_SCR      EQU 09
CAM_LEFT        EQU 10
CAM_RIGHT       EQU 11
CAM_UP          EQU 12
CAM_DOWN        EQU 13
PAUSE           EQU 14
GET_OBJ         EQU 15
DESTROY_OBJ     EQU 16
DO_FADEOUT      EQU 17
SET_FLAG        EQU 18
ADD_FLAG        EQU 19
FOLLOW_LIST     EQU 20
SET_MAP         EQU 21
DO_FADEIN       EQU 22
GOTO_SEQ        EQU 23
MESSAGE         EQU 24
SET_GFXPTR      EQU 25
BREAK           EQU 26
CAM_AUTO        EQU 27
SP_SWITCH_TO    EQU 28
NEW_SPRITE      EQU 29
SET_MOVEMENT    EQU 30
ADJUST_OFFSET   EQU 31
SET_STILE       EQU 32
RUN_FIGHT       EQU 33
CREATE_OBJ      EQU 34
PLAY_SONG       EQU 35
PLAY_SFX        EQU 36
MUTE_SOUND      EQU 37
DO_FADECLOSE    EQU 38
RESTART_GAME    EQU 39
FIRE_EFFECT     EQU 40
CHAPTER_SIGN    EQU 41
SAYS_2          EQU 42

DO_LOOP         EQU 43
END_LOOP        EQU $ff

IF_FLAG_EQ      EQU 44
IF_FLAG_NOTEQ   EQU 45
IF_OBJ_CARR     EQU 46
IF_FLAG_NOTMIN  EQU 47
IF_FLAG_MIN     EQU 48
IF_OBJ_NOTCARR  EQU 49
IF_FLAG_BIT     EQU 50
IF_CONFIRM      EQU 51
END_IF          EQU $fe

END_SCRIPT      EQU $ff

BREAKPOINT      EQU $f0

S_FGON          EQU  0
S_FCES          EQU  1
S_XAN           EQU  2
S_BORRACHO      EQU  3
S_CURA          EQU  4
S_AMA           EQU  5
S_BOTICARIO     EQU  6
S_PAISANO       EQU  7
S_MEIGA         EQU  8
S_CERRAJERO     EQU  9
S_BRUNILDA      EQU 10
S_MISTERIOSO    EQU 11
S_PIEDRA        EQU 12
S_EXPLOSION     EQU 13
S_ENEMIGO       EQU 14
S_ANTON         EQU 15

O_VESTIDO       EQU  0
O_ORUJO         EQU  1
O_CARTAS        EQU  2
O_COFRE         EQU  3
O_CRANEO_F      EQU  4
O_CRANEO_V      EQU  5
O_DIARIO        EQU  6
O_DINERO        EQU  7
O_ESQ_1         EQU  8
O_ESQ_2         EQU  9
O_ESQ_3         EQU 10
O_ESQ_4         EQU 11
O_FAROL         EQU 12
O_GALLINA       EQU 13
O_LLAVE         EQU 14
O_PALA          EQU 15
O_SETA_R        EQU 16
O_SETA_N        EQU 17
O_CRUCIFIJO     EQU 18
O_AGUA          EQU 19
O_JARRA         EQU 20
O_AGUA_F        EQU $ff

SCRIPTS:
        SC00:                                            ; 00 Intro
               defb MUTE_SOUND
               defb SET_COORD, 48, 54
               defb FOLLOW_LIST, 0
               defb SET_FLAG, 3, 32

               defb CHAPTER_SIGN, 1

               defb MESSAGE,  2, 8, 6
               defb MESSAGE,  5,  5, 7
               ;defb MESSAGE,  9,  5, 8
               defb MESSAGE, 14,  5, 9
               defb PAUSE, 0
               defb DO_FADEOUT
               defb SET_CAM, 39, 46
               defb DO_FADEIN

               defb SELECT_SPR, S_FCES
               defb SET_COORD, 48, 56


               defb DO_LOOP, 8
                    defb SELECT_SPR, S_FGON, GO_UP, 1
                    defb SELECT_SPR, S_FCES, GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb GO_LEFT, 0, SELECT_SPR, S_FGON, GO_RIGHT, 0, UPDATE_SCR

               defb SAYS, S_FGON, 12
               
               defb SAYS, S_FCES, 13
               defb SAYS, S_FGON, 14
               defb SAYS, S_FCES, 15
               defb SAYS, S_FGON, 16

               defb SET_FLAG, 10, $7f ; 0x0111 1111
               defb SET_FLAG, 13, $0f ; 0x0000 1111
               defb GOTO_SEQ, 59                  ; Creaci�n de setas y crucifijos

               ;defb END_SCRIPT

        SC01:                                                  ; 01 Trigger FCes
               defb IF_FLAG_EQ, 0, 3                          ; Conversaci�n antes de la cena
                    defb IF_FLAG_BIT, 1, 1
                         defb SAYS, S_FCES, 93
                         defb BREAK
                    defb END_IF
                    defb SAYS, S_FCES, 88
                    defb SAYS, S_FGON, 89
                    defb SAYS, S_FCES, 90
                    defb SAYS, S_FGON, 91
                    defb SAYS, S_FCES, 92
                    defb ADD_FLAG, 1, 2
                    defb ADD_FLAG, 3, -4
               defb END_IF

               defb IF_FLAG_EQ, 0, 0
                    defb SAYS, S_FCES, 11 ; Fray Ces�reo no nos deja salir del bosque
               defb END_IF
               defb IF_FLAG_EQ, 0, 1
                    defb SAYS, S_FCES, 31   ; Fray Ces�reo s�lo nos deja ir a ver al cura
               defb END_IF
               defb IF_FLAG_EQ, 0, 2
                    defb SAYS, S_FCES, 44  ; Fray Ces�reo s�lo nos deja seguir al cura
               defb END_IF
               defb BREAK
;               defb END_SCRIPT

        SC02:  defb SAYS, S_FCES, 10, END_SCRIPT ; 02 Fray Ces�reo s�lo nos deja entrar en la posada

        SC03:                                    ; 03 Trigger Xan
               defb IF_FLAG_EQ, 0, 0                    ; Primer di�logo en la taberna
                    defb SAYS, S_FGON, 17
                    defb SAYS, S_XAN, 18
                    defb SAYS, S_FCES, 19
                    defb SAYS, S_FGON, 20
                    defb SAYS, S_XAN, 21
                    defb SAYS, S_BORRACHO, 22
                    defb SAYS, S_XAN, 23
                    defb SAYS, S_BORRACHO, 24
                    defb SAYS, S_XAN, 25
                    defb SAYS, S_FGON, 26
                    defb SAYS, S_XAN, 27
                    defb SAYS, S_BORRACHO, 28
                    defb SAYS, S_XAN, 29
                    defb SAYS, S_FGON, 58
                    defb IF_FLAG_MIN, 3, 5
                         defb SET_FLAG, 3, 5
                    defb END_IF
                    defb ADD_FLAG, 3, -4
                    defb SET_FLAG, 0, 1
                    defb GO_DOWN, 0, UPDATE_SCR
                    defb SAYS, S_FGON, 233
                    defb GOTO_SEQ, 30
               defb END_IF

               defb IF_FLAG_MIN, 0, 4                    ; Segundo di�logo en la taberna
                    defb SAYS, S_XAN, 27
                    defb SAYS, S_BORRACHO, 22
                    defb SAYS, S_XAN, 25
                    defb SAYS, S_BORRACHO, 24
                    defb SAYS, S_XAN, 23
                    defb SAYS, S_XAN, 29
                    defb GOTO_SEQ, 30
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 18              ; Ya le dimos el orujo
                    defb SAYS, S_XAN, 216
                    defb BREAK
               defb END_IF
                   
               defb IF_FLAG_NOTMIN, 0, 16              ; �ltimo cap�tulo: Orujo
                    defb IF_OBJ_CARR, O_ORUJO                ; Si llevamos el orujo
                         defb DESTROY_OBJ, O_ORUJO
                         defb GET_OBJ, O_DINERO
                         defb SAYS, S_XAN, 217
                         defb SET_FLAG, 0, 18
                         defb BREAK
                    defb END_IF
                    defb SAYS, S_XAN, 103              ; Como no lo llevamos, nos lo pide
                    defb SET_FLAG, 0, 17
                    defb BREAK
               defb END_IF
                    
;               defb IF_FLAG_NOTMIN, 0, 4                     ; Estamos buscando a la meiga
                    defb SAYS, S_FGON, 104
                    defb SAYS, S_XAN, 105
                    defb IF_FLAG_MIN, 3, 5
                         defb SET_FLAG, 3, 5
                    defb END_IF                    
                    defb ADD_FLAG, 3, -4
                    defb IF_FLAG_NOTMIN, 3, 24
                         defb SAYS, S_XAN, 107
                    defb END_IF
                    defb GOTO_SEQ, 30
;               defb END_IF
               defb END_SCRIPT

        SC04:  defb SAYS, S_BORRACHO, 28, END_SCRIPT   ; 04 Di�logo con borracho

        SC05:  defb SAYS, S_FGON, 108, END_SCRIPT      ; 05 No podemos salir del pueblo abandonando a su suerte al pobre Fray Ces�reo

        SC06:                                              ; 06 Trigger paisano
               defb IF_FLAG_EQ, 0, 0
                    defb SAYS, S_FGON, 17
                    defb SAYS, S_PAISANO, 106
                    defb SAYS, S_FGON, 58
                    defb BREAK
               defb END_SCRIPT
               defb IF_FLAG_MIN, 0, 5
                    defb SAYS, S_PAISANO, 27
               defb END_IF
               defb IF_FLAG_NOTMIN, 0, 5
                    defb SAYS, S_PAISANO, 105
                    defb IF_FLAG_MIN, 3, 5
                         defb SET_FLAG, 3, 5
                    defb END_IF                    
                    defb ADD_FLAG, 3, -4
                    defb IF_FLAG_NOTMIN, 3, 24
                         defb SAYS, S_PAISANO, 107
                    defb END_IF
                    defb GOTO_SEQ, 30
               defb END_IF
               defb END_SCRIPT

        SC07:                                              ; 07 Conversaci�n con el cura (iglesia)
               defb SAYS, S_CURA, 32
               defb SAYS, S_FGON, 33
               defb SAYS, S_CURA, 34
               defb SAYS, S_FGON, 35
               defb SAYS, S_CURA, 36
               defb SAYS, S_FGON, 37
               defb SAYS, S_CURA, 38
               defb SAYS, S_FCES, 39
               defb SAYS, S_CURA, 40
               defb SAYS, S_FCES, 13
               defb SAYS, S_FGON, 41
               defb SAYS, S_CURA, 42
               defb SET_COORD, 4, 20
               defb SELECT_SPR, S_FCES, SET_COORD, 3, 17, GO_RIGHT, 0
               defb SELECT_SPR, S_FGON, SET_COORD, 3, 19, GO_RIGHT, 0
               defb DO_FADEOUT
               defb SET_CAM, 0, 11
               defb FOLLOW_LIST, 1
               defb PLAY_SONG, 11
               defb UPDATE_SCR
               defb SAYS, S_CURA, 43
               defb SET_MOVEMENT
               defw PRIEST_MOVEMENT

               defb SET_FLAG, 0, 2
               defb END_SCRIPT

        SC08:                                                ; 08 Trigger BOticario
               defb IF_OBJ_CARR, O_CARTAS
                    defb SAYS, S_BOTICARIO, 230
                    defb SET_COORD, $f0, $f0
                    defb DO_FADEOUT
                    defb DESTROY_OBJ, O_CARTAS
                    defb SET_FLAG, 0, 19
                    defb BREAK
               defb END_IF

               defb IF_OBJ_CARR, O_VESTIDO
                    defb IF_OBJ_NOTCARR, O_LLAVE
                         defb SAYS_2, S_FGON, 1
                         defb SAYS, S_BOTICARIO, 222
                         defb GET_OBJ, O_LLAVE
                         defb BREAK
                    defb END_IF
                    defb SAYS, S_BOTICARIO, 223
                    defb BREAK
               defb END_IF
               
               defb IF_FLAG_MIN, 0, 12
                    defb SAYS, S_BOTICARIO, 84
               defb END_IF

               defb IF_FLAG_BIT, 10, 7
                    defb SAYS, S_BOTICARIO, 168
                    defb IF_FLAG_NOTMIN, 0, 16
                         defb IF_OBJ_NOTCARR, O_LLAVE
                              defb SAYS_2, S_FGON, 1
                              defb SAYS_2, S_BOTICARIO, 2
                         defb END_IF
                    defb END_IF
                    defb BREAK
               defb END_IF
               
               defb IF_FLAG_NOTMIN, 0, 12
                    defb IF_FLAG_MIN, 10, 128
                         defb SAYS, S_BOTICARIO, 164
                         defb SAYS, S_BOTICARIO, 165
                         defb IF_OBJ_CARR, O_SETA_R
                              defb DESTROY_OBJ, O_SETA_R
                              defb ADD_FLAG, 10, 128
                              defb SAYS, S_BOTICARIO, 166
                              defb DO_LOOP, 6
                                   defb GO_LEFT, 1, UPDATE_SCR
                              defb END_LOOP
                              defb DO_LOOP, 11
                                   defb GO_DOWN, 1, UPDATE_SCR
                              defb END_LOOP
                              defb DO_LOOP, 2
                                   defb GO_LEFT, 1, UPDATE_SCR
                              defb END_LOOP
                              defb SET_COORD, 32, 14
                              defb SET_MOVEMENT
                              defw BOTICARIO_MOV
                              defb UPDATE_SCR, BREAK
                         defb END_IF
                    defb END_IF
               defb END_IF
               defb END_SCRIPT

        SC09:                                               ; 09 Paso de mapa a la casa del boticario        
               defb SET_COORD, 9, 1
               defb DO_FADEOUT, SET_MAP, 1
               defb SELECT_SPR, S_FCES, SET_COORD, 8, 0
               defb SELECT_SPR, S_CURA, SET_COORD, 8, 2
               defb CAM_AUTO
;               defb DO_FADEIN
               defb SET_FLAG, 6, 0

               defb DO_LOOP, 10
                    defb SELECT_SPR, S_FGON, GO_RIGHT, 1
                    defb SELECT_SPR, S_FCES, GO_RIGHT, 1
                    defb SELECT_SPR, S_CURA, GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 4
                    defb GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 2
                    defb SELECT_SPR, S_FCES, GO_RIGHT, 1
                    defb SELECT_SPR, S_FGON, GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 4
                    defb GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 2
                    defb SELECT_SPR, S_FCES, GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 6
                    defb CAM_UP
                    defb SELECT_SPR, S_FCES, GO_UP, 1
                    defb SELECT_SPR, S_FGON, GO_UP, 1
                    defb SELECT_SPR, S_CURA, GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb GO_UP, 1, UPDATE_SCR

               defb SAYS, S_CURA, 45
               defb SELECT_SPR, S_AMA, GO_DOWN, 1, SET_COORD, 2, 8
               defb UPDATE_SCR
               defb SAYS, S_AMA, 46

               defb SAYS, S_CURA, 47
               defb SAYS, S_AMA, 48
               defb SAYS, S_CURA, 49
               defb DO_FADEOUT

;               defb SET_CAM, 11, 36     ; C�mara al recibidor
               defb SET_COORD, 15, 46, GO_LEFT, 0   ; Cura
               defb SELECT_SPR, S_FGON, SET_COORD, 16, 46, GO_LEFT, 0
               defb SELECT_SPR, S_FCES, SET_COORD, 17, 46, GO_LEFT, 0
               defb SELECT_SPR, S_AMA, SET_COORD, 16, 44, GO_RIGHT, 0
               defb SELECT_SPR, S_BOTICARIO, SET_COORD, 14, 39, GO_DOWN, 1, GO_RIGHT, 0
               defb CAM_AUTO
               defb SET_FLAG, 6, 255
               defb DO_FADEIN

               defb DO_LOOP, 10
                    defb GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb GO_LEFT,0,GO_RIGHT, 0      ; Posici�n de "reposo"
               defb UPDATE_SCR

               defb SAYS, S_BOTICARIO, 50
               defb SAYS, S_CURA, 51
               defb SAYS, S_BOTICARIO, 52
               defb SAYS, S_FGON, 53
               defb SAYS, S_CURA, 54
               defb SAYS, S_BOTICARIO, 55
               defb SAYS, S_AMA, 56
               defb DO_FADEOUT

;               defb SET_CAM, 23, 53            ; C�mara a la habitaci�n
               defb SELECT_SPR, S_FGON, SET_COORD, 27, 59, GO_RIGHT, 0
               defb SELECT_SPR, S_FCES, SET_COORD, 27, 61
               defb SELECT_SPR, S_AMA, SET_COORD, 30, 60, GO_UP, 2

               defb CAM_AUTO
               defb DO_FADEIN

               defb SAYS, S_AMA, 57
               defb SAYS, S_FGON, 58

               defb DO_LOOP, 7
                    defb SELECT_SPR, S_AMA, GO_LEFT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 2
                    defb GO_DOWN, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb SET_COORD, $ff, $ff
               defb UPDATE_SCR

               defb SET_FLAG, 0, 3
               defb FOLLOW_LIST, 0
               defb END_SCRIPT

        SC10:                                           ; 10 Cena con el boticario
               defb DO_FADEOUT
               defb SET_COORD, 2, 25, GO_RIGHT, 1, GO_DOWN, 0
               defb IF_FLAG_NOTEQ, 0, 3
                    defb CAM_AUTO
                    defb UPDATE_SCR
                    defb BREAK
               defb END_IF

               defb SET_CAM, 64, 45
;               defb SELECT_SPR, S_FGON
               defb GO_UP, 1, SET_COORD, 72, 52
               defb SELECT_SPR, S_CURA, SET_COORD, 68, 49, GO_RIGHT, 0
               defb SELECT_SPR, S_FCES, GO_UP, 1, SET_COORD, 72, 51
               defb SELECT_SPR, S_BOTICARIO, SET_COORD, 68, 55, GO_LEFT, 0
               defb SELECT_SPR, S_AMA, SET_COORD, 66, 48, GO_RIGHT, 0

               defb DO_FADEIN

               defb SAYS, S_BOTICARIO, 59
               defb DO_LOOP, 2
                    defb SELECT_SPR, S_FCES, GO_UP, 1
                    defb SELECT_SPR, S_FGON, GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb GO_UP, 1, GO_DOWN, 0, GO_UP, 0
               defb SELECT_SPR, S_FCES, GO_LEFT, 1
               defb UPDATE_SCR
               defb GO_LEFT, 1
               defb UPDATE_SCR
               defb SELECT_SPR, S_FGON, SET_COORD, 70, 52
               defb UPDATE_SCR

               defb DO_LOOP, 6
                    defb SELECT_SPR, S_FCES, GO_LEFT, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 12
                    defb GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 10
                    defb GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb GO_DOWN, 0
               defb SET_COORD, 66, 52
               defb UPDATE_SCR
               defb PAUSE, 10

               defb DO_FADEOUT
               defb MESSAGE, 4, 5, 96
               defb PAUSE, $ff
               defb DO_FADEOUT
               defb DO_FADEIN

               defb SAYS, S_BOTICARIO, 60
               defb SAYS, S_CURA, 61
               defb SAYS, S_BOTICARIO, 62
               defb SAYS, S_CURA, 63
               defb SAYS, S_BOTICARIO, 64
               defb SAYS, S_FCES, 65
               defb SAYS, S_BOTICARIO, 66
               defb SAYS, S_CURA, 67
               defb SAYS, S_FGON, 68

               defb DO_LOOP, 6
                    defb SELECT_SPR, S_FGON, GO_DOWN, 1
                    defb SELECT_SPR, S_CURA, GO_DOWN, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 4
                    defb GO_DOWN, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb PAUSE, 20
               defb DO_FADEOUT
               defb MESSAGE, 4, 5, 97
               defb PAUSE, $ff
               defb DO_FADEOUT

;               defb SET_CAM, 23, 55                   ; Habitaci�n
               defb SET_STILE, 26, 56, 223
               defb SET_STILE, 26, 57, 224
               defb SELECT_SPR, S_FGON, SET_COORD, 26, 57
               defb CAM_AUTO
               defb SET_COORD, 0, 0
               defb DO_FADEIN

               defb PAUSE, 20
               defb SET_COORD, 26, 58
               defb SET_STILE, 26, 56, 98
               defb SET_STILE, 26, 57, 99
               defb UPDATE_SCR

               defb SAYS, S_FGON, 69

               defb DO_LOOP, 8
                    defb GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 4
                    defb GO_RIGHT, 1
                    defb CAM_RIGHT
                    defb UPDATE_SCR
               defb END_LOOP

               defb SAYS, S_FGON, 70
               defb SET_FLAG, 0, 4

               defb SELECT_SPR, S_FCES, SET_COORD, $ff, $ff
               defb SELECT_SPR, S_CURA, SET_COORD, 3, 62, GO_LEFT, 1
               defb SELECT_SPR, S_BOTICARIO, SET_COORD, 5, 62, GO_UP, 1, GO_LEFT, 1, GO_RIGHT, 0
               defb SELECT_SPR, S_AMA, SET_COORD,  37, 48
               defb SET_MOVEMENT
               defw AMA_MOVEMENT        ; Para que reinicie el movimiento desde principio

               defb END_SCRIPT

        SC11:                                     ; 11 Puerta cerrada
               defb IF_FLAG_EQ, 0, 0
                    defb GOTO_SEQ, 2
               defb END_IF
               defb IF_FLAG_MIN, 0, 3
                    defb GOTO_SEQ, 1
               defb END_IF
               defb SAYS, S_FGON, 71
               defb BREAK
               defb END_SCRIPT

        SC12:                                     ; 12 No podemos irnos de la casa sin Fray Ces�reo
               defb IF_FLAG_EQ, 0, 4
                    defb SAYS, S_FGON, 72
                    defb BREAK
               defb END_IF
               defb IF_OBJ_CARR, O_VESTIDO
                    defb GOTO_SEQ, 47                       ; No nos deja salir vestido de criada
               defb END_IF
               defb GOTO_SEQ, 26

        SC13:                                                  ; 13 Inicio cap�tulo 2: Encuentro en la cama a FCes
               defb DO_FADEOUT
               defb SET_COORD, 6, 57, GO_RIGHT, 0
               defb ADJUST_OFFSET, 1, 0
               defb CAM_AUTO
               defb IF_FLAG_NOTEQ, 0, 4
                    defb UPDATE_SCR
                    defb BREAK
               defb END_IF

               defb SET_FLAG, 4, 1
               defb CHAPTER_SIGN, 2
               defb UPDATE_SCR
               defb SAYS, S_CURA, 73, GO_RIGHT, 0
               defb DO_LOOP, 10
                    defb SELECT_SPR, S_FGON, GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 4
                    defb GO_UP, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 2
                    defb GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb GO_UP, 1
               defb UPDATE_SCR

               defb SAYS, S_FGON, 74
               defb SAYS, S_BOTICARIO, 75
               defb SAYS, S_CURA, 76
               defb SAYS, S_FGON, 77
               defb SAYS, S_BOTICARIO, 78
               defb SAYS, S_FGON, 79
               defb SAYS, S_BOTICARIO, 80
               defb SAYS, S_CURA, 81
               defb SAYS, S_FGON, 82
               defb SAYS, S_BOTICARIO, 83
               defb SAYS, S_CURA, 84
               defb SAYS, S_FGON, 85
               defb SAYS, S_BOTICARIO, 86
               defb SAYS, S_CURA, 87

               defb SET_FLAG, 0, 5
               defb IF_FLAG_MIN, 3, 9
                    defb SET_FLAG, 3, 9
               defb END_IF
               defb ADD_FLAG, 3, -8
               defb END_SCRIPT

        SC14:                                                 ; 14 Ama de llaves
               defb IF_FLAG_EQ, 0, 4
                    defb SAYS, S_AMA, 102
                    defb BREAK
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 16
                    defb IF_OBJ_NOTCARR, O_LLAVE
                         defb SAYS_2, S_FGON, 1
                         defb SAYS, S_AMA, 220
                    defb END_IF
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 12
                         defb SAYS, S_AMA, 153
               defb END_IF

               defb IF_FLAG_EQ, 0, 17
                    defb IF_OBJ_NOTCARR, O_ORUJO
                         defb IF_OBJ_NOTCARR, O_CARTAS
                              defb IF_OBJ_NOTCARR, O_DINERO
                                   defb SAYS, S_FGON, 218
                                   defb SAYS, S_AMA, 219
                                   defb SAYS, S_FGON, 58
                                   defb GET_OBJ, O_ORUJO
                                   defb BREAK
                              defb END_IF
                         defb END_IF
                    defb END_IF
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 5
                    defb IF_FLAG_MIN, 0, 12
                         defb SAYS, S_AMA, 99
                         defb IF_OBJ_CARR, O_GALLINA
                              defb BREAK
                         defb END_IF
                         defb SAYS, S_FGON, 100
                         defb SAYS, S_AMA, 101
                         defb GET_OBJ, O_GALLINA
                    defb END_IF
               defb END_IF

               defb END_SCRIPT

        SC15:
               defb IF_FLAG_EQ, 0, 1                          ; 15 Trigger Cura
                    defb GOTO_SEQ, 7
               defb END_IF
               defb IF_FLAG_EQ, 0, 2
                    defb SAYS, S_CURA, 43
                    defb BREAK
               defb END_IF
               defb IF_FLAG_NOTMIN, 0, 5
                    defb IF_FLAG_MIN, 0, 8
                         defb SAYS, S_CURA, 87, BREAK
                    defb END_IF
               defb END_IF
               defb SAYS, S_CURA, 232
               defb END_SCRIPT

       SC16:                                                 ; 16 Armario criada
               defb IF_FLAG_NOTMIN, 0, 16
                    defb IF_OBJ_CARR, O_VESTIDO
                         defb SET_GFXPTR
                         defw FGON
                         defb SAYS, S_FGON, 126
                         defb DESTROY_OBJ, O_VESTIDO
                         defb BREAK
                    defb END_IF
                    defb SAYS, S_FGON, 184
                    defb SET_GFXPTR
                    defw AMA
                    defb GET_OBJ, O_VESTIDO
                    defb BREAK
               defb END_IF
               defb SAYS, S_FGON, 95
               defb END_SCRIPT

       SC17:                                                 ; 17 Cerrajero
               defb SAYS, S_CERRAJERO, 94
               defb IF_OBJ_CARR, O_COFRE
                    defb SAYS, S_CERRAJERO, 228
                    defb IF_OBJ_CARR, O_DINERO
                         defb SAYS, S_CERRAJERO, 229
                         defb DESTROY_OBJ, O_COFRE, DESTROY_OBJ, O_DINERO
                         defb GET_OBJ, O_CARTAS
                    defb END_IF
               defb END_IF
               defb IF_FLAG_EQ, 0, 5
                    defb SAYS, S_CERRAJERO, 105
                    defb IF_FLAG_MIN, 3, 5
                         defb SET_FLAG, 3, 5
                    defb END_IF                    
                    defb ADD_FLAG, 3, -4
                    defb IF_FLAG_NOTMIN, 3, 24
                         defb SAYS, S_CERRAJERO, 107
                    defb END_IF
                    defb GOTO_SEQ, 30                    
               defb END_IF
               defb END_SCRIPT

       SC18:                                                 ; 18 Coger objetos
               defb IF_FLAG_EQ, 5, O_CRANEO_F
                    defb IF_FLAG_MIN, 0, 12
                         defb SAYS, S_FGON, 98
                         defb BREAK
                    defb END_IF
                    defb IF_FLAG_MIN, 10, 128
                         defb SAYS, S_BOTICARIO, 167
                         defb BREAK
                    defb END_IF
               defb END_IF
               defb IF_FLAG_EQ, 5, O_AGUA_F
                    defb SAYS, S_FGON, 182
                    defb BREAK
               defb END_IF
               defb IF_FLAG_EQ, 5, O_DIARIO
                    defb SAYS, S_FGON, 224
                    defb GET_OBJ, O_DIARIO
                    defb BREAK
               defb END_IF
               defb IF_FLAG_EQ, 5, O_COFRE
                    defb SAYS, S_FGON, 227
                    defb GET_OBJ, O_COFRE
                    defb BREAK
               defb END_IF

               defb IF_FLAG_EQ, 5, O_AGUA
                    defb IF_OBJ_CARR, O_JARRA
                         defb DESTROY_OBJ, O_JARRA
                    defb END_IF
               defb END_IF
               
               defb GET_OBJ, $ff
               defb END_SCRIPT

       SC19:                                                ; 19 Llegada a la casa de la meiga
               defb IF_FLAG_EQ, 6, 0
                    defb GOTO_SEQ, 11
               defb END_IF
               defb SET_COORD, 66, 77, ADJUST_OFFSET, 0, 1
               defb CAM_AUTO
               defb DO_FADEOUT

               defb IF_FLAG_EQ, 0, 5
                    defb DO_LOOP, 4
                         defb GO_UP, 1
                         defb UPDATE_SCR
                    defb END_LOOP

                    defb DO_LOOP, 4
                         defb CAM_UP
                         defb GO_UP, 1
                         defb UPDATE_SCR
                    defb END_LOOP

                    defb SELECT_SPR, S_MEIGA, GO_DOWN, 0
                    defb UPDATE_SCR

                    defb SAYS, S_MEIGA, 109
                    defb SET_FLAG, 0, 6

               defb END_IF

               defb END_SCRIPT

       SC20:                                                 ; 20 Conversaci�n con la meiga
               defb IF_FLAG_EQ, 0, 6
                    defb SAYS, S_FGON, 110
                    defb SAYS, S_MEIGA, 111
                    defb SET_FLAG, 0, 7
                    defb IF_OBJ_CARR, O_GALLINA                     ; Si llevamos la gallina
                         defb GOTO_SEQ, 21
                    defb END_IF
                    defb SAYS, S_FGON, 114
                    defb SAYS, S_MEIGA, 115
               defb END_IF

               defb IF_FLAG_EQ, 0, 7
                    defb IF_OBJ_CARR, O_GALLINA                     ; Si llevamos la gallina
                         defb GOTO_SEQ, 21
                    defb END_IF
                    defb SAYS, S_MEIGA, 116
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 12
                    defb IF_FLAG_EQ, 2, 4
                         defb SAYS, S_MEIGA, 197
                    defb END_IF

                    defb IF_FLAG_EQ, 2, 3
                         defb IF_OBJ_CARR, O_CRANEO_F
                              defb IF_OBJ_CARR, O_ESQ_1
                                   defb IF_OBJ_CARR, O_ESQ_2
                                        defb IF_OBJ_CARR, O_ESQ_3
                                             defb IF_OBJ_CARR, O_ESQ_4
                                                  defb SAYS, S_MEIGA, 196
                                                  defb ADD_FLAG, 2, 1
                                                  defb SET_FLAG, 0, 13
                                                  defb BREAK
                                             defb END_IF
                                        defb END_IF
                                   defb END_IF
                              defb END_IF
                         defb END_IF
                         defb SAYS, S_MEIGA, 175
                    defb END_IF
                    defb IF_FLAG_EQ, 2, 1
                         defb SAYS, S_MEIGA, 172
                         defb SET_FLAG, 2, 2
                    defb END_IF
                    defb IF_FLAG_EQ, 2, 2
                         defb IF_FLAG_EQ, 7, 7
                              defb SAYS, S_MEIGA, 174
                              defb GET_OBJ, O_FAROL
                              defb DESTROY_OBJ, 17
                              defb SET_FLAG, 7, 0
                              defb SET_FLAG, 2, 3
                              defb BREAK
                         defb END_IF
                         defb SAYS, S_MEIGA, 173
                    defb END_IF
                    defb IF_FLAG_EQ, 2, 0
                         defb SAYS, S_MEIGA, 169
                         defb SAYS, S_MEIGA, 170
                         defb SAYS, S_MEIGA, 171                         
                         defb SET_FLAG, 2, 1
                         defb GET_OBJ, O_JARRA
                    defb END_IF
               defb END_IF

               defb END_SCRIPT


       SC21:                                                  ; 21 Vamos con la meiga a la casa del boticario
               defb SAYS, S_FGON, 112
               defb SAYS, S_MEIGA, 113
               defb DESTROY_OBJ, O_GALLINA
               defb DO_FADEOUT
               defb SET_MAP, 1
               defb SELECT_SPR, S_FGON, SET_COORD, 4, 66, GO_LEFT, 1
               defb SELECT_SPR, S_MEIGA, SET_COORD, 3, 66, GO_LEFT, 1
               defb CAM_AUTO
               defb DO_FADEIN
               defb SAYS, S_BOTICARIO, 117
               defb SAYS, S_MEIGA, 118
               defb SAYS, S_CURA, 119
               defb SAYS, S_FGON, 120
               defb SAYS, S_MEIGA, 121
               defb SAYS, S_FGON, 122
               defb SAYS, S_MEIGA, 123
               defb SAYS, S_CURA, 124
               defb SAYS, S_BOTICARIO, 125
               defb SAYS, S_FGON, 126
               defb SAYS, S_MEIGA, 127
               defb DO_FADEOUT
               defb GOTO_SEQ, 56 ; Cap�tulo nuevo
               
     SC22:                                                  ; 22 Inicializaci�n de mapas y Creaci�n de Sprites
               defb MUTE_SOUND
               defb IF_FLAG_EQ, 4, 0                                                      ; Mapa del pueblo
                    defb PLAY_SONG, 11
               
                    defb NEW_SPRITE, S_FCES, 48, 56
                    defw FCES

                    defb NEW_SPRITE, S_CURA, 69, 13
                    defw CURA
                    defb SET_MOVEMENT
                    defw PRIEST_WALK

                    defb IF_FLAG_EQ, 6, 255
                         defb NEW_SPRITE, S_XAN, 83, 24
                         defw XAN
                         defb ADJUST_OFFSET, 1, 0

                         defb NEW_SPRITE, S_BORRACHO, 86, 30
                         defw BORRACHO
                         defb ADJUST_OFFSET, 0, 1

                         defb NEW_SPRITE, S_PAISANO, 29, 16
                         defw PAISANO
                         defb SET_MOVEMENT
                         defw PAISANO_MOVEMENT
                    defb END_IF

                    defb IF_FLAG_EQ, 0, 19
                         defb SET_STILE, 51, 2, 193
                         defb SET_STILE, 51, 3, 194
                         defb SET_STILE, 52, 2, 195
                         defb SET_STILE, 52, 3, 196

                         defb SET_STILE, 51, 5, 193
                         defb SET_STILE, 51, 6, 194
                         defb SET_STILE, 52, 5, 195
                         defb SET_STILE, 52, 6, 196

                         defb GOTO_SEQ, 30
                    defb END_IF

                    defb IF_FLAG_NOTEQ, 0, 0
                         defb IF_FLAG_MIN, 0, 12
                              defb SELECT_SPR, S_CURA
                              defb SET_COORD, 41, 38  ; Cura est� en casa del boticario
                              defb SET_MOVEMENT
                              defw NO_MOV
                         defb END_IF
                    defb END_IF

                    defb IF_FLAG_NOTEQ, 0, 0
                         defb SELECT_SPR, S_FCES
                         defb SP_SWITCH_TO, S_BRUNILDA
                         defb SET_GFXPTR
                         defw BRUNILDA
                    defb END_IF

                    defb IF_FLAG_NOTMIN, 0, 10
                         defb SET_STILE, 54, 16, 17
                         defb SET_STILE, 53, 16, 17
                         defb SET_STILE, 54, 17, 17
                         defb SET_STILE, 53, 17, 17
                         defb SELECT_SPR, S_BRUNILDA
                         defb IF_FLAG_MIN, 0, 12
                              defb SET_COORD, 51, 1, GO_RIGHT, 0
                         defb END_IF
                         defb IF_FLAG_NOTMIN, 0, 12
                              defb SET_STILE, 51, 2, 203
                              defb SET_STILE, 51, 3, 204
                              defb SET_STILE, 52, 2, 205
                              defb SET_STILE, 52, 3, 206
                         defb END_IF                         
                         defb IF_FLAG_NOTMIN, 0, 13
                              defb SET_COORD, 51, 4, GO_LEFT, 0
                         defb END_IF
                         defb IF_FLAG_NOTMIN, 0, 14
                              defb SET_STILE, 51, 2, 199
                              defb SET_STILE, 51, 3, 200
                              defb SET_STILE, 52, 2, 201
                              defb SET_STILE, 52, 3, 202
                              defb IF_FLAG_NOTMIN, 0, 16
                                   defb SET_STILE, 51, 1, 216
                              defb END_IF                              
                         defb END_IF
                    defb END_IF

                    defb GOTO_SEQ, 30
               defb END_IF

               defb IF_FLAG_EQ, 4, 1                                                      ; Mapa boticario
                    defb PLAY_SONG, 12

                    defb NEW_SPRITE, S_FCES, 90, 90
                    defw FCES

                    defb NEW_SPRITE, S_MEIGA, 90, 90
                    defw MEIGA

                    defb NEW_SPRITE, S_AMA, 37, 48
                    defw AMA
                    defb SET_MOVEMENT
                    defw AMA_MOVEMENT

                    defb NEW_SPRITE, S_BOTICARIO, 15, 6
                    defw BOT
                    defb GO_DOWN, 0
                    defb SET_MOVEMENT
                    defw NO_MOV

                    defb IF_FLAG_MIN, 0, 8
                         defb SET_COORD, 4, 61    ; Boticario
                         defb ADJUST_OFFSET, 1, 1, GO_RIGHT, 0

                         defb NEW_SPRITE, S_CURA, 03, 61
                         defw CURA
                         defb GO_RIGHT, 1
                    defb END_IF

                     defb IF_FLAG_MIN, 0, 12
                          defb IF_FLAG_NOTMIN, 0, 8
                               defb SET_COORD, 90, 90
                               defb SELECT_SPR, S_AMA, SET_COORD, 90, 90
                          defb END_IF
                     defb END_IF

                     defb IF_FLAG_BIT, 10, 7
                          defb SELECT_SPR, S_BOTICARIO
                          defb SET_COORD, 32, 14
                          defb SET_MOVEMENT
                          defw BOTICARIO_MOV
                     defb END_IF

                     defb IF_FLAG_EQ, 0, 19
                         defb SELECT_SPR, S_BOTICARIO, SET_COORD, 90, 90
                         defb SET_MOVEMENT
                         defw NO_MOV
                     defb END_IF

                     defb GOTO_SEQ, 51            ; Pasadizo
               defb END_IF

               defb IF_FLAG_EQ, 4, 2                                                      ; Mazmorras
                    defb PLAY_SONG, 1
                    defb NEW_SPRITE, S_ENEMIGO, $f0, $f0
                    defw ENEMIES
                    defb NEW_SPRITE, S_ENEMIGO, $f0, $f0
                    defw ENEMIES
                    defb NEW_SPRITE, S_ENEMIGO, $f0, $f0
                    defw ENEMIES
                    defb NEW_SPRITE, S_ENEMIGO, $f0, $f0
                    defw ENEMIES
               defb END_IF

               defb IF_FLAG_EQ, 4, 3                                                      ; Mapa bosque meiga
                    defb PLAY_SONG, 7
                    defb IF_FLAG_MIN, 0, 14
                         defb NEW_SPRITE, S_MEIGA, 62, 74
                         defw MEIGA
                         defb SET_MOVEMENT
                         defw MEIGA_MOVEMENT
                    defb END_IF
                    
                    defb GOTO_SEQ, 37                ; Inicializaci�n ST's entrada cuevas

               defb END_IF
               
               defb IF_FLAG_EQ, 4, 4                                                      ; Monte das pozas
                    defb PLAY_SONG, 8
                    defb NEW_SPRITE, S_PIEDRA, $f0, $f0
                    defw PIEDRA
                    defb NEW_SPRITE, S_PIEDRA, $f0, $f0
                    defw PIEDRA
                    defb NEW_SPRITE, S_PIEDRA, $f0, $f0
                    defw PIEDRA
                    ;defb NEW_SPRITE, S_PIEDRA, $f0, $f0
                    ;defw PIEDRA
               defb END_IF

               defb END_SCRIPT


     SC23:                                                  ; 23 Entrada a posada:  No deja entrar cuando es de noche
               defb IF_FLAG_EQ, 6, 0
                    defb GOTO_SEQ, 11
               defb END_IF
               defb END_SCRIPT

     SC24:                                                  ; 24 Si Flag 0 = 8, puerta cerrada
               defb IF_FLAG_NOTMIN, 0, 8                        ; Si Flag < 3 FCes no nos deja
                    defb IF_FLAG_MIN, 0, 12
                         defb GOTO_SEQ, 11                       ; Cerrajer�a, casa pueblo y dormitorios boticario
                    defb END_IF
               defb END_IF
               defb IF_FLAG_MIN, 0, 3
                    defb GOTO_SEQ, 11
               defb END_IF
               defb END_SCRIPT

     SC25:                                                  ; 25 Entrada Iglesia
               defb IF_FLAG_EQ, 0, 0
                    defb GOTO_SEQ, 2
               defb END_IF
               defb IF_FLAG_EQ, 0, 2
                    defb GOTO_SEQ, 1
               defb END_IF
               defb IF_FLAG_EQ, 0, 19
                    defb GOTO_SEQ, 54
               defb END_IF
               
               defb IF_FLAG_EQ, 0, 1
                    defb SET_COORD, 87, 7, ADJUST_OFFSET, 0, 1, GO_RIGHT, 0
                    defb SELECT_SPR, S_FCES
                    defb SET_COORD, 87, 9, ADJUST_OFFSET, 0, 1, GO_LEFT, 0
                    defb FOLLOW_LIST, 0
                    defb DO_FADEOUT
                    defb CAM_AUTO
                    defb DO_FADEIN
                    defb PLAY_SONG, 4
                    defb SAYS, S_FGON, 234
                    defb BREAK
               defb END_IF
               defb IF_FLAG_EQ, 0, 8
                    defb SAYS, S_FGON, 71
                    defb SELECT_SPR, S_BRUNILDA, SET_COORD, 2, 17, GO_RIGHT, 0
                    defb DO_FADEIN
                    defb PLAY_SFX, 2
                    defb SAYS, S_BRUNILDA, 142
                    defb PLAY_SFX, 5
                    defb SET_COORD, $ff, $ff
                    defb DO_FADEIN
                    defb SET_FLAG, 6, 255
                    defb PLAY_SONG, 4
                    defb SELECT_SPR, S_FGON, SET_COORD, 87, 8, ADJUST_OFFSET, 0, 1, GO_UP, 0
                    defb CAM_AUTO
                    defb DO_FADEOUT
                    defb SET_FLAG, 0, 9
                    defb BREAK
               defb END_IF
               defb IF_FLAG_NOTMIN, 0, 7
                    defb IF_FLAG_MIN, 0, 12
                         defb GOTO_SEQ, 27
                    defb END_IF
               defb END_IF
               defb END_SCRIPT

     SC26:                                                  ; 26 Salida noche a luz apagada
               defb IF_FLAG_MIN, 0, 2
                    defb GOTO_SEQ, 1
               defb END_IF
               defb IF_FLAG_NOTMIN, 0, 8
                    defb IF_FLAG_MIN, 0, 12
                         defb SET_FLAG, 6, 0
                    defb END_IF
               defb END_IF
               defb GOTO_SEQ, 30
;               defb END_SCRIPT

     SC27:                                                  ; 27 Entrada noche a luz encencida
               defb IF_FLAG_MIN, 0, 2
                    defb GOTO_SEQ, 1
               defb END_IF
               defb SET_FLAG, 6, 255
               defb END_SCRIPT

     SC28:                                                    ; 28 Entrada al cementerio
               defb SAYS, S_FGON, 71
               defb IF_FLAG_NOTEQ, 0, 9
                    defb BREAK
               defb END_IF

               defb SELECT_SPR, S_BRUNILDA, SET_COORD, 55, 15, GO_RIGHT, 0
               defb DO_FADEIN
               defb PLAY_SFX, 2
               defb SAYS, S_BRUNILDA, 142
               defb SET_STILE, 54, 16, 17
               defb SET_STILE, 53, 16, 17
               defb SET_STILE, 54, 17, 17
               defb SET_STILE, 53, 17, 17
               defb SET_FLAG, 0, 10
               defb PLAY_SFX, 5
               
               defb UPDATE_SCR

               defb PAUSE, 25

               defb SET_COORD, 51, 1
               defb DO_FADEIN
               defb END_SCRIPT

     SC29:                                                             ; 29 BRUNILDA
               defb IF_FLAG_EQ, 0, 13
                    defb GOTO_SEQ, 44
               defb END_IF
               
               defb IF_OBJ_CARR, O_CARTAS
                    defb SAYS, S_ANTON, 213
                    defb BREAK
               defb END_IF

               defb IF_OBJ_CARR, O_COFRE
                    defb SAYS_2, S_BRUNILDA, 11
                    defb BREAK
               defb END_IF

               defb IF_FLAG_NOTMIN, 0, 16
                    defb SAYS, S_BRUNILDA, 212
                    defb SAYS, S_BRUNILDA, 214
                    defb BREAK
               defb END_IF                    

               defb IF_FLAG_EQ, 0, 10
                    defb SAYS, S_BRUNILDA, 143
                    defb SAYS, S_FGON, 144
                    defb SET_FLAG, 0, 11
               defb END_IF

               defb SAYS, S_BRUNILDA, 145

               defb IF_OBJ_CARR, O_PALA
                    defb DO_FADEOUT
                    defb MESSAGE, 4, 5, 96
                    defb PAUSE, $ff
                    defb DO_FADEOUT

                    defb SET_STILE, 51, 2, 203
                    defb SET_STILE, 51, 3, 204
                    defb SET_STILE, 52, 2, 201
                    defb SET_STILE, 52, 3, 202

                    defb SELECT_SPR, S_FGON, SET_COORD, 51, 4, GO_LEFT, 0
                    defb SELECT_SPR, S_BRUNILDA , SET_COORD, 52, 4, GO_LEFT, 0

                    defb UPDATE_SCR

                    defb SAYS, S_BRUNILDA, 148
                    defb SAYS, S_FGON, 149
                    defb SAYS, S_BRUNILDA, 150
                    defb SAYS, S_FGON, 151
                    defb SAYS, S_BRUNILDA, 152

                    defb PLAY_SONG, 6
                    defb SAYS, S_MISTERIOSO, 154

                    defb SELECT_SPR, S_BRUNILDA , GO_RIGHT, 0, UPDATE_SCR, PAUSE, 25
                    defb GO_LEFT, 0, UPDATE_SCR, PAUSE, 25

                    defb SAYS, S_BRUNILDA, 155
                    defb SAYS, S_MISTERIOSO, 156
                    defb SAYS, S_BRUNILDA, 157
                    defb SET_STILE, 52, 2, 205
                    defb SET_STILE, 52, 3, 206
                    defb SET_FLAG, 6, 255
                    defb DO_FADEIN
                    defb SET_FLAG, 6, 0
                    defb UPDATE_SCR


                    defb SAYS, S_FGON, 158
                    defb SAYS, S_BRUNILDA, 157
                    defb SAYS, S_MISTERIOSO, 160

                    defb MUTE_SOUND
                    defb SELECT_SPR, S_BRUNILDA, SET_COORD, 41, 38
                    defb PLAY_SFX, 2
                    defb SET_FLAG, 6, 255, DO_FADEIN, SET_FLAG, 6, 0, UPDATE_SCR

                    defb SAYS, S_FGON, 130
                    defb DO_FADEOUT
                    defb MESSAGE, 4, 5, 97
                    defb DESTROY_OBJ, O_PALA
                    defb PAUSE, $ff
                    defb GOTO_SEQ, 57
               defb END_IF

               defb SAYS, S_FGON, 146
               defb SAYS, S_BRUNILDA, 147
               defb END_SCRIPT

     SC30:                                   ; 30 Ajusta el mapa 0 en funci�n de la Fe
               defb IF_FLAG_EQ, 4, 0
                    defb SET_STILE, 24, 60, 111
                    defb IF_FLAG_MIN, 3, 24
                         defb SET_STILE, 24, 60, 2
                    defb END_IF
               defb END_IF
               defb END_SCRIPT

     SC31:                                        ; 31 : Entrar en una cueva
               defb IF_OBJ_NOTCARR, O_FAROL
                    defb SAYS, S_FGON, 179
                    defb BREAK
               defb END_IF
               defb SET_FLAG, 6, 1
               defb END_SCRIPT

     SC32:                                        ; 32: No nos deja subir al monte si no es "el momento"
               defb IF_FLAG_EQ, 2, 0
                    defb SAYS, S_FGON, 181
                    defb BREAK
               defb END_IF
               defb END_SCRIPT

     SC33:                                        ; 33: Monolito 1
               defb IF_FLAG_EQ, 2, 0
                    defb SAYS, S_FGON, 186        ; Todav�a no sabemos para qu� sirven los monolitos
                    defb BREAK
               defb END_IF
               defb IF_FLAG_MIN, 9, 7
                    defb SAYS, S_FGON, 183        ; No tenemos agua suficiente
                    defb BREAK                          
               defb END_IF
               defb IF_FLAG_BIT, 1, 2             ; Ya est� abierta
                    defb SAYS, S_FGON, 185
                    defb BREAK
               defb END_IF
               
               defb SAYS, S_FGON, 184
               defb SET_STILE, 6, 15, 76
               defb PLAY_SFX, 7
               defb DO_FADEIN


               defb GO_LEFT, 0, UPDATE_SCR
               defb ADD_FLAG, 1, 4
               defb GOTO_SEQ, 38

     SC34:                                        ; 34: Monolito 2
               defb IF_FLAG_EQ, 2, 0
                    defb SAYS, S_FGON, 186        ; Todav�a no sabemos para qu� sirven los monolitos
                    defb BREAK
               defb END_IF
               defb IF_FLAG_MIN, 9, 7
                    defb SAYS, S_FGON, 183        ; No tenemos agua suficiente
                    defb BREAK
               defb END_IF
               defb IF_FLAG_BIT, 1, 3             ; Ya est� abierta
                    defb SAYS, S_FGON, 185
                    defb BREAK
               defb END_IF

               defb SAYS, S_FGON, 184
               defb SET_STILE, 5, 23, 77
               defb PLAY_SFX, 7
               defb DO_FADEIN

               defb GO_RIGHT, 0, UPDATE_SCR
               defb ADD_FLAG, 1, 8
               defb GOTO_SEQ, 38

     SC35:                                        ; 35: Monolito 3
               defb IF_FLAG_EQ, 2, 0
                    defb SAYS, S_FGON, 186        ; Todav�a no sabemos para qu� sirven los monolitos
                    defb BREAK
               defb END_IF
               defb IF_FLAG_MIN, 9, 7
                    defb SAYS, S_FGON, 183        ; No tenemos agua suficiente
                    defb BREAK
               defb END_IF
               defb IF_FLAG_BIT, 1, 4             ; Ya est� abierta
                    defb SAYS, S_FGON, 185
                    defb BREAK
               defb END_IF

               defb SAYS, S_FGON, 184
               defb SET_STILE, 17, 36, 78
               defb PLAY_SFX, 7
               defb DO_FADEIN

               defb GO_LEFT, 0, UPDATE_SCR
               defb ADD_FLAG, 1, 16
               defb GOTO_SEQ, 38


     SC36:                                        ; 36: Monolito 4
               defb IF_FLAG_EQ, 2, 0
                    defb SAYS, S_FGON, 186        ; Todav�a no sabemos para qu� sirven los monolitos
                    defb BREAK
               defb END_IF
               defb IF_FLAG_MIN, 9, 7
                    defb SAYS, S_FGON, 183        ; No tenemos agua suficiente
                    defb BREAK
               defb END_IF
               defb IF_FLAG_BIT, 1, 5             ; Ya est� abierta
                    defb SAYS, S_FGON, 185
                    defb BREAK
               defb END_IF

               defb SAYS, S_FGON, 184
               defb SET_STILE, 28, 14, 79
               defb PLAY_SFX, 7
               defb DO_FADEIN

               defb GO_LEFT, 0, UPDATE_SCR
               defb ADD_FLAG, 1, 32
               defb GOTO_SEQ, 38


     SC37:                                          ; 37: Inicializaci�n de puertas de las cuevas
               defb IF_FLAG_BIT, 1, 2
                    defb SET_STILE, 6, 12, 36
                    defb SET_STILE, 6, 15, 76
               defb END_IF

               defb IF_FLAG_BIT, 1, 3
                    defb SET_STILE, 5, 25, 36
                    defb SET_STILE, 5, 23, 77
               defb END_IF

               defb IF_FLAG_BIT, 1, 4
                    defb SET_STILE, 17, 33, 36
                    defb SET_STILE, 17, 36, 78
               defb END_IF

               defb IF_FLAG_BIT, 1, 5
                    defb SET_STILE, 28, 11, 36
                    defb SET_STILE, 28, 14, 79
               defb END_IF

               defb END_SCRIPT

     SC38:                                                                 ; 38 Comprobar si hay que destruir el agua
               defb IF_OBJ_CARR, O_AGUA
                    defb IF_FLAG_BIT, 1, 2
                         defb IF_FLAG_BIT, 1,  3
                              defb IF_FLAG_BIT, 1,  4
                                   defb IF_FLAG_BIT, 1,  5
                                        defb DESTROY_OBJ, O_AGUA
                                   defb END_IF
                              defb END_IF
                         defb END_IF
                    defb END_IF
               defb END_IF
               defb GOTO_SEQ, 37

     SC39:                                                                      ; 39 Vuelta de las luchas
               defb GO_DOWN, 0
               defb SET_MAP, 3
               defb IF_FLAG_MIN, 12, 128 ; Si has perdido la lucha
                    defb SET_FLAG, 3, 2
               defb END_IF
;               defb CAM_AUTO
               defb DO_FADEIN
               defb END_SCRIPT


     SC40:                                                                      ; 40 Lucha en la primera caverna
               defb IF_FLAG_BIT, 12, 3
                    defb SAYS, S_FGON, 195
                    defb BREAK
               defb END_IF
               defb DO_FADEOUT
               defb SET_COORD, 8, 6
               defb ADJUST_OFFSET, 0, 1
               defb CAM_AUTO
               defb SET_FLAG, 6, $ff
               defb DO_LOOP, 5
                    defb GO_UP, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 2
                    defb GO_UP, 1
                    defb CAM_UP, UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 2
                    defb GO_UP, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb PAUSE, 25
               defb SET_COORD, 7, 12
               defb RUN_FIGHT, 0
               defb IF_FLAG_BIT, 12, 7
                    defb ADD_FLAG, 1, 128
                    defb GET_OBJ, O_ESQ_1
                    defb ADD_FLAG, 12, 8
               defb END_IF
               defb GOTO_SEQ, 39

     SC41:                                                                      ; 41 Lucha en la segunda caverna
               defb IF_FLAG_BIT, 12, 4
                    defb SAYS, S_FGON, 195
                    defb BREAK
               defb END_IF
               defb DO_FADEOUT
               defb SET_COORD, 16, 57
               defb ADJUST_OFFSET, 1, 0
               defb CAM_AUTO
               defb SET_FLAG, 6, $ff
               defb DO_LOOP, 16
                    defb GO_LEFT, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb PAUSE, 25
               defb SET_COORD, 6, 25
               defb RUN_FIGHT, 1
               defb IF_FLAG_BIT, 12, 7
                    defb ADD_FLAG, 1, 128
                    defb GET_OBJ, O_ESQ_2
                    defb ADD_FLAG, 12, 16
               defb END_IF
               defb GOTO_SEQ, 39

     SC42:                                                                       ; 42 Tercera Lucha
               defb IF_FLAG_BIT, 12, 5
                    defb SAYS, S_FGON, 195
                    defb BREAK
               defb END_IF
               defb DO_FADEOUT
               defb SET_COORD, 53, 6
               defb ADJUST_OFFSET, 0, 1
               defb CAM_AUTO
               defb SET_FLAG, 6, $ff
               defb DO_LOOP, 5
                    defb GO_UP, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 2
                    defb GO_UP, 1
                    defb CAM_UP, UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb DO_LOOP, 4
                    defb GO_UP, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb PAUSE, 25
               defb SET_COORD, 18, 33
               defb RUN_FIGHT, 2
               defb IF_FLAG_BIT, 12, 7
                    defb ADD_FLAG, 1, 128
                    defb GET_OBJ, O_ESQ_3
                    defb ADD_FLAG, 12, 32
               defb END_IF
               defb GOTO_SEQ, 39

     SC43:                                                                            ; 43 4� lucha
               defb IF_FLAG_BIT, 12, 6
                    defb SAYS, S_FGON, 195
                    defb BREAK
               defb END_IF
               defb DO_FADEOUT
               defb SET_COORD, 50, 57
               defb ADJUST_OFFSET, 1, 0
               defb CAM_AUTO
               defb SET_FLAG, 6, $ff
               defb DO_LOOP, 16
                    defb GO_LEFT, 1
                    defb UPDATE_SCR, UPDATE_SCR
               defb END_LOOP
               defb PAUSE, 25
               defb SET_COORD, 29, 11
               defb RUN_FIGHT, 3
               defb IF_FLAG_BIT, 12, 7
                    defb ADD_FLAG, 1, 128
                    defb GET_OBJ, O_ESQ_4
                    defb ADD_FLAG, 12, 64
               defb END_IF
               defb GOTO_SEQ, 39

     SC44:                                                  ; 44 Tumba de Brunilda con todas las piezas
          defb SAYS, S_BRUNILDA, 198
          defb SET_STILE, 51, 2, 199
          defb SET_STILE, 51, 3, 200
          defb SET_STILE, 52, 2, 201
          defb SET_STILE, 52, 3, 202
          defb SET_FLAG, 0, 14
          defb DESTROY_OBJ, O_CRANEO_F
          defb DESTROY_OBJ, O_ESQ_1
          defb DESTROY_OBJ, O_ESQ_2
          defb DESTROY_OBJ, O_ESQ_3
          defb DESTROY_OBJ, O_ESQ_4
          defb SELECT_SPR, S_FGON, SET_COORD, 52, 4, GO_LEFT, 0
          defb DO_FADEOUT, DO_FADEIN
          defb SAYS, S_BRUNILDA, 199
          defb SAYS, S_FGON, 200
          defb SAYS, S_BRUNILDA, 201


          defb SELECT_SPR, S_XAN
          defb SP_SWITCH_TO, S_EXPLOSION, SET_GFXPTR
          defw EXPLODE;MEIGA
          defb SET_COORD, 52, 1
          defb DO_LOOP, 8
               defb UPDATE_SCR
          defb END_LOOP
          defb SP_SWITCH_TO, S_MEIGA, SET_GFXPTR
          defw MEIGA
          defb GO_RIGHT, 0, DO_FADEIN
          
          defb PLAY_SONG, 6

          defb SAYS, S_MEIGA, 202
          defb SAYS, S_BRUNILDA, 203
          defb SAYS, S_MEIGA, 204
          defb SAYS, S_FGON, 205
          defb SAYS, S_MEIGA, 206

          ;defb GOTO_SEQ, 45

     SC45:                                        ; 45: 1� Lucha con la meiga

          defb SET_FLAG, 0, 15
          defb RUN_FIGHT, 4
          defb SET_MAP, 0

          defb IF_FLAG_BIT, 12, 7
               defb PLAY_SONG, 13
               defb CREATE_OBJ, O_CRANEO_V, 0, 52, 8
               defb PAUSE, 25
               defb SELECT_SPR, S_FGON
               defb DO_LOOP, 7
                    defb GO_RIGHT, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb GET_OBJ, 5
               defb PAUSE, 25
               defb DO_LOOP, 7
                    defb GO_LEFT, 1
                    defb UPDATE_SCR
               defb END_LOOP
               defb DESTROY_OBJ, O_CRANEO_V
               defb SAYS, S_BRUNILDA, 207
               defb SAYS, S_FGON, 208
               defb SET_STILE, 51, 1, 216
               defb SAYS, S_ANTON, 209
               defb SAYS, S_BRUNILDA, 210
               defb SAYS, S_ANTON, 211
               defb SAYS, S_BRUNILDA, 212
               defb SAYS, S_ANTON, 213
               defb SAYS, S_BRUNILDA, 214
               defb MUTE_SOUND
               defb GOTO_SEQ, 58
          defb END_IF

          defb SET_FLAG, 3, 2
          defb SELECT_SPR, S_FGON
          defb SET_COORD, 3, 18
          defb ADJUST_OFFSET, 0, 1
          defb GO_DOWN, 0
          defb CAM_AUTO
          defb PLAY_SONG, 10
          

          defb END_SCRIPT

     SC46:                                                                 ; 46 Salida al cementerio
          defb IF_FLAG_NOTEQ, 0, 15
               defb GOTO_SEQ, 26
          defb END_IF
          defb SET_COORD, 52, 4, GO_LEFT, 0
          defb DO_FADEOUT

          defb SELECT_SPR, S_XAN
          defb SET_COORD, 52, 1
          defb SP_SWITCH_TO, S_MEIGA, SET_GFXPTR
          defw MEIGA
          defb GO_RIGHT, 0

          defb CAM_AUTO
          defb DO_FADEIN
          defb SAYS, S_MEIGA, 215
          defb GOTO_SEQ, 45

     SC47:                                                            ; 47 No nos deja salir vestido de criada
          defb IF_OBJ_CARR, O_VESTIDO
               defb SAYS, S_FGON, 221
               defb BREAK
          defb END_IF
          defb END_SCRIPT

     SC48:                                                            ; 48 Puerta ala oeste
          defb IF_OBJ_CARR, O_VESTIDO
               defb GOTO_SEQ, 47
          defb END_IF
                    
          defb IF_FLAG_BIT, 1, 0
               defb DO_FADEOUT
               defb SET_COORD, 25, 49
               defb ADJUST_OFFSET, 1, 0
               defb CAM_AUTO
               defb BREAK
          defb END_IF

          defb SAYS, S_FGON, 71
          defb IF_OBJ_CARR, O_LLAVE
               defb SAYS, S_FGON, 184
               defb PLAY_SFX, 5
               defb ADD_FLAG, 1, 1
               defb GOTO_SEQ, 48
          defb END_IF
          defb END_SCRIPT

     SC49:                                                                                ; 49 Examinar librer�a
          defb IF_OBJ_CARR, O_DIARIO
               defb IF_FLAG_BIT, 1, 6
                    defb SAYS, S_FGON, 225
                    defb SAYS_2, S_FGON, 10
                    defb BREAK
               defb END_IF
               defb ADD_FLAG, 1, 64
               defb SAYS, S_FGON, 226
               defb PLAY_SFX, 7
               defb GOTO_SEQ, 51
          defb END_IF
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 10
          defb END_SCRIPT
          
     SC50:                                                                           ; 50 Librer�a "normal" 1
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 3
          defb END_SCRIPT

     SC51:                                                                           ; 51 Pasadizo abierto
          defb IF_FLAG_BIT, 1, 6
               defb SET_STILE, 58, 17, 40
               defb SET_STILE, 58, 18, 124
               defb SET_STILE, 58, 19, 125

               defb SET_STILE, 59, 17, 2
               defb SET_STILE, 59, 18, 126
               defb SET_STILE, 59, 19, 127
          defb END_IF
          defb END_SCRIPT

     SC52:                                                                           ; 52 Entrada pasadizo
          defb SET_FLAG, 6, 1
          defb SET_COORD, 88, 4
          defb SELECT_SPR, S_BOTICARIO, SP_SWITCH_TO, S_ENEMIGO, SET_COORD, $f0, $f0
          defb SELECT_SPR, S_AMA, SP_SWITCH_TO, S_ENEMIGO, SET_COORD, $f0, $f0
          defb SELECT_SPR, S_MEIGA, SP_SWITCH_TO, S_ENEMIGO, SET_COORD, $f0, $f0
          defb SELECT_SPR, S_FCES, SP_SWITCH_TO, S_ENEMIGO, SET_COORD, $f0, $f0
          defb DO_FADEOUT
          defb CAM_AUTO
          defb END_SCRIPT

     SC53:                                                                          ; 53 Salida pasadizo
          defb SET_FLAG, 6, $ff
          defb SET_COORD, 59, 17
          defb DO_FADEOUT
          defb SET_MAP, 1
          defb END_SCRIPT

     SC54:                                                            ; 54 Escena final
          defb SET_COORD, 52, 15
          defb DO_FADEOUT
          defb PLAY_SONG, 10
          defb SET_CAM, 47, 0
          defb DO_FADEIN
     
          defb SELECT_SPR, S_BORRACHO, SP_SWITCH_TO, S_BOTICARIO
          defb SET_GFXPTR
          defw BOT
          defb SET_COORD, 52, 7, GO_LEFT, 0

          defb SELECT_SPR, S_CURA
          defb SET_COORD, 51, 7, GO_LEFT, 0

          defb DO_LOOP, 12
               defb SELECT_SPR, S_FGON
               defb GO_LEFT, 1
               defb UPDATE_SCR, UPDATE_SCR
          defb END_LOOP

          defb SELECT_SPR, S_CURA, GO_RIGHT, 0
          defb SELECT_SPR, S_BOTICARIO, GO_RIGHT, 0
          defb UPDATE_SCR

          defb SAYS, S_BOTICARIO, 235
          defb SAYS, S_CURA, 236

          defb SELECT_SPR, S_CURA, GO_LEFT, 0
          defb SELECT_SPR, S_BOTICARIO, GO_LEFT, 0

          defb PLAY_SONG, 6
          defb SELECT_SPR, S_XAN
          defb SP_SWITCH_TO, S_EXPLOSION, SET_GFXPTR
               defw EXPLODE;MEIGA
          defb SET_COORD, 52, 4
          defb DO_LOOP, 4
               defb UPDATE_SCR
          defb END_LOOP
          defb SP_SWITCH_TO, S_MEIGA, SET_GFXPTR
               defw MEIGA
          defb SET_COORD, 52, 4
          
          defb GO_RIGHT, 0, DO_FADEIN

          defb SAYS, S_MEIGA, 237
          defb SAYS, S_CURA, 238

          defb RUN_FIGHT, 4

          defb IF_FLAG_MIN, 12, 128                              ; Game over malo
               defb DO_FADEOUT
               defb PLAY_SONG, 13
               defb MESSAGE, 4, 0, 240
               defb FIRE_EFFECT
               defb DO_FADECLOSE
               defb RESTART_GAME
          defb END_IF

          defb SET_MAP, 0
          defb PLAY_SONG, 10

          defb SELECT_SPR, S_XAN
          defb SP_SWITCH_TO, S_BOTICARIO
          defb SET_GFXPTR
          defw BOT
          defb SET_COORD, 52, 7, GO_RIGHT, 0

          defb SELECT_SPR, S_CURA
          defb SET_COORD, 51, 7, GO_RIGHT, 0

          defb SAYS, S_CURA, 241
          defb SAYS, S_FGON, 242
          defb SAYS, S_BOTICARIO, 243

          defb DO_FADEOUT
          defb SET_MAP, 1
          defb SELECT_SPR, S_FGON, SET_COORD, 4, 60, GO_RIGHT, 0
          defb SELECT_SPR, S_BOTICARIO, SET_COORD, 5, 60, GO_RIGHT, 0
          defb NEW_SPRITE, S_CURA, 6, 60
          defw CURA
          defb GO_RIGHT, 0
          defb SELECT_SPR, S_FCES, SET_COORD, 4, 62, GO_LEFT, 0
          defb CAM_AUTO

          defb SET_STILE, 3, 63, 98
          defb SET_STILE, 3, 64, 99
          
          defb DO_FADEIN

          defb SAYS, S_FGON, 244
          defb SAYS, S_FCES, 245
          defb SAYS, S_BOTICARIO, 246

          defb DO_FADEOUT
          defb MESSAGE, 4, 5, 96
          defb PAUSE, $ff
          defb DO_FADEOUT

          defb SELECT_SPR, S_FGON, SET_COORD, 26, 59
          defb SELECT_SPR, S_FCES, SET_COORD, 26, 61
          defb CAM_AUTO
          defb DO_FADEIN

          defb SAYS, S_FCES, 247
          defb SAYS, S_FGON, 248

          defb DO_LOOP, 2
               defb SELECT_SPR, S_FGON, GO_LEFT, 1
               defb SELECT_SPR, S_FCES, GO_RIGHT, 1
               defb UPDATE_SCR, UPDATE_SCR
          defb END_LOOP

          defb SET_FLAG, 0, 20
          defb SET_STILE, 26, 56, 223
          defb SET_STILE, 26, 57, 224

          defb DO_LOOP, 4
               defb GO_RIGHT, 1
               defb UPDATE_SCR, UPDATE_SCR
          defb END_LOOP

          defb SET_STILE, 26, 65, 223
          defb SET_STILE, 26, 66, 224
          defb SET_COORD, 90, 90
          defb UPDATE_SCR
          defb PAUSE, 25
          defb DO_FADEOUT

          defb PAUSE, 50

          defb SELECT_SPR, S_BOTICARIO, SP_SWITCH_TO, S_BRUNILDA
          defb SET_COORD, 26, 58
          defb SET_GFXPTR
          defw BRUNILDA
          defb GO_LEFT, 0

          defb SELECT_SPR, S_FGON, SET_COORD, 26,57
          defb SET_FLAG, 6, 1
          defb UPDATE_SCR
          defb SAYS, S_BRUNILDA, 30
          defb DO_FADEOUT
          defb SET_COORD, 90, 90

          defb SET_FLAG, 6, 255

          defb SET_CAM, 23, 54

          defb MESSAGE, 4, 5, 97
          defb PAUSE, $ff
          defb DO_FADEOUT
          
          defb DO_FADEIN
          defb PAUSE, 50

          defb SET_STILE, 26, 65, 98
          defb SET_STILE, 26, 66, 99
          defb SELECT_SPR, S_FCES, SET_COORD, 26, 64
          defb UPDATE_SCR
          defb PAUSE, 25


          defb SELECT_SPR, S_FCES
          defb DO_LOOP, 10
               defb GO_LEFT, 1
               defb UPDATE_SCR, UPDATE_SCR
          defb END_LOOP

          defb SAYS, S_FCES, 249
          defb SET_STILE, 26, 56, 98
          defb SET_STILE, 26, 57, 99
          defb SET_FLAG, 0, 19
          defb SELECT_SPR, S_FGON, SET_COORD, 26, 58, GO_RIGHT, 0
          defb UPDATE_SCR

          defb SAYS, S_FGON, 250
          defb SAYS, S_FCES, 120
          defb SAYS, S_FGON, 252
          defb SAYS, S_FCES, 253
          defb SAYS, S_FGON, 254
          defb SAYS, S_FCES, 255

          defb DO_FADEOUT
          defb PLAY_SONG, 13
          defb MESSAGE, 4, 0, 251
          defb FIRE_EFFECT
          defb DO_FADECLOSE
          defb RESTART_GAME                                                  ; Game over bueno

     SC55:                                                            ; 55 Nos quedamos sin fe
          defb IF_OBJ_CARR, O_CRUCIFIJO
               defb GOTO_SEQ, 63
          defb END_IF
          defb DO_FADEOUT
          defb PLAY_SONG, 13          
          defb MESSAGE, 4, 0, 8
          defb FIRE_EFFECT
          defb DO_FADECLOSE
          defb RESTART_GAME

     SC56:                                                          ; 56 Inicio cap�tulo 3: Primera aparici�n de Brunilda
               defb SET_FLAG, 4, 1
               defb CHAPTER_SIGN, 3
               defb MESSAGE, 4, 5, 96
               defb PAUSE, $ff
               defb DO_FADEOUT

               defb SELECT_SPR, S_FGON, SET_COORD, 26, 58, GO_DOWN, 0
               defb CAM_AUTO
               defb DO_FADEIN
               defb SAYS, S_FGON, 128
               defb DO_FADEOUT
               defb MESSAGE, 4, 5, 96
               defb PAUSE, $ff
               defb DO_FADEOUT
               defb SET_STILE, 26, 56, 223
               defb SET_STILE, 26, 57, 224
               defb SET_COORD, 26, 57
               defb SET_FLAG, 6, 0
               defb DO_FADEIN

               defb PAUSE, 20
               defb SET_COORD, 26, 58
               defb SET_STILE, 26, 56, 98
               defb SET_STILE, 26, 57, 99
               defb UPDATE_SCR
               defb SAYS, S_FGON, 129

               defb SELECT_SPR, S_MEIGA, SP_SWITCH_TO, S_BRUNILDA
               defb SET_GFXPTR
               defw BRUNILDA
               defb SET_COORD, 26, 60, GO_LEFT, 0
               defb MUTE_SOUND, PLAY_SFX, 2
               defb SET_FLAG, 6, 0, DO_FADEOUT, DO_FADEIN, SET_FLAG, 6, 255
               defb DO_FADEIN
               defb PAUSE, 5
               defb SELECT_SPR, S_FGON, GO_RIGHT, 0
               defb UPDATE_SCR
               defb PLAY_SONG, 10
               defb SAYS, S_FGON, 130
               defb SAYS, S_BRUNILDA, 131
               defb SAYS, S_FGON, 132
               defb SAYS, S_BRUNILDA, 133
               defb SAYS, S_BRUNILDA, 134
               defb SAYS, S_BRUNILDA, 135
               defb SAYS, S_FGON, 136
               defb SAYS, S_BRUNILDA, 137
               defb SAYS, S_FGON, 138
               defb SAYS, S_BRUNILDA, 139
               defb SAYS, S_FGON, 140
               defb SAYS, S_BRUNILDA, 141
               defb SELECT_SPR, S_BRUNILDA, SET_COORD, $ff, $ff
               defb DO_FADEIN
               defb MUTE_SOUND, PLAY_SFX, 2, PLAY_SONG, 12
               defb SAYS, S_FGON, 130
               defb IF_FLAG_MIN, 3, 9
                    defb SET_FLAG, 3, 9
               defb END_IF
               defb ADD_FLAG, 3, -8
               defb SET_FLAG, 0, 8
               defb SELECT_SPR, S_CURA, SET_COORD, $ff, $ff
               defb SELECT_SPR, S_BOTICARIO, SET_COORD, $ff, $ff
               defb SELECT_SPR, S_AMA, SET_COORD, $ff, $ff
               defb END_SCRIPT
     
     SC57:                                                  ; 57 Inicio cap�tulo 4: Cura nos encuentra en el cementerio
                    defb DO_FADEOUT
                    defb SET_FLAG, 0, 12
                    defb SET_FLAG, 6, 255
                    defb SET_FLAG, 3, 8
                    defb SELECT_SPR, S_FGON, SET_COORD, 51, 4, GO_LEFT, 0
                    defb CHAPTER_SIGN, 4
                    defb PLAY_SONG, 10
                    defb DO_FADEIN

                    defb SELECT_SPR, S_CURA
                    defb SET_COORD, 52, 14
                    defb DO_LOOP, 20
                         defb GO_LEFT, 1
                         defb UPDATE_SCR
                    defb END_LOOP
                    defb GO_UP, 1
                    defb UPDATE_SCR

                    defb SAYS, S_CURA, 161
                    defb SAYS, S_FGON, 162
                    defb SAYS, S_CURA, 163

                    defb ADD_FLAG, 1, 128
                    defb GET_OBJ, O_PALA, DESTROY_OBJ, O_PALA

                    defb GO_DOWN, 1
                    defb DO_LOOP, 22
                         defb GO_RIGHT, 1
                         defb UPDATE_SCR
                    defb END_LOOP
                    defb SET_COORD, 69, 13
                    defb SET_MOVEMENT
                    defw PRIEST_WALK

                    defb BREAK

     SC58:                                                            ; 58 Inicio Cap�tulo 5
               defb SELECT_SPR, S_FGON, SET_COORD, 52, 4, GO_LEFT, 0
               defb SET_FLAG, 0, 16
               defb SET_FLAG, 1, 60
               defb SET_FLAG, 2, 3
               defb SET_FLAG, 4, 0
               defb SET_FLAG, 10, $80
               defb SET_FLAG, 11, 6
               defb SET_FLAG, 12, $ff
               defb DO_FADEOUT
               defb CHAPTER_SIGN, 5
               defb PLAY_SONG, 10
               defb UPDATE_SCR
                              
               defb ADD_FLAG, 1, 128
               defb GET_OBJ, O_CRANEO_F, DESTROY_OBJ, O_CRANEO_F
               defb ADD_FLAG, 1, 128
               defb GET_OBJ, O_PALA, DESTROY_OBJ, O_PALA
               defb ADD_FLAG, 1, 128
               defb GET_OBJ, O_SETA_R, DESTROY_OBJ, O_SETA_R
               defb BREAK

     SC59:                                                            ; 59 Creaci�n de setas
          defb SET_FLAG, 7, 7
          defb IF_FLAG_BIT, 10, 0
               defb CREATE_OBJ, O_SETA_N, 3, $0b, $17
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 1
               defb CREATE_OBJ, O_SETA_N, 3, $09, $56
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 2
               defb CREATE_OBJ, O_SETA_N, 3, $21, $47
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 3
               defb CREATE_OBJ, O_SETA_N, 3, $2a, $3a
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 4
               defb CREATE_OBJ, O_SETA_N, 3, $3c, $1d
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 5
               defb CREATE_OBJ, O_SETA_N, 3, $16, $3a
               defb ADD_FLAG, 7, -1
          defb END_IF
          defb IF_FLAG_BIT, 10, 6
               defb CREATE_OBJ, O_SETA_N, 3, $01, $2c
               defb ADD_FLAG, 7, -1
          defb END_IF

          defb IF_FLAG_BIT, 13, 1
               defb CREATE_OBJ, O_CRUCIFIJO, 0, 58, 32
          defb END_IF
          defb IF_FLAG_BIT, 13, 2
               defb CREATE_OBJ, O_CRUCIFIJO, 0, 74, 44
          defb END_IF
          defb IF_FLAG_BIT, 13, 3               
               defb CREATE_OBJ, O_CRUCIFIJO, 1, 64, 70
          defb END_IF

          defb END_SCRIPT

     SC60:                                                  ; 60 Inicializaci�n
               defb SET_GFXPTR
               defw FGON
               defb SET_FLAG, 6, 255
               defb SET_FLAG, 13, 1
               defb SET_FLAG, 11, 1

               defb CREATE_OBJ, O_COFRE, 1, $26, $57
               defb CREATE_OBJ, O_CRANEO_F, 1, $11, $0b
               defb CREATE_OBJ, O_DIARIO, 1, $33, $0d
               defb CREATE_OBJ, O_PALA, 1, $4e, $3a

               defb CREATE_OBJ, O_SETA_R, 3, $43, $01

               defb CREATE_OBJ, O_AGUA, 4, $0a, $2e
               defb CREATE_OBJ, O_AGUA, 4, $21, $2f
               defb CREATE_OBJ, O_AGUA, 4, $2c, $29
               defb CREATE_OBJ, O_AGUA, 4, $3d, $32
               defb CREATE_OBJ, O_AGUA, 4, $52, $2d
               defb CREATE_OBJ, O_AGUA, 4, $4c, $0a
               defb CREATE_OBJ, O_AGUA, 4, $11, $05

               defb PLAY_SONG, 0  ; M�sica para el men�

               defb END_SCRIPT

     SC61:                                                  ; 61 Retorno al pueblo desde el bosque de la meiga
               defb SET_COORD, 27, 61
               defb DO_FADEOUT
               defb SET_MAP, 0
               defb UPDATE_SCR

               defb IF_FLAG_MIN, 3, 24
                    defb BREAK
               defb END_IF

               defb SELECT_SPR, S_FGON

               defb DO_LOOP, 2
                    defb GO_LEFT, 1, UPDATE_SCR
               defb END_LOOP

               defb DO_LOOP, 5
                    defb GO_UP, 1, UPDATE_SCR
               defb END_LOOP

               defb SET_COORD, 24, 60, ADJUST_OFFSET, 0, 0, UPDATE_SCR

               defb DO_LOOP, 2
                    defb GO_UP, 1, UPDATE_SCR
               defb END_LOOP

               defb END_SCRIPT

     SC62:                                                       ; 62: Avisa que en la ermita sube la fe
               defb DO_FADEOUT
               defb SET_COORD, 52, 77, ADJUST_OFFSET, 0, 1
               defb SET_FLAG, 6, $ff
               defb GO_UP, 0
               defb CAM_AUTO, UPDATE_SCR
               defb IF_FLAG_BIT, 13, 0
                    defb SAYS, S_FGON, 234
                    defb ADD_FLAG, 13, -1
               defb END_IF
               defb BREAK
               
     SC63:                                                       ; 63: Usamos un crucifijo para rezar
          defb SET_FLAG, 3, 16
          defb ADD_FLAG, 8, -1
          defb IF_FLAG_EQ, 8, 0
               defb DESTROY_OBJ, O_CRUCIFIJO
          defb END_IF
          defb SAYS, S_FGON, 231
          defb END_SCRIPT

     SC64:                                                  ; 64 Abortar partida
          defb GO_DOWN, 0
          defb SAYS, S_FGON, 159
          defb IF_CONFIRM
               defb DO_FADECLOSE, RESTART_GAME
          defb END_IF
          defb END_SCRIPT

     SC65:                                                  ; 65 Paso a habitaci�n del ama de llaves
          defb DO_FADEOUT
          defb SET_COORD, 18, 64
          defb ADJUST_OFFSET, 0, 1
          defb CAM_AUTO
          defb UPDATE_SCR
          defb IF_FLAG_BIT, 14, 0
               defb BREAK
          defb END_IF
          defb SAYS_2, S_FGON, 0
          defb ADD_FLAG, 14, 1
          defb END_SCRIPT

     SC66:                                                                           ; 66 Librer�a "normal" 2
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 4
          defb END_SCRIPT

     SC67:                                                                           ; 67 Librer�a "normal" 3
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 5
          defb END_SCRIPT

     SC68:                                                                           ; 68 Librer�a "normal" 4
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 6
          defb END_SCRIPT

     SC69:                                                                           ; 69 Librer�a "normal" 5
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 7
          defb END_SCRIPT

     SC70:                                                                           ; 70 Librer�a "normal" 6
          defb SAYS, S_FGON, 225
          defb SAYS_2, S_FGON, 8
          defb END_SCRIPT

     SC71:                                                                           ; 71 Habitaci�n Brunilda
          defb SET_COORD, 52, 11
          defb DO_FADEOUT, CAM_AUTO, UPDATE_SCR
          defb IF_FLAG_BIT, 14, 1
               defb BREAK
          defb END_IF
          defb SAYS_2, S_FGON, 9
          defb ADD_FLAG, 14, 2
          defb END_SCRIPT

     SC72:                                                                           ; 71 Entrada biblioteca
          defb SET_COORD, 66, 19
          defb DO_FADEOUT, CAM_AUTO, UPDATE_SCR
          defb IF_FLAG_BIT, 1, 6
               defb BREAK
          defb END_IF
          defb IF_OBJ_CARR, O_DIARIO
               defb SAYS, S_FGON, 224 
          defb END_IF
          defb END_SCRIPT

SCRIPT_TABLE:
     defw SC00, SC01, SC02, SC03, SC04, SC05, SC06, SC07, SC08, SC09
     defw SC10, SC11, SC12, SC13, SC14, SC15, SC16, SC17, SC18, SC19
     defw SC20, SC21, SC22, SC23, SC24, SC25, SC26, SC27, SC28, SC29
     defw SC30, SC31, SC32, SC33, SC34, SC35, SC36, SC37, SC38, SC39
     defw SC40, SC41, SC42, SC43, SC44, SC45, SC46, SC47, SC48, SC49
     defw SC50, SC51, SC52, SC53, SC54, SC55, SC56, SC57, SC58, SC59
     defw SC60, SC61, SC62, SC63, SC64, SC65, SC66, SC67, SC68, SC69
     defw SC70, SC71, SC72
     
; ----------------------------------------------------------------------------------------------------------------------------------------------------------------
; Scripts de movimiento
; Primer byte: Direcci�n: 0, 1, 2, 3 (Izqda, Dcha, Abajo, Arriba). Pausa ($f0), Salto relativo hacia atr�s ($fd), Trigger script ($fe), Fin ($ff)
; Segundo byte: N�mero ciclos, bytes a restar al puntero (si 1er byte = $fd), o Script (cuando el 1er byte es un $fe)
PAISANO_MOVEMENT:
     defb 1, 10, $f0, 10, 3, 10, $f0, 10, 0, 10, $f0, 10, 2, 10, $f0, 10, $fd, 17
PRIEST_MOVEMENT:
     defb 1, 8, 2, 6, 1, 8, 2, 10, 1, 8, 3, 2, 1, 24, 3, 2, 1, 8, 2, 2, 1, 20, 3, 8, 1, 6, $fe, 9
NO_MOV:
defb $ff
PRIEST_WALK:
     defb 0, 18, 1, 1, $f0, 10, 1, 18, 0, 1, $f0, 10, $fd, 13
AMA_MOVEMENT:
     defb 0, 12, 2, 2, $f0, 20
     defb 3, 2, 0, 8, 2, 2, $f0, 20
     defb 3, 2, 1, 20, $f0, 30
     defb $fd, 21
MEIGA_MOVEMENT:
     defb 3, 1, $f0, 30, 1, 14, 3, 6, $f0, 60
     defb 2, 7, 0, 14, $fd, 15
BOTICARIO_MOV:
     defb 3, 1, 0, 4, 2, 11, 1, 6, 2, 4, $f0, 60
     defb 3, 4, 0, 6, 3, 11, 1, 4, 2, 1, $f0, 60
     defb $fd, 25
