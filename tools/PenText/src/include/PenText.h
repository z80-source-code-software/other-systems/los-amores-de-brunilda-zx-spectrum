#ifndef PENTEXT_H
#define PENTEXT_H

#include <iostream>#include <fstream>#include <string>#include <ctype.h>#include <cstdlib>
#include <vector>
#include "../include/parser.h"
#include "../include/constantes.h"

class PenText
{
    public:
        PenText();
        virtual ~PenText();
        int Run (Parser* parser);
    protected:
    private:
        unsigned char MyByte, Bit, TextLoaded [65536];
        bool SeparateStrings, CustomTable, Verbose;
        ofstream *FileSave, *Header;
        Parser *P;
        string OutputFile, OutputPath;
        int CharCounter, LoadBytesCounter, StringCounter, FilesCounter, HeaderMode, MaxLength;
        int CharFreq [128];
        unsigned char CompressTable [32*3];
        int LoadFile ();
        int DoMagic ();
        int Compare (const string &c, const string &c2);
        int AddByte (unsigned char  Byte);
        int SaveByte (unsigned char Byte);
        int OrderInTable (int Byte);
        int NewFile ();

};

#endif // PENTEXT_H
