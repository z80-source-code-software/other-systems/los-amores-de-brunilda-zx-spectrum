#include "../include/PenText.h"

PenText::PenText()
{
    unsigned char CTable [32*3] = {
         0,  97,  98,  99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122,  32,  44,  46, 255, 254, // Minusculas, espacio, , y .
        58,  65,  66,  67,  68,  69,  70,  71,  72,  73,  74,  75,  76,  77,  78,  79,  80,  81,  82,  83,  84,  85,  86,  87,  88,  89,  90,  33,  34,  35,  36,  37, // Mayúsculas, :, ! "
        39,  40,  41,  42,  43,  45,  47,  48,  49,  50,  51,  52,  53,  54,  55,  56,  57,  59,  60,  61,  62,  63,  91,  92,  93,  94,  95,  96, 123, 124, 125, 126}; // Números y demás
        // En esta tabla se quedan sin poner el &, el € y el (c)

    for (int n = 0; n < 32 * 3; n++)
        CompressTable [n] = CTable [n];

    for (int m = 1; m < 128; m++)
        CharFreq[m] = 0;

    MyByte = 0;
    Bit = 7;
    SeparateStrings = true;
    FilesCounter = 0;
    HeaderMode = 1;
    CustomTable = true;
    MaxLength = 0;
    FileSave = NULL;
    CharCounter = 0;
    LoadBytesCounter = 0;
    StringCounter = 0;
    //ctor
}

PenText::~PenText()
{
   // if (TextLoaded)
    //    delete TextLoaded;
    //dtor
}

int PenText::Compare (const string &c, const string &c2)
{
    string::const_iterator p = c.begin();
    string::const_iterator p2 = c2.begin();

    while (p != c.end() && p2 != c.end()) {
        if (toupper(*p) != toupper(*p2))
            return (toupper (*p) < toupper (*p2))?-1:1;
        ++p;
        ++p2;
    };

    return (c2.size() == c.size()) ? 0 : (c.size() < c2.size()) ? -1 : 1;
}

int PenText::LoadFile ()
{
    ifstream *File;
    ofstream *FileTable;
    string FileName, TempString;
    int GC;
    unsigned int PathPos;
    int LengthCounter = 0;

    if (P->Counter() < 2) {
        cout << "ERROR! You have to specify an input file!\n" << "Type 'PenText --h' for help\n";
        return 1;
    }


    if (P->Counter() > 2)
        FileName = P->Arg(2);
     else
        FileName = P->Arg(1);

    PathPos = FileName.find_last_of ("/\\"); // Find it both in Windows & Unix systems
    if (PathPos != string::npos)
        OutputPath = FileName.substr(0,PathPos+1);
    OutputFile = FileName.substr (PathPos+1);

    FileName = P->Arg(1);

    File = new ifstream (FileName.c_str(), ios::in);

    if (File->fail())
        {
            delete File;
            cout << "ERROR! " << FileName << " could not be opened! It does really exists?\n";
            return 1;
        }

    if (!(FileName.substr (FileName.find_last_of ("."))).compare (".txt") ||
        !(FileName.substr (FileName.find_last_of ("."))).compare (".TXT")) {
        do { // TEXT FILE!!!!
            getline (*File, TempString);
            while (TempString.length() && TempString.substr (0,1) == " ")
                TempString = TempString.substr (1, TempString.length() - 1);
            if (TempString.length() > 1 && TempString.substr (0, 1) != "#") {
//                    StringCounter++;

                while (TempString.find("á")!=std::string::npos)
                    TempString.replace (TempString.find ("á"),2,"[");
                while (TempString.find("é")!=std::string::npos)
                    TempString.replace (TempString.find ("é"),2,"@");
                while (TempString.find("í")!=std::string::npos)
                    TempString.replace (TempString.find ("í"),2,"]");
                while (TempString.find("ó")!=std::string::npos)
                    TempString.replace (TempString.find ("ó"),2,"^");
                while (TempString.find("ú")!=std::string::npos)
                    TempString.replace (TempString.find ("ú"),2,"_");
                while (TempString.find("¿")!=std::string::npos)
                    TempString.replace (TempString.find ("¿"),2,"|");
                while (TempString.find("¡")!=std::string::npos)
                    TempString.replace (TempString.find ("¡"),2,"{");
                while (TempString.find("ñ")!=std::string::npos)
                    TempString.replace (TempString.find ("ñ"),2,"~");
                while (TempString.find("Ñ")!=std::string::npos)
                    TempString.replace (TempString.find ("Ñ"),2,"}");
                while (TempString.find("ü")!=std::string::npos)
                    TempString.replace (TempString.find ("u"),2,"}");

//                LengthCounter = 0;

                while (TempString.substr (0,1).compare (" ") >= 0) {
                    string ReadChar;
//                    LengthCounter++;
                    ReadChar = TempString.substr (0,1);
                    if (ReadChar.compare ("\\") == 0) {
                        ReadChar = TempString.substr (1,2);
                        TextLoaded[LoadBytesCounter++] = strtol (ReadChar.c_str(),NULL,16); // Convert HEX to DEC
                        TempString = TempString.substr (3, TempString.length() - 3);
                        }
                    else {
                        if (ReadChar.compare ("@") == 0)
                            ReadChar = "\\";
                        TextLoaded[LoadBytesCounter++] = (unsigned char) ReadChar.c_str()[0];

                        TempString = TempString.substr (1, TempString.length() - 1);
                        }
                    }
                TextLoaded[LoadBytesCounter++] = 0;
//                if (++LengthCounter > MaxLength)
//                    MaxLength = LengthCounter;
                }
	//    if (*((char*) (Mensajes[Cont-1].substr (Mensajes[Cont-1].length() - 1, 1)).c_str()) == 13) // si mete el retorno de carro como caracter en la cadena
		//    Mensajes [Cont-1] = Mensajes [Cont-1].substr (0, Mensajes[Cont-1].length () - 1); // Asi que hay que quitarlo :)
        } while (!File->eof());

        unsigned char* LastPhrase = &TextLoaded[0];

        for (int n = 0; n < LoadBytesCounter; n++) {
            GC = TextLoaded[n];
            if (GC < 128)
                CharFreq[GC]++;
            if (!GC) {
                StringCounter++;
                if (Verbose)
                    printf ("%i: %s\n", StringCounter - 1, LastPhrase);
                LastPhrase = &TextLoaded[n+1];
                if (LengthCounter > MaxLength)
                    MaxLength = LengthCounter;
                LengthCounter = 0;
            }
              else
                LengthCounter++;
            }


        }

     else
        do {
            GC = File->get();
            if (!File->eof()) {
                if (GC > 127) {
                    delete File;
                    cout << "ERROR! " << FileName << " is not a valid text file.\n";
                    return 1;
                }
                CharFreq[GC]++;
                TextLoaded [LoadBytesCounter++] = GC;
                if (!GC) {
                    StringCounter++;
                    if (LengthCounter > MaxLength)
                        MaxLength = LengthCounter;
                    LengthCounter = 0;
                }
                  else
                    LengthCounter++;
            }
        } while (!File->eof());

    delete File;
/*
//    TextLoaded = new unsigned char [LoadBytesCounter];

    File = new ifstream (FileName.c_str(), ios::in);

    for (int n = 0; n < LoadBytesCounter; n++)
        TextLoaded[n] = (unsigned char) File->get();

    delete File;
*/
    cout << "Loaded " << FileName << ": " << LoadBytesCounter << " bytes (" << StringCounter << " strings found).\n";
    cout << "Longest string has " << MaxLength << " bytes.\n";

    if (CustomTable == false)
        return 0;

    cout << "Creating compression table...\n";

    for (int n = 0; n < 32 * 3; n++){
        while (CompressTable[n] == 0 || CompressTable[n] > 127)   // Sáltate las posiciones fijas de la tabla (fin de frase y cambio de tabla)
            n++;
        int MaxFreq = -1;
        for (int m = 1; m < 128; m++)
            if (MaxFreq < 0 || CharFreq[m] > CharFreq [MaxFreq])
                MaxFreq = m;
        CompressTable[n] = MaxFreq;
        CharFreq[MaxFreq] = -1;
    }

    FileName = OutputPath + OutputFile + ".5tt";
    FileTable = new ofstream (FileName.c_str() , ios::out | ios::binary);
    for (int n = 0; n < 32 * 3; n++)
        *FileTable << CompressTable [n];

    FileTable->flush();
    delete FileTable;

    cout << "Customized compression table saved: " << FileName << endl;

    return 0;
}

int PenText::NewFile ()
{
    string FileName;
    char FileNameC [100];
    FileName = OutputFile;

    if (StringCounter <= 1 || SeparateStrings == false)
       sprintf (FileNameC, "%s.5tx", FileName.c_str());
      else {
        FileName +="_";
        if (FilesCounter < 10)
            FileName += "0";
        if (FilesCounter < 100)
            FileName += "0";
        sprintf (FileNameC, "%s%s%i.5tx", OutputPath.c_str(),FileName.c_str(), FilesCounter++);
        }

    FileSave = new ofstream (FileNameC, ios::out | ios::binary);

    if (FileSave->fail())
       cout << "ERROR!!! I can't create " << FileNameC << " file!";

    return FileSave->fail();
}

int PenText::SaveByte (unsigned char Byte)
{
    CharCounter++;

    if (!FileSave)
        if (NewFile())
           return 1;

    FileSave->put (Byte);

    return FileSave->fail();
}

int PenText::OrderInTable (int Byte)
{
    for (int n = 0; n < 128; n++)
        if (Byte == CompressTable [n])
            return n;

    return OrderInTable (' ');
}

int PenText::AddByte (unsigned char Byte)
{
    while (Byte > 31)
        Byte -= 32;

    for (int n = 4; n >= 0; n--) {
        if  (Byte & (1 << n))
            MyByte |= 1 << Bit;
        if (Bit == 0) {
            Bit = 8;
            if (SaveByte (MyByte))
               return 1;
            MyByte = 0;
            }
        Bit--;
    }

    return 0;
}


int PenText::DoMagic ()
{
    unsigned char Provisional;
    char FileNameC[100];

    for (int bc = 0; bc < LoadBytesCounter; bc++) {
        Provisional = (unsigned char) OrderInTable (TextLoaded[bc]);
        if (Provisional > 63)
            AddByte (31);
         else
            if (Provisional > 31)
                AddByte (30);
        AddByte (Provisional);
        if (TextLoaded[bc] == 0 && SeparateStrings)
            {
                if (Bit != 7) {
                    SaveByte (MyByte);
                    MyByte = 0;
                    Bit = 7;
                    FileSave->flush();
                    }
                delete FileSave;
                FileSave = NULL;
                }
            }

    if (Bit != 7) {
        SaveByte (MyByte);
        FileSave->flush();
        delete FileSave;
    }

    if (HeaderMode && SeparateStrings) {
        if (HeaderMode == 1) // Asm
            sprintf (FileNameC, "%s%s.asm", OutputPath.c_str(), OutputFile.c_str());
/*          else
            sprintf (FileNameC, "%h.asm", OutputFile.c_str());*/
        Header = new ofstream (FileNameC, ios::out);
        if (HeaderMode == 1) { // Asm
            *Header << "; Header file generated by PenText\n\n";
            *Header << "BUFFER_SIZE equ " << MaxLength << "\n\n";
            *Header << "COMPRESS_TABLE:\nincbin " << '"' << OutputPath << OutputFile << ".5tt" << '"' << "\n\n";

            *Header << OutputFile << "_INDEX:\nDEFW ";
            for (int n = 0; n < FilesCounter; n++) {
                *Header << OutputFile;
                if (n < 10)
                    *Header << "0";
                if (n < 100)
                    *Header << "0";
                *Header << n;
                if (!((1+n)%10))
                    *Header << "\nDEFW ";
                  else
                    if (n < (FilesCounter -1))
                        *Header << ", ";
            }
            *Header << endl << endl;
            for (int n = 0; n < FilesCounter; n++) {
                *Header << OutputFile;
                if (n < 10)
                    *Header << "0";
                if (n < 100)
                    *Header << "0";
                *Header << n << ":\nincbin " << '"' << OutputPath << OutputFile << "_";
                if (n < 10)
                    *Header << "0";
                if (n < 100)
                    *Header << "0";
                *Header << n << ".5tx" << '"' << "\n\n";
            }
        }
        Header->flush();
        delete Header;
        cout << "Header file " << FileNameC << " generated.\n";
        }

    return 0;
}

int PenText::Run (Parser *parser)
{
    P = parser;
    Verbose = false;

    cout << "PenText V." << VERSION << endl << "Text Compressor for z80\n" << "Coded by Luis I. Garcia a.k.a. Benway (garvelu@yahoo.es)\n";
    cout << "Released under GPLv3 license (http://www.gnu.org/licenses/gpl-3.0.html)\n\n";

    for (int n = 0; n < P->Counter() ; n++) {
         if (!Compare (P->Arg(n), "--h") || !Compare (P->Arg(n), "-h") || !Compare (P->Arg(n), "--help")) {
             cout << "\nUsage:\nPenText [-v] <INPUT_FILE> [<OUTPUT_FILE>]\n";
             cout << "Input file must be a .txt file saved using UTF8 codification if special chars present.\n";
             cout << "Output name is optional. If it is not specificated, same name as input file will be used with .5tx extension.\n";
             cout << "-v --v --verbose: Verbose all found strings.\n";
             return 0;
         }
         if (!Compare (P->Arg(n), "--v") || !Compare (P->Arg(n), "-v") || !Compare (P->Arg(n), "--verbose"))
            Verbose = true;
    }



    if (LoadFile())
        return 1;

    if (DoMagic())
       cout << "FATAL ERROR!\n\n";
    cout << "Generated ";
    if (FilesCounter <= 1)
        cout << OutputFile;
     else
        cout << FilesCounter << " files";

    cout << ": " << CharCounter << " bytes. (1:" << (unsigned int ) CharCounter / LoadBytesCounter << "." << (unsigned int) (1000 * CharCounter / LoadBytesCounter) << " ratio)\n";

    return 0;
}
