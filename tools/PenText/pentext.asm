; ============================================================================================================================
; PENTEXT: Descomprime el texto indicado en HL en (STRING_BUFFER)
; ============================================================================================================================

PENTEXT:
     exx
     ld hl, STRING_BUFFER          ; En HL' nos colocamos el origen del buffer donde vamos a descomprimir
     ld de, 0                      ; Y DE' lo necesitamos a 0, porque el primer "mini_byte" descomprimido lo iremos a buscar en la primera tabla
     exx

     ld de, $0100                  ; En D = Bit por donde vamos del byte actual de datos comprimidos. E = Donde vamos a colocar el byte comprimido para "extraer" los 5 bits
     PT_START_NEW_DATA:
     ld bc, $0005                  ; B = Donde vamos a meter los 5 bits "extra�dos". C = 5 bits por cada "mini_byte"

     PT_LOOP1:
     dec d                         ; Un bit menos por correr del byte comprimido
     jr nz, PT_NO_NEW_BYTE_NEEDED  ; Si se nos ha acabado, cogemos un Byte nuevo
     ld d, 8                       ; Empezamos un nuevo byte
     ld e, (hl)                    ; Cogemos el byte
     inc hl                        ; e incrementamos el puntero

     PT_NO_NEW_BYTE_NEEDED:
     sla e                         ; Saca al carry el bit de m�s a la izquierda del Byte comprimido
     rl b                          ; Mete a la derecha (y desplaza todo a la izquierda) el bit que acabas de sacar
     dec c                         ; Un bit menos por correr del mini_byte
     jr nz, PT_LOOP1               ; Hasta que terminemos el mini_byte

     ld a, b                       ; Ponemos el mini_byte en A, porque vamos a cambiar los registros
     exx

     ld b, a                       ; El mini_byte en B'

     ld a, d
     and e
     jr nz, PT_TABLE_FOUND         ; Si DE' no es 0, es porque tenemos que leer en una tabla distinta de la primera, y ya tenemos el puntero preparado de antes

     xor a
     or b
     jr z, PT_EXIT                 ; DE' es 0, luego si el minibyte es 0 tambi�n (0 de la primera tabla), es el fin de la frase

     ld de, COMPRESS_TABLE
     PT_TABLE_FOUND:
     push hl                       ; Nos guardamos nuestro puntero al buffer de descompresi�n
     ld l, b
     ld h, 0
     add hl, de
     ex de, hl
     ld a, (de)                    ; Leemos el valor de la tabla

     cp 127
     jr c, PT_WRITE_MINIBYTE       ; Si es > 127 es porque nos indica que tenemos que leer de otra tabla el siguiente "mini_byte"

     ld hl, COMPRESS_TABLE
     ld de, 32                     ; En principio de la segunda...
     add hl, de

     cp $ff
     jr z, PT_SET_NEXT_TABLE
     add hl, de                    ; Pero si es $fe entonces es de la tercera tabla.

     PT_SET_NEXT_TABLE:
     ex de, hl
     pop hl                        ; Restauramos el puntero al buffer. En DE dejamos preparado el puntero a la tabla de la que hay que sacar el pr�ximo "mini_byte"
     jr PT_TABLE_FOUND_EXIT

     PT_WRITE_MINIBYTE:
     pop hl                        ; Restauramos en HL' el puntero al b�ffer
     ld (hl), a                    ; Y ponemos el valor correspondiente
     inc hl                        ; Incrementamos el puntero de escritura en el b�ffer
     ld de, 0                      ; Hemos escrito, luego el siguiente "mini_byte" hay que irlo a buscar a la primera tabla
     PT_TABLE_FOUND_EXIT:
     exx                           ; Antes de volver, restauramos los registros "originales"
     jr PT_START_NEW_DATA          ; Y nos vamos a por otro "mini_byte"

     PT_EXIT:
     ld (hl), a                    ; Hemos terminado, pero hay que escribir el �ltimo dato

; ============================================================================================================================
