; ============================================================================================================================
; PENTEXT: Descomprime el texto indicado en HL
; ============================================================================================================================

PENTEXT:                exx                             ;Mantener HL en el set alternativo
                        ld      de, STRING_BUFFER-1     ;Buffer para escribir, empieza con -1 para contrarestar el primer incremento
                        ld      b, 128                  ;Bitstream: solo el bit marcador. De B pasar� a A
PT_loop:                ld      hl, COMPRESS_TABLE      ;HL apunta a la tabla principal
                        inc     de                      ;Apuntar a la siguiente posici�n
PT_next5bits:           ld      a, b                    ;El bitstream en A
                        ld      bc, 8                   ;El bit marcador empieza estando en el bit 3 del registro C. Tras 5 rotaciones,
                                                        ;  saldr� y activar� el flag carry. B lo ponemos a 0 para sumar posteriormente a HL
PT_getnewbit:           add     a, a                    ;Extraer un bit a la izquierda
                        jr      nz, PT_nogetnewbyte     ;Si sigue habiendo bits en el bitstream, no hace falta leer nada de la memoria
                        exx                             ;Restaurar HL para que apunte a los datos comprimidos que estamos leyendo
                        ld      a, (hl)                 ;Leer siguiente byte en el bitstream
                        inc     hl                      ;Apuntar al siguiente
                        exx                             ;HL vuelve al registro alternativo
                        adc     a, a                    ;Extraer un bit a la izquierda, y al mismo tiempo meter el bit marcador en el bit 0
PT_nogetnewbyte:        rl      c                       ;Introducir el bit extraido (y mover el bit marcador) hacia la izquierda. Cuando
                                                        ;  salga el bit marcador, activar� el carry.
                        jr      nc, PT_getnewbit        ;Si no hay carry, sigue leyendo bits

                        add     hl, bc                  ;A�adir a HL el valor de 5 bits leido. Ahora HL apunta al siguiente caracter
                        ld      b, a                    ;Guardar bitstream actual en el registro B
                        ld      a, (hl)                 ;Leer byte en A
                        ld      (de),a                  ;Escribirlo. Si es un n�mero negativo, ya lo sobreescribiremos
                                                        ;   en la siguiente pasada con el valor correcto
                        and     a                       ;Comprobar valor
                        ret     z                       ;Si era 0, salir de la rutina
                        jp      p, PT_loop              ;Si es positivo, repetir para el siguiente valor
                       
                        rra                             ;Comprobar bit 0 (rotando a la derecha, el valor sale en carry)
                        ld      hl, COMPRESS_TABLE+32   ;En principio la segunda
                        jr      c, PT_next5bits         ;Si sali� el bit 1, es que ten�amos $FF -> ir a leer el valor de la segunda tabla
                        ld      hl, COMPRESS_TABLE+64   ;Si sali� el 0, pues usamos la tercera
                        jr      PT_next5bits            ;Ir a leer el valor de la tercera tabla

; ============================================================================================================================

; Y si el byte bajo de la COMPRESS_TABLE no est� posicionado entre 192 y 223, se puede ahorrar otro byte en la pen�ltima l�nea,
;     haciendo ld l, (COMPRESS_TABLE+64)%256... De hecho, se podr�a ahorrar otro m�s si dicho byte bajo est� entre 0 y 160, ya
;     que el valor de H no variar�a.
