#ifndef TILEEXTRACT_H#define TILEEXTRACT_H#include <iostream>#include <fstream>#include <string>#include <ctype.h>#include <stdlib.h>#include "speccybmp.h"#include "parser.h"#include "supertile.h"using namespace std;class TileExtract{    public:        TileExtract();        ~TileExtract();        int Run(Parser *parser);    private:        int Width, Height, OutputType, AttrType, NumberOfFiles, NumberOfTiles, NumberOfSTiles;        string OutputName, InputName;        Parser *P;        SpeccyBMP *Screens;        SpeccyChar TileSet[256];        SuperTile **SuperTiles;
        int *STOnScreen;        int Compare (const string &c, const string &c2);        int AskForHelp();        int Validate ();        int OpenFiles ();        int ExistsTile (SpeccyChar T);        int InsertTile (SpeccyChar T);        int ExistsSTile (SuperTile &ST);        int InsertSTile (SuperTile &ST);
        int ExtractTiles ();        int ExtractSTiles();
        int OutputASM ();
        int OutputC ();
        int OutputBIN ();
};#endif
