#include "supertile.h"SuperTile::SuperTile(int w, int h){    Width = w;    Height = h;    Tile = new unsigned char* [w];    Attr = new unsigned char* [w];    for (int n = 0; n < w; n++) {        Tile[n] = new unsigned char [h];        Attr[n] = new unsigned char [h];    }}SuperTile::~SuperTile(){    /*for (int n = 0; n < Width; n++) {        delete [] Tile [n];        delete [] Attr [n];    }    delete [] Tile;    delete [] Attr;*/}int SuperTile::Compare (SuperTile ST){    if (Width != ST.Width || Height != ST.Height)        return 0;    for (int n = 0; n < Width; n++)        for (int m = 0; m < Height; m++) {            if (Tile[n][m] != ST.GetTile(n, m))                return 0;            if (Attr[n][m] != ST.GetAttr (n, m))                return 0;        }    return 1;}void SuperTile::Copy (SuperTile &ST){    for (int n = 0; n < Width; n++)        for (int m = 0; m < Height; m++) {            Tile[n][m] = ST.GetTile(n,m);            Attr[n][m] = ST.GetAttr(n,m);        }}unsigned char SuperTile::GetTile (int x, int y){    return Tile[x][y];}unsigned char SuperTile::GetAttr (int x, int y){    return Attr[x][y];}void SuperTile::SetTile (int x, int y, unsigned char NTile){    Tile[x][y]  = NTile;}void SuperTile::SetAttr (int x, int y, unsigned char NAttr){    Attr[x][y] = NAttr;}

void SuperTile::Print (SpeccyChar *TileSet)
{
    for (int y = 0; y < Height; y++)
        for (int byte = 0; byte < 8; byte++) {
            for (int x = 0; x < Width; x++)
                TileSet[Tile[x][y]].Print (byte);
        cout << "\n";
        }
        cout << "\n";

}
