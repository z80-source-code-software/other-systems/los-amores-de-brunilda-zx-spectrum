#include "speccybmp.h"SpeccyBMP::SpeccyBMP(){}SpeccyBMP::~SpeccyBMP(){}
int SpeccyBMP::Load(string Name)
{
   int BT = 0;
   char C;
   ifstream ScreenStream (Name.c_str(), ios::binary | ios::in);

    for (int t = 0; t < 3; t++)
        for (int byte = 0; byte < 8; byte++)
            for (int y = 0; y < 8; y++)
                for (int x = 0; x < 32; x++, BT++) {
                    ScreenStream.get (C);
                    Chars [x][y + 8 * t].SetByte (byte, (unsigned char) C);
                }

    for (int y = 0; y < 24; y++)
        for (int x = 0; x < 32; x++, BT++) {
                    ScreenStream.get (C);
//                    if (C != 0)
//                        printf ("%i\n", (unsigned char) C);
                    Attr [x][y] = (unsigned char) C;
        }
    return 0;
}SpeccyChar SpeccyBMP::GetChar (int x, int y){    return Chars[x][y];}unsigned char SpeccyBMP::GetAttr (int x, int y){    return Attr[x][y];}
