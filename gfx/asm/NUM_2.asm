; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 32,  48)
;Char Size:       (  4,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

NUM_2:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0, 15,252,  0,128,255, 31,  0
	DEFB	  0, 63,255,192,224,255, 63,  0
	DEFB	  0,127,255,240,224, 31,128,  0
	DEFB	  0,  0,  7,224,192,  1,  0,  0
	DEFB	  0,  0,  3,192,128,  3,  0,  0
	DEFB	  0,  0,  7,  0,  0,  6,  0,  0
	DEFB	  0,  0, 12,  0,  0, 28,  0,  0
	DEFB	  0,  0, 56,  0,  0,112,  0,  0
	DEFB	  0,  0, 96,  0,  0,192,  0,  0
	DEFB	  0,  1,192,  0,  0,128,  3,  0
	DEFB	  0,  7,  0,  0,  0,  0,  6,  0
	DEFB	  0, 14,  0,  0,  0,  0, 28,  0
	DEFB	  0, 24,  0,  0,  0,  0, 56,  0
	DEFB	  0, 48,  0,  0,192,255,127,  0
	DEFB	  0,255,255,192,192,255,255,  0
	DEFB	  1,255,255,192,128,255,255,  3
	DEFB	  0,  0,  1,  0,  0,  2,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66