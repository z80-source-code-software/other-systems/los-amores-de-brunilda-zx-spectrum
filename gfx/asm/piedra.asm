; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  48)
;Char Size:       (  2,   6)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

PIEDRA:
	DEFB	 80,163, 76,145, 34, 36,106, 69
	DEFB	  5,194,177, 88,188, 92,168, 82
	DEFB	110, 71,106, 85, 40,148, 65,160
	DEFB	168, 82,164,  0,169,  2, 81,  2
	DEFB	 64,143, 24, 49, 42, 55,110, 69
	DEFB	  5,242,  9, 68,164, 20,162, 84
	DEFB	106, 85, 34, 53, 27,141, 66,160
	DEFB	170, 80,164, 96,201, 34,133, 10
	DEFB	 80,163, 78,157, 58, 53,122, 85
	DEFB	  5,194,177, 88, 44, 70,168,114
	DEFB	106, 84, 34, 49, 24,140, 65,160
	DEFB	184, 82,168, 82,  0,  4, 81,  2