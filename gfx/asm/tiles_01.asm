; SuperTile definition generated by TileExtract

SuperTile_0:
defb 0, 0, 0, 0
SuperTile_0_ATTR:
defb  71, 71, 71, 71

SuperTile_1:
defb 1, 2, 17, 18
SuperTile_1_ATTR:
defb  38, 38, 38, 38

SuperTile_2:
defb 3, 3, 3, 3
SuperTile_2_ATTR:
defb  39, 39, 39, 39

SuperTile_3:
defb 4, 5, 19, 20
SuperTile_3_ATTR:
defb  46, 46, 46, 46

SuperTile_4:
defb 4, 6, 19, 21
SuperTile_4_ATTR:
defb  46, 38, 46, 38

SuperTile_5:
defb 7, 8, 22, 23
SuperTile_5_ATTR:
defb  38, 46, 38, 46

SuperTile_6:
defb 9, 10, 24, 25
SuperTile_6_ATTR:
defb  38, 38, 38, 38

SuperTile_7:
defb 11, 10, 14, 25
SuperTile_7_ATTR:
defb  39, 38, 39, 38

SuperTile_8:
defb 4, 5, 12, 13
SuperTile_8_ATTR:
defb  46, 46, 22, 22

SuperTile_9:
defb 12, 13, 12, 13
SuperTile_9_ATTR:
defb  22, 22, 22, 22

SuperTile_10:
defb 12, 13, 19, 20
SuperTile_10_ATTR:
defb  22, 22, 46, 46

SuperTile_11:
defb 14, 10, 24, 25
SuperTile_11_ATTR:
defb  39, 38, 38, 38

SuperTile_12:
defb 15, 10, 24, 15
SuperTile_12_ATTR:
defb  35, 38, 38, 34

SuperTile_13:
defb 15, 15, 15, 15
SuperTile_13_ATTR:
defb  35, 39, 38, 34

SuperTile_14:
defb 0, 0, 0, 0
SuperTile_14_ATTR:
defb  13, 13, 13, 13

SuperTile_15:
defb 16, 16, 16, 16
SuperTile_15_ATTR:
defb  13, 13, 13, 13

SuperTile_16:
defb 26, 27, 55, 56
SuperTile_16_ATTR:
defb  71, 67, 7, 66

SuperTile_17:
defb 28, 26, 57, 55
SuperTile_17_ATTR:
defb  66, 71, 2, 7

SuperTile_18:
defb 29, 30, 58, 59
SuperTile_18_ATTR:
defb  7, 3, 71, 2

SuperTile_19:
defb 31, 29, 60, 58
SuperTile_19_ATTR:
defb  2, 7, 2, 71

SuperTile_20:
defb 32, 33, 34, 35
SuperTile_20_ATTR:
defb  66, 2, 71, 7

SuperTile_21:
defb 34, 35, 61, 62
SuperTile_21_ATTR:
defb  71, 7, 3, 66

SuperTile_22:
defb 36, 37, 63, 64
SuperTile_22_ATTR:
defb  7, 71, 3, 66

SuperTile_23:
defb 38, 39, 36, 37
SuperTile_23_ATTR:
defb  66, 2, 7, 71

SuperTile_24:
defb 40, 41, 65, 66
SuperTile_24_ATTR:
defb  7, 7, 71, 71

SuperTile_25:
defb 41, 42, 67, 68
SuperTile_25_ATTR:
defb  7, 7, 71, 7

SuperTile_26:
defb 43, 44, 45, 69
SuperTile_26_ATTR:
defb  7, 71, 7, 71

SuperTile_27:
defb 45, 46, 70, 71
SuperTile_27_ATTR:
defb  7, 71, 7, 7

SuperTile_28:
defb 47, 48, 72, 73
SuperTile_28_ATTR:
defb  71, 71, 7, 7

SuperTile_29:
defb 49, 50, 73, 74
SuperTile_29_ATTR:
defb  71, 7, 7, 7

SuperTile_30:
defb 51, 52, 75, 54
SuperTile_30_ATTR:
defb  71, 7, 71, 7

SuperTile_31:
defb 53, 54, 76, 77
SuperTile_31_ATTR:
defb  71, 7, 7, 7

SuperTile_32:
defb 78, 79, 101, 102
SuperTile_32_ATTR:
defb  8, 8, 8, 8

SuperTile_33:
defb 80, 81, 16, 103
SuperTile_33_ATTR:
defb  8, 8, 8, 8

SuperTile_34:
defb 81, 80, 103, 16
SuperTile_34_ATTR:
defb  8, 8, 8, 8

SuperTile_35:
defb 82, 83, 104, 105
SuperTile_35_ATTR:
defb  66, 69, 69, 5

SuperTile_36:
defb 84, 84, 106, 106
SuperTile_36_ATTR:
defb  69, 69, 5, 5

SuperTile_37:
defb 85, 82, 107, 108
SuperTile_37_ATTR:
defb  69, 66, 5, 69

SuperTile_38:
defb 86, 87, 86, 87
SuperTile_38_ATTR:
defb  69, 5, 69, 5

SuperTile_39:
defb 88, 89, 88, 89
SuperTile_39_ATTR:
defb  5, 69, 5, 69

SuperTile_40:
defb 90, 91, 82, 109
SuperTile_40_ATTR:
defb  69, 5, 66, 69

SuperTile_41:
defb 92, 93, 110, 82
SuperTile_41_ATTR:
defb  5, 69, 69, 66

SuperTile_42:
defb 94, 94, 111, 111
SuperTile_42_ATTR:
defb  5, 5, 69, 69

SuperTile_43:
defb 95, 96, 112, 113
SuperTile_43_ATTR:
defb  71, 7, 7, 5

SuperTile_44:
defb 40, 41, 114, 115
SuperTile_44_ATTR:
defb  67, 67, 66, 66

SuperTile_45:
defb 41, 42, 116, 117
SuperTile_45_ATTR:
defb  67, 67, 66, 66

SuperTile_46:
defb 97, 98, 118, 119
SuperTile_46_ATTR:
defb  66, 66, 66, 66

SuperTile_47:
defb 99, 100, 119, 120
SuperTile_47_ATTR:
defb  66, 66, 66, 66

SuperTile_48:
defb 40, 42, 142, 143
SuperTile_48_ATTR:
defb  71, 70, 70, 6

SuperTile_49:
defb 121, 122, 144, 145
SuperTile_49_ATTR:
defb  70, 6, 6, 6

SuperTile_50:
defb 40, 42, 146, 147
SuperTile_50_ATTR:
defb  71, 70, 70, 6

SuperTile_51:
defb 123, 124, 144, 145
SuperTile_51_ATTR:
defb  70, 6, 6, 6

SuperTile_52:
defb 65, 67, 148, 149
SuperTile_52_ATTR:
defb  66, 66, 66, 66

SuperTile_53:
defb 67, 66, 149, 150
SuperTile_53_ATTR:
defb  66, 66, 66, 66

SuperTile_54:
defb 66, 68, 150, 151
SuperTile_54_ATTR:
defb  66, 66, 66, 66

SuperTile_55:
defb 125, 126, 152, 153
SuperTile_55_ATTR:
defb  32, 32, 32, 32

SuperTile_56:
defb 127, 128, 154, 155
SuperTile_56_ATTR:
defb  122, 122, 56, 56

SuperTile_57:
defb 129, 130, 156, 157
SuperTile_57_ATTR:
defb  33, 32, 32, 32

SuperTile_58:
defb 131, 132, 158, 159
SuperTile_58_ATTR:
defb  33, 33, 33, 38

SuperTile_59:
defb 133, 134, 160, 161
SuperTile_59_ATTR:
defb  33, 33, 38, 32

SuperTile_60:
defb 135, 136, 162, 163
SuperTile_60_ATTR:
defb  33, 32, 32, 32

SuperTile_61:
defb 137, 138, 164, 165
SuperTile_61_ATTR:
defb  32, 32, 32, 32

SuperTile_62:
defb 139, 139, 166, 166
SuperTile_62_ATTR:
defb  66, 66, 66, 66

SuperTile_63:
defb 140, 141, 167, 168
SuperTile_63_ATTR:
defb  71, 7, 7, 5

SuperTile_64:
defb 169, 170, 172, 177
SuperTile_64_ATTR:
defb  70, 70, 70, 70

SuperTile_65:
defb 170, 170, 173, 173
SuperTile_65_ATTR:
defb  70, 70, 70, 70

SuperTile_66:
defb 170, 171, 179, 176
SuperTile_66_ATTR:
defb  70, 70, 70, 6

SuperTile_67:
defb 172, 173, 172, 173
SuperTile_67_ATTR:
defb  70, 70, 70, 70

SuperTile_68:
defb 173, 174, 174, 173
SuperTile_68_ATTR:
defb  70, 70, 70, 6

SuperTile_69:
defb 175, 173, 173, 175
SuperTile_69_ATTR:
defb  6, 6, 6, 6

SuperTile_70:
defb 173, 176, 173, 176
SuperTile_70_ATTR:
defb  6, 6, 6, 6

SuperTile_71:
defb 172, 174, 180, 181
SuperTile_71_ATTR:
defb  70, 70, 70, 6

SuperTile_72:
defb 173, 173, 181, 181
SuperTile_72_ATTR:
defb  6, 6, 6, 6

SuperTile_73:
defb 175, 176, 181, 182
SuperTile_73_ATTR:
defb  6, 6, 6, 6

SuperTile_74:
defb 177, 173, 173, 177
SuperTile_74_ATTR:
defb  70, 70, 70, 70

SuperTile_75:
defb 96, 178, 113, 96
SuperTile_75_ATTR:
defb  79, 15, 79, 79

SuperTile_76:
defb 178, 178, 95, 96
SuperTile_76_ATTR:
defb  15, 15, 79, 79

SuperTile_77:
defb 178, 96, 95, 113
SuperTile_77_ATTR:
defb  15, 79, 79, 79

SuperTile_78:
defb 65, 68, 148, 151
SuperTile_78_ATTR:
defb  66, 66, 66, 66

SuperTile_79:
defb 0, 0, 0, 0
SuperTile_79_ATTR:
defb  0, 0, 0, 0

