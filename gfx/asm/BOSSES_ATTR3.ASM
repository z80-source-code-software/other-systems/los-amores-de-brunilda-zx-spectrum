; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 64,  64)
;Char Size:       (  8,   8)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Attr
;Interleave:      Sprite
;Mask:            No

BOSSES_ATTR3:
	DEFB	  7,  7, 69, 69, 69,  6,  7, 71
	DEFB	  7,  5,  5, 69, 71, 71,  7,  7
	DEFB	 69, 69,  5, 69,  5, 70, 70, 66
	DEFB	 69,  7,  7,  5,  6,  6, 70, 70
	DEFB	 71, 71, 71,  7,  7,  6, 71, 70
	DEFB	 71, 71,  7,  7,  7,  5, 70,  6
	DEFB	 71, 71, 71, 69,  5,  5, 69,  2
	DEFB	 71, 71, 69, 69,  5,  5, 69,  2