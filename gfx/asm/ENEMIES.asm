; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16, 192)
;Char Size:       (  2,  24)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

ENEMIES:
	DEFB	  0,  1,  3, 15, 31, 48, 36, 38
	DEFB	  0,128, 64,176,248, 12, 36,100
	DEFB	 32, 56, 79, 79, 79, 94, 53,  0
	DEFB	  4, 28,242,162, 82,170, 84,  0
	DEFB	  0,  0,  1,  3, 15, 31, 48, 36
	DEFB	  0,  0,128, 64,176,248, 12, 36
	DEFB	 38, 96, 88, 95, 63, 62,117,  0
	DEFB	110, 26, 18,242,210,170, 84,  0
	DEFB	  0,  0,  1,  3, 15, 31, 48, 36
	DEFB	  0,  0,128, 64,176,248, 12, 36
	DEFB	118, 88, 72, 79, 79, 95, 58,  0
	DEFB	100,  6, 26,250,236, 84,170,  0
	DEFB	  0,  6, 31, 51, 49, 57, 63, 29
	DEFB	  0,160,248,204,136,156,244,184
	DEFB	 71,101, 96, 75,  5, 24, 60,  0
	DEFB	226,166,  6,210,160, 24, 60,  0
	DEFB	  0,  0,  6, 31, 51, 49, 57, 63
	DEFB	  0,  0,160,248,204,136,156,244
	DEFB	 93,103, 69,  0, 27, 61, 28,  0
	DEFB	184,226,160,  2,214,166,  2,  0
	DEFB	  0,  0,  6, 31, 51, 49, 57, 63
	DEFB	  0,  0,160,248,204,136,156,244
	DEFB	 29, 71,  5, 64,107,101, 64,  0
	DEFB	186,230,162,  0,216,188, 56,  0
	DEFB	  0, 28, 10,  9,  7, 13, 46, 78
	DEFB	  0, 56, 80,144,224,176,116,114
	DEFB	 67, 72, 72, 82,100, 96, 64,  0
	DEFB	194, 18, 18, 74, 38,  6,  2,  0
	DEFB	  0,  0,  0, 28, 10,  9,  7, 45
	DEFB	  0,  0,  0, 56, 80,144,224,180
	DEFB	 78, 78,115, 72, 16,  0,  0,  0
	DEFB	114,114,206, 18,  8,  0,  0,  0
	DEFB	  0, 64, 96, 96, 80, 64, 92, 74
	DEFB	  0,  2,  6,  6, 10,  2, 58, 82
	DEFB	 73, 71, 45, 14, 14, 35, 16,  0
	DEFB	146,226,180,112,112,196,  8,  0
	DEFB	  0, 24, 36,  1, 55, 73, 78, 65
	DEFB	  0, 24, 36,128,236,146,114,130
	DEFB	 10, 16, 36, 36, 36,  2,  0,  0
	DEFB	 82,  8, 36, 36, 32, 64,  0,  0
	DEFB	  0, 48,  8,  4, 33, 87, 73, 78
	DEFB	  0, 12, 16, 32,132,234,146,114
	DEFB	  1, 26, 32, 68, 72, 40,  4,  0
	DEFB	128, 88,  4, 34, 18, 20, 32,  0
	DEFB	  0, 16,  8,  4, 36, 81, 87, 73
	DEFB	  0,  8, 16, 32, 36,138,234,146
	DEFB	 14,  1, 58, 64, 76, 80, 24,  0
	DEFB	112,128, 92,  2, 50, 10, 24,  0