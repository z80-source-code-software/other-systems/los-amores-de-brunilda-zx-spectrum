; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      (256,  40)
;Char Size:       ( 32,   5)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 12,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 48, 12,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 48
	DEFB	 88,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 22, 88,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 22
	DEFB	222,255,255,255,255,255,255,255
	DEFB	255,255,255,255,255,255,255,255
	DEFB	255,255,255,123,222,255,255,255
	DEFB	255,255,255,255,255,255,255,123
	DEFB	 53, 85, 85, 85, 85, 85, 85, 85
	DEFB	 85, 85, 85,172, 53, 85, 85, 85
	DEFB	 85, 85, 85, 85, 85, 85, 85, 85
	DEFB	 85, 85, 85, 85, 85, 85, 85,172
	DEFB	 80,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 10, 80,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 10
	DEFB	 20,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 40, 20,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 40
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 24
	DEFB	 16,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,126,  0
	DEFB	  0,  0,  0,  0,  0,124,  0, 24
	DEFB	 16,  0,198,  0,  3,193,131,192
	DEFB	  0,253,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,122,  0
	DEFB	 48,140, 49, 12,  0,131,  0, 24
	DEFB	 16,  0,185,  0, 17,140, 49,136
	DEFB	  0, 52,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0, 56,  0
	DEFB	132,193,131, 33,  0,165,  0, 24
	DEFB	 16,  0,133,  0,  0,  0,  0,  0
	DEFB	  0, 52,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0, 56,  0
	DEFB	  0,  0,  0,  0,  0,205,  0, 24
	DEFB	 16,  0,121,  0,255,255,255,255
	DEFB	  8, 52, 16,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,248,251,159
	DEFB	  0,  0,  0,  0,  1,  3,  0, 24
	DEFB	 16,  0, 15,  3,  0,  0,  0,  0
	DEFB	223,247,248,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24, 80,189,218
	DEFB	  0,  0,  0,  0,  3,123,  0, 24
	DEFB	 16,  3,226,  3,  0,  0,  0,  0
	DEFB	213, 90,168,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24, 16, 60,136
	DEFB	  0,  0,  0,  0,  1,130, 15, 24
	DEFB	 16, 57,194,120,255,255,255,255
	DEFB	  0, 56,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0, 60,  0
	DEFB	255,255,255,255,204, 98, 96, 24
	DEFB	 16, 79, 54,134,  0,  0,  0,  0
	DEFB	  0, 56,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0, 52,  0
	DEFB	  0,  0,  0,  0,146, 30, 89, 24
	DEFB	 16, 81,142,218, 33,131,193,132
	DEFB	  0, 56,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0, 52,  0
	DEFB	136, 49,140, 17,114,134, 76, 24
	DEFB	 16, 97,131,  6, 12, 49,140, 48
	DEFB	  0,122,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,245,  0
	DEFB	192,131,193,  3,140,  1, 51, 24
	DEFB	 16, 30,  0,248,  0,  0,  0,  0
	DEFB	  0,106,  0,  8, 16,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  8
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 24
	DEFB	 24,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 24, 24,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 24
	DEFB	 40,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 20, 40,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 20
	DEFB	 10,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 80, 10,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 80
	DEFB	172, 85, 85, 85, 85, 85, 85, 85
	DEFB	 85, 85, 85, 85, 85, 85, 85, 85
	DEFB	 85, 85, 85, 53,172, 85, 85, 85
	DEFB	 85, 85, 85, 85, 85, 85, 85, 53
	DEFB	 91,255,255,255,255,255,255,255
	DEFB	255,255,255,214, 91,255,255,255
	DEFB	255,255,255,255,255,255,255,255
	DEFB	255,255,255,255,255,255,255,214
	DEFB	 30,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,120, 30,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,120
	DEFB	 48,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0, 12, 48,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0, 12
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 69, 65, 65, 65, 65, 65
	DEFB	 65, 67, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 68, 68, 69,104,112,112, 88
	DEFB	 67, 66, 66, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 69, 68, 69, 65, 65, 65, 65
	DEFB	 65, 66, 65, 65, 65, 69, 69, 69
	DEFB	 69, 69, 69, 69, 69, 69, 69, 69
	DEFB	 69, 69, 69, 69, 69, 69, 69, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65
	DEFB	 65, 65, 65, 65, 65, 65, 65, 65