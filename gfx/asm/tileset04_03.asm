; SuperTile definition generated by TileExtract

SuperTile_37:
defb 51, 52, 54, 72
SuperTile_37_ATTR:
defb  1, 69, 69, 5

SuperTile_38:
defb 53, 53, 73, 73
SuperTile_38_ATTR:
defb  69, 69, 5, 5

SuperTile_39:
defb 53, 51, 74, 57
SuperTile_39_ATTR:
defb  69, 1, 5, 69

SuperTile_40:
defb 54, 55, 54, 55
SuperTile_40_ATTR:
defb  69, 5, 69, 5

SuperTile_41:
defb 56, 57, 56, 57
SuperTile_41_ATTR:
defb  5, 69, 5, 69

SuperTile_42:
defb 54, 58, 51, 75
SuperTile_42_ATTR:
defb  69, 5, 1, 69

SuperTile_43:
defb 59, 59, 75, 75
SuperTile_43_ATTR:
defb  5, 5, 69, 69

SuperTile_44:
defb 60, 57, 75, 51
SuperTile_44_ATTR:
defb  5, 69, 69, 1

SuperTile_45:
defb 61, 62, 76, 77
SuperTile_45_ATTR:
defb  1, 1, 1, 1

SuperTile_46:
defb 63, 55, 63, 55
SuperTile_46_ATTR:
defb  2, 5, 2, 5

SuperTile_47:
defb 56, 63, 56, 63
SuperTile_47_ATTR:
defb  5, 2, 5, 2

SuperTile_48:
defb 59, 59, 63, 63
SuperTile_48_ATTR:
defb  5, 5, 2, 2

SuperTile_49:
defb 64, 65, 78, 79
SuperTile_49_ATTR:
defb  71, 71, 71, 7

SuperTile_50:
defb 66, 67, 80, 81
SuperTile_50_ATTR:
defb  71, 71, 71, 7

SuperTile_51:
defb 68, 69, 82, 83
SuperTile_51_ATTR:
defb  71, 71, 71, 7

SuperTile_52:
defb 70, 71, 84, 85
SuperTile_52_ATTR:
defb  71, 71, 71, 7

SuperTile_53:
defb 86, 87, 110, 111
SuperTile_53_ATTR:
defb  71, 71, 71, 7

SuperTile_54:
defb 88, 89, 112, 113
SuperTile_54_ATTR:
defb  69, 5, 71, 7

SuperTile_55:
defb 88, 89, 112, 113
SuperTile_55_ATTR:
defb  87, 87, 7, 7

SuperTile_56:
defb 90, 91, 114, 115
SuperTile_56_ATTR:
defb  14, 12, 12, 4

SuperTile_57:
defb 92, 93, 116, 117
SuperTile_57_ATTR:
defb  33, 33, 33, 38

SuperTile_58:
defb 94, 95, 118, 119
SuperTile_58_ATTR:
defb  33, 33, 38, 32

SuperTile_59:
defb 96, 97, 120, 121
SuperTile_59_ATTR:
defb  33, 32, 32, 32

SuperTile_60:
defb 98, 99, 122, 123
SuperTile_60_ATTR:
defb  32, 32, 32, 32

SuperTile_61:
defb 100, 101, 124, 125
SuperTile_61_ATTR:
defb  32, 32, 34, 34

SuperTile_62:
defb 102, 103, 126, 127
SuperTile_62_ATTR:
defb  32, 32, 34, 34

SuperTile_63:
defb 104, 105, 128, 100
SuperTile_63_ATTR:
defb  34, 33, 34, 32

SuperTile_64:
defb 106, 107, 105, 106
SuperTile_64_ATTR:
defb  32, 34, 33, 32

SuperTile_65:
defb 108, 109, 124, 125
SuperTile_65_ATTR:
defb  34, 34, 34, 34

SuperTile_66:
defb 100, 103, 126, 127
SuperTile_66_ATTR:
defb  32, 32, 34, 34

SuperTile_67:
defb 105, 106, 129, 119
SuperTile_67_ATTR:
defb  33, 32, 32, 32

SuperTile_68:
defb 105, 106, 100, 103
SuperTile_68_ATTR:
defb  33, 32, 32, 32

SuperTile_69:
defb 100, 103, 124, 125
SuperTile_69_ATTR:
defb  32, 32, 34, 34

SuperTile_70:
defb 130, 131, 126, 127
SuperTile_70_ATTR:
defb  34, 34, 34, 34

SuperTile_71:
defb 132, 133, 133, 132
SuperTile_71_ATTR:
defb  69, 5, 5, 69

SuperTile_72:
defb 134, 135, 146, 147
SuperTile_72_ATTR:
defb  71, 7, 7, 5

SuperTile_73:
defb 51, 51, 148, 148
SuperTile_73_ATTR:
defb  71, 71, 69, 69

SuperTile_74:
defb 51, 3, 148, 3
SuperTile_74_ATTR:
defb  71, 65, 69, 65

SuperTile_75:
defb 3, 51, 3, 148
SuperTile_75_ATTR:
defb  65, 71, 65, 69

SuperTile_76:
defb 104, 136, 128, 149
SuperTile_76_ATTR:
defb  71, 71, 71, 7

SuperTile_77:
defb 137, 107, 150, 151
SuperTile_77_ATTR:
defb  7, 5, 5, 5

SuperTile_78:
defb 108, 109, 124, 125
SuperTile_78_ATTR:
defb  7, 7, 7, 71

SuperTile_79:
defb 130, 131, 126, 127
SuperTile_79_ATTR:
defb  7, 5, 5, 5

SuperTile_80:
defb 138, 139, 152, 153
SuperTile_80_ATTR:
defb  71, 7, 7, 5

SuperTile_81:
defb 140, 141, 148, 154
SuperTile_81_ATTR:
defb  66, 2, 66, 71

SuperTile_82:
defb 141, 142, 155, 148
SuperTile_82_ATTR:
defb  2, 2, 71, 66

SuperTile_83:
defb 143, 144, 132, 156
SuperTile_83_ATTR:
defb  71, 71, 71, 7

SuperTile_84:
defb 145, 143, 157, 133
SuperTile_84_ATTR:
defb  7, 7, 5, 5

SuperTile_85:
defb 158, 63, 172, 173
SuperTile_85_ATTR:
defb  71, 5, 7, 5

SuperTile_86:
defb 63, 159, 172, 173
SuperTile_86_ATTR:
defb  5, 7, 7, 5

SuperTile_87:
defb 158, 159, 172, 173
SuperTile_87_ATTR:
defb  71, 7, 7, 5

SuperTile_88:
defb 160, 161, 174, 175
SuperTile_88_ATTR:
defb  69, 69, 7, 7

SuperTile_89:
defb 160, 3, 174, 3
SuperTile_89_ATTR:
defb  69, 65, 7, 65

SuperTile_90:
defb 3, 161, 3, 175
SuperTile_90_ATTR:
defb  65, 69, 65, 7

SuperTile_91:
defb 24, 25, 176, 177
SuperTile_91_ATTR:
defb  80, 80, 5, 69

SuperTile_92:
defb 162, 163, 178, 179
SuperTile_92_ATTR:
defb  13, 77, 66, 66

SuperTile_93:
defb 43, 164, 43, 180
SuperTile_93_ATTR:
defb  7, 6, 69, 5

SuperTile_94:
defb 165, 166, 180, 180
SuperTile_94_ATTR:
defb  2, 4, 69, 69

SuperTile_95:
defb 167, 43, 180, 43
SuperTile_95_ATTR:
defb  3, 69, 69, 7

SuperTile_96:
defb 43, 166, 43, 180
SuperTile_96_ATTR:
defb  7, 4, 69, 5

SuperTile_97:
defb 167, 165, 180, 180
SuperTile_97_ATTR:
defb  3, 2, 69, 69

SuperTile_98:
defb 164, 43, 180, 43
SuperTile_98_ATTR:
defb  6, 69, 69, 7

SuperTile_99:
defb 168, 169, 178, 179
SuperTile_99_ATTR:
defb  87, 87, 66, 66

SuperTile_100:
defb 170, 171, 181, 182
SuperTile_100_ATTR:
defb  66, 66, 66, 66

SuperTile_101:
defb 183, 184, 178, 179
SuperTile_101_ATTR:
defb  25, 25, 5, 5

SuperTile_102:
defb 170, 185, 181, 197
SuperTile_102_ATTR:
defb  66, 66, 66, 66

SuperTile_103:
defb 185, 171, 197, 182
SuperTile_103_ATTR:
defb  66, 66, 66, 66

SuperTile_104:
defb 186, 187, 197, 182
SuperTile_104_ATTR:
defb  66, 66, 66, 66

SuperTile_105:
defb 188, 166, 164, 187
SuperTile_105_ATTR:
defb  66, 23, 21, 66

SuperTile_106:
defb 170, 171, 188, 187
SuperTile_106_ATTR:
defb  66, 66, 66, 66

SuperTile_107:
defb 143, 3, 132, 133
SuperTile_107_ATTR:
defb  67, 67, 67, 70

SuperTile_108:
defb 189, 190, 132, 133
SuperTile_108_ATTR:
defb  214, 214, 71, 71

SuperTile_109:
defb 3, 143, 132, 133
SuperTile_109_ATTR:
defb  67, 3, 70, 3

SuperTile_110:
defb 154, 191, 143, 192
SuperTile_110_ATTR:
defb  88, 67, 67, 3

SuperTile_111:
defb 192, 3, 3, 189
SuperTile_111_ATTR:
defb  3, 67, 67, 214

SuperTile_112:
defb 193, 194, 198, 199
SuperTile_112_ATTR:
defb  4, 4, 70, 70

SuperTile_113:
defb 3, 195, 190, 3
SuperTile_113_ATTR:
defb  67, 3, 214, 67

SuperTile_114:
defb 196, 155, 195, 143
SuperTile_114_ATTR:
defb  3, 24, 3, 3

SuperTile_115:
defb 154, 3, 154, 3
SuperTile_115_ATTR:
defb  88, 88, 88, 88

SuperTile_116:
defb 3, 191, 191, 192
SuperTile_116_ATTR:
defb  88, 67, 67, 3

SuperTile_117:
defb 192, 195, 208, 209
SuperTile_117_ATTR:
defb  3, 3, 6, 6

SuperTile_118:
defb 196, 15, 195, 196
SuperTile_118_ATTR:
defb  3, 25, 3, 3

SuperTile_119:
defb 16, 155, 16, 155
SuperTile_119_ATTR:
defb  25, 24, 25, 24

SuperTile_120:
defb 3, 3, 3, 3
SuperTile_120_ATTR:
defb  88, 88, 88, 88

SuperTile_121:
defb 3, 15, 3, 15
SuperTile_121_ATTR:
defb  88, 25, 88, 25

SuperTile_122:
defb 16, 15, 16, 15
SuperTile_122_ATTR:
defb  25, 25, 25, 25

SuperTile_123:
defb 200, 201, 210, 211
SuperTile_123_ATTR:
defb  67, 3, 67, 3

SuperTile_124:
defb 202, 203, 212, 213
SuperTile_124_ATTR:
defb  67, 2, 2, 2

SuperTile_125:
defb 204, 53, 214, 73
SuperTile_125_ATTR:
defb  66, 69, 2, 5

SuperTile_126:
defb 205, 16, 215, 16
SuperTile_126_ATTR:
defb  70, 1, 6, 1

SuperTile_127:
defb 61, 206, 76, 207
SuperTile_127_ATTR:
defb  69, 69, 69, 69

SuperTile_128:
defb 206, 206, 216, 217
SuperTile_128_ATTR:
defb  69, 69, 69, 69

SuperTile_129:
defb 206, 206, 218, 218
SuperTile_129_ATTR:
defb  69, 69, 69, 69

SuperTile_130:
defb 206, 206, 219, 220
SuperTile_130_ATTR:
defb  69, 69, 69, 69

SuperTile_131:
defb 206, 62, 221, 222
SuperTile_131_ATTR:
defb  69, 69, 5, 5

SuperTile_132:
defb 76, 207, 76, 207
SuperTile_132_ATTR:
defb  69, 69, 69, 69

SuperTile_133:
defb 207, 207, 207, 207
SuperTile_133_ATTR:
defb  69, 69, 69, 69

SuperTile_134:
defb 207, 223, 223, 226
SuperTile_134_ATTR:
defb  69, 69, 69, 5

SuperTile_135:
defb 224, 221, 226, 224
SuperTile_135_ATTR:
defb  5, 5, 5, 5

SuperTile_136:
defb 221, 221, 221, 221
SuperTile_136_ATTR:
defb  5, 5, 5, 5

SuperTile_137:
defb 76, 223, 76, 232
SuperTile_137_ATTR:
defb  69, 69, 69, 5

SuperTile_138:
defb 225, 226, 232, 232
SuperTile_138_ATTR:
defb  5, 5, 5, 5

SuperTile_139:
defb 224, 222, 232, 77
SuperTile_139_ATTR:
defb  5, 5, 5, 5

SuperTile_140:
defb 227, 228, 233, 234
SuperTile_140_ATTR:
defb  22, 86, 22, 86

SuperTile_141:
defb 229, 230, 235, 236
SuperTile_141_ATTR:
defb  86, 86, 22, 22

SuperTile_142:
defb 229, 230, 235, 236
SuperTile_142_ATTR:
defb  71, 7, 7, 5

SuperTile_143:
defb 228, 231, 234, 237
SuperTile_143_ATTR:
defb  86, 71, 86, 71

SuperTile_144:
defb 231, 228, 237, 234
SuperTile_144_ATTR:
defb  71, 86, 71, 86

SuperTile_145:
defb 104, 136, 128, 149
SuperTile_145_ATTR:
defb  67, 67, 67, 3

SuperTile_146:
defb 137, 107, 150, 151
SuperTile_146_ATTR:
defb  3, 66, 66, 2

SuperTile_147:
defb 108, 109, 124, 125
SuperTile_147_ATTR:
defb  67, 3, 67, 67

SuperTile_148:
defb 130, 131, 126, 127
SuperTile_148_ATTR:
defb  66, 2, 3, 2

SuperTile_149:
defb 28, 29, 110, 111
SuperTile_149_ATTR:
defb  7, 71, 7, 71

SuperTile_150:
defb 202, 203, 240, 241
SuperTile_150_ATTR:
defb  67, 2, 2, 2

SuperTile_151:
defb 100, 101, 124, 125
SuperTile_151_ATTR:
defb  4, 32, 2, 2

SuperTile_152:
defb 102, 103, 126, 127
SuperTile_152_ATTR:
defb  32, 4, 2, 2

SuperTile_153:
defb 108, 109, 124, 125
SuperTile_153_ATTR:
defb  2, 2, 2, 2

SuperTile_154:
defb 100, 103, 126, 127
SuperTile_154_ATTR:
defb  4, 4, 2, 2

SuperTile_155:
defb 100, 103, 124, 125
SuperTile_155_ATTR:
defb  4, 4, 2, 2

SuperTile_156:
defb 130, 131, 126, 127
SuperTile_156_ATTR:
defb  2, 2, 2, 2

SuperTile_157:
defb 104, 105, 128, 100
SuperTile_157_ATTR:
defb  2, 33, 2, 32

SuperTile_158:
defb 106, 107, 105, 106
SuperTile_158_ATTR:
defb  32, 2, 33, 32

SuperTile_159:
defb 238, 239, 242, 243
SuperTile_159_ATTR:
defb  71, 7, 7, 6

