; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16, 192)
;Char Size:       (  2,  24)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Gfx
;Interleave:      Line
;Mask:            No

ESCALO:
	DEFB	  0,  0, 31, 39,  3, 19,107,123
	DEFB	  0,  0,248,236,198,234,242,234
	DEFB	  3,  0, 62, 31, 31, 31, 10,  0
	DEFB	194, 16,168, 80,168, 80,168,  0
	DEFB	  0,  0, 15, 19,  1,  9, 53, 61
	DEFB	  0,  0,252,246,227,245,243,246
	DEFB	129,192,254, 95, 31, 31, 10,  0
	DEFB	224, 18,168, 82,168, 80,168,  0
	DEFB	  0,  0, 63, 79,  7, 39,215,247
	DEFB	  0,  0,240,216,140,214,226,208
	DEFB	  3,  0, 14, 31, 31, 31, 10,  0
	DEFB	129, 18,169, 82,169, 80,168,  0
	DEFB	  0,  0, 31, 55, 99, 87, 79, 87
	DEFB	  0,  0,248,228,192, 72,150, 94
	DEFB	 66,  8, 31, 30, 31, 30, 21,  0
	DEFB	128,  0, 84,168, 80,168, 80,  0
	DEFB	  0,  0, 63,111,199,174,207,110
	DEFB	  0,  0,240,200,128,144, 44,188
	DEFB	  5, 72,127, 94, 31, 30, 21,  0
	DEFB	  1,  2, 85,170, 80,168, 80,  0
	DEFB	  0,  0, 15, 27, 49,107, 79, 15
	DEFB	  0,  0,252,242,224,164,203,175
	DEFB	131,200,255,110, 31, 30, 21,  0
	DEFB	 64,  0, 80,168, 80,168, 80,  0
	DEFB	  0, 13, 31, 63, 63, 63, 61, 85
	DEFB	  0, 80,168,244,208,164,  8,146
	DEFB	 65, 93, 61, 30, 31, 31, 10,  0
	DEFB	  2,148, 40, 80,168, 80,168,  0
	DEFB	 13, 31, 63, 63, 63, 61, 85, 65
	DEFB	  0, 80,168,244,208, 36,136, 80
	DEFB	 62, 30, 31, 31, 31, 10,  0,  0
	DEFB	130,212, 42, 80,170, 80,168,  0
	DEFB	  1, 10, 31, 63, 63, 63, 61, 21
	DEFB	 80,168,244,208,164, 10,144,  2
	DEFB	  3, 90,123, 92, 95, 31, 10,  0
	DEFB	148, 40, 80,168, 80,168,128,  0
	DEFB	  0, 13, 31, 63, 63, 63, 61, 85
	DEFB	  0, 80,168,244,208,164,  8,146
	DEFB	 65, 93, 61, 30, 31, 31, 10,  0
	DEFB	  2,148, 40, 80,168, 80,168,  0
	DEFB	 13, 31, 63, 63, 63, 61, 85, 65
	DEFB	  0, 80,168,244,208, 36,136, 80
	DEFB	 62, 30, 31, 31, 31, 10,  0,  0
	DEFB	130,212, 42, 80,170, 80,168,  0
	DEFB	  1, 10, 31, 63, 63, 63, 61, 21
	DEFB	 80,168,244,208,164, 10,144,  2
	DEFB	  3, 90,123, 92, 95, 31, 10,  0
	DEFB	148, 40, 80,168, 80,168,128,  0