; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 32,  48)
;Char Size:       (  4,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

NUM_4:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0, 24,  0,  0
	DEFB	  0,  0, 48,  0,  0,112,  0,  0
	DEFB	  0,  0,240,  0,  0,224,  0,  0
	DEFB	  0,  0,224,  0,  0,224,  0,  0
	DEFB	  0,  0,192,  0,  0,193,  1,  0
	DEFB	  0,  1,129,  0,  0,131,  1,  0
	DEFB	  0,  3,  7,  0,  0, 15,  3,  0
	DEFB	  0,  6, 31,  0,  0, 31, 12,  0
	DEFB	  0, 28, 31,  0,  0, 31, 56,  0
	DEFB	  0,112, 31,  0,  0, 31,224,  0
	DEFB	  0,252, 31,  0,240,255,255,  1
	DEFB	  3,255,255,224,192,255,255,  3
	DEFB	  7,255,255,128,  0, 62,  0,  0
	DEFB	  0,  0, 62,  0,  0, 62,  0,  0
	DEFB	  0,  0, 62,  0,  0, 62,  0,  0
	DEFB	  0,  0, 62,  0,  0, 62,  0,  0
	DEFB	  0,  0, 62,  0,  0, 62,  0,  0
	DEFB	  0,  0, 60,  0,  0, 48,  0,  0
	DEFB	  0,  0, 32,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66