; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 32,  48)
;Char Size:       (  4,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

NUM_3:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0, 15,  0
	DEFB	  0, 63,240,  0,  0,252,127,  0
	DEFB	  0,127,254,  0,  0,255,255,  0
	DEFB	  1,  0,126,  0,  0, 62,  0,  0
	DEFB	  0,  0, 28,  0,  0, 24,  0,  0
	DEFB	  0,  0, 48,  0,  0, 32,  0,  0
	DEFB	  0,  0,192,  0,  0,248,  3,  0
	DEFB	  0,  7,254,  0,  0,255, 15,  0
	DEFB	  0, 31,255,128,192,255, 51,  0
	DEFB	  0,  0,127,192,192, 31,  0,  0
	DEFB	  0,  0, 15,224,224, 15,  0,  0
	DEFB	  0,  0,  7,224,224,  7,  0,  0
	DEFB	  0,  0,  3,192,192,  3,  0,  0
	DEFB	  0,  0,  3,128,  0,  3,  0,  0
	DEFB	  0,  0,  6,  0,  0,  4,  0,  0
	DEFB	  0,  0,  8,  0,  0, 48,  0,  0
	DEFB	  0,  0,192,  0,  0,128, 31,  0
	DEFB	  0, 62,  0,  0,  0,  0, 56,  0
	DEFB	  0, 64,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66