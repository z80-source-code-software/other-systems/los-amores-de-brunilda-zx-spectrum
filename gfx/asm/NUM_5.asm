; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 32,  48)
;Char Size:       (  4,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

NUM_5:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  3,128, 96,224,255,  7,  0
	DEFB	  0, 15,255,192,128,255, 15,  0
	DEFB	  0, 13,254,  0,  0,  0, 24,  0
	DEFB	  0, 24,  0,  0,  0,  0, 24,  0
	DEFB	  0, 48,  0,  0,  0,  0, 48,  0
	DEFB	  0, 48,  0,  0,  0,  0,112,  0
	DEFB	  0,112,  0,  0,  0,  0,120,  0
	DEFB	  0,127,192,  0,  0,248,255,  0
	DEFB	  0,127,252,  0,  0,254,  1,  0
	DEFB	  0,  0,127,  0,128, 63,  0,  0
	DEFB	  0,  0, 31,128,128, 31,  0,  0
	DEFB	  0,  0, 15,128,128, 15,  0,  0
	DEFB	  0,  0, 15,128,128, 15,  0,  0
	DEFB	  0,  0, 15,  0,  0, 14,  0,  0
	DEFB	  0,  0, 28,  0,  0, 56,  0,  0
	DEFB	  0,  0,240,  0,  0,192, 31,  0
	DEFB	  0, 63,  0,  0,  0,  0,124,  0
	DEFB	  0,112,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  2,  2,  2,  2,  2,  2,  2,  2
	DEFB	  2,  2,  2,  2,  2,  2,  2,  2
	DEFB	  2,  2,  2,  2,  2,  2,  2,  2