; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  16)
;Char Size:       (  2,   2)
;Frames:            21
;Sort Priorities: Char line, X char, Y char, Frame number
;Data Outputted:  Gfx+Attr
;Interleave:      Frames
;Mask:            No

OBJECTS:
	DEFB	  0,  7, 12, 30, 43,119, 35,  4
	DEFB	  0,224, 48,120,212,174, 68, 32
	DEFB	  7, 15, 13, 31, 13, 31, 11,  0
	DEFB	224,240,176,248,176,248, 80,  0
	DEFB	  0,  3,  5,  6,  3,  3,  3, 15
	DEFB	  0,192,160, 96,192,192, 64,240
	DEFB	 31, 51, 32, 32, 48, 28,  7,  0
	DEFB	120,204,  4,132,140, 56,224,  0
	DEFB	  0,  2,  5, 13, 28, 61, 65, 55
	DEFB	  0,  0,  0,128, 64,224,192,160
	DEFB	 87, 46, 85, 42, 21, 10,  4,  0
	DEFB	 64,160, 84,190, 62, 28,  8,  0
	DEFB	  0, 64, 95, 94, 85, 64,119,  3
	DEFB	  0,  4,212,164, 84,  0,212,128
	DEFB	114,116,118,100,115, 96, 85,  0
	DEFB	148, 72,212, 72,148,  8, 84,  0
	DEFB	  0,  3, 14, 31, 57, 49, 17, 51
	DEFB	  0,192,176,232,148,140,136,204
	DEFB	125,101,119, 35, 12,  7,  3,  0
	DEFB	186,164,234,196, 32,208, 64,  0
	DEFB	  0,  3, 14, 31, 63, 49, 17, 63
	DEFB	  0,192,176,232,244,140,136,252
	DEFB	126,124,119, 35, 12,  7,  3,  0
	DEFB	122, 60,234,196, 32,208, 64,  0
	DEFB	  0, 95,114, 87,126, 84,116, 94
	DEFB	  0,252,164,244,188, 20, 20, 60
	DEFB	119, 83,127, 64, 85, 96, 95,  0
	DEFB	116,228,252,  0, 84,  0,252,  0
	DEFB	  0,  0,  0,  0,  0, 24, 60, 24
	DEFB	  0,  0,  0, 90,165,145,117, 54
	DEFB	 38,111, 54,  0, 24, 60, 24,  0
	DEFB	 24, 34,117,123,126,117, 42,  0
	DEFB	  0,  0,  1,  3,  6,  0,  8, 24
	DEFB	221,193,157,  1, 31,  1, 14,  1
	DEFB	 48, 32, 24, 52, 48, 62, 40, 36
	DEFB	  6, 24, 47, 63, 31, 14,  0,  0
	DEFB	187,131,185,128,248,128,112,128
	DEFB	  0,  0,128,192, 96,  0, 16, 24
	DEFB	 96, 24,244,252,248,112,  0,  0
	DEFB	 12,  4, 24, 44, 12,124, 20, 36
	DEFB	  0,  0,  1,  3,  6, 12, 48, 48
	DEFB	192,192,128,  0,  0,  0,  0,  0
	DEFB	 12,  7,  1,  0,  0,  6,  9,  0
	DEFB	  0,  0,192,108, 14,220,224,  0
	DEFB	  3,  3,  1,  0,  0,  0,  0,  0
	DEFB	  0,  0,128,192, 96, 48, 12, 12
	DEFB	  0,  0,  3, 54,112, 59,  7,  0
	DEFB	 48,224,128,  0,  0, 96,144,  0
	DEFB	  0,  1,  2,  1, 16,  1, 67,  7
	DEFB	  0,192, 32, 64,132, 64, 33, 80
	DEFB	 15,  7, 72,  4,  0, 36,  3,  0
	DEFB	 40, 64,137, 16,128, 18, 96,128
	DEFB	  0, 10,  0, 47,107, 30, 88, 24
	DEFB	  0,  0,128,  0, 64,  4, 40, 26
	DEFB	 63,123, 87, 39, 83, 40, 21,  0
	DEFB	236, 60,152,104,208, 40, 80,  0
	DEFB	  0, 62, 64, 48, 15,  0,  1,  3
	DEFB	  0,120,252, 14,230,  4,202,244
	DEFB	  7, 14, 28, 58,108, 54, 28,  8
	DEFB	104,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  1
	DEFB	  0, 16, 56, 76, 70,100,248,192
	DEFB	 31, 35, 93,189,125, 58, 20,  8
	DEFB	128,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  3,  5,  5,  7, 15, 54,103
	DEFB	  0,192,160, 96,160, 80,172,214
	DEFB	126, 59,  7,  8, 29, 31, 13,  0
	DEFB	170, 92,224,  0, 88,232, 80,  0
	DEFB	  0,  3, 12, 31, 55, 51,127, 94
	DEFB	  0,192,176, 88,140, 68,170, 86
	DEFB	 54, 15,  0,  6, 15, 15,  6,  0
	DEFB	108,240,  0,160,240,208,160,  0
	DEFB	  0,  0,  2,  0,  8,  1, 10,  0
	DEFB	  0,144,  0,  4, 80,  2,  8,  2
	DEFB	 24, 24,126,126, 24, 24, 24,  0
	DEFB	  8,  2,  8, 34,128,  4, 80,  0
	DEFB	  0, 15, 48, 71, 40, 16, 20, 36
	DEFB	  0,224, 16,252, 18, 34, 34, 18
	DEFB	 43, 87, 87, 91, 39, 24,  7,  0
	DEFB	212,232,168,104,144, 96,128,  0
	DEFB	  0, 15, 48, 71, 40, 16, 20, 32
	DEFB	  0,224, 16,252, 18, 34, 34, 18
	DEFB	 44, 76, 64, 72, 32, 24,  7,  0
	DEFB	 20,  8, 40,104, 16, 96,128,  0
	DEFB	 67,  3, 66,  2, 70,  6,  6,  4
	DEFB	 71, 70, 70, 66, 69,  5, 70,  6
	DEFB	 79, 15, 15, 14, 71,  7,  7,  6
	DEFB	 67,  3,  3,  2, 70, 67, 70, 67
	DEFB	 71, 71,  7, 71,  7, 71,  7, 71
	DEFB	 71, 71, 71,  7, 71, 71,  7, 71
	DEFB	 71, 69, 70,  6, 71,  7,  7,  5
	DEFB	 70, 71, 71,  7,162,226,231,162
	DEFB	197,133,133,132,131,131,131,130
	DEFB	 71, 69, 66,  5, 71,  7, 69,  5
	DEFB	 71,  7,  7, 69