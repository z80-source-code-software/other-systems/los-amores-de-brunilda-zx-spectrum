; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  48)
;Char Size:       (  2,   6)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

EXPLODE:
	DEFB	  0, 32, 80, 32,  0,  2,  3,  5
	DEFB	  0, 40, 20,  8,  2,192,192,160
	DEFB	 71,  3,  1, 64, 32, 80, 40,  0
	DEFB	226, 96,128,  0,  2,  4, 10,  0
	DEFB	  0, 32, 64,  6,  7, 12, 28, 24
	DEFB	  0,  4,  2,192,160,240, 56, 48
	DEFB	 12, 10, 71,  3,  0,  0, 80,  0
	DEFB	 24,120,208,224,128,  0,  2,  0
	DEFB	  0, 58, 95,127,120, 80, 56, 25
	DEFB	  0,112,252,222,182, 14, 10, 76
	DEFB	 56, 80,120,124, 55, 27,  7,  0
	DEFB	132, 14, 30,108,254,218,140,  0