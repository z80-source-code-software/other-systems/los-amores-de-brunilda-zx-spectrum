; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 80,  56)
;Char Size:       ( 10,   7)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Attr
;Interleave:      Line
;Mask:            No

MAGICSTAR_ATTR:
	DEFB	128,128,128,127, 45,  9,  0,  0
	DEFB	  0,  0,128,128,128,127, 45,  9
	DEFB	  0,  0,  0,  0,128,128,127,127
	DEFB	127, 45,  9,  0,  0,  0,127,127
	DEFB	127,127,127,127,127, 45,  9,  0
	DEFB	128,128,127,127,127, 45,  9,  0
	DEFB	  0,  0,128,128,127, 45,127, 45
	DEFB	  9,  0,  0,  0,128,127, 45,  9
	DEFB	  0,127, 45,  9,  0,  0