; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  16)
;Char Size:       (  2,   2)
;Frames:            12
;Sort Priorities: Char line, X char, Y char, Frame number
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

AMA:
	DEFB	  0, 15, 19, 60, 39,124,119, 63
	DEFB	  0,236,246, 98,148,160,208,160
	DEFB	 15, 96,119,  7,112,127, 63,  0
	DEFB	 64,  0, 96, 96,104, 12,240,  0
	DEFB	  0,  0, 15, 19, 60, 39,124,119
	DEFB	  0,  0,236,246, 98,148,160,208
	DEFB	 63, 15, 96,119,  7,112, 63,  0
	DEFB	160, 64, 48,120, 24,230,248,  0
	DEFB	  0,  0, 15, 19, 60, 39,124,119
	DEFB	  0,  0,236,246, 98,148,160,208
	DEFB	 63, 15, 96,118,  5,113, 62,  0
	DEFB	160, 64,  0,192,192,166,120,  0
	DEFB	  0, 55,111, 70, 41,  5, 11,  5
	DEFB	  0,240,200, 60,228, 62,238,252
	DEFB	  2,  0,  6,  6, 22, 48, 15,  0
	DEFB	240,  6,238,224, 14,254,252,  0
	DEFB	  0,  0, 55,111, 70, 41,  5, 11
	DEFB	  0,  0,240,200, 60,228, 62,238
	DEFB	  5,  2,  0,  3,  3,101, 30,  0
	DEFB	252,240,  6,110,160,142,124,  0
	DEFB	  0,  0, 55,111, 70, 41,  5, 11
	DEFB	  0,  0,240,200, 60,228, 62,238
	DEFB	  5,  2, 12, 30, 24,103, 31,  0
	DEFB	252,240,  6,238,224, 14,252,  0
	DEFB	  0,  7, 15, 25, 22, 47, 57, 31
	DEFB	  0,224,240,152,104,244,156,248
	DEFB	 30, 15, 48, 38, 22, 25, 15,  0
	DEFB	120,240, 12,100,104,152,240,  0
	DEFB	  0,  0,  7, 15, 25, 22, 47, 57
	DEFB	  0,  0,224,240,152,104,244,156
	DEFB	 31, 94,111, 16, 54, 57, 31,  0
	DEFB	248,120,240, 14,110,144,224,  0
	DEFB	  0,  0,  7, 15, 25, 22, 47, 57
	DEFB	  0,  0,224,240,152,104,244,156
	DEFB	 31, 30, 15,112,118,  9,  7,  0
	DEFB	248,122,246,  8,108,156,248,  0
	DEFB	  0,  7, 14, 29, 27, 59, 61, 30
	DEFB	  0,224, 48,152,200,212,168, 80
	DEFB	 31, 13, 48, 47, 31, 31, 13,  0
	DEFB	168, 80, 12,164,208,168, 80,  0
	DEFB	  0,  0,  7, 14, 29, 27, 59, 61
	DEFB	  0,  0,224, 48,152,200,212,168
	DEFB	 30, 95,109, 48, 15, 29, 26,  0
	DEFB	 80,168, 80, 12,172, 76,  0,  0
	DEFB	  0,  0,  7, 14, 29, 27, 59, 61
	DEFB	  0,  0,224, 48,152,200,212,168
	DEFB	 30, 31, 13, 96,111,111,  2,  0
	DEFB	 80,170, 86, 12,160, 80,168,  0