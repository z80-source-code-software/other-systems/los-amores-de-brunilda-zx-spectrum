; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  16)
;Char Size:       (  2,   2)
;Frames:            12
;Sort Priorities: Char line, X char, Y char, Frame number
;Data Outputted:  Gfx
;Interleave:      Sprite
;Mask:            No

FCES:
	DEFB	  0,  1, 55,123,126,127, 58,  7
	DEFB	  0, 80,232,244,184,180,120,224
	DEFB	  8, 31, 31, 47, 49,  6,  7,  0
	DEFB	 16,248,204,236,224, 16,224,  0
	DEFB	  0,  0,  1, 55,123,126,127, 58
	DEFB	  0,  0, 80,232,244,184,180,120
	DEFB	 71, 24,  7, 27, 29, 14,  3,  0
	DEFB	228, 30,246,242,224,  0,128,  0
	DEFB	  0,  0,  1, 55,123,126,127, 58
	DEFB	  0,  0, 80,232,244,184,180,120
	DEFB	  7,  8, 29, 93,110, 51,  8,  0
	DEFB	224, 16,176,234,  2,180, 56,  0
	DEFB	  0, 10, 23, 47, 29, 45, 30,  7
	DEFB	  0,128,236,222,126,254, 92,224
	DEFB	  8, 31, 51, 55,  7,  8,  7,  0
	DEFB	 16,248,248,244,140, 96,224,  0
	DEFB	  0,  0, 10, 23, 47, 29, 45, 30
	DEFB	  0,  0,128,236,222,126,254, 92
	DEFB	 39,120,111, 79,  7,  0,  1,  0
	DEFB	226, 24,224,216,184,112,192,  0
	DEFB	  0,  0, 10, 23, 47, 29, 45, 30
	DEFB	  0,  0,128,236,222,126,254, 92
	DEFB	  7,  8, 13, 87, 64, 45, 28,  0
	DEFB	224, 16,184,186,118,204, 16,  0
	DEFB	  0, 21, 47,  5, 52, 59, 23,  7
	DEFB	  0,168,244,160, 44,220,232,224
	DEFB	 19, 40, 95, 95, 15, 16, 30,  0
	DEFB	200, 20,250,250,240,  8,120,  0
	DEFB	  0,  0, 21, 47,  5, 52, 59, 23
	DEFB	  0,  0,168,244,160, 44,220,232
	DEFB	 23, 43,116,119, 15, 55,  0,  0
	DEFB	232,214, 56,240,238,220, 48,  0
	DEFB	  0,  0, 21, 47,  5, 52, 59, 23
	DEFB	  0,  0,168,244,160, 44,220,232
	DEFB	 23,107, 28, 15,119, 59, 12,  0
	DEFB	232,212, 46,238,240,236,  0,  0
	DEFB	  0,  5, 10,  5, 42, 37, 26,  7
	DEFB	  0, 80,160, 80,164, 84,184,224
	DEFB	 19, 40, 95, 95, 15, 16, 30,  0
	DEFB	200, 20,250,250,240,  8,120,  0
	DEFB	  0,  0,  5, 10,  5, 42, 37, 26
	DEFB	  0,  0, 80,160, 80,164, 84,184
	DEFB	  7, 43,116,119, 15, 23,  0,  0
	DEFB	224,214, 56,224,216,184, 48,  0
	DEFB	  0,  0,  5, 10,  5, 42, 37, 26
	DEFB	  0,  0, 80,160, 80,164, 84,184
	DEFB	  7,107, 28,  7, 27, 29, 12,  0
	DEFB	224,196, 46,238,240,232,  0,  0
