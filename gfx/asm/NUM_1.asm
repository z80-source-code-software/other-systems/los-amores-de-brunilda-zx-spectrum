; ASM source file created by SevenuP v1.21
; SevenuP (C) Copyright 2002-2007 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 32,  48)
;Char Size:       (  4,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Gfx+Attr
;Interleave:      Sprite
;Mask:            No
;Zigzag:          Horizontal

NUM_1:
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  4,  0,  0
	DEFB	  0,  0, 12,  0,  0, 60,  0,  0
	DEFB	  0,  0,252,  0,  0,252,  3,  0
	DEFB	  0,  7,252,  0,  0,124, 14,  0
	DEFB	  0, 28,124,  0,  0,124, 48,  0
	DEFB	  0, 64,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,124,  0,  0,124,  0,  0
	DEFB	  0,  0,126,  0,  0,127,  0,  0
	DEFB	  0,  0,126,  0,  0,120,  0,  0
	DEFB	  0,  0, 96,  0,  0, 64,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	  0,  0,  0,  0,  0,  0,  0,  0
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66
	DEFB	 66, 66, 66, 66, 66, 66, 66, 66