; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 16,  48)
;Char Size:       (  2,   6)
;Sort Priorities: X char, Char line, Y char
;Data Outputted:  Attr
;Interleave:      Sprite
;Mask:            No

BIGSTARS_ATTR:
	DEFB	 66, 66, 66, 66, 70, 70, 70, 70
	DEFB	 68, 68, 68, 68