; ASM source file created by SevenuP v1.20
; SevenuP (C) Copyright 2002-2006 by Jaime Tejedor Gomez, aka Metalbrain

;GRAPHIC DATA:
;Pixel Size:      ( 64, 128)
;Char Size:       (  8,  16)
;Sort Priorities: Char line, X char, Y char
;Data Outputted:  Attr
;Interleave:      Sprite
;Mask:            No

BOSSES_ATTR2:
	DEFB	 71, 69,  3,  6,  4,  4,  4,  6
	DEFB	 69,  5,  3,  3,  4,  4,  6,  6
	DEFB	 71,  5,  5,  3,  4,  6,  2,  2
	DEFB	 69, 69,  5,  5,  4,  4,  6,  6
	DEFB	 71, 71,  5,  5,  4,  6, 71, 71
	DEFB	  4,  4,  4,  4,  4,  4,  4,  4
	DEFB	  4,  6,  4,  4,  4,  6,  4,  6
	DEFB	  6,  6,  6,  6,  4,  6,  6,  6
	DEFB	 68, 68,  4, 68,  4, 68, 68, 68
	DEFB	 66,  4,  4,  4, 68,  4,  4,  4
	DEFB	 71, 71, 70, 70, 71, 70, 67,  3
	DEFB	 70, 70, 70, 70, 71, 70, 67,  3
	DEFB	 71,  6, 66, 70,  6,  6, 70, 70
	DEFB	 70,  6, 66, 71,  6,  6,  6,  6
	DEFB	 66, 66, 66, 70,  6, 66, 66, 66
	DEFB	 66, 66, 66, 66, 70, 66, 66, 66