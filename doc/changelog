2014-01-11 -------------------------------------------------------------------------------------------------
.- BUGFIX: Ahora se restaura el puntero base a la tabla de mensajes antes de poner el mensaje de examinar
           un objeto
.- BUGFIX: Restaurado el puntero de las luchas para que las fichas caigan por su sitio

2014-01-01 -------------------------------------------------------------------------------------------------
.- BUGFIX: La dirección de EXOMIZER en el player estaba transcrita a mano, así que si se modestaificaba el
           código, la dirección dejaba de ser válida.
.- BUGFIX: En el mapa 1 (del pueblo) había un sitio donde nos podíamos quedar encerrados con FCes
.- Modificada la pantalla del "menú" para incluir "V 1.2"
.- DOC: Actualizado mapa de memoria

2013-12-25 -------------------------------------------------------------------------------------------------
.- BUGFIX: Los gráficos de los tiles de las cuevas se corrompían por desbordamiento de las 16k de la
           página 3 al comprimir el bloque page3.bin
.- Modificada la pantalla del "menú" para incluir "V 1.1"

2013-12-19 -------------------------------------------------------------------------------------------------
.- Cambio tipo de marcador: Ahora no es una "barra de energía", sino un punto móvil sobre la barra de fe
.- BUGFIX: Conversación con cura mientras le seguimos
.- BUGFIX: Pequeñas correcciones en los textos
.- BUGFIX: Todavía quedaba una línea de 1 píxel de los contadores de objetos que no se borraba en el
           marcador

2013-12-18 -------------------------------------------------------------------------------------------------
.- Al examinar librería del pasadizo con el pasadizo abierto, vuelve a describirla como al principio
.- Al entrar en la biblioteca con el diario (si el pasadizo está cerrado), vuelve a leerlo
.- Al entrar en la habitación de Brunilda la primera vez, nos dice que es la habitación de Brunilda
.- Hay un tipo menos de libros en las descripciones de la biblioteca
.- Incluídos los PSD de las carátulas en el SVN
.- BUGFIX: Si interacciona con un sprite, no intenta interaccionar después con los ST's que haya en esa
           posición
.- BUGFIX: Conversación con el boticario teniendo la llave: Ya no se la vuelve a pedir.
.- BUGFIX: Pulsación de fuego en joystick Sinclair de Spectrum +2 (gris) real para "pasar texto" (como
           "pulsar cualquier tecla") ahora funciona correctamente

2013-12-12 -------------------------------------------------------------------------------------------------
.- Pequeños cambios en las instrucciones
.- BUGFIX: Conversación con Brunilda teniendo el cofre en nuestro poder
.- BUGFIX: "Examinar" el marco de la puerta de la bibloteca en el despacho del boticario

2013-12-10 -------------------------------------------------------------------------------------------------
.- Incluido "instructions.txt" en el SVN

2013-12-07 -------------------------------------------------------------------------------------------------
.- BUGFIX: Mejorado comportamiento al pulsar controles incompatibles simultáneamente

2013-12-06 -------------------------------------------------------------------------------------------------
.- Incluído archivo .tap en el SVN
.- Más cambios pequeños en los mapas
.- BUGFIX: Le pedíamos la llave al ama de llaves aunque la tuviéramos ya.
.- BUGFIX: En la lucha contra la meiga, tras reordenar los carriles, aparecía una línea punteada sobre
           la barra de energía de la meiga.

2013-12-06 -------------------------------------------------------------------------------------------------
.- Pequeños cambios en los mapas para arreglar problemas de atributos o encierros con FCes.
.- Añadido "instrucciones.txt"
.- Añadido un "fade_close" al pulsar tecla para borrar la pantalla de carga y empezar a jugar
.- BUGFIX: Si comenzamos el juego con una contraseña donde ya habíamos cogido el agua, al intentar
           volver a cogerla, lo que cogía era la jarra, en lugar del mensaje de error.
2013-12-05 -------------------------------------------------------------------------------------------------
.- Añadida pantalla de "Retroworks presents..." antes de la pantalla de carga
.- Cambiada la rutina de carga por la "2" del utoloader (sin cabecera, sin turbo, líneas rojas y negras)

2013-12-05 -------------------------------------------------------------------------------------------------
.- Añadidos textos en inglés
.- Cambiados dos ST's del primer mapa (invertidos) para que quede mejor con los atributos de noche
.- BUGFIX: Comportamiento del contador de agua cogida cuando también tenemos setas
.- BUGFIX: Había un sitio a la entrada del pueblo donde nos podíamos quedar encerrados si nos seguía FCes

2013-11-29 -------------------------------------------------------------------------------------------------
.- Los enemigos en las cuevas / laberintos ahora no aparecen en un radio de 2 ST's alrededor de FGon
.- Añadida una segunda tabla de mensajes.
.- Añadido un mensaje de "pista" al pasar por primera vez a la habitación del ama de llaves, para hacer
   notar que el armario está abierto.
.- Añadido nueva frase para pedir la llave al boticario, y su respuesta si vamos sin disfrazar.
.- También le pedimos ahora la llave al ama de llaves, y se niega a dárnosla.
.- Añadidos distintos mensajes descriptivos para las distintas estanterías.
.- Actualizado mapa de memoria.

2013-11-25 -------------------------------------------------------------------------------------------------
.- Añadido objeto "Jarra"
.- Cambiado dibujo del agua cogida por jarra con agua
.- Modificado marcador (Desaparece el texto INV)
.- Correcciones ortográficas en los textos

2013-11-24 -------------------------------------------------------------------------------------------------
.- Añadidos SFX al introducir contraseñas
.- Reevaluada la dificultad en las luchas
.- BUGFIX: Al cambiar de canción ya no mete "mierda" en el player: Se llama antes a MUTE_PLAYER
.- BUGFIX: Ahora los sprites tienen prioridad sobre los objetos al pulsar "fuego" si ambos coinciden en
           el mismo ST
.- BUGFIX: Ahora no tarda tanto en que vuelvan a caer fichas en las luchas tras reordenar las pistas la
           meiga
.- BUGFIX: Al empezar desde una contraseña, si no hablamos con ningún personaje y examinamos un cartel,
           los atributos del texto son correctos
2013-11-22 -------------------------------------------------------------------------------------------------
.- Añadidos más efectos de sonido
.- Resincronizada las músicas en las luchas
.- Reparado el tamaño del búfer de instrumentos en las músicas que corrompían los FX de las luchas
.- Añadido un texto al final del juego
.- Añadido un efecto de agitar el sprite grande el personaje que pierda las luchas
.- Incluída la pantalla de carga en los créditos
.- Actualizado mapa de memoria
.- BUGFIX: La meiga no aparecía tras el humo antes de la lucha final

2013-11-21 -------------------------------------------------------------------------------------------------
.- Bugfixes varios: Buffer de música de tamaño incorrecto, conversación con Brunilda teniendo las cartas...
.- Añadidos más efectos de sonido
.- Más normalizaciones en músicas
.- Añadidos dos patterns a la música de las cuevas

2013-11-20 -------------------------------------------------------------------------------------------------

.- Añadidas músicas ingame
.- Corregido el sprite de Antón (no tenía posición de reposo)
.- Cambio de efecto de "aparición de Brunilda".
.- Añadidos algunos SFX

2013-10-02 -------------------------------------------------------------------------------------------------

.- Añadida pantalla de carga

2013-09-25 -------------------------------------------------------------------------------------------------

.- Ajustes en las luchas

2013-09-24 -------------------------------------------------------------------------------------------------

.- Reprogramación completa de las luchas

2013-06-13 -------------------------------------------------------------------------------------------------

.- Optimizaciones múltiples en las luchas
.- Las luchas con la meiga tienen un orden distinto de movimientos en el tablero
.- Músicas ingame en bosque de la meiga, cuevas y monte das pozas.
.- Efectos de sonido en luchas.
.- Modificados canales en la música de las luchas.
.- Sincronizadas las luchas con las nuevas músicas.
.- Modificadas las escenas donde FGon salía en la cama, para ser coherentes con la cama que usa, y para
usar los STs de "Fraile Acostado".
.- Añadida una opción para reiniciar la partida desde el juego (Tecla G)
.- Correcciones en los textos
.- Separado una fila el texto de "Pulsa fuego" de la contraseña
.- BUGFIX: Ahora comprueba la fe en la escena de encontrar a FCes en la cama, para no pasarse restando.
.- BUGFIX: El cura nos decía que se iba a rezar cuando estaba en la habitación con FCes.
.- BUGFIX: El posadero nos hablaba de la meiga al repetir la conversación inicial.
.- BUGFIX: Ahora no parpadea el marcador al subir la fe
.- BUGFIX: El cura estaba también en la iglesia en el capítulo 2
,. BUGFIX: Conversación con el boticario
.- BUGFIX: Comportamiento "nocturno" de la luz de la ermita
.- BUGFIX: Ahora avisa de que sube la fe en la ermita cuando restauramos partida.

2013-06-12 -------------------------------------------------------------------------------------------------

.- Modificaciones en el bitmap del título del juego y los créditos para mejorar la compresibilidad
.- Modificaciones en la distribución en memoria de los distintos mapas comprimidos
.- Modificación letrero junto al camino de la casa del boticario (pasa a tener el nombre del pueblo)
.- Modificación letrero en la casa de la meiga
.- Se pueden examinar los grimorios de la meiga
.- Inclusión letrero junto a la ermita en el mapa de la meiga
.- Incluídas "pistas" en laberinto de la mansión del boticario
.- Incluídos los crucifijos en la contraseña
.- Actualizado tileset de cavernas
.- Modificados "monstruos" en el mapa de las cavernas: Ahora en cada caverna aparece el ST correspondiente
al jefe que sea.
.- Modificación de las contraseñas (retrocompatible) para añadir el número de crucifijos portados
.- BUGFIX: ST's que cambiaban dependiendo de la fe NO SOLO en mapa 0
.- BUGFIX: Aviso de que la fe sube en la ermita
.- BUGFIX: El cráneo falso desaparecía al luchar contra un jefe de caverna
.- BUGFIX: Boticario no salía en el laboratorio en el capítulo 5
.- BUGFIX: La meiga aparecía en la casa en el capítulo 5
.- BUGFIX: Los crucifijos no aparecían al restaurar una partida en el capítulo 5
.- BUGFIX: Nos expulsaba de la ermita al entrar

2013-06-10 -------------------------------------------------------------------------------------------------
.- Incluído control Kempston
.- Incluídos crucifijos
.- Incluída ermita en el bosque de la meiga
.- Pequeñas correcciones en los textos
.- BUGFIX: Las luchas no se podían ganar
.- BUGFIX: Los enemigos mataban con un solo contacto

2013-06-06 -------------------------------------------------------------------------------------------------
.- Incluido el control por telcas OPQAM
.- BUGFIX: Al volver de ganar una lucha contra un jefe de caverna, enseñaba el inventario antes de dibujar
el marcador
.- BUGFIX: Al restar la fe tras hablar con los personajes en el pueblo, detecta si nos quedamos sin nada,
y, en ese caso, nos deja con la fe a 1.

2013-06-05 -------------------------------------------------------------------------------------------------

.- Cambio de la versión de exomizer y exoopt a la última disponible.
.- BUGFIX: El boticario no se iba al laboratorio al entregarle la seta rara
.- BUGFIX: El juego se bloqueaba al chocar con un enemigo (roca o enemigo de las cuevas)
.- BUGFIX: Corregidos algunos tiles en el mapa del bosque de la meiga que corrompian los atributos de las
setas colindantes.
.- BUGFIX: Los ojos de los monstruos en las cuevas no se borraban cuando el monstruo terminaba su
"ciclo vital"
.- BUGFIX: Cuando estábamos buscando a la meiga, no nos dejaba volver a la habitación de Fray Cesáreo
.- Desactivadas las teclas "D", "H", y "N" para cambiar la luz.

2013-06-04 -------------------------------------------------------------------------------------------------

.- Creado sistema de contraseñas para la restauración de partidas.
.- Añadidos controles joystick SINCLAIR (puertos 1 y 2).
.- Autodetección de control al pulsar fuego al iniciar.
.- Cambio en los scripts para modificar la inicialización del juego.
.- Modificados flags:
     * Ya no se usa flag 13 [ex bitmap de setas], sino los 7 primeros bits del flag 9.
     * El flag 7 (número de setas que tenemos en el inventario) se inicializa solo al crear las setas.
     * El bit 7 del flag 9 pasa a ser el marcador de si el boticario está en el laboratorio.
.- Actualizado mapa de memoria.
